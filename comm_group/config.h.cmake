/* config.h.cmake  */

/* Define with hostname for server */
#cmakedefine GROUP_SERVER_HOST "@GROUP_SERVER_HOST@"

/* Define with port for server to use */
#cmakedefine GROUP_SERVER_PORT @GROUP_SERVER_PORT@

/* Define with service domain for server */
#cmakedefine GROUP_SERVICE_DOMAIN "@GROUP_SERVICE_DOMAIN@"

/* define if struct hostent is declared in netdb.h or winsock.h */
#cmakedefine HAS_STRUCT_HOSTENT

/* Define to 1 if you have the `atexit' function. */
#cmakedefine HAVE_ATEXIT

/* Define to 1 if you have the `gethostbyname' function. */
#cmakedefine HAVE_GETHOSTBYNAME

/* Define to 1 if you have the `getlogin' function. */
#cmakedefine HAVE_GETLOGIN

/* Define to 1 if you have the <inttypes.h> header file. */
#cmakedefine HAVE_INTTYPES_H

/* Define to 1 if you have the <malloc.h> header file. */
#cmakedefine HAVE_MALLOC_H

/* Define to 1 if you have the `memcmp' function. */
#cmakedefine HAVE_MEMCMP

/* Define to 1 if you have the `memcpy' function. */
#cmakedefine HAVE_MEMCPY

/* Define to 1 if you have the <memory.h> header file. */
#cmakedefine HAVE_MEMORY_H

/* Define to 1 if you have the `on_exit' function. */
#cmakedefine HAVE_ON_EXIT

/* Define to 1 if you have the <pwd.h> header file. */
#cmakedefine HAVE_PWD_H

/* Define to 1 if you have the <stdint.h> header file. */
#cmakedefine HAVE_STDINT_H

/* Define to 1 if you have the <stdlib.h> header file. */
#cmakedefine HAVE_STDLIB_H

/* Define to 1 if you have the <strings.h> header file. */
#cmakedefine HAVE_STRINGS_H

/* Define to 1 if you have the <string.h> header file. */
#cmakedefine HAVE_STRING_H

/* Define to 1 if you have the <sys/stat.h> header file. */
#cmakedefine HAVE_SYS_STAT_H

/* Define to 1 if you have the <sys/types.h> header file. */
#cmakedefine HAVE_SYS_TYPES_H

/* Define to 1 if you have the <unistd.h> header file. */
#cmakedefine HAVE_UNISTD_H

/* Define to 1 if you have the ANSI C header files. */
#cmakedefine STDC_HEADERS

/* Define to empty if `const' does not conform to ANSI C. */
#cmakedefine const

/* Define to `int' if <sys/types.h> doesn't define. */
#cmakedefine gid_t

/* Define to `int' if <sys/types.h> does not define. */
#cmakedefine mode_t

/* Define to `int' if <sys/types.h> does not define. */
#cmakedefine pid_t

/* Define to `unsigned int' if <sys/types.h> does not define. */
#cmakedefine size_t

/* Define to `int' if <sys/types.h> doesn't define. */
#cmakedefine uid_t
