dnl -*-m4-*-
dnl
dnl  This is meant to be a generic configure.in file to be used as an example
dnl  for HPPCEL projects.  
dnl

dnl  the filename in AC_INIT() should be some source file in your project
dnl  configure uses it to locate the source code
AC_INIT(comm_group2.h)

dnl  configure automake
AC_CONFIG_AUX_DIR([config])
AC_CONFIG_MACRO_DIR([config])
AM_INIT_AUTOMAKE(commgrp,1.0)
AM_MAINTAINER_MODE
AM_CONFIG_HEADER(config.h)

AC_PROG_MAKE_SET
dnl find the C compiler
AC_PROG_CC

dnl find the C++ compiler.  kill these lines if you're not using C++
AC_LANG_CPLUSPLUS
AC_PROG_CXX
AC_LANG_C

dnl find the install program (must have install-sh in project)
AC_PROG_INSTALL

dnl  configure libtool
AC_LIBTOOL_DLOPEN
AM_PROG_LIBTOOL
test -n "$LIBTOOL" && LIBTOOL="$LIBTOOL --silent"
AC_SUBST(LIBTOOL_DEPS)
test "$CC" = "gcc" && CFLAGS="$CFLAGS -Wall"

LIB_PRE=lib
LIB_POST=.a
case "$host_os" in
 cygwin* | mingw* | pw32*)
   LIB_PRE=
   LIB_POST=.lib
   CFLAGS="$CFLAGS /MT"
   SOCK_LIBS="wsock32.lib user32.lib advapi32.lib"
esac

dnl The lines below make sure to link in libraries necessary for sockets to
dnl work.  They're ugly because socket routine location is non-standard.
tk_checkBoth=0
AC_CHECK_FUNC(connect, tk_checkSocket=0, tk_checkSocket=1)
if test "$tk_checkSocket" = 1; then
    AC_CHECK_LIB(socket, main, LIBS="$LIBS -lsocket", tk_checkBoth=1)
fi
if test "$tk_checkBoth" = 1; then
    tk_oldLibs=$LIBS
    LIBS="$LIBS -lsocket -lnsl"
    AC_CHECK_FUNC(connect, , [LIBS=$tk_oldLibs])
fi
AC_CHECK_FUNC(gethostbyname, , AC_CHECK_LIB(nsl, main, [LIBS="$LIBS -lnsl"]))
AC_FUNC_MEMCMP

dnl if you use no math lib functions, you may not need these lines.
MATH_LIBS=""
AC_CHECK_FUNC(sin, , MATH_LIBS="-lm")
AC_CHECK_LIB(ieee, main, [MATH_LIBS="-lieee $MATH_LIBS"])
AC_SUBST(MATH_LIBS)

dnl The CERCS_REQUIRE_PACKAGE macro helps find libraries and include files
dnl for some of our packages in ~chaos (or ~parallel).  It also allows you to
dnl override its normal search by specifying a -with-<package>=<directory>
dnl argument to configure.  ~chaos or ~parallel aren't found it searches
dnl standard system install locations.  If required libraries or include files
dnl are not in standard locations, it adds appropriate -I or -L flags to
dnl CPPFLAGS or LDFLAGS.  Arguments are:
dnl   1st -- name of package 
dnl   2nd -- full name of .h file to search for (may be omitted)
dnl   3rd -- full name of library file to search file (may be omitted)
dnl The AC_CHECK_LIB forces the configure to fail if the library is not found.

CERCS_REQUIRE_PACKAGE(atl, atl.h, libatl.la, $LIBS)

CERCS_REQUIRE_PACKAGE(gen_threads, gen_thread.h, libgen_thread.la)
SAVE_LIBS=$LIBS
AC_CHECK_LIB(pthread,pthread_create,[LIBS="$LIBS -lpthread"
dnl sometimes libpthread.a is there but pthread.h won't compile
AC_TRY_COMPILE([#include <pthread.h>],,[
AC_DEFINE(USE_PTHREADS,[1],[Define if we should use Pthreads for locking])
USE_PTHREADS="yes"
])
],,)
THR_LIBS=$LIBS
LIBS=$SAVE_LIBS
AC_SUBST(THR_LIBS)

CERCS_REQUIRE_PACKAGE(ffs, ffs.h, libffs.la)
CERCS_REQUIRE_PACKAGE(evpath, evpath.h, libevpath.la, $LIBS)
CERCS_REQUIRE_PACKAGE(cercs_env,cercs_env.h,libcercs_env.la)

dnl checks for header files
AC_HEADER_STDC
AC_CHECK_HEADERS(string.h memory.h malloc.h pwd.h unistd.h windows.h)
AC_CHECK_FUNCS(memcpy memcmp gethostbyname getlogin)

dnl checks for typedefs, structures and compiler characteristics
AC_TYPE_SIZE_T
AC_TYPE_UID_T
AC_TYPE_MODE_T
AC_TYPE_PID_T
AC_TYPE_SIGNAL
AC_C_CONST
AC_C_BIGENDIAN
AC_TYPE_PID_T
AC_ACCEPT_DEFINED
AC_BIND_DEFINED
AC_BZERO_DEFINED
AC_CONNECT_DEFINED
AC_FPRINTF_DEFINED
AC_GETHOSTNAME_DEFINED
AC_GETDOMAINNAME_DEFINED
AC_GETPEERNAME_DEFINED
AC_GETSOCKNAME_DEFINED
AC_GETTIMEOFDAY_DEFINED
AC_LISTEN_DEFINED
AC_PRINTF_DEFINED
AC_PUTENV_DEFINED
AC_SELECT_DEFINED
AC_SETSOCKOPT_DEFINED
AC_SETRLIMIT_DEFINED
AC_READ_DEFINED
AC_SPRINTF_DEFINED
AC_SSCANF_DEFINED
AC_STRDUP_DEFINED
AC_WRITE_DEFINED
AC_SENDTO_DEFINED

dnl the *_DEFINED macros check for declarations of the named subroutines in
dnl standard locations.  If a decl is found, a #define FPRINTF_DEFINED (or the
dnl equivalend) is put in the config.h.  These are HPPCEL local macros that
dnl help us avoid compiler warnings.  Search chaos_base/acsite.m4 for other
dnl available functions.
AC_FPRINTF_DEFINED
AC_PRINTF_DEFINED
AC_STRDUP_DEFINED

AC_CHECK_FUNCS(on_exit atexit getlogin)

AC_ARG_WITH(group_server, [  --with-group-server=HOST    Which host will run the group server.],GROUP_SERVER_HOST=$withval)

if test "x$GROUP_SERVER_HOST" = "x"; then
   hostname=`hostname`
   domain_path=`which domainname`
   if test "x$domain_path" = "x"; then
      # no domainname command, try this
      domainname=`nslookup $hostname | grep Name: | sed 's/.*: *[^.]*.//g'`
   else 
      domainname=`domainname`
   fi
   highdomain=`echo $domainname | sed 's/.*\.\([[^.]]*\.[[^.]]*\)$/\1/'`
   if test "x$highdomain" = "xgatech.edu"; then
      GROUP_SERVER_HOST="grouphost.cercs.gatech.edu";
      GROUP_SERVICE_DOMAIN="gatech.edu";
   else
      GROUP_SERVER_HOST=$hostname.$domainname
   fi
fi
if test "x$GROUP_SERVER_PORT" = "x"; then
   GROUP_SERVER_PORT=8647
fi
if test "x$GROUP_SERVICE_DOMAIN" = "x"; then
   GROUP_SERVICE_DOMAIN="gatech.edu";
fi

AC_ARG_WITH(service_domain, [  --with-service-domain=DOMAINNAME What domain the server will serve.],GROUP_SERVER_DOMAIN=$withval)
AC_ARG_WITH(group_server_port, [  --with-group-server-port=PORT What port the server listens on.],GROUP_SERVER_PORT=$withval)
AC_DEFINE_UNQUOTED(GROUP_SERVER_HOST, "$GROUP_SERVER_HOST", [Define with hostname for server])
AC_DEFINE_UNQUOTED(GROUP_SERVICE_DOMAIN, "$GROUP_SERVICE_DOMAIN", [Define with service domain for server])
AC_DEFINE_UNQUOTED(GROUP_SERVER_PORT, $GROUP_SERVER_PORT, [Define with port for server to use])
AC_SUBST(ARCH)

dnl Add libtool libraries that we didn't add earlier.
LIBS="$cercs_cv_evpath_link_dir/libevpath.la $cercs_cv_atl_link_dir/libatl.la $LIBS"

dnl Create Makefile from Makefile.in with results of configure.
AC_OUTPUT(Makefile tests/Makefile)
