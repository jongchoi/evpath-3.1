/* -*-C-*-
*******************************************************************************
* 
* File:        group_formats.c
* RCS:         $Id: group_formats.h,v 1.2 2008-02-04 21:20:54 eisen Exp $
* Description: Msgs struct decls kept in abc order (Greg's code modified for CM)
* Author:      Fabian E. Bustamante (fabianb@cc.gatech.edu)
*              Systems Research Group
*              College of Computing
*              Georgia Institute of Technology
* Created:
* Modified:
*  
* (C) Copyright 2000, all rights reserved.
*
*******************************************************************************
*/

#define MAXHOSTNAMELEN  256	/* from /usr/include/netdb.h */

typedef struct _assoc_group_info_msg {
  int cond;
  char *group_name;
  int data_len;
  void *data;
} assoc_group_info_msg, *assoc_group_info_msg_ptr;

typedef struct _find_matching_msg {
  int cond;
  char *user_name;
  char *app_list;
  char *type_list;
} find_matching_msg, *find_matching_msg_ptr;

typedef struct _init_contact_msg {
  int cond;
  char *group_id;		/* specifies one particular group */
  char *user_name;		/* first level differentiator */
  char *application_name;	/* second level differentiator */
  char *group_type;		/* third level differentiator */
} init_contact_msg, *init_contact_msg_ptr;

typedef struct _provisional_use_msg {
  char *domain;
  long time;
} provisional_use_msg, *provisional_use_msg_ptr;

typedef struct _remove_group_msg {
  char *host;
  int port;
  char *group_id;
} remove_group_msg, *remove_group_msg_ptr;

typedef struct _return_contact_msg {
  int cond;
  char *host;
  int port;
  char *group_id;
} return_contact_msg, *return_contact_msg_ptr;

typedef struct _setup_group_msg {
  int cond;
  char *user_name;
  char *application_name;
  char *group_type;
  int expiration;		/* in seconds, 0=never */
  char *host;
  int port;
} setup_group_msg, *setup_group_msg_ptr;

extern FMField assoc_group_info_msg_flds[];
extern FMField comm_group_struct_flds[];
extern FMField find_matching_msg_flds[];
extern FMField init_contact_msg_flds[];
extern FMField provisional_use_msg_flds[];
extern FMField remove_group_msg_flds[];
extern FMField return_contact_msg_flds[];
extern FMField return_matching_msg_flds[];
extern FMField setup_group_msg_flds[];

extern FMStructDescRec assoc_group_info_msg_formats[]; 
extern FMStructDescRec find_matching_msg_formats[];
extern FMStructDescRec init_contact_msg_formats[];
extern FMStructDescRec provisional_use_msg_formats[];
extern FMStructDescRec remove_group_msg_formats[];
extern FMStructDescRec return_contact_msg_formats[];
extern FMStructDescRec return_matching_msg_formats[];
extern FMStructDescRec setup_group_msg_formats[];

extern FMStructDescRec return_format_list[];
