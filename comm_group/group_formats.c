/* -*-C-*-
*******************************************************************************
* 
* File:        group_formats.c
* RCS:         $Id: group_formats.c,v 1.3 2008-02-04 21:20:54 eisen Exp $
* Description: Msgs formats decl kept in abc order (Greg's code modified for CM)
* Author:      Fabian E. Bustamante (fabianb@cc.gatech.edu)
*              Systems Research Group
*              College of Computing
*              Georgia Institute of Technology
* Created:
* Modified:
*  
* (C) Copyright 2000, all rights reserved.
*
*******************************************************************************
*/

#include "config.h"

#include "atl.h"
#include "fm.h"			/* pbio format decls */
#include "evpath.h"
#include "comm_group2.h"
#include "group_formats.h"	/* msgs structs decls */

FMField assoc_group_info_msg_flds[] =
{
  {"cond", "integer", sizeof(int),
   FMOffset(assoc_group_info_msg_ptr, cond)},
  {"group name", "string", sizeof(char *),
   FMOffset(assoc_group_info_msg_ptr, group_name)},
  {"data_len", "integer", sizeof(int),
   FMOffset(assoc_group_info_msg_ptr, data_len)},
  {"data", "char[data_len]", sizeof(char),
   FMOffset(assoc_group_info_msg_ptr, data)},
  {(char *) 0, (char *) 0, 0, 0,}
};

FMField comm_group_struct_flds[] =
{
  {"user_name", "string", sizeof(char *),
   FMOffset(comm_group_list, user_name)},
  {"application_name", "string", sizeof(char *),
   FMOffset(comm_group_list, application_name)},
  {"group_type", "string", sizeof(char *),
   FMOffset(comm_group_list, group_type)},
  {"group_id", "string", sizeof(char *),
   FMOffset(comm_group_list, group_id)},
  {"host", "string", sizeof(char *),
   FMOffset(comm_group_list, host)},
  {"port", "integer", sizeof(int),
   FMOffset(comm_group_list, port)},
  {"info_len", "integer", sizeof(int),
   FMOffset(comm_group_list, info_len)},
  {"info[info_len]", "char[info_len]", 1,
   FMOffset(comm_group_list, info)},
  {(char *) 0, (char *) 0, 0, 0,}
};

FMField find_matching_msg_flds[] =
{
  {"cond", "integer", sizeof(int),
   FMOffset(find_matching_msg_ptr, cond)},
  {"user_name", "string", sizeof(char *), 
   FMOffset(find_matching_msg_ptr, user_name)},
  {"app_list", "string", sizeof(char *), 
   FMOffset(find_matching_msg_ptr, app_list)},
  {"type_list", "string", sizeof(char *), 
   FMOffset(find_matching_msg_ptr, type_list)},
  {(char *) 0, (char *) 0, 0, 0}
};


FMField init_contact_msg_flds[] =
{
  {"cond", "integer", sizeof(int),
   FMOffset(init_contact_msg_ptr, cond)},
  {"group id", "string", sizeof(char *), 
   FMOffset(init_contact_msg_ptr, group_id)},
  {"user name", "string", sizeof(char *), 
   FMOffset(init_contact_msg_ptr, user_name)},
  {"application name", "string", sizeof(char *),
   FMOffset(init_contact_msg_ptr, application_name)},
  {"group type", "string", sizeof(char *),
   FMOffset(init_contact_msg_ptr, group_type)},
  {(char *) 0, (char *) 0, 0, 0}
};

FMField provisional_use_msg_flds[] =
{
  {"domain", "string", sizeof(char *),
   FMOffset(provisional_use_msg_ptr, domain)},
  {"time", "integer", sizeof(int),
   FMOffset(provisional_use_msg_ptr, time)},
  {(char *) 0, (char *) 0, 0, 0,}
};

FMField remove_group_msg_flds[] =
{
  {"host", "string", sizeof(char *), 
   FMOffset(remove_group_msg_ptr, host)},
  {"port", "integer", sizeof(int), 
   FMOffset(remove_group_msg_ptr, port)},
  {"group id", "string", sizeof(char *), 
   FMOffset(remove_group_msg_ptr, group_id)},
  {(char *) 0, (char *) 0, 0, 0}
};

FMField return_contact_msg_flds[] =
{  
  {"cond", "integer", sizeof(int),
   FMOffset(return_contact_msg_ptr, cond)},
  {"host", "string", sizeof(char *), 
   FMOffset(return_contact_msg_ptr, host)},
  {"port", "integer", sizeof(int), 
   FMOffset(return_contact_msg_ptr, port)},
  {"group id", "string", sizeof(char *), 
   FMOffset(return_contact_msg_ptr, group_id)},
  {(char *) 0, (char *) 0, 0, 0}
};

FMStructDescRec return_format_list[] = {
  {"comm_group_struct", comm_group_struct_flds},
  {NULL,NULL}
};

FMField return_matching_msg_flds[] =
{
  {"cond", "integer", sizeof(int),
   FMOffset(comm_group_return, cond)},
  {"count", "integer", sizeof(int), 
   FMOffset(comm_group_return, count)},
  {"list", "comm_group_struct[count]", sizeof(comm_group_struct),
   FMOffset(comm_group_return, list)},
  {(char *) 0, (char *) 0, 0, 0,}
};

FMField setup_group_msg_flds[] =
{
  {"cond", "integer", sizeof(int),
   FMOffset(setup_group_msg_ptr, cond)},
  {"user name", "string", sizeof(char *), 
   FMOffset(setup_group_msg_ptr, user_name)},
  {"application name", "string", sizeof(char *),
   FMOffset(setup_group_msg_ptr, application_name)},
  {"group type", "string", sizeof(char *),
   FMOffset(setup_group_msg_ptr, group_type)},
  {"expiration", "integer", sizeof(int),
   FMOffset(setup_group_msg_ptr, expiration)},
  {"host", "string", sizeof(char *),
   FMOffset(setup_group_msg_ptr, host)},
  {"port", "integer", sizeof(int),
   FMOffset(setup_group_msg_ptr, port)},

  {(char *) 0, (char *) 0, 0, 0}
};

FMStructDescRec assoc_group_info_msg_formats[] = {
    {"assoc group info", assoc_group_info_msg_flds, sizeof(assoc_group_info_msg), NULL},
    {NULL, NULL, 0, NULL}};
FMStructDescRec find_matching_msg_formats[] = {
    {"find_matching", find_matching_msg_flds, sizeof(find_matching_msg), NULL},
    {NULL, NULL, 0, NULL}};
FMStructDescRec init_contact_msg_formats[] = {
    {"init contact", init_contact_msg_flds, sizeof(init_contact_msg), NULL},
    {NULL, NULL, 0, NULL}};
FMStructDescRec provisional_use_msg_formats[] ={
    {"provisional use", provisional_use_msg_flds, sizeof(provisional_use_msg), NULL},
    {NULL, NULL, 0, NULL}};
FMStructDescRec remove_group_msg_formats[] = {
    {"remove group", remove_group_msg_flds, sizeof(remove_group_msg), NULL},
    {NULL, NULL, 0, NULL}};
FMStructDescRec return_contact_msg_formats[] = {
    {"return contact", return_contact_msg_flds, sizeof(return_contact_msg), NULL},
    {NULL, NULL, 0, NULL}};
FMStructDescRec return_matching_msg_formats[] = {
    {"return matching", return_matching_msg_flds, sizeof(ret_group_struct), NULL},
    {"comm_group_struct", comm_group_struct_flds, sizeof(comm_group_struct), NULL},
    {NULL, NULL, 0, NULL}};

FMStructDescRec setup_group_msg_formats[] = {
    {"setup group", setup_group_msg_flds, sizeof(setup_group_msg), NULL},
    {NULL, NULL, 0, NULL}};

