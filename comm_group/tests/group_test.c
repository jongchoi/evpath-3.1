/* -*-C-*-
*******************************************************************************
* 
* File:        group_test.c
* RCS:         $Id: group_test.c,v 1.4 2008-04-14 17:51:23 eisen Exp $
* Description: Test for group (Greg's code - almost untouched)
* Author:      Fabian E. Bustamante (fabianb@cc.gatech.edu)
*              Systems Research Group
*              College of Computing
*              Georgia Institute of Technology
* Created:
* Modified:
*  
* (C) Copyright 2000, all rights reserved.
*
*******************************************************************************
*/
#include "../config.h"
#if 0
#include "/users/f/fabianb/cm/libltdl/ltdl.h"
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <atl.h>
#include <evpath.h>
#include <comm_group2.h>

extern int fflush ARGS((FILE *));

extern int CMgroup_debug;

int
main()
{
#if 0
  LTDL_SET_PRELOADED_SYMBOLS();
#endif
  /* CMgroup_debug = 1; */
  
  while (1) {
    char line[80];

    printf("Setup, Normal,Group id or Find ? ->");
    fflush(stdout);
    fgets(line, sizeof(line), stdin);
    switch (line[0]) {
    case 's':
    case 'S':{
      char user_name[80];
      char application_name[80];
      char group_type[80];
      char host[80];
      char number_line[80];
      int expiration, port;
      char *group_id;

      printf("\tUser name : ");
      fflush(stdout);
      fgets(user_name, sizeof(user_name), stdin);

      if (user_name[strlen(user_name) - 1] == '\n') {
	user_name[strlen(user_name) - 1] = 0;
      }
      
      printf("\tApplication name : ");
      fflush(stdout);
      fgets(application_name, sizeof(application_name), stdin);

      if (application_name[strlen(application_name) - 1] == '\n') {
	application_name[strlen(application_name) - 1] = 0;	
      }
      
      printf("\tGroup type : ");
      fflush(stdout);
      fgets(group_type, sizeof(group_type), stdin);
      if (group_type[strlen(group_type) - 1] == '\n') {
	group_type[strlen(group_type) - 1] = 0;
      }
      
      printf("\tExpiration time (seconds) : ");
      fflush(stdout);
      expiration = 0;
      fgets(number_line, sizeof(number_line), stdin);
      sscanf(number_line, "%d\n", &expiration);
      printf("\tHost : ");
      fflush(stdout);
      fgets(host, sizeof(host), stdin);
      if (host[strlen(host) - 1] == '\n') {
	host[strlen(host) - 1] = 0;
      }
      
      printf("\tPort number : ");
      fflush(stdout);
      port = 0;
      fgets(number_line, sizeof(number_line), stdin);
      sscanf(number_line, "%d", &port);

      group_id = setup_comm_group(user_name, application_name, group_type,
				  expiration, host, port);
      printf("\t\tCreated group \"%s\"\n", group_id);
    }
    break;
    case 'n':
    case 'N':{
      char user_name[80];
      char application_name[80];
      char group_type[80];
      char *host;
      int port;

      printf("\tUser name : ");
      fflush(stdout);
      fgets(user_name, sizeof(user_name), stdin);
      if (user_name[strlen(user_name) - 1] == '\n') 
	user_name[strlen(user_name) - 1] = 0;
      printf("\tApplication name : ");
      fflush(stdout);
      fgets(application_name, sizeof(application_name), stdin);
      if (application_name[strlen(application_name) - 1] == '\n') 
	application_name[strlen(application_name) - 1] = 0;
      printf("\tGroup type : ");
      fflush(stdout);
      fgets(group_type, sizeof(group_type), stdin);
      if (group_type[strlen(group_type) - 1] == '\n') 
	group_type[strlen(group_type) - 1] = 0;

      init_comm_group_contact(user_name, application_name, group_type,
			      &host, &port);
      if (host != NULL) {
	printf("\t\tContact point is host \"%s\", port %d\n",
	       host, port);
      } else {
	printf("\t\tNo matching group.\n");
      }
    }
    break;
    case 'g':
    case 'G':{
      char group_id[80];
      char *host;
      int port;

      printf("\tGroup id : ");
      fflush(stdout);
      fgets(group_id, sizeof(group_id), stdin);
      if (group_id[strlen(group_id) - 1] == '\n') 
	group_id[strlen(group_id) - 1] = 0;

      init_specific_comm_group_contact(group_id, &host, &port);
      if (host != NULL) {
	printf("\t\tContact point is host \"%s\", port %d\n",
	       host, port);
      } else {
	printf("\t\tNo matching group.\n");
      }
    }
    break;
    case 'F':
    case 'f':{
      char app_list[80];
      char type_list[80];
      int i;
      comm_group_return group_return = NULL;

      printf("\tApplication name list: ");
      fflush(stdout);
      fgets(app_list, sizeof(app_list), stdin);
      if (app_list[strlen(app_list) - 1] == '\n') 
	app_list[strlen(app_list) - 1] = 0;
      printf("application name list \"%s\"\n", app_list);
      printf("\tGroup type list: ");
      fflush(stdout);
      fgets(type_list, sizeof(type_list), stdin);
      if (type_list[strlen(type_list) - 1] == '\n') 
	type_list[strlen(type_list) - 1] = 0;

      group_return = matching_comm_groups(app_list, type_list);

      if (group_return != NULL) {
	printf("Group return count = %d\n", group_return->count);
	for (i = 0; i < group_return->count; i++) {
	  if (group_return->list[i].application_name == NULL)
	    group_return->list[i].application_name = "NULL";
	  if (group_return->list[i].group_id == NULL)
	    group_return->list[i].group_id = "NULL";
	  if (group_return->list[i].group_type == NULL)
	    group_return->list[i].group_type = "NULL";
	  if (group_return->list[i].host == NULL)
	    group_return->list[i].host = "NULL";
	  printf("\tapp - \"%s\", type - \"%s\",  ID=%s on %s,%d\n",
		 group_return->list[i].application_name,
		 group_return->list[i].group_type,
		 group_return->list[i].group_id,
		 group_return->list[i].host,
		 group_return->list[i].port);
	}
	free(group_return);
      } else
	printf("Comm group return is null\n");
      
    }
    break;
    case 'x':
      exit(0);
    }
  }
}
