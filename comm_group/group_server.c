/* -*-C-*-
*******************************************************************************
* 
* File:        group_server.c
* RCS:         $Id: group_server.c,v 1.16 2008-09-23 18:49:34 eisen Exp $
* Description: Server side (Greg's code modified for CM)
* Author:      Fabian E. Bustamante (fabianb@cc.gatech.edu)
*              Systems Research Group
*              College of Computing
*              Georgia Institute of Technology
* Created:
* Modified:
*  
* (C) Copyright 2000, all rights reserved.
*
*******************************************************************************
*/

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include <sys/types.h>
#ifndef HAVE_WINDOWS_H
#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif
#include <sys/socket.h>
#ifdef HAVE_SYS_RESOURCE_H
#include <sys/resource.h>
#endif
#include <netinet/in.h>
#ifdef HAVE_NETDB
#include <netdb.h>
#endif
#include <unistd.h>
#ifdef HAVE_SYS_SELECT_H
#include <sys/select.h>
#endif
#include <signal.h>
#else
#include <windows.h>
#include <winsock.h>
#endif
#include "atl.h"
#include "fm.h"
#include "evpath.h"
#include "cercs_env.h"
#ifdef HAVE_GEN_THREAD_H
#include "gen_thread.h"
#endif

#include "group_formats.h"
#include "comm_group2.h"

/* global variables */
static CManager cm = NULL;
static int debug_flag = 0;
static FILE *debug_log = NULL;
char debug_log_name[BUFSIZ];

CMFormat return_format = NULL;
CMFormat return_matching_format = NULL;
CMFormat provisional_use_format = NULL;

typedef struct group {
  char *user_name;
  char *application_name;
  char *group_type;
  int expiration;		/* in seconds, 0=never */
  time_t create_time;
  char *group_id;
  char *host;
  int port;
  void *data;
  int data_len;
} *group_ptr;

typedef struct group_list {
  group_ptr group;
  struct group_list *next;
} *group_list;

group_list global_group_list = NULL;

time_t current_time = 0;

static void assign_unique_group_id();


/*
 * dump_group_list: Dump a group list in debug_log
 *
 */

void
dump_group_list(list)
  group_list list;
{
  fprintf(debug_log, "Group list includes: ");
  while (list != NULL) {
    fprintf(debug_log," \"%s\",", list->group->group_id);
    list = list->next;
  }
  fprintf(debug_log,"\n");
  fflush(debug_log);
} /* end dump_group_list */


/*
 * free_group: free element group
 *
 */

void
free_group(grp)
  group_ptr grp;
{
  free(grp->user_name);
  free(grp->application_name);
  free(grp->group_type);
  free(grp->group_id);
  if (grp->host) free(grp->host);
  free(grp);
} /* end free_group */



/*
 * free_group_list: free for list of groups
 *
 */

void
free_group_list(list)
  group_list list;
{
  while (list != NULL) {
    group_list next = list->next;
    free(list);
    list = next;
  }
} /* end free_group_list */



/*
 * append_group_list: append for list of groups
 *
 */

group_list
append_group_list(list, new)
  group_list list;
group_list new;
{
  group_list orig = list;
  if (list == NULL)
    return new;

  while (list->next != NULL) {
    list = list->next;
  }
  list->next = new;
  return orig;
} /* end append_group_list */


/*
 * count_group_list: count of list of groups, returns size of list
 *
 */

int
count_group_list(list)
  group_list list;
{
  int count = 0;
  while (list != NULL) {
    list = list->next;
    count++;
  }
  return count;
} /* end count_group_list */



/*
 * copy_group_list: copy for list of groups, returns a new list
 *
 */

group_list
copy_group_list(list)
  group_list list;
{
  group_list copy_base = NULL;
  group_list last = NULL;
  while (list != NULL) {
    group_list next = list->next;
    group_list tmp = (group_list) malloc(sizeof(struct group_list));
    tmp->next = NULL;
    tmp->group = list->group;
    if (last != NULL) {
      last->next = tmp;
    } else {
      copy_base = tmp;
    }
    last = tmp;
    list = next;
  }
  return copy_base;
} /* end copy_group_list */



/*
 * search_group_list: search in list of groups using compar_func
 *
 */

group_list
search_group_list(list, compar_func, data)
  group_list list;
int (*compar_func) ();
void *data;
{
  group_list return_list = NULL;
  group_list last = NULL;
  while (list != NULL) {
    if ((compar_func) (list->group, data)) {
      group_list tmp = (group_list) malloc(sizeof(struct group_list));

      tmp->group = list->group;
      if (last != NULL) {
	last->next = tmp;
	last = tmp;
      } else {
	return_list = tmp;
	last = tmp;
      }
      last->next = NULL;
    }
    list = list->next;
  }
  return return_list;
} /* end search_group_list */



/*
 * return_contact: returns contact to client, needs to return condition too
 *
 */

void
return_contact(grp, cond, cm, conn)
  group_ptr grp;
int cond;
CManager cm;
CMConnection conn;
{
  return_contact_msg msg;

  msg.cond = cond;

  if (grp == NULL) {
    msg.host = NULL;
    msg.port = -1;
    msg.group_id = NULL;
    if (debug_flag) {
      fprintf(debug_log, "No valid group selected\n");
    }
  } else {
    msg.host = grp->host;
    msg.port = grp->port;
    msg.group_id = grp->group_id;
    if (debug_flag) {
      fprintf(debug_log, "Returning group\n"); 
      fprintf(debug_log,  "\tgroup_id = %s\n", grp->group_id);
      fprintf(debug_log,  "\tcontact point = host %s - port %d\n", grp->host == 0 ? "(NULL)": grp->host,
		grp->port);
    }
  }
  fflush(debug_log);
  CMwrite(conn, return_format, &msg);
} /* end return_contact */



/*
 * eliminate_expired_groups: eleminate groups whose time has expired 
 *
 */

void
eliminate_expired_groups()
{
  group_list tmp_list = global_group_list;
  current_time = time(NULL);
  if (tmp_list == NULL)
    return;

  while (tmp_list->next != NULL) {
    group_ptr grp = tmp_list->next->group;
    if (current_time >= grp->create_time + grp->expiration) {
      group_list next = tmp_list->next->next;
      free_group(grp);
      free(tmp_list->next);
      tmp_list->next = next;
    } else {
      tmp_list = tmp_list->next;
    }
  }

  tmp_list = global_group_list;
  if (tmp_list != NULL) {
    if (current_time >= tmp_list->group->create_time +
	tmp_list->group->expiration) {
      global_group_list = tmp_list->next;
      free(tmp_list);
      return;
    }
  }
} /* end eliminate_expired_groups */



/*
 * group_id_eq: use for search
 *
 */

int
group_id_eq(grp, name)
  group_ptr grp;
char *name;
{
  if (grp->group_id != NULL) {
    return (strcmp(grp->group_id, name) == 0);
  } else {
    return 0;
  }
} /* end group_id_eq */



/*
 * user_name_eq: use for search
 *
 */

int
user_name_eq(grp, name)
  group_ptr grp;
char *name;
{
  if (grp->user_name != NULL) {
    return (strcmp(grp->user_name, name) == 0);
  } else {
    return 0;
  }
} /* end user_name_eq */



/*
 * application_name_eq: use for search
 *
 */

int
application_name_eq(grp, name)
  group_ptr grp;
char *name;
{
  if (grp->application_name != NULL) {
    return (strcmp(grp->application_name, name) == 0);
  } else {
    return 0;
  }
} /* application_name_eq */



/*
 * application_name_neq: use for search
 *
 */

int
application_name_neq(grp, name)
  group_ptr grp;
char *name;
{
  if (grp->application_name != NULL) {
    return (strcmp(grp->application_name, name) != 0);
  } else {
    return 1;
  }
} /* end application_name_neq */



/*
 * group_type_eq: use for search
 *
 */

int
group_type_eq(grp, name)
  group_ptr grp;
char *name;
{
  if (grp->group_type != NULL) {
    return (strcmp(grp->group_type, name) == 0);
  } else {
    return 0;
  }
} /* end group_type_eq */



/*
 * group_type_neq: use for search
 *
 */

int
group_type_neq(grp, name)
  group_ptr grp;
char *name;
{
  if (grp->group_type != NULL) {
    return (strcmp(grp->group_type, name) != 0);
  } else {
    return 1;
  }
} /* end group_type_neq */



/*
 * all_eq: use for search
 *
 */

int
all_eq(grp, match)
  group_ptr grp;
group_ptr match;
{
  if (match->user_name != NULL) {
    if (grp->user_name == NULL)
      return 0;
    if (strcmp(grp->user_name, match->user_name) != 0) {
      return 0;
    }
  } else if (grp->user_name != NULL) {
    return 0;
  }
  if (match->application_name != NULL) {
    if (grp->application_name == NULL)
      return 0;
    if (strcmp(grp->application_name, match->application_name) != 0) {
      return 0;
    }
  } else if (grp->application_name != NULL) {
    return 0;
  }
  if (match->group_type != NULL) {
    if (grp->group_type == NULL)
      return 0;
    if (strcmp(grp->group_type, match->group_type) != 0) {
      return 0;
    }
  } else if (grp->group_type != NULL) {
    return 0;
  }
  return 1;
} /* end all_eq */



/*
 * search_for_match: Returns a list of matching comm groups
 *
 */

static group_list
search_for_match(group_id, user_name, application_name, group_type)
  char *group_id;
char *user_name;
char *application_name;
char *group_type;
{
  group_list current_list, tmp_list;
  struct group tmp_grp;

  
  if (debug_flag) {
    fprintf(debug_log,  "\nBefore Expiration ");
    dump_group_list(global_group_list);
    fflush(debug_log);
  }
  
  eliminate_expired_groups();

  if (debug_flag) {
    fprintf(debug_log,  "Requesting contact point for group:\n");
    if (group_id != NULL) {
      fprintf(debug_log, "\tGroup id = %s\n", group_id);
    }
    if (user_name != NULL) {
      fprintf(debug_log,  "\tUser name = %s\n", user_name);
    }
    if (application_name != NULL) {
      fprintf(debug_log,  "\tApplication name = %s\n", application_name);
    }
    if (group_type != NULL) {
      fprintf(debug_log,  "\tGroup type = %s\n", group_type);
    }
    fflush(debug_log);
  }
  
  if (group_id != NULL) {
    /* if group_id is specified, *MUST* match exactly */
    current_list = search_group_list(global_group_list, group_id_eq,
				     (void *) group_id);
    return current_list;
  }
  current_list = copy_group_list(global_group_list);

   if (debug_flag) { 
     fprintf(debug_log, "Initial ");
     dump_group_list(current_list);
     fflush(debug_log);
   }

  tmp_list = current_list;
  tmp_grp.user_name = user_name;
  tmp_grp.application_name = application_name;
  tmp_grp.group_type = group_type;
  current_list = search_group_list(global_group_list, all_eq,
				   (void *) &tmp_grp);
  free_group_list(tmp_list);

  if (debug_flag) { 
    fprintf(debug_log,  "Final match ");
    dump_group_list(current_list);
    fflush(debug_log);
  }
  
  return current_list;
} /* end search_for_match */



/*
 * handle_setup_group: handler for setup request
 *
 */

extern void
handle_setup_group(cm, conn, data, param, attrs)
CManager cm;
CMConnection conn;
void *data;
void *param;
attr_list attrs;
{
  setup_group_msg_ptr msg = (setup_group_msg_ptr) data;
  group_ptr new_grp;
  group_list tmp, match_list;
  
  if (msg->host == NULL) {
    return_contact((group_ptr)NULL, msg->cond, cm, conn);
  }
  /* 
   *  As a step to eliminate race conditions in group inquiries and setup,
   *  a repeated setup within 10 seconds for the same group identifiers 
   *  will not be accepted.
   */
  match_list = search_for_match(NULL, msg->user_name, msg->application_name,
				msg->group_type);
  if (match_list != NULL) {
    if (match_list->next != NULL) {
      /* take most recent (should be first) */
      if ((time(NULL) - match_list->group->create_time) < 10) {
	return_contact((group_ptr)NULL, msg->cond, cm, conn);
      }
    } else {
      if ((time(NULL) - match_list->group->create_time) < 10) {
	return_contact((group_ptr)NULL, msg->cond, cm, conn);
      }
    }
  }
  new_grp = (group_ptr) malloc(sizeof(struct group));
  if (msg->user_name != NULL) {
    new_grp->user_name = strdup(msg->user_name);
  } else {
    new_grp->user_name = NULL;
  }
  if (msg->application_name != NULL) {
    new_grp->application_name = strdup(msg->application_name);
  } else {
    new_grp->application_name = NULL;
  }
  if (msg->group_type != NULL) {
    new_grp->group_type = strdup(msg->group_type);
  } else {
    new_grp->group_type = NULL;
  }
  new_grp->expiration = msg->expiration;
  new_grp->create_time = time(NULL);
  new_grp->host = strdup(msg->host);
  new_grp->port = msg->port;
  new_grp->data = NULL;
  new_grp->data_len = 0;

  if ((new_grp->expiration > 604800) ||
      (new_grp->expiration < 0)) {
    /* 
     * if they ask for negative time or for more than a week,
     * give them a day, arbitrarily.
     */
    new_grp->expiration = 86400;	/* = 60*60*24 */
  }
  assign_unique_group_id(new_grp, global_group_list);

  tmp = (group_list) malloc(sizeof(struct group_list));
  tmp->group = new_grp;
  tmp->next = global_group_list;
  global_group_list = tmp;

  if (debug_flag) {
    fprintf(debug_log,  "\nCreating group:\n");
    if (new_grp->user_name != NULL) {
      fprintf(debug_log,  "\tUser name = %s\n", new_grp->user_name);
    } else {
      fprintf(debug_log,  "\tUser name = <NULL>\n");
    }
    if (new_grp->application_name != NULL) {
      fprintf(debug_log,  "\tApplication name = %s\n", new_grp->application_name);
    } else {
      fprintf(debug_log,  "\tApplication name = <NULL>\n");
    }
    if (new_grp->group_type != NULL) {
      fprintf(debug_log,  "\tGroup type = %s\n", new_grp->group_type);
    } else {
      fprintf(debug_log, "\tGroup type = NULL\n");
    }
    fprintf(debug_log, "\tvalidity duration = %d (seconds)\n", new_grp->expiration);
    fprintf(debug_log, "\tcreate time = %s", ctime(&new_grp->create_time));
    fprintf(debug_log, "\tassigned group_id = %s\n", new_grp->group_id);
    fprintf(debug_log, "\tcontact point = host %s - port %d\n", new_grp->host,
	     new_grp->port);
    
    fprintf(debug_log, "\nAfter creation ");
    dump_group_list(global_group_list);
    fflush(debug_log);
  }
  
  return_contact(new_grp, msg->cond, cm, conn);
} /* end handle_setup_group */



/*
 * handle_remove_group: handler for remove group request
 *
 */

extern void
handle_remove_group(cm, conn, data, param, attrs)
CManager cm;
CMConnection conn;
void *data;
void *param;
attr_list attrs;
{
  remove_group_msg_ptr msg = (remove_group_msg_ptr) data;
  group_list match_list;

  match_list = search_for_match(msg->group_id, NULL, NULL, NULL);

  if (match_list != NULL) {
    match_list->group->expiration = 0;
  }
  eliminate_expired_groups();
} /* end gen_unique_group_id */



/*
 * gen_unique_group_id: 
 *
 */

void
gen_unique_group_id(grp, base, list)
  group_ptr grp;
char *base;
group_list list;
{
  char *tmp_area = malloc(strlen(base) + 10);
  int counter = 1;
  while (1) {			/* forever (will return below) */
    group_list tmp;
    sprintf(tmp_area, "%s_%d", base, counter++);
    tmp = search_group_list(list, group_id_eq, (void *) tmp_area);
    if (tmp == NULL) {
      grp->group_id = strdup(tmp_area);
      return;
    } else {
      free_group_list(tmp);
    }
  }
} /* end gen_unique_group_id */



/*
 * assign_unique_group_id:
 *
 */

static void
assign_unique_group_id(grp, list)
  group_ptr grp;
group_list list;
{
  if (grp->application_name != NULL) {
    gen_unique_group_id(grp, grp->application_name, list);
  } else if (grp->user_name != NULL) {
    gen_unique_group_id(grp, grp->user_name, list);
  } else if (grp->group_type != NULL) {
    gen_unique_group_id(grp, grp->group_type, list);
  } else {
    gen_unique_group_id(grp, "anon", list);
  }
} /* end assign_unique_group_id */



/*
 * out_domain_rejection: ### we don't know how to get the hostname with the new cm
 *    so we're not going to be picky for now ... just do nothing for now
 *
 */

/* 5 days of free use of our daemons for every host */
#define GRACE_PERIOD_SEC 60 * 60 * 24 * 5

static void
out_domain_rejection(cm, conn)
  CManager cm;
CMConnection conn;
{
  
  return;
  
#ifdef OUT_DOMAIN_REJECTION
  if (strcmp(GROUP_SERVICE_DOMAIN, "") == 0) {
    /* non discriminatory */
    return;
  }
  
  if (from_port == NULL) {
    CMConnection_close(conn);
    return;
  }
  if (getpeername(file_id_IOfile(from_port), &sock_addr,
		  &sock_len) == 0) {
    if (sock_addr.sa_family == AF_INET) {
#ifdef HAVE_STRUCT_HOSTENT
      struct hostent *host;
      struct sockaddr_in *in_sock = (struct sockaddr_in *) &sock_addr;
      host = gethostbyaddr((char *) &in_sock->sin_addr,
			   sizeof(struct in_addr), AF_INET);
      if (host != NULL) {
	char **alias;
	time_t intro_time;
	int postlen = strlen(postfix);
	int hostlen = strlen(host->h_name);
	/* 
	 * Check to see if our GROUP_SERVICE_DOMAIN is a postfix
	 * of the hostname.  If not, we'll reject the host as being
	 * outside of our service domain.
	 */
	if (hostlen > postlen) {
	  if (strcmp(&host->h_name[hostlen - postlen], postfix) == 0) {
	    /* good host */
	    return;
	  }
	} else {
	  if (strcmp(host->h_name, "localhost") == 0) {
	    return;
	  }
	}
	for (alias = host->h_aliases; *alias != 0; alias++) {
	  if (hostlen > postlen) {
	    if (strcmp(alias[hostlen - postlen], postfix) == 0) {
	      /* good host */
	      return;
	    }
	  }
	}

	intro_time = get_time_for_host(in_sock->sin_addr, host->h_name);
	if ((time(NULL) - intro_time) < GRACE_PERIOD_SEC) {
	  provisional_use_msg msg;
	  msg.domain = postfix;
	  msg.time = intro_time + GRACE_PERIOD_SEC - time(NULL);
	  CMwrite(conn, provisional_use_format, &msg);
	  return;
	}
	printf("Rejecting out-domain host %s, aliases: ", host->h_name);
	for (alias = host->h_aliases; *alias != 0; alias++) {
	  printf(" %s", *alias);
	}
	printf("\n");
      }
#endif
    }
  }
  /* must not be a good host, close it down ... */
  CMConnection_close(conn);
#endif
} /* end out_domain_rejection */



/*
 * handle_init_contact:
 *
 */

extern void
handle_init_contact(cm, conn, data, param, attrs)
CManager cm;
CMConnection conn;
void *data;
void *param;
attr_list attrs;
{
  init_contact_msg_ptr msg = (init_contact_msg_ptr) data;
  group_list match_list;

  out_domain_rejection(cm, conn); /* ### see header function comment */

  match_list = search_for_match(msg->group_id, msg->user_name,
				msg->application_name, msg->group_type);

  if (match_list == NULL) {
    return_contact((group_ptr)NULL, msg->cond, cm, conn);
  } else {
    if (match_list->next != NULL) {
      /* take most recent (should be first) */
      return_contact(match_list->group, msg->cond, cm, conn);
    } else {
      return_contact(match_list->group, msg->cond, cm, conn);
    }
  }
  free_group_list(match_list);
} /* end handle_init_contact */



/*
 * handle_assoc_group_info:
 *
 */

extern void
handle_assoc_group_info(cm, conn, data, param, attrs)
CManager cm;
CMConnection conn;
void *data;
void *param;
attr_list attrs;
{
  assoc_group_info_msg_ptr msg = (assoc_group_info_msg_ptr) data;
  group_list match_list;

  match_list = search_for_match(msg->group_name, NULL, NULL, NULL);

  if (match_list == NULL) {
    return_contact((group_ptr)NULL, msg->cond, cm, conn);
  } else {
    if (match_list->group->data != NULL) {
      free(match_list->group->data);
    }
    match_list->group->data = malloc(msg->data_len);
    memcpy(match_list->group->data, msg->data, msg->data_len);
    match_list->group->data_len = msg->data_len;
    return_contact(match_list->group, msg->cond, cm, conn);
  }
  free_group_list(match_list);
} /* end handle_assoc_group_info */



/*
 * parse_list:
 *
 */

static char **
parse_list(string)
  char *string;
{
  int count = 0;
  char *end;
  char *tmp;
  char **result = (char **) malloc(sizeof(char *));

  if (string == NULL) {
    return NULL;
  }
  end = string + strlen(string);
  result[count] = NULL;
  for (tmp = string; tmp < end; tmp++) {
    if (*tmp == ',') {
      *(tmp--) = 0;
    }
    while (((*tmp == ' ') || (*tmp == '	')) && (tmp >= string)) {
      /* swallow preceeding white */
      *(tmp--) = 0;
    }
  }
  tmp = string;
  while (tmp < end) {
    if ((*tmp == ' ') || (*tmp == '	') || (*tmp == 0)) {
      /* skip white */
      tmp++;
    } else {
      result = (char **) realloc(result, (count + 2) * sizeof(char *));
      result[count] = tmp;
      tmp += strlen(tmp);
      count++;
    }
  }
  result[count] = NULL;
  return result;
} /* end parse_list */



/*
 * handle_find_matching:
 *
 */

extern void
handle_find_matching(cm, conn, data, param, attrs)
CManager cm;
CMConnection conn;
void *data;
void *param;
attr_list attrs;
{
  find_matching_msg_ptr msg = (find_matching_msg_ptr) data;
  ret_group_struct return_msg;
  group_list match_result = NULL;
  group_list tmp_list;
  char **app_list;
  char **type_list;
  group_list user_list_result;
  int app, type, count;

  if (debug_flag) {
    fprintf(debug_log, "\nBefore Expiration ");
    dump_group_list(global_group_list);
    fflush(debug_log);
  }
  
  
  eliminate_expired_groups();

  if (debug_flag) {
    fprintf(debug_log, "Got find_matching request:\n");
    if (msg->user_name != NULL) {
      fprintf(debug_log, "\tUser name = %s\n", msg->user_name);
    }
    if (msg->app_list != NULL) {
      fprintf(debug_log, "\tApplication list = %s\n", msg->app_list);
    }
    if (msg->type_list != NULL) {
      fprintf(debug_log, "\tGroup type list = %s\n", msg->type_list);
    }
    
    fprintf(debug_log, "Initial ");
    dump_group_list(global_group_list);
    fflush(debug_log);
  }
  
  app_list = parse_list(msg->app_list);
  type_list = parse_list(msg->type_list);

  user_list_result = search_group_list(global_group_list, user_name_eq,
				       (void *) msg->user_name);
  for (app = 0; (app_list != NULL) && (app_list[app] != NULL); app++) {
    group_list app_list_result;
    if (strcmp(app_list[app], "*") != 0) {
      app_list_result = search_group_list(user_list_result,
					  application_name_eq,
					  (void *) app_list[app]);
    } else {
      /* wildcard */
      int prev_app;
      app_list_result = copy_group_list(user_list_result);
      for (prev_app = 0; prev_app < app; prev_app++) {

	group_list tmp = app_list_result;
	app_list_result = search_group_list(app_list_result,
					    application_name_neq,
					    (void *) app_list[prev_app]);
	free_group_list(tmp);
      }
    }
    for (type = 0; (type_list != NULL) && (type_list[type] != NULL); type++) {
      group_list result;
      if (strcmp(type_list[type], "*") != 0) {
	result = search_group_list(app_list_result, group_type_eq,
				   (void *) type_list[type]);
      } else {
	/* wildcard */
	int prev_type;
	result = copy_group_list(app_list_result);
	for (prev_type = 0; prev_type < type; prev_type++) {
	  group_list tmp = result;
	  result = search_group_list(result, group_type_neq,
				     (void *) type_list[prev_type]);
	  free_group_list(tmp);
	}
      }
      match_result = append_group_list(match_result, result);
    }
    free_group_list(app_list_result);
  }
  free_group_list(user_list_result);

 if (debug_flag) {
  fprintf(debug_log, "Final match ");
  dump_group_list(match_result);
  fflush(debug_log);
 }
 
  return_msg.cond = msg->cond;
  return_msg.count = count_group_list(match_result);
  return_msg.list = (comm_group_list) malloc(return_msg.count * 
					     sizeof(comm_group_struct));
  tmp_list = match_result;
  count = 0;
  while (tmp_list != NULL) {
    return_msg.list[count].user_name = tmp_list->group->user_name;
    return_msg.list[count].application_name = tmp_list->group->application_name;
    return_msg.list[count].group_type = tmp_list->group->group_type;
    return_msg.list[count].group_id = tmp_list->group->group_id;
    return_msg.list[count].host = tmp_list->group->host;
    return_msg.list[count].port = tmp_list->group->port;
    return_msg.list[count].info_len = tmp_list->group->data_len;
    return_msg.list[count].info = tmp_list->group->data;
    
    if (debug_flag) {
      fprintf(debug_log, "Here! %d - %d - %d - %d\n", 
	       (return_msg.list[count].application_name == NULL),
	       (return_msg.list[count].group_type == NULL),
	       (return_msg.list[count].group_id== NULL),
	       (return_msg.list[count].host== NULL));
      fprintf(debug_log, "\tapp - \"%s\", type - \"%s\",  ID=%s on %s,%d\n",
	       return_msg.list[count].application_name,
	       return_msg.list[count].group_type,
	       return_msg.list[count].group_id,
	       return_msg.list[count].host,
	       return_msg.list[count].port);
      fflush(debug_log);
    }
    count++;
    tmp_list = tmp_list->next;
  }
  CMwrite(conn, return_matching_format, &return_msg);
  free_group_list(match_result);
} /* end handle_find_matching */



/*
 * get_time_for_host:
 *
 */

#ifdef HAVE_STRUCT_HOSTENT
typedef struct host_info {
  struct in_addr ip_addr;
  time_t intro_time;
} *host_info_p;

static host_info_p hostlist = NULL;
static int host_count = 0;

static time_t
get_time_for_host(ip_addr, hostname)
  struct in_addr ip_addr;
char *hostname;
{
  int i;
  if (hostlist == NULL) {
    hostlist = malloc(sizeof(struct host_info));
  }
  for (i = 0; i < host_count; i++) {
    if (memcmp(&ip_addr, &hostlist[i].ip_addr, sizeof(struct in_addr)) == 0) {
      return hostlist[i].intro_time;
    }
  }
  hostlist = realloc(hostlist, sizeof(struct host_info) * (host_count + 1));
  memcpy(&hostlist[host_count].ip_addr, &ip_addr, sizeof(struct in_addr));
  hostlist[host_count].intro_time = time(NULL);
  host_count++;
  return hostlist[host_count - 1].intro_time;
} /* end get_time_for_host */

static char *postfix = GROUP_SERVICE_DOMAIN;
#endif



/*
 * die_with_error:
 *
 */

static void
die_with_error(sig)
  int sig;
{
  fprintf(stderr, "Group server not responding.  Timeout.\n");
  exit(1);
} /* end die_with_error */



/*
 * interruptHandler: Nice going out, checkpointing included
 *
 */

static void
interrupt_handler (int signo)
{
  if (debug_flag == 2)
    fclose(debug_log);
  exit (0);
} /* end interrupt_handler */

/*
 * main:
 *
 */

char *usage = "Usage: %s [-h|-help] [-debug|-log] [-no_fork]\n \
\t-h|-help -- this message\n \
\t-debug   -- turns on local printf-style debugging output\n \
\t-log     -- turns on logging to /tmp/group_server_<pid>.log\n \
\t-no_fork -- turns of forking of thread.\n\n";


static void
rewrite_server_pid()
{
    FILE *group_server_pid_file;
    group_server_pid_file = fopen("/tmp/group_server2_pid", "w");
    if (group_server_pid_file) {
      /*  printf("Created a group_server_pid file!\n"); */
      fprintf(group_server_pid_file, "%ld\n", (long)getpid());
      fclose(group_server_pid_file);
    }
}

extern int
main(argc, argv)
  int argc;
char **argv;
{
  char env[128];
  int i, no_fork = 0;
  attr_list contact_info;
  CMFormat tmp_format;

  cm = CManager_create();

  for (i = 1; i < argc; i++) {
    
    if ((strcmp(argv[i], "-h") == 0) || (strcmp(argv[i], "-help") == 0)){
      printf(usage, argv[0]);
      exit(0);
    }
    
    if (strcmp(argv[i], "-no_fork") == 0) {
      printf("%s: no forking.\n", argv[0]);
      no_fork++;
    }
    
    if (strcmp(argv[i], "-debug") == 0) {
      if (debug_flag != 0) { 
	printf(usage, argv[0]);
	exit(1);
      }
      printf("%s: debug flag set.\n", argv[0]);
      debug_flag = 1;
      debug_log = stdout;
    }
    
    if (strcmp(argv[i], "-log") == 0) {
      if (debug_flag != 0) { 
	printf(usage, argv[0]);
	exit(1);
      }
      printf("%s: log flag set.\n", argv[0]);
      debug_flag = 2;
      sprintf(debug_log_name, "/tmp/group_server_%ld.log", (long) getpid());
      debug_log = fopen(debug_log_name, "w");
      if (debug_log == NULL) {
	perror("problems opening log file");
	debug_flag = 0;
      }
    }
    
  }
   
  /* substitute this environment the group server host by our host name */
  strcpy(env, "GROUP_SERVER_HOST=localhost");
  putenv(env);

  contact_info = create_attr_list();
  set_attr (contact_info, attr_atom_from_string("IP_HOSTNAME"), Attr_String, 
	    strdup (cercs_getenv ("GROUP_SERVER_HOST")));
  set_attr (contact_info, attr_atom_from_string("IP_PORT"), Attr_Int4,
	    (attr_value) GROUP_SERVER_PORT);

  /*   printf("Server attr list: \n"); */
  /*   dump_attr_list(contact_info); */
  
#ifndef HAVE_WINDOWS_H
  signal(SIGALRM, die_with_error);
  alarm(30);  
  signal (SIGINT, interrupt_handler);
#endif
  
  if (group_server_test()) {
    /* already running */
    exit(0);
#ifndef HAVE_WINDOWS_H
  } else {
    /* make really, really certain there's no group_server running */
    FILE *group_server_pid_file;
    group_server_pid_file = fopen("/tmp/group_server2_pid", "r");
    alarm(0);
    if (group_server_pid_file != NULL) {
      long server_pid;
      /* printf("found a group_server2_pid file!\n"); */
      if (fscanf(group_server_pid_file, "%lx", &server_pid) == 1) {
	if (kill(server_pid, 0) == 0) {
	  fprintf(stderr, "Group Server %lx not responding, but still running\n",
		  server_pid);
	  exit(0);
	}
      }
      fclose(group_server_pid_file);
    }
    if (!no_fork) {
      if (fork() != 0) {
	/* I'm the parent, return now */
	exit(0);
      }
    }
    rewrite_server_pid();
    CMadd_periodic_task(cm, 24*60*60 /* once per day*/ , 0,
			(CMPollFunc)rewrite_server_pid, NULL);
#endif
  }
#ifdef RLIMIT_CPU
  {
    struct rlimit limit;
    limit.rlim_cur = limit.rlim_max = 300;
    if (setrlimit(RLIMIT_CPU, &limit) != 0)
      perror("rlimit cpu");
    limit.rlim_cur = limit.rlim_max = 1024 * 1024 * 100;
    if (setrlimit(RLIMIT_DATA, &limit) != 0)
      perror("rlimit data");
    limit.rlim_cur = limit.rlim_max = 1024 * 1024 * 100;
    if (setrlimit(RLIMIT_STACK, &limit) != 0)
      perror("rlimit stack");
#ifdef RLIMIT_VMEM
    limit.rlim_cur = limit.rlim_max = 1024 * 1024 * 100;
    if (setrlimit(RLIMIT_VMEM, &limit) != 0)
      perror("rlimit vmem");
#endif
  }
#endif
  if (CMlisten_specific (cm, contact_info) == 0) {
    fprintf(stderr, "CManager listen failed\n");
    dump_attr_list (contact_info);
    exit(1);
  }  
    
  tmp_format = CMregister_format(cm, setup_group_msg_formats);
  CMregister_handler(tmp_format, handle_setup_group, NULL);

  tmp_format = CMregister_format(cm, remove_group_msg_formats);
  CMregister_handler(tmp_format, handle_remove_group, NULL);

  tmp_format = CMregister_format(cm, init_contact_msg_formats);
  CMregister_handler(tmp_format, handle_init_contact, NULL);

  tmp_format = CMregister_format(cm, assoc_group_info_msg_formats);
  CMregister_handler(tmp_format, handle_assoc_group_info, NULL);

  tmp_format = CMregister_format(cm, find_matching_msg_formats);
  CMregister_handler(tmp_format, handle_find_matching, NULL);

  provisional_use_format = CMregister_format(cm, provisional_use_msg_formats);

  return_format = CMregister_format(cm, return_contact_msg_formats);

  return_matching_format = CMregister_format(cm, return_matching_msg_formats);
  
  while (1) {
    CMrun_network(cm);
  }
} /* end main */
