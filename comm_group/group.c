/* -*-C-*-
*******************************************************************************
* 
* File:        group.c
* RCS:         $Id: group.c,v 1.13 2008-09-23 18:49:34 eisen Exp $
* Description: Client side (Greg's code modified for CM)
* Author:      Fabian E. Bustamante (fabianb@cc.gatech.edu)
*              Systems Research Group
*              College of Computing
*              Georgia Institute of Technology
* Created:
* Modified:  
*  
* (C) Copyright 2000, all rights reserved.
*
*******************************************************************************
*/

#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#ifdef HAVE_WINDOWS_H
#include <windows.h>
#include <winsock.h>
#else
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef HAVE_PWD_H
#include "pwd.h"
#endif
#endif
#include <string.h>
#include "atl.h"
#include "fm.h"
#include "evpath.h"
#include "cercs_env.h"
#include "comm_group2.h"
#include "group_formats.h"

#ifdef GM_DEBUG
#define DEBUG(x) fprintf(stderr, x)
#else
#define DEBUG(x)
#endif

int CMgroup_debug = 0;

typedef struct _group_server {
  CManager cm;
  CMConnection conn;
} group_server_struct, *group_server;

static char group_server_host[MAXHOSTNAMELEN];

static char *get_comm_user();
static char *get_comm_app_name();
static char *get_comm_comm_type();
static void setup_on_exit_handler ARGS((char *group_id, char *host, int port));


/*
 * default_error_routine:
 *
 */
static void
default_error_routine()
{
  fprintf(stderr, "Failed to contact Group server on host %s, port %d\n",
	  group_server_host, GROUP_SERVER_PORT);
  fprintf(stderr, " For more info on servers see:\n");
  fprintf(stderr, "  http://www.cc.gatech.edu/systems/projects/MOSS/servers.html\n");
  exit(1);
} /* end default_error_routine */

static void (*group_error_routine) () = default_error_routine;


/*
 * set_comm_group_error_handler:
 *
 */

extern void
set_comm_group_error_handler(error_routine)
  group_error_routine_type error_routine;
{
  group_error_routine = error_routine;
} /* end set_comm_group_error_handler */


/*
 * connect_comm_group_server:
 *
 */

static group_server
connect_comm_group_server(do_err, do_fallback)
  int do_err;
int do_fallback;
{
  attr_list contact_list;
  char *host_name = NULL;
  int group_server_port = GROUP_SERVER_PORT;
  
  group_server gs = (group_server) CMmalloc(sizeof(group_server_struct));
  gs->cm = NULL;
  gs->conn = NULL;
  
  if (host_name == NULL) {	/* environment override */
    host_name = cercs_getenv("GROUP_SERVER_HOST");
  }
  if (host_name == NULL) {
    host_name = GROUP_SERVER_HOST;	/* from configure */
  }
  
  gs->cm = CManager_create();

  strcpy(group_server_host, host_name);
  contact_list = create_attr_list();
  set_attr (contact_list, attr_atom_from_string("IP_HOST"), Attr_String,
	    group_server_host);
  set_int_attr (contact_list, attr_atom_from_string("IP_PORT"), group_server_port);

  if (CMgroup_debug) fprintf(stderr,"Contacting group server at ... \n");
  if (CMgroup_debug) dump_attr_list(contact_list);

  gs->conn = CMinitiate_conn (gs->cm, contact_list);

  if ((gs->conn == NULL) && do_fallback) {
    strcpy(group_server_host, "grouphost.cercs.gatech.edu");
    contact_list = create_attr_list();
    set_attr (contact_list, attr_atom_from_string("IP_HOST"), Attr_String,
	      group_server_host);
    set_int_attr (contact_list, attr_atom_from_string("IP_PORT"), 
		  group_server_port);

    if (CMgroup_debug) fprintf(stderr,"Contacting group server at ...\n");
    if (CMgroup_debug) dump_attr_list(contact_list);

    gs->conn = CMinitiate_conn (gs->cm, contact_list);
  }

  if (gs->conn == NULL) {
    if (do_err && (group_error_routine != NULL)) {
      group_error_routine();
    }
    return NULL;
  }
  return gs;
} /* end connect_comm_group_server */


/*
 * shutdown_comm_group_server:
 *
 */

static void
shutdown_comm_group_server(gs)
  group_server gs;
{
  CManager_close(gs->cm);
  CMfree(gs);
} /* end shutdown_comm_group_server */



/*
 * handle_provisional_use:
 *
 *
 */
extern void
handle_provisional_use (CManager cm, CMConnection conn, void *data, void *param,
			attr_list attrs)
{  
  provisional_use_msg_ptr msg = (provisional_use_msg_ptr) data;
  static int warned = 0;

  if (CMgroup_debug) fprintf(stderr,"> handle_provisional_use\n");

  if (warned)
    return;

  warned++;
  fprintf(stderr, "The group_server daemon on %s serves the domain \"%s\"\n",
	  group_server_host, msg->domain);
  fprintf(stderr, 
	  " See http://www.cc.gatech.edu/systems/projects/MOSS/servers.html "
	  "for more info.\n");
  fprintf(stderr, "  Temporary use allowed for ");
  if (msg->time > 60 * 60 * 48) {	/* 2 days */
    int days = msg->time / (60 * 60 * 24);
    fprintf(stderr, "%d more days.\n", days);
  } else if (msg->time > 60 * 60 * 2) {
    int hours = msg->time / (60 * 60);
    fprintf(stderr, "%d more hours.\n", hours);
  } else {
    int mins = msg->time / 60;
    fprintf(stderr, "%d more minutes.\n", mins);
  }
} /* end handle_provisional_use */


/*
 * handle_return_contact:
 *
 *
 */
extern void
handle_return_contact (CManager cm, CMConnection conn, void *data, void *param, attr_list attrs)
{  
  return_contact_msg_ptr ret_msg = (return_contact_msg_ptr) data;
  return_contact_msg_ptr msg_p;


  if (CMgroup_debug) fprintf(stderr,"> handle_return_contact\n");

  (msg_p) = (return_contact_msg_ptr) CMCondition_get_client_data(cm, ret_msg->cond);
  msg_p->cond = 0;	/* you don't need it anymore */ 
  if (ret_msg->host != NULL)
    msg_p->host = strdup(ret_msg->host);
  else
    msg_p->host = NULL;

  msg_p->port = ret_msg->port;

  if (ret_msg->group_id != NULL)
    msg_p->group_id = strdup(ret_msg->group_id);
  else 
    msg_p->group_id = NULL;
  
  CMCondition_signal(cm, ret_msg->cond);
} /* end handle_return_contact */


/*
 * handle_comm_group_return:
 *
 *
 */
extern void
handle_comm_group_return (CManager cm, CMConnection conn, void *data, void *param,
			  attr_list attrs)
{  
  comm_group_return ret_msg = (comm_group_return) data;
  comm_group_return *msg_p;
  int i;

  (msg_p) = (comm_group_return*) CMCondition_get_client_data(cm, ret_msg->cond);

  (*msg_p)->cond = 0;	/* you don't need it anymore */
  (*msg_p)->count = ret_msg->count;
  (*msg_p)->list = CMmalloc(sizeof(comm_group_struct) * (*msg_p)->count);

  if (CMgroup_debug) 
    fprintf(stderr,"> handle_comm_group_return - group count: %d\n", 
	    (*msg_p)->count);

  for (i = 0; i < (*msg_p)->count; i++) {
    if (ret_msg->list[i].user_name != NULL) 
      (*msg_p)->list[i].user_name= strdup(ret_msg->list[i].user_name);
    else 
      (*msg_p)->list[i].user_name= NULL;
    if (ret_msg->list[i].application_name != NULL)
      (*msg_p)->list[i].application_name = strdup(ret_msg->list[i].application_name);
    else  
      (*msg_p)->list[i].application_name = NULL;
    if (ret_msg->list[i].group_type != NULL)
      (*msg_p)->list[i].group_type = strdup(ret_msg->list[i].group_type);  
    else
      (*msg_p)->list[i].group_type = NULL;
    if (ret_msg->list[i].group_id)
      (*msg_p)->list[i].group_id = strdup(ret_msg->list[i].group_id);
    else 
      (*msg_p)->list[i].group_id = NULL;
    if (ret_msg->list[i].host != NULL)
      (*msg_p)->list[i].host = strdup(ret_msg->list[i].host);
    else
      (*msg_p)->list[i].host = NULL;
    (*msg_p)->list[i].port = ret_msg->list[i].port;
    (*msg_p)->list[i].info_len = ret_msg->list[i].info_len;
    if ((ret_msg->list[i].info != NULL) || ((*msg_p)->list[i].info_len != 0))
      (*msg_p)->list[i].info = strdup(ret_msg->list[i].info);  
    else
      (*msg_p)->list[i].info = NULL;
  }

  CMCondition_signal(cm, ret_msg->cond);
} /* end handle_comm_group_return */


/*
 * setup_comm_group:
 *
 */

extern char *
setup_comm_group(user_name, application_name, group_type, expiration_time,
		 host, port)
  char *user_name;
char *application_name;
char *group_type;
int expiration_time;
char *host;
int port;
{
  setup_group_msg msg;

  if (CMgroup_debug) fprintf(stderr,"> setup_comm_group\n");

  msg.application_name = get_comm_app_name();
  if (msg.application_name == NULL) {
    msg.application_name = application_name;
  }
  msg.group_type = get_comm_comm_type();
  if (msg.group_type == NULL) {
    msg.group_type = group_type;
  }
  return setup_specific_comm_group(msg.application_name,
				   msg.group_type, expiration_time, host,
				   port);
} /* end setup_comm_group */



/*
 * setup_specific_comm_group:
 *
 */

extern char *
setup_specific_comm_group(application_name, group_type,
			  expiration_time, host, port)
  char *application_name;
char *group_type;
int expiration_time;
char *host;
int port;
{
  setup_group_msg msg;
  return_contact_msg return_msg;
  CMFormat setup_format;
  CMFormat tmp_format;
  group_server gs = NULL;
  char *group_id = NULL;

  if (CMgroup_debug) fprintf(stderr,"> setup_specific_comm_group\n");

  msg.user_name = get_comm_user();
  msg.application_name = application_name;
  msg.group_type = group_type;
  msg.expiration = expiration_time;
  msg.host = host;
  msg.port = port;
  if (host == NULL) {
    return NULL;
  }

  if (CMgroup_debug) 
    fprintf(stderr,"Setup comm group %s, %s, %s\n",
	    msg.user_name, msg.application_name,
	    msg.group_type);
  
  gs = connect_comm_group_server(1, 1);
  if (gs == NULL) {
    return NULL;
  }

  msg.cond = CMCondition_get(gs->cm, gs->conn);

  setup_format = CMregister_format(gs->cm, setup_group_msg_formats);
  
  tmp_format = CMregister_format(gs->cm, return_contact_msg_formats);
  CMregister_handler (tmp_format, handle_return_contact, NULL);

  tmp_format = CMregister_format(gs->cm, provisional_use_msg_formats);
  CMregister_handler (tmp_format, handle_provisional_use, NULL);

  CMwrite(gs->conn, setup_format, &msg);

  /* now wait */

  CMCondition_set_client_data(gs->cm, msg.cond, &return_msg);
  
  if (CMCondition_wait(gs->cm, msg.cond)) {
    /* go it in 'return_msg' */ 
    if (return_msg.group_id != NULL) {
      group_id = strdup(return_msg.group_id);
    } else {
      group_id = NULL;
    }
    shutdown_comm_group_server(gs);
    setup_on_exit_handler(group_id, host, port);
    return group_id;
  } else {
    perror("Connection to server is gone");
    if (group_error_routine != NULL) {
      group_error_routine();
    }
    shutdown_comm_group_server(gs); 
    setup_on_exit_handler(group_id, host, port);
    return NULL;
  }
} /* end setup_specific_comm_group */


/*
 * assoc_group_info:
 *
 */

extern void
assoc_group_info(group_name, data, data_len)
  char *group_name;
void *data;
int data_len;
{
  assoc_group_info_msg msg;
  return_contact_msg return_msg;
  CMFormat assoc_format;
  CMFormat tmp_format;
  group_server gs = NULL;

  msg.group_name = group_name;
  msg.data = data;
  msg.data_len = data_len;

  if (CMgroup_debug) 
    fprintf(stderr,"Assoc group info - group \"%s\"\n", msg.group_name);

  gs = connect_comm_group_server(1, 1);
  if (gs == NULL) {
    return;
  }

  msg.cond = CMCondition_get(gs->cm, gs->conn);

  assoc_format = CMregister_format(gs->cm, assoc_group_info_msg_formats);

  tmp_format = CMregister_format(gs->cm, return_contact_msg_formats);
  CMregister_handler (tmp_format, handle_return_contact, NULL);

  tmp_format = CMregister_format(gs->cm, provisional_use_msg_formats);
  CMregister_handler (tmp_format, handle_provisional_use, NULL);

  CMwrite(gs->conn, assoc_format, &msg);

  CMCondition_set_client_data(gs->cm, msg.cond, &return_msg);
  
  if (CMCondition_wait(gs->cm, msg.cond)) {
    /* go it in 'return_msg' */ 
    shutdown_comm_group_server(gs);
  } else {
    perror("Connection to server is gone");
    if (group_error_routine != NULL) {
      group_error_routine();
    }
    shutdown_comm_group_server(gs);
  }
} /* end assoc_group_info */


/*
 * group_server_present:
 *
 */

extern int
group_server_present()
{
  group_server gs = connect_comm_group_server(0, 1);
  if (gs == NULL)
    return 0;
  shutdown_comm_group_server(gs);
  return 1;
} /* end group_server_present */


/*
 * group_server_test:
 *
 */

extern int
group_server_test()
{
  group_server gs;
  /* no fallback for testing locality */
  gs = connect_comm_group_server(0, 0);
  if (gs == NULL)
    return 0;
  shutdown_comm_group_server(gs);
  return 1;
} /* end group_server_test */


/*
 * init_comm_group_contact:
 *
 */

extern int
init_comm_group_contact(user_name, application_name, group_type, host_p, port_p)
  char *user_name;
char *application_name;
char *group_type;
char **host_p;
int *port_p;
{
  init_contact_msg msg;
  return_contact_msg return_msg;
  CMFormat init_format;
  CMFormat tmp_format;
  group_server gs = NULL;
  int return_value;

  msg.group_id = NULL;
  if (user_name && (user_name[0] == 0)) {
    msg.user_name = NULL;
  } else {
    msg.user_name = user_name;
  }
  if (application_name && (application_name[0] == 0)) {
    msg.application_name = NULL;
  } else {
    msg.application_name = application_name;
  }
  if (group_type && (group_type[0] == 0)) {
    msg.group_type = NULL;
  } else {
    msg.group_type = group_type;
  }

  if (CMgroup_debug) 
    fprintf(stderr,"Init comm group contact %s, %s, %s\n", 
	    msg.user_name, msg.application_name, msg.group_type);

  gs = connect_comm_group_server(1, 1);
  if (gs == NULL) {
    return 0;
  }

  msg.cond = CMCondition_get(gs->cm, gs->conn);

  init_format = CMregister_format(gs->cm, init_contact_msg_formats);

  tmp_format = CMregister_format(gs->cm, return_contact_msg_formats);
  CMregister_handler (tmp_format, handle_return_contact, NULL);

  tmp_format = CMregister_format(gs->cm, provisional_use_msg_formats);
  CMregister_handler (tmp_format, handle_provisional_use, NULL);

  CMwrite(gs->conn, init_format, &msg);

  CMCondition_set_client_data(gs->cm, msg.cond, &return_msg);
  
  if (CMCondition_wait(gs->cm, msg.cond)) {
    /* go it in 'return_msg' */ 
    if (host_p != NULL) {
      if (return_msg.host != NULL) {
	*host_p = strdup(return_msg.host);
      } else {
	*host_p = NULL;
      }
    }
    if (port_p != NULL) {
      *port_p = return_msg.port;
    }
    return_value = ((return_msg.host != NULL) && (return_msg.port != -1));
    shutdown_comm_group_server(gs);
    return return_value;
  } else {
    perror("Connection to server is gone");
    if (group_error_routine != NULL) {
      group_error_routine();
    }
    return -1;
  }
} /* end init_comm_group_contact */



/*
 * init_specific_comm_group_contact:
 *
 */

extern int
init_specific_comm_group_contact(group_id, host_p, port_p)
  char *group_id;
char **host_p;
int *port_p;
{
  init_contact_msg msg;
  return_contact_msg return_msg;
  CMFormat init_format;
  CMFormat tmp_format;
  group_server gs = NULL;
  int return_value;

  if (group_id && (group_id[0] == 0)) {
    msg.group_id = NULL;
  } else {
    msg.group_id = group_id;
  }

  msg.user_name = NULL;
  msg.application_name = NULL;
  msg.group_type = NULL;
  
  if (CMgroup_debug) fprintf(stderr,"init specific group contact %s\n", group_id);
  
  gs = connect_comm_group_server(1, 1);
  if (gs == NULL) {
    return 0;
  }

  msg.cond = CMCondition_get(gs->cm, gs->conn);
  
  init_format = CMregister_format(gs->cm, init_contact_msg_formats);
  tmp_format = CMregister_format(gs->cm, return_contact_msg_formats);
  CMregister_handler (tmp_format, handle_return_contact, NULL);

  tmp_format = CMregister_format(gs->cm, provisional_use_msg_formats);
  CMregister_handler (tmp_format, handle_provisional_use, NULL);

  CMwrite(gs->conn, init_format, &msg);
  
  CMCondition_set_client_data(gs->cm, msg.cond, &return_msg);

  if (CMCondition_wait(gs->cm, msg.cond)) {
    /* go it in 'return_msg' */
    if (host_p != NULL) {
      if (return_msg.host != NULL) {
	*host_p = strdup(return_msg.host);
      } else {
	*host_p = NULL;
      }
    }
    if (port_p != NULL) {
      *port_p = return_msg.port;
    }
    return_value = ((return_msg.host != NULL) && (return_msg.port != -1));
    shutdown_comm_group_server(gs);
    return return_value;
  } else {
    perror("Connection to server is gone");
    if (group_error_routine != NULL) {
      group_error_routine();
    }
    return -1;
  }
} /* end init_specific_comm_group_contact */


static char *comm_group_user = NULL;

#ifdef HAVE_WINDOWS_H
#define MAX_USER_ID_SIZE	40
static char buffer[MAX_USER_ID_SIZE];
#endif


/*
 * get_comm_user:
 *
 */

static
char *
get_comm_user()
{
  if (comm_group_user == NULL) {
    comm_group_user = cercs_getenv("GROUP_SERVER_USER");

    /* if not, try getlogin() */
    if (comm_group_user == NULL) {
#ifdef HAVE_WINDOWS_H
      DWORD len = MAX_USER_ID_SIZE;
      if (GetUserName(buffer, &len)) {
	comm_group_user = buffer;
      }
#elif defined(HAVE_GETLOGIN)
      /* should use getlogin_r() if available */
      if (getlogin() != NULL) {
	comm_group_user = strdup(getlogin());
      }
      /* if not, try getpwuid(getuid()) */
      if (comm_group_user == NULL) {
	struct passwd *pw = getpwuid(getuid());
	if ((pw != NULL) && (pw->pw_name != NULL)) {
	  comm_group_user = strdup(pw->pw_name);
	}
      }
#endif
    }
  }
  return comm_group_user;
} /* end get_comm_user */

static char *comm_group_app_name = NULL;


/*
 * comm_group_set_app_name:
 *
 */

extern
void
comm_group_set_app_name(app_name)
  char *app_name;
{
  char *last_slash = strrchr(app_name, '/');
  if (last_slash != NULL) {
    comm_group_app_name = strdup(++last_slash);
  } else {
    comm_group_app_name = app_name;
  }
} /* end comm_group_set_app_name */


/*
 * get_comm_app_name:
 *
 */

static
char *
get_comm_app_name()
{
  if (comm_group_app_name == NULL) {
    comm_group_app_name = cercs_getenv("COMM_GROUP_APP_NAME");
  }
  return comm_group_app_name;
} /* end get_comm_app_name */

static char *comm_group_app_list = NULL;


/*
 * comm_group_set_app_list:
 *
 */

extern void
comm_group_set_app_list(list)
  char *list;
{
  comm_group_app_list = list;
} /* end comm_group_set_app_list */


/*
 * get_comm_group_app_list:
 *
 */

static
char *
get_comm_group_app_list()
{
  if (comm_group_app_list == NULL) {
    comm_group_app_list = cercs_getenv("COMM_GROUP_APP_LIST");
  }
  if (comm_group_app_list == NULL) {
    comm_group_app_list = get_comm_app_name();
  }
  return comm_group_app_list;
} /* end get_comm_group_app_list */


static char *comm_group_comm_type = NULL;


/*
 * comm_group_set_comm_type
 *
 */

extern
void
comm_group_set_comm_type(comm_type)
  char *comm_type;
{
  comm_group_comm_type = comm_type;
} /* end comm_group_set_comm_type */


/*
 * get_comm_comm_type:
 *
 */

static
char *
get_comm_comm_type()
{
  if (comm_group_comm_type == NULL) {
    comm_group_comm_type = cercs_getenv("COMM_GROUP_COMM_TYPE");
  }
  return comm_group_comm_type;
} /* end get_comm_comm_type */

static char *comm_group_type_list = NULL;


/*
 * comm_group_set_type_list:
 *
 */

extern
void
comm_group_set_type_list(list)
  char *list;
{
  comm_group_type_list = list;
} /* end comm_group_set_type_list */


/*
 * get_comm_group_type_list:
 *
 */

static
char *
get_comm_group_type_list()
{
  if (comm_group_type_list == NULL) {
    comm_group_type_list = cercs_getenv("COMM_GROUP_TYPE_LIST");
  }
  if (comm_group_type_list == NULL) {
    comm_group_type_list = get_comm_comm_type();
  }
  return comm_group_type_list;
} /* end get_comm_group_type_list */


/*
 * matching_comm_groups:
 *
 */

comm_group_return
matching_comm_groups(app_list, type_list)
  char *app_list;
char *type_list;
{
  char *comm_user = get_comm_user();
  find_matching_msg msg;
  comm_group_return return_msg;
  CMFormat find_format;
  CMFormat tmp_format;
  group_server gs;

  if (app_list == NULL) {
    app_list = get_comm_group_app_list();
  }
  if (type_list == NULL) {
    get_comm_group_type_list();
  }

  
  if (CMgroup_debug) 
    fprintf(stderr, "matching comm groups %s, %s\n", app_list, type_list);

  gs = connect_comm_group_server(1, 1);
  if (gs == NULL) {
    return NULL;
  }

  /* need to contact first */
  tmp_format = CMregister_format(gs->cm, return_contact_msg_formats);
  CMregister_handler (tmp_format, handle_return_contact, NULL);

  tmp_format = CMregister_format(gs->cm, provisional_use_msg_formats);
  CMregister_handler (tmp_format, handle_provisional_use, NULL);

  /* what I ask */
  find_format = CMregister_format(gs->cm, find_matching_msg_formats);

  /* what I'll get back */
  tmp_format = CMregister_format(gs->cm, return_matching_msg_formats);

  CMregister_handler (tmp_format, handle_comm_group_return, NULL);

  msg.cond = CMCondition_get(gs->cm, gs->conn);
  msg.user_name = comm_user;
  msg.app_list = app_list;
  msg.type_list = type_list;

  CMwrite(gs->conn, find_format, &msg);

  return_msg = CMmalloc(sizeof(*return_msg));

  CMCondition_set_client_data(gs->cm, msg.cond, &return_msg);

  if (CMCondition_wait(gs->cm, msg.cond)) {
    /* go it in 'return_msg' */
    shutdown_comm_group_server(gs);
    return return_msg;
  } else {
    perror("Connection to server is gone");
    if (group_error_routine != NULL) {
      group_error_routine();
    }
    return NULL;
  }
} /* end matching_comm_groups */


/*
 * CMConnection_initiate_first:
 *
 */

extern CMConnection
CMConnection_initiate_first(cm, groups)
  CManager cm;
comm_group_return groups;
{
  attr_list contact_list;
  CMConnection conn = NULL;
  int i;
  atom_t CM_IP_HOSTNAME = attr_atom_from_string("IP_HOST");
  atom_t CM_IP_PORT = attr_atom_from_string("IP_PORT");
  for (i = 0; i < groups->count; i++) {
    contact_list = create_attr_list();
    set_attr (contact_list, CM_IP_HOSTNAME, Attr_String, 
	      groups->list[i].host); 
    set_int_attr (contact_list, CM_IP_PORT, groups->list[i].port);
    conn = CMinitiate_conn(cm, contact_list);
    if (conn != NULL) {
      return conn;
    }
    free (contact_list);
  }
  return NULL;
} /* end CMConnection_initiate_first */

#ifdef HAVE_ON_EXIT
extern int on_exit();


/*
 * group_on_exit_handler:
 *
 */

static void
group_on_exit_handler(status, arg)
  int status;
void *arg;
{
  remove_group_msg_ptr tmp_msg = (remove_group_msg_ptr) arg;
  CMFormat tmp_format;
  group_server gs;

  gs = connect_comm_group_server(0, 1);
  if (gs == NULL) {
    return;
  }
  tmp_format = CMregister_format(gs->cm, remove_group_msg_formats);

  CMwrite(gs->conn, tmp_format, tmp_msg);

  shutdown_comm_group_server(gs);

  CMfree(tmp_msg->group_id);
  CMfree(tmp_msg->host);
  CMfree(tmp_msg);
} /* end group_on_exit_handler */


/*
 * setup_on_exit_handler:
 *
 */

static void
setup_on_exit_handler(group_id, host, port)
  char *group_id;
char *host;
int port;
{
  remove_group_msg_ptr tmp_msg =
    (remove_group_msg_ptr) CMmalloc(sizeof(remove_group_msg));
  tmp_msg->group_id = NULL;
  if (group_id) tmp_msg->group_id = strdup(group_id);
  tmp_msg->host = NULL;
  if (host) {
      tmp_msg->host = strdup(host);
  } else {
      tmp_msg->host = NULL;
  }
  tmp_msg->port = port;
  if (on_exit(group_on_exit_handler, tmp_msg) != 0) {
    fprintf(stderr, "On Exit failed\n");
  }
} /* end setup_on_exit_handler */
#else
#ifdef HAVE_ATEXIT
typedef struct _linked_list {
  remove_group_msg_ptr msg;
  struct _linked_list *next;
} *linked_list_ptr;

static linked_list_ptr list_of_msgs = NULL;


/*
 * group_atexit_handler
 *
 */

static void
group_atexit_handler()
{
  CMFormat tmp_format;
  group_server gs;

  gs = connect_comm_group_server(0, 1);
  if (gs == NULL) {
    return;
  }
  tmp_format = CMregister_format(gs->cm, remove_group_msg_formats);
  while (list_of_msgs != NULL) {
    linked_list_ptr tmp_next = list_of_msgs->next;
    CMwrite(gs->conn, tmp_format, list_of_msgs->msg);

    CMfree(list_of_msgs->msg->group_id);
    CMfree(list_of_msgs->msg->host);
    CMfree(list_of_msgs->msg);
    CMfree(list_of_msgs);

    list_of_msgs = tmp_next;
  }

  shutdown_comm_group_server(gs);

} /* end group_atexit_handler */


/*
 * setup_on_exit_handler:
 *
 */

static void
setup_on_exit_handler(group_id, host, port)
  char *group_id;
char *host;
int port;
{
  static int already_registered_handler = 0;

  remove_group_msg_ptr tmp_msg = CMmalloc(sizeof(remove_group_msg));
  linked_list_ptr list_elem = CMmalloc(sizeof(struct _linked_list));

  tmp_msg->group_id = strdup(group_id);
  if (host) {
      tmp_msg->host = strdup(host);
  } else {
      tmp_msg->host = NULL;
  }
  tmp_msg->port = port;
  list_elem->msg = tmp_msg;

  /* 
   * CRITICAL SECTION -- UNPROTECTED ACCESS TO SHARED DATA
   * Potential problem if threaded.
   * Danger is slight and failure is tolerable (a group doesn't get removed).
   */
  list_elem->next = list_of_msgs;
  list_of_msgs = list_elem;
  /* End of Critical Section */

  if (already_registered_handler == 0) {
    if (atexit((void (*)(void)) group_atexit_handler) != 0) {
      fprintf(stderr, "Atexit failed\n");
    }
    already_registered_handler = 1;
  }
} /* end setup_on_exit_handler */

#else

/*
 * no on_exit and no atexit.  Nothing we can do to remove registered
 * groups 
 *
 */

static void
setup_on_exit_handler(group_id, host, port)
  char *group_id;
char *host;
int port;
{
  /* nothing to work with */
}

#endif
#endif
