/* Required definitions. */
#define MODULE
#define __KERNEL__

/* Various required header files. */
#include <linux/config.h> 
#include <linux/module.h> 
#include <linux/version.h> 
#include <linux/kernel.h> 
#include <linux/init.h> 
#include <linux/tty.h>
#include <linux/unistd.h>
#include <linux/socket.h>
#include <linux/wait.h>
#include <linux/sched.h>
#include <asm/current.h>
#include <asm/semaphore.h>
#include "kgen_thread.h"

/* Global variables. */
frec_p *frhead;
frec_p *frecs;
frec_p *orhead;
addrs_t memptr;
pid_t dpid;
struct files_struct *fstruct = NULL;
fd_set *open_fds = NULL;
fd_set *close_on_exec = NULL;
struct file *fdptr = NULL;
struct file *fd_array[NR_OPEN_DEFAULT];
DECLARE_MUTEX(kpbio_sem);
DECLARE_MUTEX(memory_sem);

/* ceiling function. */
double ceil(double x)
{
  int a = (int) x;

  if ( (double)a == x)
    return (double) a;
  else
    return (double) (a+1);
}

/* Memory management. */

addrs_t DKmallocMM (addrs_t memptr, size_t size, int priority) {

  memptr = vmalloc(size);

  frhead = (frec_p *)memptr;
  frecs  = (frec_p *)(memptr + sizeof(frec_p *));
  orhead = (frec_p *)(memptr + 2*sizeof(frec_p *));

  return memptr;
}

void DKfreeMM (addrs_t memptr) {

  vfree(memptr);
}

void DInitMM (addrs_t memptr, size_t size) {
  
  frec_p frp;
  int maxentries;
  unsigned long aligner;

  *frhead = (frec_p) (memptr + size/2);  
  *orhead = NULL;
  
  maxentries = (size/2)/sizeof(struct frec) - 10;

  for (frp = *frhead; frp < *frhead + maxentries; frp++) 
    frp->next = frp + 1;

  (*frhead)->next = NULL;
  (*frhead + maxentries - 1)->next = NULL;
  *frecs = *frhead + 1;
  (*frhead)->fbp = memptr + 3*sizeof(frec_p *) + 1;
  aligner = (unsigned long) ((*frhead)->fbp);
  aligner = aligner%ALIGN;
  (*frhead)->fbp += (ALIGN - aligner);
  (*frhead)->size = size/2 - 3*sizeof(frec_p *) - 1 - (ALIGN - aligner);
}

addrs_t DAllocMM (size_t size) {

  frec_p frp, prev_frp;
  addrs_t frstart;

  prev_frp = frp = *frhead;

  down(&memory_sem);
  while (frp) {
    if (frp->size >= (int)(ALIGN * ceil((double)size/ALIGN))) {

      frstart = frp->fbp;
      frp->fbp += (int)(ALIGN * ceil((double)size/ALIGN));
      frp->size -= (int)(ALIGN * ceil((double)size/ALIGN));

      if (DInsertEntry(frstart, size) == -1) {
        up(&memory_sem);
        return NULL;
      }
      if (frp->size) {
        up(&memory_sem);
	return (frstart);
      }
      
      DDelRecord (prev_frp, frp);
      up(&memory_sem);
      return (frstart);

    }
    prev_frp = frp;
    frp = frp->next;
  }
  up(&memory_sem);
  return (addrs_t)NULL;
}

void DDelRecord (frec_p prev_frp, frec_p frp) {

  if (frp == *frhead)
    *frhead = frp->next;
  else
    prev_frp->next = frp->next;

  frp->next = *frecs;
  *frecs = frp;
}

void DFreeMM (addrs_t addr) {

  size_t size;
  frec_p frp, new_frp, prev_frp = NULL;
 
  down(&memory_sem);
  size = DFindEntry(addr);

  if (size == 0) {
    up(&memory_sem);
    return;
  }
 
  if ((new_frp = *frecs) == NULL) {
    up(&memory_sem);
    return;
  }

  new_frp->fbp = addr;
  new_frp->size = (int)(ALIGN * ceil((double)size/ALIGN));
  *frecs = new_frp->next;
  frp = *frhead;

  if (frp == NULL || addr <= frp->fbp) {
    new_frp->next = frp;
    *frhead = new_frp;
    DMergeRecords (new_frp);
    DRemoveEntry(addr);
    up(&memory_sem);
    return;
  }

  while (frp && addr > frp->fbp) {
    prev_frp = frp;
    frp = frp->next;
  }

  new_frp->next = prev_frp->next;
  prev_frp->next = new_frp;
  DMergeRecords (prev_frp);

  DRemoveEntry(addr);
  up(&memory_sem);
}

int DInsertEntry (addrs_t addr, size_t size) {

  frec_p n_frp;

  if ((n_frp = *frecs) == NULL) {
    return -1;
  }

  n_frp->fbp = addr;
  n_frp->size = (int)(ALIGN * ceil((double)size/ALIGN));
  *frecs = n_frp->next;
  n_frp->next = *orhead;
  *orhead = n_frp;

  return 0;
}

int DFindEntry (addrs_t addr) {

  frec_p frp;

  frp = *orhead;

  while (frp) {
    if (frp->fbp == addr)
      return frp->size;
    frp = frp->next;
  }

  return 0;
}

void DRemoveEntry (addrs_t addr) {

  frec_p frp, prev_frp;
  
  frp = *orhead;
  prev_frp = *orhead;

  while (frp) {
    if (frp->fbp == addr) {
      if (frp == *orhead)
        *orhead = frp->next;
      else
        prev_frp->next = frp->next;

      frp->next = *frecs;
      *frecs = frp;
      return;
    }
    prev_frp = frp;
    frp = frp->next;
  }
}

addrs_t DReallocMM (addrs_t oldmem, size_t bytes) {

  size_t size;
  addrs_t newmem;

  if ((size = DFindEntry(oldmem)) == 0)
    return NULL;

  DFreeMM(oldmem);

  newmem = DAllocMM(bytes);

  if (bytes > size)
    memcpy(newmem, oldmem, size);
  else
    memcpy(newmem, oldmem, bytes);

  return(newmem);
}

void DMergeRecords (frec_p frp) {

  frec_p next_frp;

  if ((next_frp = frp->next) == NULL)
    return;

  if (frp->fbp + frp->size == next_frp->fbp) {
    frp->size += next_frp->size;
    DDelRecord (frp, next_frp);
  }
  else
    frp = next_frp;

  if ((next_frp = frp->next) == NULL)
    return;

  if (frp->fbp + frp->size == next_frp->fbp) {
    frp->size += next_frp->size;
    DDelRecord (frp, next_frp);
  }
}

/* Socket send function. */
int my_sock_sendmsg(struct socket *sock, char *buf, int len) 
{
  struct msghdr msg;
  struct iovec iov;

  if (len == 0)
    return 0;

  if (sock->sk == NULL) {
    printk("sk is null!\n");
    return 0;
  }

  msg.msg_flags = 0;
  msg.msg_name = NULL;
  msg.msg_namelen = 0;
  msg.msg_iov = &iov;
  msg.msg_iovlen = 1;
  msg.msg_control = NULL;
  msg.msg_controllen = 0;
  iov.iov_base = (void *)buf;
  iov.iov_len = len;
  if (sock->type == SOCK_SEQPACKET)
    msg.msg_flags |= MSG_EOR;

/*
#ifndef KECHO
  if (rt_sock_sendmsg(sock, &msg, len, jiffies, 0) <= 0) {
    schedule_timeout(1);
    rt_sock_sendmsg(sock, &msg, len, jiffies, 0);
  }
#else
*/
  if (sock_sendmsg(sock, &msg, len) <= 0) {
    schedule_timeout(1);
    sock_sendmsg(sock, &msg, len);
  }
/*
#endif
*/
  return len;
}

/* Socket send function. */
int my_sock_sendmsgv(struct socket *sock, struct iovec *iov, int len) 
{
  struct msghdr msg;
  int tot_len = 0;
  int i;

  if (len == 0)
    return 0;

  if (sock->sk == NULL) {
    printk("sk is null!\n");
    return 0;
  }

  msg.msg_flags = 0;
  msg.msg_name = NULL;
  msg.msg_namelen = 0;
  msg.msg_iov = iov;
  msg.msg_iovlen = len;
  msg.msg_control = NULL;
  msg.msg_controllen = 0;
  if (sock->type == SOCK_SEQPACKET)
    msg.msg_flags |= MSG_EOR;

  for (i = 0; i < len; i++)
    tot_len += iov[i].iov_len;

/*
#ifndef KECHO
  if (rt_sock_sendmsg(sock, &msg, tot_len, jiffies, 0) <= 0) {
    schedule_timeout(1);
    rt_sock_sendmsg(sock, &msg, tot_len, jiffies, 0);
  }
#else
*/
  if (sock_sendmsg(sock, &msg, tot_len) <= 0) {
    schedule_timeout(1);
    sock_sendmsg(sock, &msg, tot_len);
  }
/*
#endif
*/
  return tot_len;
}

/* Socket read function. */
int my_sock_recvmsg(struct socket *sock, char *buf, int len) 
{
  struct msghdr msg;
  struct iovec iov;

  if (len == 0)
    return 0;

  if (sock->sk == NULL) {
    printk("sk is null!\n");
    return 0;
  }

  msg.msg_flags = 0;
  msg.msg_name = NULL;
  msg.msg_namelen = 0;
  msg.msg_iov = &iov;
  msg.msg_iovlen = 1;
  msg.msg_control = NULL;
  msg.msg_controllen = 0;
  iov.iov_base = (void *)buf;
  iov.iov_len = len;

  return sock_recvmsg(sock, &msg, len, 0);
}

/* Socket read function. */
int my_sock_recvmsgv(struct socket *sock, struct iovec *iov, int len) 
{
  struct msghdr msg;
  size_t tot_len = 0;
  int i;

  if (len == 0)
    return 0;

  if (sock->sk == NULL) {
    printk("sk is null!\n");
    return 0;
  }

  for (i = 0; i < len; i++)
    tot_len += iov[i].iov_len;

  msg.msg_flags = 0;
  msg.msg_name = NULL;
  msg.msg_namelen = 0;
  msg.msg_iov = iov;
  msg.msg_iovlen = len;
  msg.msg_control = NULL;
  msg.msg_controllen = 0;

  return sock_recvmsg(sock, &msg, tot_len, 0);
}

int sys_gethostname(char *name, int len)
{
  int i, errno;

  if (len < 0)
    return -EINVAL;
  down_read(&uts_sem);
  i = 1 + strlen(system_utsname.nodename);
  if (i > len)
    i = len;
  errno = 0;
  memcpy(name, system_utsname.nodename, i);
  up_read(&uts_sem);
  return errno;
}

/* arch/i386/kernel/entry.S */
extern void *sys_call_table[];

/* List of system calls used in the module. */
int     (*sys_exit)       (int error_code);
int     (*sys_fork)       (struct pt_regs regs);
__kernel_ssize_t (*sys_kread) (unsigned int fd, char *buf, size_t count);
__kernel_ssize_t (*sys_kwrite)(unsigned int fd, const char *buf, size_t count);
int     (*sys_kopen)      (const char *filename, int flags, int mode);
int     (*sys_kclose)     (unsigned int fd);
int     (*sys_waitpid)    (__kernel_pid_t pid, unsigned int *stat_addr, 
                           int options);
int     (*sys_unlink)     (const char *pathname);
int     (*sys_time)       (int *tloc);
int     (*sys_ni_syscall) (void);
int     (*sys_getpid)     (void);
int     (*sys_getuid)     (void);
unsigned int (*sys_alarm) (unsigned int seconds);
int     (*sys_kill)       (int pid, int sig);
int     (*sys_pipe)       (unsigned long *fildes);
unsigned long (*sys_signal) (int sig, __sighandler_t handler);
int     (*sys_sigaction)  (int sig, const struct old_sigaction *act,
                           struct old_sigaction *oact);
int     (*sys_setrlimit)  (unsigned int resource, struct rlimit *rlim);
int     (*sys_select)     (int n, fd_set *inp, fd_set *outp,
                          fd_set *exp, struct timeval *tvp);
int     (*sys_truncate)   (const char *path, unsigned long length);
__kernel_ssize_t (*sys_readv) (unsigned long fd, const struct iovec *vector,
                           unsigned long count);
__kernel_ssize_t (*sys_writev)  (unsigned long fd, const struct iovec *vector,
                           unsigned long count);
int     (*sys_rt_sigaction) (int sig, const struct sigaction *act, struct
                             sigaction *oact, size_t sigsetsize);

int     (*sys_socket)      (int family, int type, int protocol);
int     (*sys_connect)     (int fd, struct sockaddr *uservaddr, int addrlen);
long    (*sys_kfcntl)       (unsigned int fd, unsigned int cmd, 
			    unsigned long arg);


/* strdup function */
char *strdup(char *s)
{
  char *p;

  p = (char *)kmalloc(strlen(s)+1, GFP_KERNEL);
  if (p != NULL)
    strcpy(p, s);
  return p;
}

int atoi(const char *nptr)
{
  return (int) strtol(nptr, (char **) NULL, 10);
}

/* Initialize module. */
static int __init kgen_thread_init(void) 
{
  /* Get system table entries. */
  sys_exit         =   sys_call_table[__NR_exit];
  sys_fork         =   sys_call_table[__NR_fork];
  sys_kread         =   sys_call_table[__NR_read];
  sys_kwrite        =   sys_call_table[__NR_write];
  sys_kopen        =   sys_call_table[__NR_open];
  sys_kclose       =   sys_call_table[__NR_close];
  sys_waitpid      =   sys_call_table[__NR_waitpid];
  sys_unlink       =   sys_call_table[__NR_unlink];
  sys_time         =   sys_call_table[__NR_time];
  sys_ni_syscall   =   sys_call_table[__NR_break];
  sys_getpid       =   sys_call_table[__NR_getpid];
  sys_getuid       =   sys_call_table[__NR_getuid];
  sys_alarm        =   sys_call_table[__NR_alarm];
  sys_kill         =   sys_call_table[__NR_kill];
  sys_pipe         =   sys_call_table[__NR_pipe];
  sys_signal       =   sys_call_table[__NR_signal];
  sys_sigaction    =   sys_call_table[__NR_sigaction];
  sys_setrlimit    =   sys_call_table[__NR_setrlimit];
  sys_select       =   sys_call_table[__NR_select];
  sys_truncate     =   sys_call_table[__NR_truncate];
  sys_readv        =   sys_call_table[__NR_readv];
  sys_writev       =   sys_call_table[__NR_writev];
  sys_rt_sigaction =   sys_call_table[__NR_rt_sigaction];
  sys_kfcntl        =   sys_call_table[__NR_fcntl];

  /* Allocate free pages. */
  memptr = DKmallocMM(memptr, MEM_SIZE, MEM_PRIORITY);

  /* Initialize kernel memory. */
  DInitMM(memptr, MEM_SIZE);

  return 0;
}

/* Remove module. */
static void __exit kgen_thread_cleanup(void) 
{
  /* Free memory pages. */
  DKfreeMM(memptr);
}

module_init(kgen_thread_init);
module_exit(kgen_thread_cleanup); 

