/*
 *  header file to encapsulate build-for-kernel modifications
 */

#ifndef __KERNEL__
#define __KERNEL__
#endif

#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/fcntl.h>
#include <linux/slab.h>
#include <linux/errno.h>
#include <linux/uio.h>
#include <linux/unistd.h>
#include <linux/ctype.h>
#include <linux/limits.h>
#include <linux/fs.h>
#include <linux/string.h>
#include <asm/signal.h>
#define printf printk
#define abort() sys_exit(-1)
#define perror         printk
#define fprintf(fmt, args...) printk (args) 
#define malloc         (void *)DAllocMM
#define free(a)        DFreeMM((addrs_t)a)
#define realloc(a,b)   (void *)DReallocMM((addrs_t)a,b)
#define exit           sys_exit
#define qsort          _quicksort
#define getpid()       current->pid
