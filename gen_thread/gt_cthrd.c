#include "gen_thread.h"
#include <cthread.h>

extern thr_thread_t 
cthread_fork_wrapper(func, arg)
void_arg_func func;
void *arg;
{
    cthread_t thr;
    thr = cthread_fork((any_t (*)())func, arg, N_ANYWHERE);
    return (thr_thread_t) thr;
}

extern void 
cthread_detach_wrapper(thread)
thr_thread_t thread;
{
    cthread_detach((cthread_t)thread);
}

extern thr_mutex_t 
cthread_mutex_alloc_wrapper()
{
    RESULT result;
    mutex_t m;
    result = mutex_alloc(&m, N_ANYWHERE);
    if (result != T_SUCCEED) {
	return NULL;
    } else {
	return (thr_mutex_t) m;
    }
}

extern void 
cthread_mutex_free_wrapper(m)
thr_mutex_t m;
{
    mutex_free((mutex_t)m);
}

extern void 
cthread_mutex_lock_wrapper(m)
thr_mutex_t m;
{
    mutex_lock((mutex_t)m);
}

extern void 
cthread_mutex_unlock_wrapper(m)
thr_mutex_t m;
{
    mutex_unlock((mutex_t)m);
}
extern thr_condition_t 
cthread_condition_alloc_wrapper()
{
    RESULT result;
    condition_t c;
    result = condition_alloc(&c, N_ANYWHERE);
    if (result != T_SUCCEED) {
	return NULL;
    } else {
	return (thr_condition_t) c;
    }
}


extern void 
cthread_condition_free_wrapper(c)
thr_condition_t c;
{
    condition_free((condition_t)c);
}

extern void 
cthread_condition_wait_wrapper(c, m)
thr_condition_t c;
thr_mutex_t m;
{
    condition_wait(c, m);
}

extern void 
cthread_condition_broadcast_wrapper(c)
thr_condition_t c;
{
    condition_broadcast(c);
}

extern void 
cthread_condition_signal_wrapper(c)
thr_condition_t c;
{
    condition_signal(c);
}

extern void 
cthread_yield_wrapper()
{
    cthread_yield();
}


extern void
cthread_exit_wrapper(status)
void *status;
{
    cthread_exit((any_t)status);
}

extern int
cthread_join_wrapper(target, status_p)
thr_thread_t target;
void **status_p;
{
    return cthread_join((cthread_t)target, (any_t *) status_p);
}

extern thr_thread_t
cthread_self_wrapper()
{
    return (thr_thread_t) cthread_self();
}

extern void
gen_cthread_init()
{
    gen_thread_info_t *gen_thread_info = libgenthreads_init3();
    gen_thread_info->start_func = (thr_start_func_t) cthread_start;
    gen_thread_info->fork_func = cthread_fork_wrapper;
    gen_thread_info->detach_func = cthread_detach_wrapper;
    gen_thread_info->yield_func = cthread_yield_wrapper;
    gen_thread_info->mutex_alloc_func = cthread_mutex_alloc_wrapper;
    gen_thread_info->mutex_free_func = cthread_mutex_free_wrapper;
    gen_thread_info->mutex_lock_func = cthread_mutex_lock_wrapper;
    gen_thread_info->mutex_unlock_func = cthread_mutex_unlock_wrapper;
    gen_thread_info->condition_alloc_func = cthread_condition_alloc_wrapper;
    gen_thread_info->condition_free_func = cthread_condition_free_wrapper;
    gen_thread_info->condition_wait_func = cthread_condition_wait_wrapper;
    gen_thread_info->condition_signal_func = cthread_condition_signal_wrapper;
    gen_thread_info->condition_broadcast_func = cthread_condition_broadcast_wrapper;
    gen_thread_info->exit_func = cthread_exit_wrapper;
    gen_thread_info->join_func = cthread_join_wrapper;
    gen_thread_info->self_func = cthread_self_wrapper;
    gen_thread_info->null_thread_init = 0;
    gen_thread_info->is_kernel = 0;
}
