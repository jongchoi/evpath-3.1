#include "gen_thread.h"

extern int
gen_thr_initialized()
{
    return gen_thread_info.fork_func != (thr_fork_func_t) NULL;
}

extern void
thr_thread_start(procs)
int procs;
{
    if (gen_thread_info.start_func) {
	gen_thread_info.start_func(procs);
    }
}

extern thr_thread_t 
thr_fork(func, arg, node)
void_arg_func func;
void *arg;
int node;
{
    if (gen_thread_info.fork_func) {
	return gen_thread_info.fork_func(func, arg, node);
    } else {
	return NULL;
    }
}

extern void 
thr_thread_detach(thread)
thr_thread_t thread;
{
    if (gen_thread_info.detach_func) {
	gen_thread_info.detach_func(thread);
    }
}


extern thr_mutex_t 
thr_mutex_alloc()
{
    if (gen_thread_info.mutex_alloc_func) {
	return gen_thread_info.mutex_alloc_func();
    } else {
	return NULL;
    }
}

extern void 
thr_mutex_free(m)
thr_mutex_t m;
{
    if (gen_thread_info.mutex_free_func) {
	gen_thread_info.mutex_free_func(m);
    }
}

extern void 
thr_mutex_lock(m)
thr_mutex_t m;
{
    if (gen_thread_info.mutex_lock_func) {
	gen_thread_info.mutex_lock_func(m);
    }
}

extern void 
thr_mutex_unlock(m)
thr_mutex_t m;
{
    if (gen_thread_info.mutex_unlock_func) {
	gen_thread_info.mutex_unlock_func(m);
    }
}

extern thr_condition_t 
thr_condition_alloc()
{
    if (gen_thread_info.condition_alloc_func) {
	return gen_thread_info.condition_alloc_func();
    } else {
	return NULL;
    }
}

extern void 
thr_condition_free(m)
thr_condition_t m;
{
    if (gen_thread_info.condition_free_func) {
	gen_thread_info.condition_free_func(m);
    }
}


extern void 
thr_condition_wait(c, m)
thr_condition_t c;
thr_mutex_t m;
{
    if (gen_thread_info.condition_wait_func) {
	gen_thread_info.condition_wait_func(c, m);
    }
}

extern void
thr_condition_signal(c)
thr_condition_t c;
{
    if (gen_thread_info.condition_signal_func) {
	gen_thread_info.condition_signal_func(c);
    }
}

extern void 
thr_condition_broadcast(c)
thr_condition_t c;
{
    if (gen_thread_info.condition_broadcast_func) {
	gen_thread_info.condition_broadcast_func(c);
    }
}

