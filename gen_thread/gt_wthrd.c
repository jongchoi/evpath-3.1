#include "gen_thread.h"
#include <windows.h>
#include <winsock.h>
#include <sys/timeb.h>
#include <process.h>
#include <winbase.h>
#include <stdio.h>
#include <stdlib.h>

static thr_thread_t 
ntthread_fork_wrapper(func, arg, node)
void_arg_func func;
void *arg;
int node;
{
    unsigned t_id;
    HANDLE  thr;

    thr = (HANDLE) _beginthreadex( NULL, 0, (unsigned (_stdcall *)
					     (void*)) func, arg, 0, &t_id);
    if(thr == NULL){
	fprintf(stderr, "Error in creating thread\n");
	return NULL;
    }
    return (thr_thread_t) thr;
}

static void 
ntthread_detach_wrapper(thread)
thr_thread_t thread;
{
    CloseHandle((HANDLE) thread);
}

static thr_mutex_t 
ntthread_mutex_alloc_wrapper()
{
    HANDLE m_mutex;
    
    m_mutex = CreateMutex(NULL, FALSE, NULL);
    if (m_mutex == NULL) {
	fprintf(stderr, "Error in creating Mutex\n");
	return NULL;
    } else {
	return (thr_mutex_t) m_mutex;
    }
}

static void 
ntthread_mutex_free_wrapper(m)
thr_mutex_t m;
{
    CloseHandle((HANDLE) m);
}

static void 
ntthread_mutex_lock_wrapper(m)
thr_mutex_t m;
{
    DWORD status;

    status = WaitForSingleObject((HANDLE) m, 2000);
    if(status != WAIT_OBJECT_0){
	fprintf(stderr, "Failed in acquiring the mutex\n");
    }
}

static void 
ntthread_mutex_unlock_wrapper(m)
thr_mutex_t m;
{
    BOOL status;
    DWORD ErrorCode;

    status = ReleaseMutex((HANDLE) m);
    if(status == 0){
	ErrorCode = GetLastError();
	fprintf(stderr, "Failed to release  the mutex ErrorCode: %d\n",
		ErrorCode);
    }
}

static thr_condition_t 
ntthread_condition_alloc_wrapper()
{
    HANDLE  cond_event;

    cond_event = CreateEvent(NULL, FALSE, FALSE, NULL);
    if (cond_event == NULL) {
	fprintf(stderr, "Error in creating Event\n");
	return NULL;
    } else {
	return (thr_condition_t) cond_event;
    }
}


static void 
ntthread_condition_free_wrapper(c)
thr_condition_t c;
{
    CloseHandle((HANDLE) c);
}

static void 
ntthread_condition_wait_wrapper(c, m)
thr_condition_t c;
thr_mutex_t m;
{
    int  status;

    ntthread_mutex_unlock_wrapper((HANDLE) m);
    status = WaitForSingleObject((HANDLE) c, 120000);
    if(status != WAIT_OBJECT_0){
	fprintf(stderr, " Failed in signaling condition \n");
    }
    ntthread_mutex_lock_wrapper((HANDLE) m);
    return;
}

static void 
ntthread_condition_broadcast_wrapper(c)
thr_condition_t c;
{
    BOOL  status;

    /* DANGER!  THIS IS NOT A BROADCAST! */
    status = PulseEvent((HANDLE) c);
}

static void 
ntthread_condition_signal_wrapper(c)
thr_condition_t c;
{
    BOOL  status;

    status = PulseEvent((HANDLE) c);
    if(status == 0){
	fprintf(stderr, "Failed to signal the event \n");
    }		
}



static void 
ntthread_yield_wrapper()
{
    Sleep(0);
}

static thr_thread_t ntthread_self_wrapper();

static void
ntthread_exit_wrapper(status)
void *status;
{
    /* Needs the Thread Handle to utilize the status parameter */
    thr_thread_t self = (thr_thread_t) ntthread_self_wrapper();

    _endthreadex(0);
}

static int
ntthread_join_wrapper(target, status_p)
thr_thread_t target;
void **status_p;
{
    return (int) WaitForMultipleObjects(1, (CONST HANDLE) target, TRUE,
					INFINITE);
}

static thr_thread_t
ntthread_self_wrapper()
{
    return (thr_thread_t) GetCurrentThreadId();
}

extern void
gen_win32thread_init()
{
    gen_thread_info_t *gen_thread_info = libgenthreads_init3();
    gen_thread_info->start_func = NULL;
    gen_thread_info->fork_func = ntthread_fork_wrapper;
    gen_thread_info->detach_func = ntthread_detach_wrapper;
    gen_thread_info->yield_func = ntthread_yield_wrapper;
    gen_thread_info->mutex_alloc_func = ntthread_mutex_alloc_wrapper;
    gen_thread_info->mutex_free_func = ntthread_mutex_free_wrapper;
    gen_thread_info->mutex_lock_func = ntthread_mutex_lock_wrapper;
    gen_thread_info->mutex_unlock_func = ntthread_mutex_unlock_wrapper;
    gen_thread_info->condition_alloc_func = ntthread_condition_alloc_wrapper;
    gen_thread_info->condition_free_func = ntthread_condition_free_wrapper;
    gen_thread_info->condition_wait_func = ntthread_condition_wait_wrapper;
    gen_thread_info->condition_signal_func =
	ntthread_condition_signal_wrapper;
    gen_thread_info->condition_broadcast_func =
	ntthread_condition_broadcast_wrapper;
    gen_thread_info->exit_func = ntthread_exit_wrapper;
    gen_thread_info->join_func = ntthread_join_wrapper;
    gen_thread_info->self_func = (thr_self_func_t) ntthread_self_wrapper;
    gen_thread_info->null_thread_init = 0;
    gen_thread_info->is_kernel = 1;
}
