#include "config.h"
#include "gen_thread.h"
#ifdef HAVE_MALLOC_H
#include <malloc.h>
#endif
#include <memory.h>
#include <sys/types.h>
#include <stdlib.h>
#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif
#ifdef HAVE_WINDOWS_H
#include <windows.h>
#include <winsock.h>
#endif
#include <setjmp.h>
#ifdef HAVE_SYS_SELECT_H
#include <sys/select.h>
#endif

static jmp_buf jmp;
static void **cur_thread_result = NULL;

extern thr_thread_t 
null_fork_wrapper(func, arg)
void_arg_func func;
void *arg;
{
    void **old_thread_result = cur_thread_result;
    void **thread_result = malloc(sizeof(void*));

    cur_thread_result = thread_result;

    if (setjmp(jmp) == 0) {
	/* normal case after state save */
	*thread_result = (void *) (long)func(arg);
    } else {
	/* return from setjmp via longjmp in thread_exit */
    }
    cur_thread_result = old_thread_result;
    return (thr_thread_t)thread_result;
}

extern void 
null_detach_wrapper(thread)
thr_thread_t thread;
{
    free((void**)thread);
    return;
}

extern thr_mutex_t 
null_mutex_alloc_wrapper()
{
    thr_mutex_t tmp = malloc(sizeof(int));
    *((int *)tmp) = 0;
    return tmp;
}

extern void 
null_mutex_free_wrapper(m)
thr_mutex_t m;
{
    free(m);
}

extern void 
null_mutex_lock_wrapper(m)
thr_mutex_t m;
{
    *((int *)m) = 1;
}

extern void 
null_mutex_unlock_wrapper(m)
thr_mutex_t m;
{
    *((int *)m) = 0;
}

extern thr_condition_t 
null_condition_alloc_wrapper()
{
    thr_condition_t c = (thr_condition_t *)malloc(sizeof(int));
    *((int *)c) = 0;
    return c;
}


extern void 
null_condition_free_wrapper(c)
thr_condition_t c;
{
    free(c);
}

extern void 
null_condition_wait_wrapper(c, m)
thr_condition_t c;
thr_mutex_t m;
{
    return;
}

extern void 
null_condition_broadcast_wrapper(c)
thr_condition_t c;
{
    return;
}

extern void 
null_condition_signal_wrapper(c)
thr_condition_t c;
{
    return;
}

extern void 
null_yield_wrapper()
{
    struct timeval timeout;
    timeout.tv_sec = 0;
    timeout.tv_usec = 0;
    select(32, NULL, NULL, NULL, &timeout);
}


extern void
null_exit_wrapper(status)
void *status;
{
    *cur_thread_result = status;
    /* return to dummy fork point */
    longjmp(jmp, 1);
}

extern int
null_join_wrapper(target, status_p)
thr_thread_t target;
void **status_p;
{
    if (status_p) {
	*status_p = *(void**) target;
    }
    free(target);
    return 1;
}

extern thr_thread_t
null_self_wrapper()
{
    return (thr_thread_t) cur_thread_result;
}

extern void
gen_null_init()
{
#ifdef OLD_LINUX_BUG
    gen_thread_info_t *gen_thread_info = libgenthreads_init3();
#else
    gen_thread_info_t *gen_thread_info;
    libgenthreads_init2(&gen_thread_info);
#endif
    gen_thread_info->start_func = (thr_start_func_t) NULL;
    gen_thread_info->fork_func = null_fork_wrapper;
    gen_thread_info->detach_func = null_detach_wrapper;
    gen_thread_info->yield_func = null_yield_wrapper;
    gen_thread_info->mutex_alloc_func = null_mutex_alloc_wrapper;
    gen_thread_info->mutex_free_func = null_mutex_free_wrapper;
    gen_thread_info->mutex_lock_func = null_mutex_lock_wrapper;
    gen_thread_info->mutex_unlock_func = null_mutex_unlock_wrapper;
    gen_thread_info->condition_alloc_func = null_condition_alloc_wrapper;
    gen_thread_info->condition_free_func = null_condition_free_wrapper;
    gen_thread_info->condition_wait_func = null_condition_wait_wrapper;
    gen_thread_info->condition_signal_func = null_condition_signal_wrapper;
    gen_thread_info->condition_broadcast_func = null_condition_broadcast_wrapper;
    gen_thread_info->exit_func = null_exit_wrapper;
    gen_thread_info->join_func = null_join_wrapper;
    gen_thread_info->self_func = null_self_wrapper;
    gen_thread_info->null_thread_init = 1;
    gen_thread_info->is_kernel = 0;
}
