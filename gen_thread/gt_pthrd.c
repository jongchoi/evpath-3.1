#include "config.h"
#include "gen_thread.h"
#include <pthread.h>
#include <stdlib.h>
#ifdef HAVE_MALLOC_H
#include <malloc.h>
#endif
#include <sys/time.h>
#include <memory.h>
#ifdef HAVE_SYS_SELECT_H
#include <sys/select.h>
#endif

extern thr_thread_t 
pthread_fork_wrapper(func, arg)
void_arg_func func;
void *arg;
{
    pthread_t new_thread = 0;
    int err = pthread_create(&new_thread, NULL, (void*(*)(void*))func, arg);
    if (err != 0) {
	return NULL;
    } else {
	return (thr_thread_t) new_thread;
    }
}

extern void 
pthread_detach_wrapper(thread)
thr_thread_t thread;
{
    pthread_detach(((pthread_t)(long)thread));
}

typedef struct pmutex_t {
    pthread_mutex_t m;
    int locked;
} pthr_mutex_t;

extern thr_mutex_t 
pthread_mutex_alloc_wrapper()
{
    pthr_mutex_t * m = (pthr_mutex_t*)malloc(sizeof(pthr_mutex_t));
    if (m != NULL) {
	m->locked = 0;
	memset(m, 0, sizeof(*m));
	pthread_mutex_init(&(m->m), NULL);
    }
    return (thr_mutex_t) m;
}

extern void 
pthread_mutex_free_wrapper(m)
thr_mutex_t m;
{
    pthread_mutex_destroy(&((pthr_mutex_t *)m)->m);
    free(m);
}

extern void 
pthread_mutex_lock_wrapper(m)
thr_mutex_t m;
{
    pthread_mutex_lock(&((pthr_mutex_t *)m)->m);
#ifdef PDEBUG
    if(((pthr_mutex_t *)m)->locked != 0) {
	printf("mutex %lx already locked\n", (long)m);
    }
#endif
    ((pthr_mutex_t *)m)->locked++;
#ifdef PDEBUG
    if(((pthr_mutex_t *)m)->locked > 1) {
	printf("mutex %lx duplicate lock\n", (long)m);
    }
#endif
}

extern void 
pthread_mutex_unlock_wrapper(m)
thr_mutex_t m;
{
#ifdef PDEBUG
    if (((pthr_mutex_t *)m)->locked != 1) {
	printf("mutex %lx already unlocked\n", (long)m);
    }
#endif
    ((pthr_mutex_t *)m)->locked = 0;
    pthread_mutex_unlock(&((pthr_mutex_t *)m)->m);
}

extern thr_condition_t 
pthread_condition_alloc_wrapper()
{
    pthread_cond_t * c = (pthread_cond_t *)malloc(sizeof(pthread_cond_t));
    if (c != NULL) {
	pthread_cond_init(c, NULL);
    }
    return (thr_condition_t) c;
}


extern void 
pthread_condition_free_wrapper(c)
thr_condition_t c;
{
    pthread_cond_destroy((pthread_cond_t *)c);
    /*    free(c);*/
}

extern void 
pthread_condition_wait_wrapper(c, m)
thr_condition_t c;
thr_mutex_t m;
{
    ((pthr_mutex_t *)m)->locked = 0;
    pthread_cond_wait((pthread_cond_t*)c, (pthread_mutex_t*)m);
    ((pthr_mutex_t *)m)->locked = 1;
}

extern void 
pthread_condition_broadcast_wrapper(c)
thr_condition_t c;
{
    pthread_cond_broadcast((pthread_cond_t*)c);
}

extern void 
pthread_condition_signal_wrapper(c)
thr_condition_t c;
{
    pthread_cond_signal((pthread_cond_t*)c);
}

extern void 
pthread_yield_wrapper()
{
    struct timeval delay_time;
    delay_time.tv_sec = 0;
    delay_time.tv_usec = 10000;	/* return millisecs */
    select(32, NULL, NULL, NULL, &delay_time);
}

extern void
pthread_exit_wrapper(status)
void *status;
{
    pthread_exit(status);
}

extern int
pthread_join_wrapper(target, status_p)
thr_thread_t target;
void **status_p;
{
    pthread_t thread = (pthread_t)(long)target;
    void *ret;
    int result;
    result = pthread_join(thread, &ret);
    if (result == 0) *status_p = ret;
    return result;
}

extern thr_thread_t
pthread_self_wrapper()
{
    return (thr_thread_t) pthread_self();
}

extern void
gen_pthread_init()
{
#ifdef OLD_LINUX_BUG
    gen_thread_info_t *gen_thread_info = libgenthreads_init3();
#else
    gen_thread_info_t *gen_thread_info;
    libgenthreads_init2(&gen_thread_info);
#endif
    gen_thread_info->start_func = (thr_start_func_t) NULL;
    gen_thread_info->fork_func = pthread_fork_wrapper;
    gen_thread_info->detach_func = pthread_detach_wrapper;
    gen_thread_info->yield_func = pthread_yield_wrapper;
    gen_thread_info->mutex_alloc_func = pthread_mutex_alloc_wrapper;
    gen_thread_info->mutex_free_func = pthread_mutex_free_wrapper;
    gen_thread_info->mutex_lock_func = pthread_mutex_lock_wrapper;
    gen_thread_info->mutex_unlock_func = pthread_mutex_unlock_wrapper;
    gen_thread_info->condition_alloc_func = pthread_condition_alloc_wrapper;
    gen_thread_info->condition_free_func = pthread_condition_free_wrapper;
    gen_thread_info->condition_wait_func = pthread_condition_wait_wrapper;
    gen_thread_info->condition_signal_func = pthread_condition_signal_wrapper;
    gen_thread_info->condition_broadcast_func = pthread_condition_broadcast_wrapper;
    gen_thread_info->exit_func = pthread_exit_wrapper;
    gen_thread_info->join_func = pthread_join_wrapper;
    gen_thread_info->self_func = pthread_self_wrapper;
    gen_thread_info->null_thread_init = 0;
    gen_thread_info->is_kernel = 1;
}
