/*

extern int    gen_thr_initialized();
extern thr_thread_t thr_fork(void_arg_func func, void *arg);
extern void   thr_thread_detach(thr_thread_t thread);
extern void   thr_thread_yield();
extern thr_mutex_t thr_mutex_alloc();
extern void   thr_mutex_free(thr_mutex_t m);
extern void   thr_mutex_lock(thr_mutex_t m);
extern void   thr_mutex_unlock(thr_mutex_t m);
extern thr_condition_t thr_condition_alloc();
extern void   thr_condition_free(thr_condition_t m);
extern void   thr_condition_wait(thr_condition_t c, thr_mutex_t m);
extern void   thr_condition_signal(thr_condition_t c);
extern void   thr_condition_broadcast(thr_condition_t c);
extern void   thr_thread_exit(void *status);
extern int    thr_thread_join(thr_thread_t t, void **status_p);
extern thr_thread_t thr_thread_self();

*/

#ifndef GEN_THREAD_H
#define GEN_THREAD_H 1

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

#if defined(FUNCPROTO) || defined(__STDC__) || defined(__cplusplus) || defined(c_plusplus)
#ifndef	NULL
#define NULL	((void *) 0)
#endif
#else
#ifndef	NULL
#define NULL	0
#endif
#endif

#if defined(_MSC_VER) || defined(WINNT) || defined(__WIN32__)
/* on windows, do DLL stuff */
#ifndef DECLSPEC 
#define DECLSPEC __declspec( dllimport )
#endif
#else
#define DECLSPEC
#endif

typedef void * thr_mutex_t;

typedef void * thr_condition_t;

typedef void * thr_thread_t;

#define gen_thr_initialized() (libgenthreads_init3()->fork_func != (thr_fork_func_t)NULL)

#define gen_null_initialized() (libgenthreads_init3()->null_thread_init)

#define gen_thr_is_kernel() (libgenthreads_init3()->is_kernel)

#define thr_thread_start() do { if (libgenthreads_init3()->start_func) { libgenthreads_init3()->start_func(); } } while (0)

typedef int (*void_arg_func) (void *);

#define thr_fork(func, arg) ((libgenthreads_init3()->fork_func) ? libgenthreads_init3()->fork_func(func, arg) : NULL)

#define thr_thread_detach(thread) do {if (libgenthreads_init3()->detach_func) {	libgenthreads_init3()->detach_func(thread); } } while (0)


#define thr_thread_yield() do { if (libgenthreads_init3()->yield_func) { libgenthreads_init3()->yield_func(); } } while (0)

#define thr_mutex_alloc() ((libgenthreads_init3()->mutex_alloc_func) ? libgenthreads_init3()->mutex_alloc_func() : NULL)

#define thr_mutex_free(m) do { if (libgenthreads_init3()->mutex_free_func) { libgenthreads_init3()->mutex_free_func(m); } } while (0)

#define thr_mutex_lock(m) do { 			\
    if (libgenthreads_init3()->mutex_lock_func) {	\
	libgenthreads_init3()->mutex_lock_func(m);	\
    }						\
} while (0)

#define thr_mutex_unlock(m) do { 		\
    if (libgenthreads_init3()->mutex_unlock_func) {	\
	libgenthreads_init3()->mutex_unlock_func(m);	\
    }						\
} while (0)

#define thr_condition_alloc() ((libgenthreads_init3()->condition_alloc_func) ? libgenthreads_init3()->condition_alloc_func() : NULL)

#define thr_condition_free(m) do {	 	\
    if (libgenthreads_init3()->condition_free_func) {	\
	libgenthreads_init3()->condition_free_func(m);	\
    }						\
} while (0)

#define thr_condition_wait(c, m) do { 			\
    if (libgenthreads_init3()->condition_wait_func) {		\
	libgenthreads_init3()->condition_wait_func(c, m);	\
    }							\
} while (0)

#define thr_condition_signal(c) do { 			\
    if (libgenthreads_init3()->condition_signal_func) {	\
	libgenthreads_init3()->condition_signal_func(c);	\
    }							\
} while (0)

#define thr_condition_broadcast(c) do {			\
    if (libgenthreads_init3()->condition_broadcast_func) {	\
	libgenthreads_init3()->condition_broadcast_func(c);	\
    }							\
} while (0)

#define thr_thread_exit(status) do { 			\
    if (libgenthreads_init3()->exit_func) {			\
	libgenthreads_init3()->exit_func(status);		\
    }							\
} while (0)

#define thr_thread_join(target, status_p) do { 		\
    if (libgenthreads_init3()->join_func) {			\
	libgenthreads_init3()->join_func(target, status_p);	\
    }							\
} while (0)

#define thr_thread_self() (((libgenthreads_init3()->self_func != (thr_self_func_t)0)) ? \
	libgenthreads_init3()->self_func() : 0)

typedef void (*thr_start_func_t) ();

typedef thr_thread_t (*thr_fork_func_t) (void_arg_func, void *);

typedef void (*thr_detach_func_t) (thr_thread_t);

typedef void (*thr_yield_func_t)();

typedef thr_mutex_t (*thr_mutex_alloc_func_t) (void);

typedef void (*thr_mutex_func_t) (thr_mutex_t);

typedef thr_condition_t (*thr_condition_alloc_func_t) (void);

typedef void (*thr_condition_wait_func_t) (thr_condition_t, thr_mutex_t);

typedef void (*thr_condition_func_t) (thr_condition_t);

typedef void (*thr_exit_func_t)(void *status);

typedef int (*thr_join_func_t)(thr_thread_t, void **status);

typedef thr_thread_t (*thr_self_func_t)();

typedef struct gen_thread_info_t {
    thr_start_func_t start_func;
    thr_fork_func_t fork_func;
    thr_detach_func_t detach_func;
    thr_yield_func_t yield_func;
    thr_mutex_alloc_func_t mutex_alloc_func;
    thr_mutex_func_t mutex_free_func;
    thr_mutex_func_t mutex_lock_func;
    thr_mutex_func_t mutex_unlock_func;
    thr_condition_alloc_func_t condition_alloc_func;
    thr_condition_func_t condition_free_func;
    thr_condition_wait_func_t condition_wait_func;
    thr_condition_func_t condition_signal_func;
    thr_condition_func_t condition_broadcast_func;
    thr_exit_func_t exit_func;
    thr_join_func_t join_func;
    thr_self_func_t self_func;
    int null_thread_init;
    int is_kernel;
    int env_init;
}gen_thread_info_t;

extern int libgenthreads_init();
extern int libgenthreads_init2(gen_thread_info_t **gen_info_addr);
extern gen_thread_info_t *libgenthreads_init3();

extern void gen_cthread_init();

extern void gen_pthread_init();

extern void gen_null_init();

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif
#endif
