/* config.h.in.  Generated from configure.in by autoheader.  */

/* Define to 1 if you have the <malloc.h> header file. */
#cmakedefine HAVE_MALLOC_H

/* Define to 1 if you have the <sys/time.h> header file. */
#cmakedefine HAVE_SYS_TIME_H

/* Define to 1 if you have the <unistd.h> header file. */
#cmakedefine HAVE_UNISTD_H

/* Define to 1 if you have the <windows.h> header file. */
#cmakedefine HAVE_WINDOWS_H

/* Define if we're compiling a linux kernel module */
#cmakedefine MODULE

/* Define to 1 if you have the ANSI C header files. */
#cmakedefine STDC_HEADERS

