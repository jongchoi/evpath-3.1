#include "config.h"
#include <asm/uaccess.h>
#include <linux/in.h>
#include <asm/semaphore.h>
#include "gen_thread.h"

#ifdef MODULE
#include "kernel/kgen_thread.h"
#include "kernel/gen_thread_kernel.h"
#endif

#define MAX 80
#define MEM_SIZE (unsigned) (1<<21)
#define MEM_PRIORITY GFP_KERNEL

#define ALIGN 8
#ifndef Void_t
#define Void_t void
#endif
#define INTERNAL_SIZE_T size_t
#define SIZE_SZ (sizeof(INTERNAL_SIZE_T))
#define PREV_INUSE 0x1
#define IS_MMAPPED 0x2
#define SIZE_BITS (PREV_INUSE|IS_MMAPPED)

#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE 1
#endif

typedef struct lkmutex_t {
    struct semaphore m;
    int locked;
} lkthr_mutex_t;

extern thr_mutex_t 
lkthread_mutex_alloc_wrapper()
{
    lkthr_mutex_t * m = (lkthr_mutex_t*)malloc(sizeof(struct semaphore));
    if (m != NULL) {
	memset(&m->m, 0, sizeof(m->m));
	sema_init(&(m->m), 1);
    }
    m->locked = 0;
    return (thr_mutex_t) m;
}

extern void 
lkthread_mutex_free_wrapper(m)
thr_mutex_t m;
{
    free(m);
}

extern void 
lkthread_mutex_lock_wrapper(m)
thr_mutex_t m;
{
    down(&((lkthr_mutex_t *)m)->m);
#ifdef PDEBUG
    if(((lkthr_mutex_t *)m)->locked != 0) {
	printf("mutex %lx already locked\n", (long)m);
    }
#endif
    ((lkthr_mutex_t *)m)->locked++;
#ifdef PDEBUG
    if(((lkthr_mutex_t *)m)->locked > 1) {
	printf("mutex %lx duplicate lock\n", (long)m);
    }
#endif
}

extern void 
lkthread_mutex_unlock_wrapper(m)
thr_mutex_t m;
{
#ifdef PDEBUG
    if (((struct semaphore *)m)->locked != 1) {
	printf("mutex %lx already unlocked\n", (long)m);
    }
#endif
    ((lkthr_mutex_t *)m)->locked = 0;
    up(&((lkthr_mutex_t *)m)->m);
}

/*
extern thr_condition_t 
lkthread_condition_alloc_wrapper()
{
    lkthread_cond_t * c = (lkthread_cond_t *)malloc(sizeof(lkthread_cond_t));
    if (c != NULL) {
	lkthread_cond_init(c, NULL);
    }
    return (thr_condition_t) c;
}


extern void 
lkthread_condition_free_wrapper(c)
thr_condition_t c;
{
    lkthread_cond_destroy((lkthread_cond_t *)c);
    free(c);
}

extern void 
lkthread_condition_wait_wrapper(c, m)
thr_condition_t c;
thr_mutex_t m;
{
    ((struct semaphore *)m)->locked = 0;
    lkthread_cond_wait((lkthread_cond_t*)c, (struct semaphore*)m);
    ((struct semaphore *)m)->locked = 1;
}

extern void 
lkthread_condition_broadcast_wrapper(c)
thr_condition_t c;
{
    lkthread_cond_broadcast((lkthread_cond_t*)c);
}

extern void 
lkthread_condition_signal_wrapper(c)
thr_condition_t c;
{
    lkthread_cond_signal((lkthread_cond_t*)c);
}

extern void 
lkthread_yield_wrapper()
{
    struct timeval delay_time;
    delay_time.tv_sec = 0;
    delay_time.tv_usec = 10000;	
    select(32, NULL, NULL, NULL, &delay_time);
}

extern void
lkthread_exit_wrapper(status)
void *status;
{
    lkthread_exit(status);
}

extern int
lkthread_join_wrapper(target, status_p)
thr_thread_t target;
void **status_p;
{
    return lkthread_join((lkthread_t)(long)target, status_p);
}
*/

extern thr_thread_t
lkthread_self_wrapper()
{
    return (thr_thread_t) 1;
}

extern void
gen_lkthread_init()
{
    gen_thread_info_t *gen_thread_info = libgenthreads_init3();
    gen_thread_info->start_func = (thr_start_func_t) NULL;
    gen_thread_info->fork_func = NULL;
    gen_thread_info->detach_func = NULL;
    gen_thread_info->yield_func = NULL;
    gen_thread_info->mutex_alloc_func = lkthread_mutex_alloc_wrapper;
    gen_thread_info->mutex_free_func = lkthread_mutex_free_wrapper;
    gen_thread_info->mutex_lock_func = lkthread_mutex_lock_wrapper;
    gen_thread_info->mutex_unlock_func = lkthread_mutex_unlock_wrapper;
    gen_thread_info->condition_alloc_func = NULL;
    gen_thread_info->condition_free_func = NULL;
    gen_thread_info->condition_wait_func = NULL;
    gen_thread_info->condition_signal_func = NULL;
    gen_thread_info->condition_broadcast_func = NULL;
    gen_thread_info->exit_func = NULL;
    gen_thread_info->join_func = NULL;
    gen_thread_info->self_func = NULL;
    gen_thread_info->null_thread_init = 0;
    gen_thread_info->is_kernel = 1;
}

/* Various required header files. */
#include <linux/config.h> 
#include <linux/module.h> 
#include <linux/version.h> 
#include <linux/kernel.h> 
#include <linux/init.h> 
#include <linux/tty.h>
#include <linux/unistd.h>
#include <linux/socket.h>
#include <linux/wait.h>
#include <linux/sched.h>
#include <asm/current.h>
#include <asm/semaphore.h>


/* Various required header files. */
#include <linux/kbd_ll.h>
#include <linux/types.h> 
#include <linux/utsname.h>
#include <linux/types.h>
#include <linux/slab.h>
#include <linux/vmalloc.h>
#include <asm/segment.h> 
#include <asm/signal.h> 
#include <asm/ptrace.h> 
