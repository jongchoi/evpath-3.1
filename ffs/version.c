#include <stdio.h>

static char *FFS_version = "FFS Version 1.1.81 rev. 11862  -- 2012-09-26 14:09:08 -0400 (Wed, 26 Sep 2012)\n";

void
FFSprint_version()
{
    printf("%s", FFS_version);
}

