== Installation ==

Use the build script (evpath-compall.sh) which will execute cmake.
Out of source build is recommended.

$ mkdir build && cd build
$ evpath-compall.sh -p /dir/to/install /dir/to/source
