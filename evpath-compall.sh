#!/bin/bash 

function usage {
    echo "USAGE : `basename $0` SRC"
}

function assert ()                 #  If condition false,
{                         #+ exit from script with error message.
  E_PARAM_ERR=98
  E_ASSERT_FAILED=99


  if [ -z "$2" ]          # Not enough parameters passed.
  then
    return $E_PARAM_ERR   # No damage done.
  fi

  lineno=$2

  if [ ! $1 ] 
  then
    echo "Assertion failed:  \"$1\""
    echo "File \"$0\", line $lineno"
    echo "Message: $3"
    exit $E_ASSERT_FAILED
  # else
  #   return
  #   and continue executing script.
  fi  
}  

PREFIX=$HOME/sw/evpath/3.1/rhel6_pgi12.8

while getopts "p:" OPTION
do
    case $OPTION in
	    p) PREFIX=$OPTARG ;;
	    *) usage; exit 1;
	        ;;
    esac
done

shift $(($OPTIND - 1))

if [ $# -lt 1 ]  ; then
    usage
    exit
fi

SRC=`readlink -fn $1`
BUILD=$PWD

command -v bison > /dev/null 
assert "$? == 0" $LINENO "No bison found"

[ x"$CC" == "x" ] && CC=mpicc
[ x"$CXX" == "x" ] && CXX=mpicxx

for module in cercs_env gen_thread atl dill ffs evpath cmrpc comm_group echo2; do
    echo ">>>"
    echo ">>>" $module
    echo ">>>"

    mkdir $BUILD/$module
    cd $BUILD/$module

    CC=$CC CXX=$CXX cmake -DCMAKE_INSTALL_PREFIX:PATH=$PREFIX -DCMAKE_BUILD_TYPE=Debug $SRC/$module
    assert "$? == 0" $LINENO "Error on executing cmake"

    make -j 4 VERBOSE=1
    assert "$? == 0" $LINENO "Error on executing make"

    make -j 4 install
    assert "$? == 0" $LINENO "Error on executing 'make install'"

    cd $BUILD
done

