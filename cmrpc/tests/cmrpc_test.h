/*
 *
 *  $Id: cmrpc_test.h,v 1.3 2007-12-31 22:27:41 pmw Exp $
 *
 */
#ifndef _CMRPC_TEST
#define _CMRPC_TEST
typedef struct _nested1_struct
{
  unsigned long int1;
  char str1[30];
  float flt1;
} nested1, *nested1_ptr;
typedef struct _nested2_struct
{
  char str1[30];
} nested2, *nested2_ptr;
typedef struct _nested3_struct
{
  float flt1;
} nested3, *nested3_ptr;
typedef struct _open_service_msg_struct
{
  unsigned long addr;
  nested1 n1;
  nested2 n2;
  nested3 n3;
  unsigned short port;
  unsigned long broker;
  int options;
} open_service_msg, *open_service_msg_ptr;

FMField open_service_msg_flds[] =
  {
    { "addr", "integer", sizeof (unsigned long),
      FMOffset (open_service_msg_ptr, addr)},
    { "nested1", "nested1", sizeof(nested1),
      FMOffset (open_service_msg_ptr, n1)},
    { "nested2", "nested2", sizeof(nested2),
      FMOffset (open_service_msg_ptr, n2)},
    { "nested3", "nested3", sizeof(nested3),
      FMOffset (open_service_msg_ptr, n3)},
    { "port", "integer", sizeof (unsigned short),
      FMOffset (open_service_msg_ptr, port)},
    { "broker", "integer", sizeof (unsigned long),
      FMOffset (open_service_msg_ptr, broker)},
    { "options", "integer", sizeof (int),
      FMOffset (open_service_msg_ptr, options)},
    {(char *) 0, (char *) 0, 0, 0}
  };

FMField nested1_flds[] = 
  {
    { "int1", "integer", sizeof(unsigned long),
      FMOffset (nested1_ptr, int1) },
    {(char*)0, (char*)0, 0, 0 }
  };
FMField nested2_flds[] = 
  {
    { "str1", "char[30]", sizeof(char),
      FMOffset (nested2_ptr, str1) },
    {(char*)0, (char*)0, 0, 0 }
  };
FMField nested3_flds[] = 
  {
    { "flt1", "float", sizeof(float),
      FMOffset (nested3_ptr, flt1) },
    {(char*)0, (char*)0, 0, 0 }
  };


FMStructDescRec open_service_msg_formats[] = 
  {
    {"open_service_msg", open_service_msg_flds, sizeof(open_service_msg), NULL },
    {"nested1", nested1_flds, sizeof(nested1), NULL },
    {"nested2", nested2_flds, sizeof(nested2), NULL },
    {"nested3", nested3_flds, sizeof(nested3), NULL },
    { NULL, NULL }
  };

#define PDS_IP_PORT ATL_CHAR_CONS('C','I','P','P')
#define PDS_IP_HOST ATL_CHAR_CONS('C','I','P','H')
#define PDS_IP_ADDR ATL_CHAR_CONS('C','I','P','A')


#define OPEN_SERVICE_RPC_NAME "open service"


#endif
/*
 *
 *  $Log: not supported by cvs2svn $
 *  Revision 1.2  2007/12/24 00:45:20  pmw
 *  First working version (defined as tests/ts runs correctly) after FFS
 *  conversion
 *
 *  Revision 1.1  2004/09/24 15:40:08  pmw
 *  Restructuring to add tests and config subdirectories
 *
 *
 */
