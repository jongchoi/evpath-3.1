/*
 *
 *  $Id: cmrpc_client.c,v 1.8 2007-12-31 22:27:41 pmw Exp $
 *
 */
#include "config.h"
#ifdef HAVE_MALLOC_H
#include <malloc.h>
#endif
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>


#include "atl.h"
#include "evpath.h"
#include "cmrpc.h"
#include "cmrpc_test.h"


static CManager client_cm;

void
pds_request (char* rpc_name, attr_list contact_list, void* in_msg, void* out_msg)
{
  CMrpc_call_rpc (client_cm, rpc_name, contact_list, in_msg, out_msg, 0);
}


void register_formats()
{
   if (client_cm == NULL) client_cm = CManager_create();
   CMrpc_register_rpc_request (client_cm, OPEN_SERVICE_RPC_NAME, NULL,
			       open_service_msg_formats, open_service_msg_formats);


}
int main(int argc, char* argv[])
{
  attr_list contact_list;
  open_service_msg in_msg, out_msg;
  if (argc<2) {
    printf("Usage: %s <hostname>\n", argv[0]);
    return -1;
  }
  contact_list = create_attr_list();
  set_attr (contact_list, PDS_IP_HOST, Attr_String, strdup (argv[1]) );
  set_attr (contact_list, PDS_IP_PORT, Attr_Int4, (attr_value*)atol("8848"));


  register_formats();
  pds_request ( OPEN_SERVICE_RPC_NAME, contact_list, &in_msg, &out_msg);
  fprintf (stderr, "out_msg.broker: expected 1, received %lu\n", out_msg.broker);
  fprintf (stderr, "out_msg.n1.int1: expected 406, received %lu\n", out_msg.n1.int1);
  return 1;
}

/*
 *
 *  $Log: not supported by cvs2svn $
 *  Revision 1.7  2007/12/24 00:45:19  pmw
 *  First working version (defined as tests/ts runs correctly) after FFS
 *  conversion
 *
 *  Revision 1.6  2006/10/11 10:55:06  eisen
 *  Fix cmrpc.h and evpath.h things
 *
 *  Revision 1.5  2006/10/09 21:52:36  pmw
 *  changes to run off evpath rather than old-style CM
 *
 *  Revision 1.4  2004/11/20 15:41:08  chaos
 *  Make this compile on darwin
 *
 *  Revision 1.3  2004/10/28 19:30:58  eisen
 *  Fix decls after executable lines
 *
 *  Revision 1.2  2004/10/14 21:16:25  pmw
 *  Added doxygen documentation.
 *  Also changed prefix of everything from CM_RPC to CMrpc.  It looks better.
 *
 *  Revision 1.1  2004/09/24 15:40:08  pmw
 *  Restructuring to add tests and config subdirectories
 *
 *
 */
