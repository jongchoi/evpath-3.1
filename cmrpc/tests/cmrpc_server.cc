/*
 *
 *  $Id: cmrpc_server.cc,v 1.8 2008-09-23 18:32:27 eisen Exp $
 *
 */
#include <../config.h>
#ifdef HAVE_MALLOC_H
#include <malloc.h>
#endif
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>


#include "atl.h"
#include "evpath.h"
#include "cmrpc.h"
#include "cmrpc_test.h"

static void
handle_open_service(void* in_msg, void* out_msg, CMrpc_options opt)
{
  open_service_msg_ptr msg = static_cast<open_service_msg_ptr> (in_msg);
  open_service_msg_ptr return_msg = static_cast<open_service_msg_ptr> (out_msg);

  return_msg->addr = 1;
  return_msg->port = 1;
  return_msg->n1.int1 = 406;
  return_msg->broker = 1;  // indicates address and port set

  return;
}

extern "C" void
Chandle_open_service(void* in_msg, void* out_msg, CMrpc_options opt)
{
  handle_open_service(in_msg, out_msg, opt);
}

void register_handlers(CManager cm)
{
  CMrpc_register_rpc_handler (cm, OPEN_SERVICE_RPC_NAME, open_service_msg_formats, 
			      open_service_msg_formats,
			      Chandle_open_service, NULL, NULL);
}
int
main()
{
   attr_list my_contact_info;
   gen_pthread_init();
   my_contact_info = create_attr_list();
   set_attr (my_contact_info,
	     attr_atom_from_string("IP_PORT"),
	     Attr_Int4,
	     (attr_value) 8848);
  CManager server_cm = CManager_create();
  register_handlers (server_cm);
  CMlisten_specific (server_cm, my_contact_info);
  free_attr_list (my_contact_info);
  CMrun_network (server_cm);
}

/*
 *
 *  $Log: not supported by cvs2svn $
 *  Revision 1.7  2007/12/31 22:27:41  pmw
 *  updated tests
 *
 *  Revision 1.6  2007/12/24 00:45:19  pmw
 *  First working version (defined as tests/ts runs correctly) after FFS
 *  conversion
 *
 *  Revision 1.5  2006/10/11 10:55:06  eisen
 *  Fix cmrpc.h and evpath.h things
 *
 *  Revision 1.4  2006/10/09 21:52:36  pmw
 *  changes to run off evpath rather than old-style CM
 *
 *  Revision 1.3  2004/11/20 15:41:08  chaos
 *  Make this compile on darwin
 *
 *  Revision 1.2  2004/10/14 21:16:25  pmw
 *  Added doxygen documentation.
 *  Also changed prefix of everything from CM_RPC to CMrpc.  It looks better.
 *
 *  Revision 1.1  2004/09/24 15:40:08  pmw
 *  Restructuring to add tests and config subdirectories
 *
 *
 */
