/*
 * This is a set of GENERAL PURPOSE Queueing utility program.
 *	Sep/07/95	Bongki Moon (bkmoon@cs.umd.edu)
 */

typedef struct queue_node {
	void			*item;
	struct queue_node	*next;
} QNODE;

typedef struct queue_head {
	QNODE	*front;
	QNODE	*rear;
} QHEAD;

void initQueue(QHEAD *Queue);
void enQueue(void *entry, QHEAD *Queue);
void *deQueue(QHEAD *Queue);
void insertQueue(void *entry, int position, QHEAD *Queue);
void insertAtRearQueue (void *entry, QHEAD *Queue);
void *fetchQueue(int position, QHEAD *Queue);

