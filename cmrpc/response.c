/*
 *
 *  $Id: response.c,v 1.23 2008-01-22 18:47:57 eisen Exp $
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

#include "gen_thread.h"
#include "evpath.h"
#include "cmrpc.h"
#include "cmrpc_internal.h"
#include "tclHash.h"
#include "queue.h"

const int CMRPC_MAX_WORKER_THREADS = 1000;

static void
rpc_response_side_init ARGS((CManager cm, int poolsize));

static void
rpc_request_handler ARGS((CManager cm, CMConnection cmc, void* data, void* client_data,
			  attr_list attrs));

static CMFormat request_CM_format;
static CMFormat response_CM_format;

extern FMContext encoding_context;
extern FFSContext decoding_context;
extern unsigned long CMrpc_status;
extern thr_mutex_t info_table_lock;
extern Tcl_HashTable rpc_info_table;
extern int rpc_info_initialized;

static QHEAD thread_work_q;
static thr_mutex_t work_q_mtx;
static thr_condition_t request_up;
static thr_thread_t *workers;
static int num_worker_threads = 1;
static int queue_length = 0;
static int workers_die = 0;

/**********************************************************
 *  
 *  RPC response-side functions
 *
 *****************************************/

int
CMrpc_setconcurrency (int nthreads)
{
  int oldsize;

  if (nthreads > CMRPC_MAX_WORKER_THREADS)
    return -1;
  
  if (nthreads < 1)
    return num_worker_threads;

  oldsize = num_worker_threads;
  num_worker_threads = nthreads;
  return oldsize;
}

int
CMrpc_register_rpc_handler (CManager cm, 
			    const char* rpc_name, 
			    FMStructDescList in_desc,
			    FMStructDescList out_desc,
			    CMrpc_HandlerFunc func,
			    CMrpc_OnewayHandlerFunc oneway_func, 
			    CMrpc_HandlerCleanupFunc cleanup_func)
{
  rpc_info_ptr new_handler_info;

  if (! (CMrpc_status & CMrpc_status_response_init)) {
    rpc_response_side_init (cm, num_worker_threads);
  }

  new_handler_info = get_rpc_info (rpc_name, 1);
  new_handler_info->func = func;
  new_handler_info->oneway_func = oneway_func;
  new_handler_info->cleanup_func = cleanup_func;
  new_handler_info->io_buffer = create_FFSBuffer();

  /* 
     Do the formats.  This is the server side, so we are going to decode the in_desc
     and encode the out_desc (reversing what was done on the client in request.c).  Note
     that we still refer to the in-format and out-format in rpc_info_ptr with respect
     to the client
  */
  if( in_desc ) {
    new_handler_info->ffs_decode_format = FFSset_fixed_target( decoding_context, in_desc );
    FFS_determine_conversion( decoding_context, new_handler_info->ffs_decode_format );
  }

  if( out_desc )
    new_handler_info->fm_encode_format = register_data_format( encoding_context, out_desc );

  return 1;
}


typedef struct raw_rpc_request_struct
{
  CManager cm;
  CMConnection cmc;
  void *data;
  void *client_data;
} raw_rpc_request_t, *raw_rpc_request;




void
rpc_request_handler (CManager cm,
		     CMConnection cmc,
		     void* data,
		     void* client_data,
		     attr_list attrs)
{
  raw_rpc_request q_request = (raw_rpc_request) malloc (sizeof (raw_rpc_request_t));

  /*  tell CM to let me keep the buffer data  */
  CMtake_buffer (cm, data);

  /*  do i need to copy these to keep CM from freeing them once this handler returns? */
  q_request->cm = cm;
  q_request->cmc = cmc;
  q_request->data = data;
  q_request->client_data = client_data;
  CMConnection_add_reference(cmc);

  /*  lock the work queue  */
  thr_mutex_lock (work_q_mtx);

  /*  enqueue the request  */
  insertAtRearQueue (q_request, &thread_work_q);
  queue_length++;

  /*  release the mutex  */
  thr_mutex_unlock (work_q_mtx);

  /*  signal the condition  */
  thr_condition_signal (request_up);
}


void
work_single_request (CManager cm, CMConnection cmc, void* data, void* client_data)
{
  void* out_data;
  void* in_data;  
  rpc_info_ptr handler_info;
  rpc_response response_msg;
  CMrpc_options_t options;
  FMStructDescList out_struct_list;
  
  rpc_request_ptr request_msg = (rpc_request_ptr)data;

  /*
   *  we always assume we're going to call the cleanup function if
   *  there is one.
   */
  memset(&options, 0, sizeof(options));
  options.cm = cm;
  options.flags |= CMrpc_call_cleanup_function;

  response_msg.condition = request_msg->condition;

  handler_info = get_rpc_info (request_msg->rpc_name, 0);
#if defined(CMRPC_DEBUG)
  fprintf( stderr, "work_single_request: rpc name (%s)\n", request_msg->rpc_name );
#endif
  if (!handler_info || !handler_info->func) {
    /*
     *  If there is no RPC registered under the given name, let the caller
     *  know this and bail out.
     */
    fprintf (stderr, "No RPC registered under name %s!\n", request_msg->rpc_name);
    response_msg.rpc_status = ENOENT;
    response_msg.status = 0;
    response_msg.encoded_len = 0;
    response_msg.encoded_return_data = NULL;
    CMwrite (cmc, response_CM_format, &response_msg);
    CMConnection_close (cmc);
    return;
  }

  if (handler_info->oneway_func == NULL && request_msg->request_flags & CMrpc_oneway) {
    /*
     *  Someone tried to call an RPC as oneway that doesn't support it.
     *  Since the request side doesn't wait for a response, just NOP and 
     *  return.
     */
    CMConnection_close (cmc);
    return;
  }

  /*
   * We found the RPC information.  Just do it.
   *
   * decode the incoming parameter block
   */
  in_data = malloc( FFS_est_decode_length( decoding_context,
					   request_msg->encoded_param_data,
					   request_msg->encoded_len ) );
  FFSdecode( decoding_context, request_msg->encoded_param_data, in_data );
		    
  /*
   * check for oneway call
   */
  if (request_msg->request_flags & CMrpc_oneway) {

#if defined(CMRPC_DEBUG)
    fprintf( stderr, "work_single_request: oneway call (%s)\n", request_msg->rpc_name );
#endif

    if (handler_info->oneway_func) {
      (*(handler_info->oneway_func)) (in_data, &options);
    }

    free (in_data);
    CMConnection_close (cmc);
    return;
  }

  /*  not oneway, allocate return memory  */
  out_struct_list = get_localized_formats( FMFormat_of_original( handler_info->ffs_decode_format ) );
  out_data = calloc( out_struct_list[0].struct_size, sizeof(char) );
      
  /*
   *  perform the call
   */
  (*(handler_info->func)) (in_data, out_data, &options);
  
  /*
   * encode the results
   */
  response_msg.encoded_return_data = FFSencode( handler_info->io_buffer,
						handler_info->fm_encode_format,
						out_data,
						&response_msg.encoded_len );
  
  response_msg.status = 1;
  CMwrite (cmc, response_CM_format, &response_msg);
  CMConnection_close (cmc);
  
  /*
   *  If the user registered a memory cleanup handler, and they didn't 
   *  indicate not to call it in the handler, call it now
   *  on the out_data.
   */
  if (handler_info->cleanup_func != NULL
      && (options.flags & CMrpc_call_cleanup_function)) {
    (*(handler_info->cleanup_func)) (cm, out_data);
  }
  
  /*
   * clean up
   */

  free (in_data);
  free (out_data);

}


int
worker_thread_func (void* arg)
{
  raw_rpc_request q_request;

#if defined(CMRPC_DEBUG)
  fprintf (stderr, "thread %p started\n", thr_thread_self());
#endif

  while (1) {
    /*
     *  wait on the condition
     */
    thr_mutex_lock (work_q_mtx);
    while (queue_length < 1)
      thr_condition_wait (request_up, work_q_mtx);
    
    /*
     *  we woke up, having been signaled.  Since gen_threads doesn't implement a 
     *  thread_kill function, this is an artifical cancellation point.  Check the
     *  global continue flag and exit if it's not set.
     */
    if (workers_die) {
#if defined(CMRPC_DEBUG)
      fprintf (stderr, "thread %p stopping on global indicator\n", thr_thread_self());
#endif
      thr_thread_exit (NULL);
    }

    /*
     *  grab a request and do it
     */
#if defined(CMRPC_DEBUG)
    fprintf (stderr, "thread %p woke up for request\n", thr_thread_self());
#endif
    q_request = deQueue (&thread_work_q);
    if (q_request == NULL) {
#if defined(CMRPC_DEBUG)
      fprintf (stderr, "No request available for thread %p!!\n", thr_thread_self());
#endif
      assert(q_request != NULL);
    }
    queue_length--;
    thr_mutex_unlock (work_q_mtx);
    
    work_single_request (q_request->cm,
			 q_request->cmc,
			 q_request->data,
			 q_request->client_data);
    
    CMreturn_buffer (q_request->cm, q_request->data);
    free (q_request);
  }

  /*NOTREACHED*/
}


static void
rpc_response_side_init (CManager cm, int worker_count)
{
  if (! (CMrpc_status & CMrpc_status_info_init)) {
    rpc_info_init (cm);
  }

  if (! (CMrpc_status & CMrpc_status_response_init)) {
    int i;

    response_CM_format = CMregister_format( cm, response_format_list );
    request_CM_format = CMregister_format( cm, request_format_list );
      
    CMregister_handler( request_CM_format, rpc_request_handler, NULL );
    
    num_worker_threads = (worker_count > 1) ? worker_count : 1;
    
    work_q_mtx = thr_mutex_alloc();
    initQueue (&thread_work_q);
    queue_length = 0;
    request_up = thr_condition_alloc();
    
    workers_die = 0;
    
    workers = (thr_thread_t*) calloc (num_worker_threads, sizeof (thr_thread_t));
    for (i = 0; i < num_worker_threads; i++) {
      workers[i] = thr_fork (worker_thread_func, NULL);
    }
      
    CMrpc_status |= CMrpc_status_response_init;
  }      
}


void
CMrpc_shutdown()
{
  int i;
  workers_die = 1;
  thr_condition_broadcast (request_up);

  for (i = 0; i < num_worker_threads; i++) {      
    thr_thread_join (workers[i], NULL);
#if defined(CMRPC_DEBUG)
    fprintf (stderr, "thread %p dead\n", workers[i]);
#endif
  }

  free (workers);
}

      


/*
 *
 *  $Log: not supported by cvs2svn $
 *  Revision 1.22  2008/01/01 00:33:56  pmw
 *  Removed debug code
 *
 *  Revision 1.21  2007/12/31 22:27:40  pmw
 *  updated tests
 *
 *  Revision 1.20  2007/12/24 00:45:17  pmw
 *  First working version (defined as tests/ts runs correctly) after FFS
 *  conversion
 *
 *  Revision 1.19  2006/12/31 23:51:43  pmw
 *  it's the mac, stupid.
 *
 *  Revision 1.18  2006/10/10 23:33:03  eisen
 *  Changes so that we don't have cm.h or evpath.h in cmrpc.h.
 *
 *  Revision 1.17  2005/07/14 02:53:52  eisen
 *  fix not reached warning
 *
 *  Revision 1.16  2005/07/13 17:21:26  eisen
 *  initialize memory
 *
 *  Revision 1.15  2004/10/14 21:16:16  pmw
 *  Added doxygen documentation.
 *  Also changed prefix of everything from CM_RPC to CMrpc.  It looks better.
 *
 *  Revision 1.14  2004/09/20 16:13:02  pmw
 *  Trying to chase down resource leakage in CMRPC.  Also modified configure
 *  scripts to current versions.
 *
 *  Revision 1.13  2004/05/06 17:55:04  eisen
 *  Tweak CMRPC so that it works with CMSelfFormats set.  This required some
 *  extensions to CM, and a bit of rearranging.
 *
 *  Revision 1.12  2003/07/10 14:52:41  eisen
 *  Avoid problem with request_msg->encoded_param_data not being properly
 *  aligned.  Instead use malloc's memory which has just been filled with the
 *  contents of the encoded_param_data.
 *
 *  Revision 1.11  2002/08/15 03:55:13  pmw
 *  fixed MT problem where async calls had their connection whacked out
 *  from underneath them, causing their condition to be marked failed
 *  before any tickets corresponding to them were redeemed.
 *
 *  also cleaned up undeclared function warnings in tclHash.c.
 *
 *  also changed handling of attr_lists in request.c to eliminate
 *  attr_add_list call.
 *
 *  Revision 1.10  2002/08/10 14:43:22  eisen
 *  CM handlers now get attr values as well.
 *
 *  Revision 1.9  2002/08/08 16:31:41  pmw
 *  Removed spurious gen_pthread_init calls.
 *
 *  Revision 1.8  2002/07/09 21:17:15  pmw
 *  changes to quiet the nightly build process
 *
 *  Revision 1.7  2002/06/16 21:59:16  pmw
 *  more and more fixes.  This should at least shut up the messages from
 *  the nightly chaos builds.
 *
 *  Revision 1.6  2002/06/16 16:09:57  pmw
 *  changes for multithreading.  still doesn't quite work right.
 *
 *  Revision 1.5  2002/06/05 22:45:26  pmw
 *  more multithread changes
 *
 *  Revision 1.4  2002/05/29 13:26:37  pmw
 *  interim checkin
 *
 *  Revision 1.3  2002/05/28 22:35:35  pmw
 *  beginning changes for multithreaded RPC support
 *
 *  Revision 1.2  2002/05/27 07:44:55  pmw
 *  const correctness for C compilers sucks.  Especially when I use so much code from
 *  other people that isn't const correct :)
 *
 *  Revision 1.1  2001/10/09 15:16:26  pmw
 *  changed file names
 *
 *  Revision 1.11  2000/10/16 03:45:37  pmw
 *  Added oneway RPC calls.  This has been tested somewhat.
 *
 *  Revision 1.10  2000/10/16 02:31:23  pmw
 *  1 - fixed gotcha-in-waiting concerning IOcontext management and anonymous
 *  calls.  The IOcontext allocated for anon calls was getting freed before the
 *  user ever saw the result structure from the call, and the underlying PBIO
 *  buffer was suspect.  This meant revising the anon-call interface back to
 *  where the user supplies his own IOcontext values.
 *  2 - Added rpc-with-channel functionality, not yet tested.
 *
 *  Revision 1.9  2000/09/27 23:41:40  pmw
 *  Hopefully, this fixes the looming format problem Greg pointed out.
 *  Subcontexts are now used to hold the input and output top-level
 *  formats of each RPC format pair, and appropriate conversion setting
 *  should be being done based on matching the format names on the incoming
 *  message with those registered "locally" and setting the conversion with
 *  the local field list.
 *
 *  Revision 1.8  2000/09/19 20:54:18  pmw
 *  changed syntax of handlers to eliminate explicit passing of CManager value - now contained in options structure
 *
 *  Revision 1.7  2000/04/10 21:25:52  pmw
 *  typo
 *
 *  Revision 1.6  2000/04/10 21:22:17  pmw
 *  added options flag to handlers
 *
 *  Revision 1.5  2000/04/10 20:28:34  pmw
 *  Added cleanup handler functionality
 *
 *  Revision 1.4  2000/04/10 18:10:16  pmw
 *  more rearrangement
 *
 *  Revision 1.3  2000/04/06 22:39:19  pmw
 *  Now does sane handling of IO conversions
 *
 *  Revision 1.2  2000/04/05 03:00:08  pmw
 *  added high-level convenience functions
 *
 *  Revision 1.1  2000/04/04 19:04:52  pmw
 *  cosmetic changes, split into more source files
 *
 *  Revision 1.2  2000/04/04 18:35:29  pmw
 *  added RCS headers
 *
 *
 */
