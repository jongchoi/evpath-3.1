/*******************************************************************************
 * queue.c - General queue ADS
 *
 * $Id: queue.c,v 1.3 2002-05-28 22:35:35 pmw Exp $
 *
 * Fabian E. Bustamante
 * fabianb@cc.gatech.edu
 * Georgia Institute of Technology
 * College of Computing
 * Systems Lab
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "queue.h"



void initQueue(QHEAD *Queue)
{
	Queue->front = Queue->rear = NULL;
}

void enQueue(void *entry, QHEAD *Queue)
{
	QNODE	*node = (QNODE*)malloc(sizeof(QNODE));

	node->item = entry;
	node->next = NULL;

	if (Queue->rear == NULL)
		Queue->rear = Queue->front = node;
	else {
		Queue->rear->next = node;
		Queue->rear = node;
	}
}

void *deQueue(QHEAD *Queue)
{
	QNODE	*node;
	void	*entry;

	if (Queue->front == NULL) return NULL;

	node = Queue->front;
	entry = Queue->front->item;
	Queue->front = Queue->front->next;
	if (Queue->front == NULL) Queue->rear = NULL;

	free(node);

	return entry;
}

/* if position == 0, insert a node in the first position in the queue.
   if position == i, insert a node in the (i+1)-th position in the queue.
   if position == n, insert a node at the rear (i.e. (n+1)-th) of the queue. */
void insertQueue(void *entry, int position, QHEAD *Queue)
{
	QNODE	*node = (QNODE*)malloc(sizeof(QNODE));
	QNODE	*curr;

	node->item = entry;
	node->next = NULL;

	if (Queue->rear == NULL) {
		Queue->rear = Queue->front = node;
	} else if (position == 0) {
		node->next = Queue->front;
		Queue->front = node;
	} else if (position > 0) {
		for(curr=Queue->front; --position ;curr=curr->next);
		node->next = curr->next;
		curr->next = node;
	}
}


void insertAtRearQueue (void *entry, QHEAD *Queue)
{
  QNODE *node = (QNODE*) malloc (sizeof (QNODE));
  
  node->item = entry;
  node->next = NULL;

  if (Queue->rear == NULL)
    {
      Queue->rear = Queue->front = node;
    }
  else
    {
      Queue->rear->next = node;
      Queue->rear = node;
    }
}

void *fetchQueue(int position, QHEAD *Queue)
{
	QNODE	*node;

	for(node=Queue->front; node && position ;position--,node=node->next);
	if (node==NULL) return NULL;

	return node->item;
}

/*****************************************************************************/
