/*
 *
 *  $Id: request.c,v 1.13 2007-12-24 01:35:43 pmw Exp $
 *
 */
#include "config.h"
#include <stdio.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#ifdef HAVE_WINDOWS_H
#include <windows.h>
#include <winsock.h>
#endif

#include "gen_thread.h"
#include "cercs_env.h"
#include "ffs.h"
#include "evpath.h"
#include "cmrpc.h"
#include "cmrpc_internal.h"

static CMrpc_ticket
new_rpc_ticket ARGS((int cond));

static void
free_rpc_ticket ARGS((CMrpc_ticket p));

static rpc_call_info_ptr
new_rpc_call_info();

static void
free_rpc_call_info ARGS((rpc_call_info_ptr p));

static void
rpc_request_side_init ARGS((CManager cm));

static void
rpc_response_handler ARGS((CManager cm, CMConnection cmc, void* data, void* client_data,
			   attr_list attrs));

static CMrpc_ticket
perform_rpc_request ARGS((CManager cm, const char* rpc_name, attr_list target_spec,
			  FMFormat in_format, void* in_data, 
			  FFSTypeHandle out_format, void* out_data, 
			  unsigned long flags));


static void
terminate_rpc ARGS((CManager cm, rpc_call_info_ptr, void* out_data));


extern unsigned long CMrpc_status;

static CMFormat request_CM_format;
static CMFormat response_CM_format;
extern FFSContext decoding_context;
extern FMContext encoding_context;

#ifdef WSAEWOULDBLOCK
#define ECONNREFUSED WSAECONNREFUSED
#define ECONNABORTED WSAECONNABORTED
#define EINPROGRESS WSAEINPROGRESS
#endif
/**********************************************************
 *  
 *  RPC request-side functions
 *
 *****************************************/

static CMrpc_ticket
new_rpc_ticket (int cond)
{
  CMrpc_ticket p;

  p = malloc (sizeof (rpc_ticket_struct));
  p->condition = cond;
  p->valid = 1;
  return p;
}

static void
free_rpc_ticket (CMrpc_ticket p)
{
  free (p);
}

static rpc_call_info_ptr
new_rpc_call_info()
{
  rpc_call_info_ptr p;

  p = malloc (sizeof (rpc_call_info));
  memset (p, 0, sizeof (rpc_call_info));
  p->io_buffer = create_FFSBuffer();

  return p;
}

static void
free_rpc_call_info (rpc_call_info_ptr p)
{
  free_FFSBuffer( p->io_buffer );
  free (p);
}
       

static void
rpc_request_side_init (CManager cm)
{
  if (0 == (CMrpc_status & CMrpc_status_info_init))
    rpc_info_init(cm);

  request_CM_format = CMregister_format( cm, request_format_list );
  response_CM_format = CMregister_format( cm, response_format_list );

  CMregister_handler( response_CM_format, rpc_response_handler, NULL );

  CMrpc_status |= CMrpc_status_request_init;
}


static void
rpc_response_handler (CManager cm, 
		      CMConnection cmc, 
		      void* data, 
		      void* client_data, 
		      attr_list attrs)
{
  rpc_response_ptr msg = (rpc_response_ptr)data;
  
  rpc_call_info_ptr call_data = (rpc_call_info_ptr) CMCondition_get_client_data (cm, msg->condition);
  call_data->response_data_ptr = malloc (msg->encoded_len);
  memcpy (call_data->response_data_ptr, msg->encoded_return_data, msg->encoded_len);
  call_data->response_data_len = msg->encoded_len;
  call_data->response_status = msg->status;
  call_data->rpc_status = msg->rpc_status;
  
  CMCondition_signal (cm, msg->condition);
}


static void
terminate_rpc (CManager cm, rpc_call_info_ptr call_info, void* out_data)
{
  FFSdecode (decoding_context, call_info->response_data_ptr, out_data);
  
  CMConnection_close (call_info->cmconn);

  /*
   *  done with the call_info struct...
   */
  free_rpc_call_info (call_info);
}    

static int CMRPCVerbose = -1;

static CMrpc_ticket
perform_rpc_request (CManager cm, const char* rpc_name, attr_list target_spec, 
		     FMFormat in_format, void* in_data,
		     FFSTypeHandle out_format, void* out_data,
		     unsigned long flags)
{
  rpc_request request_msg;
  CMConnection cmc;
  rpc_call_info_ptr call_data;
  rpc_ticket_struct* retval;
  
  request_msg.request_flags = 0;
  
  if (CMRPCVerbose == -1) {
      CMRPCVerbose = (cercs_getenv("CMRPCVerbose") != NULL);
  }
  if (CMRPCVerbose) {
      printf("Executing RPC \"%s\" with parameters :", rpc_name);
      printf("\n%s\n", FMunencoded_to_XML_string( encoding_context, in_format, in_data ));
  }
  
  request_msg.rpc_name = (char*)rpc_name;
  
  /*
   *  get a connection, store a rpc_call_info structure as client data
   *  on the condition, and send the message
   */
  cmc = CMget_conn (cm, target_spec);
  if (!cmc) {
    fprintf (stderr, "Can't obtain CM connection, contact info follows...\n");
    CMrpc_errno = ECONNREFUSED;
    dump_attr_list (target_spec);
    return NULL;
  }

  call_data = new_rpc_call_info();  
  call_data->ffs_decode_format = out_format;
  call_data->flags = flags;
  call_data->cmconn = cmc;

  request_msg.encoded_param_data = 
    FFSencode( call_data->io_buffer, in_format, in_data, &request_msg.encoded_len );

  if (flags & CMrpc_oneway) {
    request_msg.request_flags |= CMrpc_oneway;
    request_msg.condition = -1;
    CMwrite (cmc, request_CM_format, &request_msg);
    CMrpc_errno = 0;
    free_rpc_call_info( call_data );
    return NULL;
  }

  request_msg.condition = CMCondition_get (cm, cmc);
  CMCondition_set_client_data (cm, request_msg.condition, call_data);

  CMwrite (cmc, request_CM_format, &request_msg);

  /*
   *  Check to see if the user wants it for here or to go.  If not async, we
   *  immediately wait on the condition.  If async, we create an rpc_ticket
   *  to give them and return.
   */

  if (flags & CMrpc_async_call) {
    retval = new_rpc_ticket (request_msg.condition);
    
  } else {
    
    /*
     * nighty-night...
     */
    if (!CMCondition_wait (cm, request_msg.condition)) {
      /*
       *  the condition failed.  probably the other side
       *  hung up the connection.  we have to return NULL.
       */
      CMConnection_close (cmc);
      retval = NULL;
      CMrpc_errno = ECONNABORTED;
      if (CMRPCVerbose) {
	printf("RPC \"%s\" ABORTED!", rpc_name);
      }
    } else {
      /*
       * we've been signalled, decode the return message
       */
      terminate_rpc (cm, call_data, out_data);
      if (CMRPCVerbose) {
	printf("Terminated RPC \"%s\" out parameters :", rpc_name);
	FMdump_encoded_data( FMFormat_of_original( out_format ), out_data, 0 );
      }
      CMrpc_errno = 0;
      retval = (CMrpc_ticket)1;
    }
  }

  return retval;
}



extern CMrpc_ticket
CMrpc_call_rpc (cm, rpc_name, contact_info, in_data, out_data, flags)
     CManager cm;
     const char* rpc_name;
     attr_list contact_info;
     void* in_data;
     void* out_data;
     unsigned long flags;
{
  rpc_info_ptr rinfo;
  attr_list server_contact_info;

  if (! (CMrpc_status_request_init & CMrpc_status)) {
    rpc_request_side_init (cm);
  }

  rinfo = get_rpc_info (rpc_name, 0);
  if (rinfo == NULL || rinfo->fm_encode_format == NULL) {
    fprintf (stderr, "You must register RPCs before calling them with this function!\n");
    CMrpc_errno = ENOENT;
    return NULL;
  }

  /*
   *  If the caller provided some contact information for this call, prefer it.
   *  If not, use registered contact information if it exists.  If it doesn't,
   *  bail out.
   */
  if (contact_info != NULL) {
    server_contact_info = contact_info;
  } else if (rinfo->target_spec != NULL) {
    server_contact_info = rinfo->target_spec;
  } else {
    fprintf (stderr, "You must register contact information for RPC %s before calling it!\n", rpc_name);
    CMrpc_errno = ENOENT;
    return NULL;
  }

  return perform_rpc_request (cm, rpc_name, server_contact_info,
                              rinfo->fm_encode_format, in_data,
			      rinfo->ffs_decode_format, out_data, 
			      flags);
}


extern CMrpc_ticket
CMrpc_call_anon_rpc (CManager cm, 
		     const char* rpc_name, 
		     attr_list target_spec,
		     FMStructDescList in_struct,
		     void* in_arg,
		     FMStructDescList out_struct,
		     void* out_arg,
		     unsigned long flags)
{
  CMrpc_ticket ticket;
  FFSTypeHandle out_format;
  FMFormat in_format;

  if (! (CMrpc_status_request_init & CMrpc_status)) {
    rpc_request_side_init (cm);
  }

  in_format = register_data_format( encoding_context, in_struct );
  out_format = FFSset_fixed_target( decoding_context, out_struct );
  FFS_determine_conversion( decoding_context, out_format );

  ticket = perform_rpc_request (cm, rpc_name, target_spec, 
				in_format, in_arg,
                                out_format, out_arg, 
				flags | CMrpc_anon_call);
  
  return ticket;
}




int
CMrpc_redeem_ticket (CManager cm, CMrpc_ticket ticket, void* out_data, int wait)
{
  int condition_is_ready;
  int wait_value;
  int result = -1;
  rpc_call_info_ptr call_info = NULL;
  
  if (!ticket->valid) {
    CMrpc_errno = EINVAL;
    return -1;
  }

  if (CMCondition_has_failed (cm, ticket->condition)) {
    ticket->valid = 0;
    CMrpc_errno = ECONNABORTED;
    return -2;
  }

  condition_is_ready = CMCondition_has_signaled (cm, ticket->condition);
  call_info = (rpc_call_info_ptr)CMCondition_get_client_data( cm, ticket->condition );
  
  if (!condition_is_ready) {
    if (!wait) {
      CMpoll_network (cm);
      CMrpc_errno = EINPROGRESS;
      return 0;
    } else {
      wait_value = CMCondition_wait (cm, ticket->condition);
    }
  } else {
    wait_value = 1;
  }

  /*
   *  the call data is ready to be picked up, and we know
   *  the user wants it now
   */

  if (wait_value) {
    terminate_rpc (cm, call_info, out_data);
    CMrpc_errno = 0;
    result = 1;
  } else {
    result = 0;
    CMrpc_errno = ECONNABORTED;
  }

  free_rpc_ticket (ticket);
  return result;
}


extern int
CMrpc_register_rpc_request (CManager cm, const char* rpc_name, 
			    attr_list contact_info,
			    FMStructDescList in_struct,
			    FMStructDescList out_struct)
{
  rpc_info_ptr new_info;

  if (! (CMrpc_status & CMrpc_status_info_init)) 
    rpc_info_init (cm);  

  new_info = get_rpc_info (rpc_name, 1);

  /* Do the contact list */
  if (new_info->target_spec) {
    free_attr_list (new_info->target_spec);
  }
  add_ref_attr_list (contact_info);
  new_info->target_spec = contact_info;

  /* Do the formats */
  if( in_struct )
    new_info->fm_encode_format = register_data_format( encoding_context, in_struct );
  if( out_struct ) {
    new_info->ffs_decode_format = FFSset_fixed_target( decoding_context, out_struct );
    FFS_determine_conversion( decoding_context, new_info->ffs_decode_format );
  }
  
  return 1;
}


void
CMrpc_call_rpc_oneway (CManager cm, 
		       const char* rpc_name, 
		       attr_list contact_info, void* in_arg, 
		       unsigned long flags)
{
  CMrpc_call_rpc (cm, rpc_name, contact_info, in_arg, NULL, flags | CMrpc_oneway);
}


/*
 *
 *  $Log: not supported by cvs2svn $
 *  Revision 1.12  2007/12/24 00:45:17  pmw
 *  First working version (defined as tests/ts runs correctly) after FFS
 *  conversion
 *
 *  Revision 1.11  2006/10/10 23:33:03  eisen
 *  Changes so that we don't have cm.h or evpath.h in cmrpc.h.
 *
 *  Revision 1.10  2005/09/07 17:39:47  eisen
 *  remove warnings.
 *
 *  Revision 1.9  2005/09/07 17:35:04  eisen
 *  Add CMRPCVerbose environment variable to get some information out of this
 *  damned thing.
 *
 *  Revision 1.8  2004/10/14 21:16:16  pmw
 *  Added doxygen documentation.
 *  Also changed prefix of everything from CM_RPC to CMrpc.  It looks better.
 *
 *  Revision 1.7  2004/09/20 16:13:02  pmw
 *  Trying to chase down resource leakage in CMRPC.  Also modified configure
 *  scripts to current versions.
 *
 *  Revision 1.6  2004/09/17 20:18:20  pmw
 *  Use CMget_conn instead of CMinitiate_conn
 *
 *  Revision 1.5  2004/05/06 17:55:04  eisen
 *  Tweak CMRPC so that it works with CMSelfFormats set.  This required some
 *  extensions to CM, and a bit of rearranging.
 *
 *  Revision 1.4  2002/08/15 03:55:13  pmw
 *  fixed MT problem where async calls had their connection whacked out
 *  from underneath them, causing their condition to be marked failed
 *  before any tickets corresponding to them were redeemed.
 *
 *  also cleaned up undeclared function warnings in tclHash.c.
 *
 *  also changed handling of attr_lists in request.c to eliminate
 *  attr_add_list call.
 *
 *  Revision 1.3  2002/08/10 14:43:22  eisen
 *  CM handlers now get attr values as well.
 *
 *  Revision 1.2  2002/05/27 07:44:55  pmw
 *  const correctness for C compilers sucks.  Especially when I use so much code from
 *  other people that isn't const correct :)
 *
 *  Revision 1.1  2001/10/09 15:16:26  pmw
 *  changed file names
 *
 *  Revision 1.19  2001/07/13 20:46:21  eisen
 *  compile under windows
 *
 *  Revision 1.18  2000/12/04 21:32:25  pmw
 *  closes CMConnection value after response is received.  I wasn't doing this
 *  before and it was causing resource leakage (sockets never being closed).
 *
 *  Revision 1.17  2000/11/28 21:01:55  pmw
 *  Now handles failed conditions in a more sane manner - will not attempt to
 *  decode after a connection is dropped.  Also more settings of CM_RPC_errno,
 *  so clients can detect exactly what has happened.
 *
 *  Revision 1.16  2000/10/24 18:03:36  pmw
 *  Fixed typo in call_rpc - wasn't passing correct variable to perform_rpc_request
 *
 *  Revision 1.15  2000/10/23 20:52:10  pmw
 *  Added contact_info parameter to call_rpc calls.  This is to be used
 *  if, for a specific invocation of an RPC, you wish to override the
 *  registered contact information.
 *
 *  Revision 1.14  2000/10/16 14:50:29  pmw
 *  Fixed stupid initialization bug on request flags in perform_rpc_request
 *
 *  Revision 1.13  2000/10/16 03:45:36  pmw
 *  Added oneway RPC calls.  This has been tested somewhat.
 *
 *  Revision 1.12  2000/10/16 02:31:22  pmw
 *  1 - fixed gotcha-in-waiting concerning IOcontext management and anonymous
 *  calls.  The IOcontext allocated for anon calls was getting freed before the
 *  user ever saw the result structure from the call, and the underlying PBIO
 *  buffer was suspect.  This meant revising the anon-call interface back to
 *  where the user supplies his own IOcontext values.
 *  2 - Added rpc-with-channel functionality, not yet tested.
 *
 *  Revision 1.11  2000/09/29 20:52:08  pmw
 *  fixed lingering problems from subcontext changes.  hopefully this closes it.
 *  make check works correctly, so I'll believe it for the moment.
 *
 *  Revision 1.10  2000/09/27 23:41:39  pmw
 *  Hopefully, this fixes the looming format problem Greg pointed out.
 *  Subcontexts are now used to hold the input and output top-level
 *  formats of each RPC format pair, and appropriate conversion setting
 *  should be being done based on matching the format names on the incoming
 *  message with those registered "locally" and setting the conversion with
 *  the local field list.
 *
 *  Revision 1.9  2000/05/12 20:54:18  pmw
 *  Re-did anonymous call syntax to avoid PBIO memory-handling issues
 *
 *  Revision 1.8  2000/05/10 19:49:11  pmw
 *  removed extra free() which caused async test bug
 *
 *  Revision 1.7  2000/04/10 18:10:16  pmw
 *  more rearrangement
 *
 *  Revision 1.6  2000/04/06 22:39:19  pmw
 *  Now does sane handling of IO conversions
 *
 *  Revision 1.5  2000/04/05 03:00:06  pmw
 *  added high-level convenience functions
 *
 *  Revision 1.4  2000/04/04 22:53:59  pmw
 *  missing thread unlock
 *
 *  Revision 1.3  2000/04/04 22:51:06  pmw
 *  Changes to support async calls
 *
 *  Revision 1.2  2000/04/04 20:40:05  pmw
 *  Restructured for asyncronous calls
 *
 *  Revision 1.1  2000/04/04 19:04:51  pmw
 *  cosmetic changes, split into more source files
 *
 *
 */
