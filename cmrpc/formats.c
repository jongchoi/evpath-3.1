/*
 *
 *  $Id: formats.c,v 1.10 2007-12-24 00:45:17 pmw Exp $
 *
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include "atl.h"
#include "gen_thread.h"

#include "tclHash.h"
#include "evpath.h"
#include "cmrpc_internal.h"

int CMrpc_errno = 0;

#define FMFieldList_Terminator {(char*)0, (char*)0, 0, 0}
#define FMStructDescList_Terminator {NULL, NULL}

FMField rpc_request_flds[] = 
{
  { "rpc_name", "string", sizeof (char*), FMOffset (rpc_request_ptr, rpc_name) },
  { "condition", "integer", sizeof (int), FMOffset (rpc_request_ptr, condition) },
  { "encoded_len", "integer", sizeof (int), FMOffset (rpc_request_ptr, encoded_len) },
  { "encoded_param_data", "char[encoded_len]", sizeof (char), 
    FMOffset (rpc_request_ptr, encoded_param_data) },
  { "request_flags", "integer", sizeof (unsigned long), FMOffset (rpc_request_ptr, request_flags) },
  FMFieldList_Terminator
};


FMField rpc_response_flds[] = 
{
  { "condition", "integer", sizeof (int), FMOffset (rpc_response_ptr, condition) },
  { "encoded_len", "integer", sizeof (int), FMOffset (rpc_response_ptr, encoded_len) },
  { "encoded_return_data", "char[encoded_len]", sizeof (char), 
    FMOffset (rpc_response_ptr, encoded_return_data) },
  { "status", "integer", sizeof (int), FMOffset (rpc_response_ptr, status) },
  { "rpc_status", "integer", sizeof (int), FMOffset (rpc_response_ptr, rpc_status) },
  FMFieldList_Terminator
};

FMStructDescRec request_format_list[] = 
  {
    { "CMrpc:request", rpc_request_flds, sizeof(rpc_request), NULL },
    FMStructDescList_Terminator
  };

FMStructDescRec response_format_list[] = 
  {
    { "CMrpc:response", rpc_response_flds, sizeof(rpc_response), NULL },
    FMStructDescList_Terminator
  };

static thr_mutex_t rpc_info_table_lock;
static Tcl_HashTable rpc_info_table;

unsigned long CMrpc_status = 0;

FMContext encoding_context;
FFSContext decoding_context;

rpc_info_ptr
get_rpc_info (const char* rpc_name, int make_new)
{
  Tcl_HashEntry* entry = NULL;
  rpc_info_ptr p;
  int is_new;

  thr_mutex_lock (rpc_info_table_lock);
  
  if (!make_new) {
    entry = Tcl_FindHashEntry (&rpc_info_table, (char*)rpc_name);
    if (entry) {
      p = (rpc_info_ptr) Tcl_GetHashValue (entry);
    } else {
      p = NULL;
    }
  } else {
    entry = Tcl_CreateHashEntry (&rpc_info_table, (char*)rpc_name, &is_new);
    if (!is_new) {
      p = (rpc_info_ptr) Tcl_GetHashValue (entry);
    } else {
      p = malloc (sizeof (rpc_info));
      memset (p, 0, sizeof (rpc_info));
      Tcl_SetHashValue (entry, p);
    }
  }
  
  thr_mutex_unlock (rpc_info_table_lock);
  
  return p;
}



int
CMrpc_unregister_rpc (const char* rpc_name)
{
  Tcl_HashEntry *entry = NULL;
  int found = 0;

  thr_mutex_lock (rpc_info_table_lock);
  
  entry = Tcl_FindHashEntry (&rpc_info_table, (char*)rpc_name);
  if (entry != NULL) {
    void* p = Tcl_GetHashValue (entry);
    free (p);
    Tcl_DeleteHashEntry (entry);
    found = 1;
  }
  
  thr_mutex_unlock (rpc_info_table_lock);

  return found;
}


void 
rpc_info_init (CManager cm)
{
  if (! (CMrpc_status & CMrpc_status_info_init)) {
    rpc_info_table_lock = thr_mutex_alloc();
    Tcl_InitHashTable (&rpc_info_table, TCL_STRING_KEYS);
        
    encoding_context = create_FMcontext();
    decoding_context = create_FFSContext_FM( encoding_context );
    
    CMrpc_status |= CMrpc_status_info_init;
  }
}


/*
 *
 *  $Log: not supported by cvs2svn $
 *  Revision 1.9  2006/10/10 23:33:03  eisen
 *  Changes so that we don't have cm.h or evpath.h in cmrpc.h.
 *
 *  Revision 1.8  2006/10/09 21:52:33  pmw
 *  changes to run off evpath rather than old-style CM
 *
 *  Revision 1.7  2004/10/14 21:16:16  pmw
 *  Added doxygen documentation.
 *  Also changed prefix of everything from CM_RPC to CMrpc.  It looks better.
 *
 *  Revision 1.6  2004/05/06 17:55:04  eisen
 *  Tweak CMRPC so that it works with CMSelfFormats set.  This required some
 *  extensions to CM, and a bit of rearranging.
 *
 *  Revision 1.5  2002/08/15 03:55:13  pmw
 *  fixed MT problem where async calls had their connection whacked out
 *  from underneath them, causing their condition to be marked failed
 *  before any tickets corresponding to them were redeemed.
 *
 *  also cleaned up undeclared function warnings in tclHash.c.
 *
 *  also changed handling of attr_lists in request.c to eliminate
 *  attr_add_list call.
 *
 *  Revision 1.4  2002/08/08 16:31:41  pmw
 *  Removed spurious gen_pthread_init calls.
 *
 *  Revision 1.3  2002/06/16 16:09:57  pmw
 *  changes for multithreading.  still doesn't quite work right.
 *
 *  Revision 1.2  2002/05/27 07:44:55  pmw
 *  const correctness for C compilers sucks.  Especially when I use so much code from
 *  other people that isn't const correct :)
 *
 *  Revision 1.1  2001/10/09 15:16:26  pmw
 *  changed file names
 *
 *  Revision 1.9  2001/10/08 23:06:00  pmw
 *  modified duplicate format registration check to make sure that formats
 *  haven't been registered in the context they're going in, not just that
 *  they haven't been registered at all (a less restrictive condition).  This
 *  was biting me when I register the same format for input and output of an
 *  RPC - the output format wasn't getting registered because the check always
 *  found that the field list had already been registered.  However, CM doesn't
 *  keep track explicitly of what context the format gets registered in, just
 *  that it was registered at all.  So I had to check the context as well if I
 *  found a format.
 *
 *  Revision 1.8  2000/10/27 19:28:54  pmw
 *  Removed CM_RPC_register_rpc_subformat().  I didn't understand how CM
 *  worked well enough; this function is really not necessary.
 *
 *  Revision 1.7  2000/10/16 03:45:36  pmw
 *  Added oneway RPC calls.  This has been tested somewhat.
 *
 *  Revision 1.6  2000/10/16 02:31:22  pmw
 *  1 - fixed gotcha-in-waiting concerning IOcontext management and anonymous
 *  calls.  The IOcontext allocated for anon calls was getting freed before the
 *  user ever saw the result structure from the call, and the underlying PBIO
 *  buffer was suspect.  This meant revising the anon-call interface back to
 *  where the user supplies his own IOcontext values.
 *  2 - Added rpc-with-channel functionality, not yet tested.
 *
 *  Revision 1.5  2000/09/27 23:41:39  pmw
 *  Hopefully, this fixes the looming format problem Greg pointed out.
 *  Subcontexts are now used to hold the input and output top-level
 *  formats of each RPC format pair, and appropriate conversion setting
 *  should be being done based on matching the format names on the incoming
 *  message with those registered "locally" and setting the conversion with
 *  the local field list.
 *
 *  Revision 1.4  2000/04/11 18:40:34  pmw
 *  added CM_RPC_errno
 *
 *  Revision 1.3  2000/04/10 20:28:33  pmw
 *  Added cleanup handler functionality
 *
 *  Revision 1.2  2000/04/10 18:10:15  pmw
 *  more rearrangement
 *
 *  Revision 1.1  2000/04/04 19:04:51  pmw
 *  cosmetic changes, split into more source files
 *
 *
 */
