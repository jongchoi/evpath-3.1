#ifndef __CMRPC_INTERNAL_H__
#define __CMRPC_INTERNAL_H__

/*
 *
 *  $Id: cmrpc_internal.h,v 1.16 2007-12-24 00:45:13 pmw Exp $
 *
 */
#include <errno.h>

#include "cmrpc.h"

/*
 *  internal flag values (should be the upper two bytes of the flags unsigned long)
 */
#define CMrpc_anon_call (0x10000)
#define CMrpc_with_channel (0x20000)
#define CMrpc_oneway (0x40000)

#define CMrpc_status_request_init (0x1)
#define CMrpc_status_response_init (0x2)
#define CMrpc_status_info_init (0x4)

/*
 * RPC stuff
 */
typedef struct rpc_request_struct
{
  char* rpc_name;
  int condition;
  int encoded_len;
  char* encoded_param_data;
  unsigned long request_flags;
} rpc_request, *rpc_request_ptr;

typedef struct rpc_response_struct
{
  int condition;
  int encoded_len;
  char* encoded_return_data;
  int status;
  int rpc_status;
} rpc_response, *rpc_response_ptr;


typedef struct rpc_call_info_struct
{
  char* response_data_ptr;
  int response_data_len;
  int response_status;
  int rpc_status;
  FFSTypeHandle ffs_decode_format;
  size_t out_struct_size;
  FFSBuffer io_buffer;
  CMConnection cmconn;
  unsigned long flags;
} rpc_call_info, *rpc_call_info_ptr;


typedef struct rpc_info_struct
{
  CMrpc_HandlerFunc func;
  CMrpc_OnewayHandlerFunc oneway_func;
  CMrpc_HandlerCleanupFunc cleanup_func;
  FMFormat fm_encode_format;
  FFSTypeHandle ffs_decode_format;
  FFSBuffer io_buffer;
  attr_list target_spec;
} rpc_info, *rpc_info_ptr;

typedef struct _CMrpc_ticket_struct
{
  int valid;
  int condition;
} rpc_ticket_struct;

extern FMField rpc_response_flds[];
extern FMField rpc_request_flds[];
extern FMStructDescRec response_format_list[];
extern FMStructDescRec request_format_list[];

extern void
do_conversion_setup ARGS((FMContext iocontext, void* buffer, CManager cm,
			  CMConnection conn));

extern void 
rpc_info_init ARGS((CManager cm));

extern rpc_info_ptr
get_rpc_info ARGS((const char* rpc_name, int make_new));

/*
 *
 *  $Log: not supported by cvs2svn $
 *  Revision 1.15  2004/10/14 21:16:15  pmw
 *  Added doxygen documentation.
 *  Also changed prefix of everything from CM_RPC to CMrpc.  It looks better.
 *
 *  Revision 1.14  2004/05/06 17:55:04  eisen
 *  Tweak CMRPC so that it works with CMSelfFormats set.  This required some
 *  extensions to CM, and a bit of rearranging.
 *
 *  Revision 1.13  2002/08/15 03:55:13  pmw
 *  fixed MT problem where async calls had their connection whacked out
 *  from underneath them, causing their condition to be marked failed
 *  before any tickets corresponding to them were redeemed.
 *
 *  also cleaned up undeclared function warnings in tclHash.c.
 *
 *  also changed handling of attr_lists in request.c to eliminate
 *  attr_add_list call.
 *
 *  Revision 1.12  2002/05/27 07:44:55  pmw
 *  const correctness for C compilers sucks.  Especially when I use so much code from
 *  other people that isn't const correct :)
 *
 *  Revision 1.11  2000/10/16 03:45:36  pmw
 *  Added oneway RPC calls.  This has been tested somewhat.
 *
 *  Revision 1.10  2000/10/16 02:31:22  pmw
 *  1 - fixed gotcha-in-waiting concerning IOcontext management and anonymous
 *  calls.  The IOcontext allocated for anon calls was getting freed before the
 *  user ever saw the result structure from the call, and the underlying PBIO
 *  buffer was suspect.  This meant revising the anon-call interface back to
 *  where the user supplies his own IOcontext values.
 *  2 - Added rpc-with-channel functionality, not yet tested.
 *
 *  Revision 1.9  2000/09/29 20:52:07  pmw
 *  fixed lingering problems from subcontext changes.  hopefully this closes it.
 *  make check works correctly, so I'll believe it for the moment.
 *
 *  Revision 1.8  2000/04/10 20:28:33  pmw
 *  Added cleanup handler functionality
 *
 *  Revision 1.7  2000/04/10 18:10:15  pmw
 *  more rearrangement
 *
 *  Revision 1.6  2000/04/06 22:39:19  pmw
 *  Now does sane handling of IO conversions
 *
 *  Revision 1.5  2000/04/05 03:00:05  pmw
 *  added high-level convenience functions
 *
 *  Revision 1.4  2000/04/04 20:40:04  pmw
 *  Restructured for asyncronous calls
 *
 *  Revision 1.3  2000/04/04 19:04:51  pmw
 *  cosmetic changes, split into more source files
 *
 *  Revision 1.2  2000/04/04 18:35:30  pmw
 *  added RCS headers
 *
 *
 */

#endif /* ifndef __CMRPC_INTERNAL_H__ */
