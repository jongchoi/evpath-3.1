/*
 *
 *  $Id: users_guide.h,v 1.2 2004-10-14 22:36:18 pmw Exp $
 *
 */
/*  
 *  This file is a fake header, not included by any code compilation.  It contains 
 *  the users guide Doxygen source and other Doxygen miscellany.
 */

/** \example ts.c
The following is a program which, when run as a server, establishes a simple RPC handler, and when run as client sets up and calls that RPC.  Note the use of CMlisten_specific on the server-side to force CM to bind to a particular IP port. 
*/

/*! \mainpage CMrpc Documentation and API Reference

Eventually there will be some more information here.  For now, the API reference is available above.

*/

/*
 *
 *  $Log: not supported by cvs2svn $
 *
 */
