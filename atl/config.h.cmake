/* config.h.in.  Generated from configure.ac by autoheader.  */

/* Define with the name of the host which will run the atom server */
#cmakedefine ATOM_SERVER_HOST "@ATOM_SERVER_HOST@"

/* Define with the name of the domain which the atom server will service */
#cmakedefine ATOM_SERVICE_DOMAIN

/* Define to 1 if you have the <cercs_env.h> header file. */
#cmakedefine HAVE_CERCS_ENV_H

/* Define to 1 if you have the `fork' function. */
#cmakedefine HAVE_FORK

/* Define to 1 if you have the <gen_thread.h> header file. */
#cmakedefine HAVE_GEN_THREAD_H

/* Define to 1 if you have the <malloc.h> header file. */
#cmakedefine HAVE_MALLOC_H

/* Define to 1 if you have the <memory.h> header file. */
#cmakedefine HAVE_MEMORY_H

/* Define to 1 if you have the <stdint.h> header file. */
#cmakedefine HAVE_STDINT_H

/* Define to 1 if you have the <stdlib.h> header file. */
#cmakedefine HAVE_STDLIB_H

/* Define to 1 if you have the <strings.h> header file. */
#cmakedefine HAVE_STRINGS_H

/* Define to 1 if you have the <string.h> header file. */
#cmakedefine HAVE_STRING_H

/* Define to 1 if you have the <sys/stat.h> header file. */
#cmakedefine HAVE_SYS_STAT_H

/* Define to 1 if you have the <sys/time.h> header file. */
#cmakedefine HAVE_SYS_TIME_H

/* Define to 1 if you have the <sys/types.h> header file. */
#cmakedefine HAVE_SYS_TYPES_H

/* Define to 1 if you have the <unistd.h> header file. */
#cmakedefine HAVE_UNISTD_H

/* Define to 1 if you have the <windows.h> header file. */
#cmakedefine HAVE_WINDOWS_H

/* Define if compiling for linux kernel */
#cmakedefine LINUX_KERNEL_MODULE

/* The number of bytes in type double */
#cmakedefine SIZEOF_DOUBLE @SIZEOF_DOUBLE@

/* The number of bytes in type float */
#cmakedefine SIZEOF_FLOAT @SIZEOF_FLOAT@

/* The number of bytes in type int */
#cmakedefine SIZEOF_INT @SIZEOF_INT@

/* The number of bytes in type short */
#cmakedefine SIZEOF_SHORT @SIZEOF_SHORT@
