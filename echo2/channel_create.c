#ifndef DOC

#include "config.h"
#include "ffs.h"
#include "atl.h"
#include "evpath.h"
#include "echo2.h"

#include <stdio.h>
#include <stdlib.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef HAVE_WINDOWS_H
#include <windows.h>
#endif
#endif
FMField simple_field_list[] = {
    {"entry", "integer", sizeof(long), 0},
    {NULL, NULL, 0, 0}};

int main()
{   /* this program creates an event channel */
    EChannel chan;
    CManager cm;
    EControlContext cc;

    cm = CManager_create();
    cc = ECho_CM_init(cm);

    chan = EChannel_typed_create(cc, simple_field_list, NULL);
    printf("Channel ID is: %s\n", ECglobal_id(chan));
    CMsleep(cm, 600);   /* handle net for 10 min */
    return 1;
}
