#include "config.h"

#define FUNCPROTO
#include "ffs.h"
#ifdef HAVE_GEN_THREAD_H
#include "gen_thread.h"
#else
#define gen_thr_initialized() 0
#define thr_mutex_lock(m)
#define thr_mutex_unlock(m)
#define thr_mutex_alloc() 0
#define thr_mutex_free(m)
#define thr_condition_wait(c, m)
#define thr_condition_signal(c)
#define thr_condition_alloc() 0
#define thr_condition_free(c)
#define thr_mutex_t void*
#define thr_condition_t void*
#endif
#include "atl.h"
#include "evpath.h"
#ifdef HAVE_MALLOC_H
#include <malloc.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <string.h>
#ifdef HAVE_STDARG_H
#include <stdarg.h>
#else
#include <varargs.h>
#endif
#ifdef HAVE_NETDB_H
#include <netdb.h>
#endif
#ifdef HAVE_WINDOWS_H
#include <windows.h>
#else
#include <netinet/in.h>
#include <arpa/inet.h>
#endif
#ifndef PRINTF_DEFINED
extern int printf ARGS((const char *, ...));
#endif
#ifndef FPRINTF_DEFINED
extern int fprintf ARGS((FILE *, const char *, ...));
#endif
#ifndef SPRINTF_DEFINED
extern int sprintf ARGS((char *, const char *, ...));
#endif
#ifndef SSCANF_DEFINED
extern int sscanf ARGS((const char *, const char *, ...));
#endif

#include "cercs_env.h"
#include "echo2.h"
#include "echo_internal.h"

FMField channel_id_flds[] = {
    {"len", "integer", sizeof(int), FMOffset(channel_ID_struct *, len)},
    {"id", "char[len]", 1, FMOffset(channel_ID_struct *, channel)},
    {(char *) 0, (char *) 0, 0, 0}
};

FMField channel_attend_msg_flds[] = {
    {"channel", "string", sizeof(char *),
     FMOffset(AttendMsgPtr, chan_str)},
    {"contact_str", "string", sizeof(char *), FMOffset(AttendMsgPtr,
						       contact_str)},
    {"condition", "integer", sizeof(int), FMOffset(AttendMsgPtr, cond)},
    {"client_channel_ID", "channel_ID", sizeof(channel_ID_struct),
     FMOffset(AttendMsgPtr, client_channel_id)},
    {"incoming_target_stone_id", "integer", sizeof(EVstone),
     FMOffset(AttendMsgPtr, incoming_target_stone_id)},
    {"proto_version", "integer", sizeof(int),
     FMOffset(AttendMsgPtr, proto_version)},
    {(char *) 0, (char *) 0, 0, 0}
};

FMStructDescRec channel_attend_msg_formats[] = {
    {"Channel Attend", channel_attend_msg_flds, sizeof(AttendMsg), NULL},
    {"channel_ID", channel_id_flds, sizeof(channel_ID_struct), NULL},
    {NULL, NULL, 0, NULL}
};
    
FMStructDescRec attend_response_format_list[];
FMStructDescRec old_attend_response_format_list[];

FMField old_channel_attend_msg_flds[] = {
    {"channel", "string", sizeof(char *),
     FMOffset(AttendMsgPtr, chan_str)},
    {"contact_str", "string", sizeof(char *), FMOffset(AttendMsgPtr,
						       contact_str)},
    {"condition", "integer", sizeof(int), FMOffset(AttendMsgPtr, cond)},
    {"client_channel_ID", "channel_ID", sizeof(channel_ID_struct),
     FMOffset(AttendMsgPtr, client_channel_id)},
    {(char *) 0, (char *) 0, 0, 0}
};

FMStructDescRec old_channel_attend_msg_formats[] = {
    {"Channel Attend", old_channel_attend_msg_flds, sizeof(AttendMsg), NULL},
    {"channel_ID", channel_id_flds, sizeof(channel_ID_struct), NULL},
    {NULL, NULL, 0, NULL}
};
    
FMField sink_subscribe_msg_flds[] = {
    {"global_channel_id", "channel_ID", sizeof(channel_ID_struct),
     FMOffset(SinkSubscribeMsgPtr, global_channel_id)},
    {"client_channel_id", "channel_ID", sizeof(channel_ID_struct),
     FMOffset(SinkSubscribeMsgPtr, client_channel_id)},
    {"client_contact_str", "string", sizeof(char *),
     FMOffset(SinkSubscribeMsgPtr, client_contact_str)},
    {(char *) 0, (char *) 0, 0, 0}
};

FMStructDescRec sink_subscribe_msg_formats[] = {
    {"Sink Subscribe", sink_subscribe_msg_flds, sizeof(SinkSubscribeMsg), NULL},
    {"channel_ID", channel_id_flds, sizeof(channel_ID_struct), NULL},
    {NULL, NULL, 0, NULL}
};
    
FMField sink_unsubscribe_msg_flds[] = {
    {"global_channel_id", "channel_ID", sizeof(channel_ID_struct),
     FMOffset(SinkSubscribeMsgPtr, global_channel_id)},
    {"client_channel_id", "channel_ID", sizeof(channel_ID_struct),
     FMOffset(SinkSubscribeMsgPtr, client_channel_id)},
    {(char *) 0, (char *) 0, 0, 0}
};

FMStructDescRec sink_unsubscribe_msg_formats[] = {
    {"Sink Unsubscribe", sink_unsubscribe_msg_flds, sizeof(SinkSubscribeMsg), NULL},
    {"channel_ID", channel_id_flds, sizeof(channel_ID_struct), NULL},
    {NULL, NULL, 0, NULL}
};
    
/* basically the same but still ... */

FMField source_subscribe_msg_flds[] = {
    {"global_channel", "channel_ID", sizeof(channel_ID_struct),
     FMOffset(SourceSubscribeMsgPtr, global_channel_id)},
    {"client_channel", "channel_ID", sizeof(channel_ID_struct),
     FMOffset(SourceSubscribeMsgPtr, client_channel_id)},
    {"client_contact_str", "string", sizeof(char *),
     FMOffset(SourceSubscribeMsgPtr, client_contact_str)},
    {(char *) 0, (char *) 0, 0, 0}
};

FMStructDescRec source_subscribe_msg_formats[] = {
    {"Source Subscribe", source_subscribe_msg_flds, sizeof(SourceSubscribeMsg), NULL},
    {"channel_ID", channel_id_flds, sizeof(channel_ID_struct), NULL},
    {NULL, NULL, 0, NULL}
};
    
FMField source_unsubscribe_msg_flds[] = {
    {"global_channel", "channel_ID", sizeof(channel_ID_struct),
     FMOffset(SourceSubscribeMsgPtr, global_channel_id)},
    {"client_channel", "channel_ID", sizeof(channel_ID_struct),
     FMOffset(SourceSubscribeMsgPtr, client_channel_id)},
    {(char *) 0, (char *) 0, 0, 0}
};

FMStructDescRec source_unsubscribe_msg_formats[] = {
    {"Source Unsubscribe", source_unsubscribe_msg_flds, sizeof(SourceSubscribeMsg), NULL},
    {"channel_ID", channel_id_flds, sizeof(channel_ID_struct), NULL},
    {NULL, NULL, 0, NULL}
};
    
FMField channel_exists_attend_response_msg_flds[] = {
    {"condition", "integer", sizeof(int),
     FMOffset(ExistsAttendResponseMsgPtr, condition)},
    {"existing_channel_id", "channel_ID", sizeof(channel_ID_struct),
     FMOffset(ExistsAttendResponseMsgPtr, existing_channel_id)},
    {(char *) 0, (char *) 0, 0, 0}
};

FMStructDescRec channel_exists_attend_response_msg_formats[] = {
    {"Channel Exists Attend Response", channel_exists_attend_response_msg_flds, sizeof(ExistsAttendResponseMsg), NULL},
    {"channel_ID", channel_id_flds, sizeof(channel_ID_struct), NULL},
    {NULL, NULL, 0, NULL}
};
    
FMField AttendRec_flds[] = {
    {"contact_string", "string", sizeof(char *),
     FMOffset(AttendRecPtr, contact_string)},
    {"client_channel_id", "channel_ID", sizeof(channel_ID_struct),
     FMOffset(AttendRecPtr, client_channel_id)}
    ,
    {"filter", "string", sizeof(char *), FMOffset(AttendRecPtr, filter)},
    {"filter_has_output_param", "integer", sizeof(int),
     FMOffset(AttendRecPtr, filter_has_output_param)},
    {(char *) 0, (char *) 0, 0, 0}
};

FMField MemberRec_flds[] = {
    {"contact_string", "string", sizeof(char *),
     FMOffset(MemberRecPtr, contact_string)},
    {"event_contact_string", "string", sizeof(char *),
     FMOffset(MemberRecPtr, event_contact_string)},
    {"client_target_stone_id", "integer", sizeof(int),
     FMOffset(MemberRecPtr, client_target_stone_id)},
    {"client_channel_id", "channel_ID", sizeof(channel_ID_struct),
     FMOffset(MemberRecPtr, client_channel_id)},
    {"has_sources", "char", sizeof(char),
     FMOffset(MemberRecPtr, has_sources)},
    {"has_sinks", "char", sizeof(char),
     FMOffset(MemberRecPtr, has_sinks)},
    {(char *) 0, (char *) 0, 0, 0}
};

FMField channel_attend_response_msg_flds[] = {
    {"condition", "integer", sizeof(int),
     FMOffset(AttendResponseMsgPtr, condition)},
    {"global_channel_id", "channel_ID", sizeof(channel_ID_struct),
     FMOffset(AttendResponseMsgPtr, global_channel_id)},
    {"type_name", "string", sizeof(char *),
     FMOffset(AttendResponseMsgPtr, type_name)},
    {"create_attrs_string", "string", sizeof(char *),
     FMOffset(AttendResponseMsgPtr, create_attrs_string)},
    {"filter_data_len", "integer", sizeof(int),
     FMOffset(AttendResponseMsgPtr, filter_data_len)},
    {"filter_data", "char[filter_data_len]", 1,
     FMOffset(AttendResponseMsgPtr, filter_data)},
    {"member_count", "integer", sizeof(int),
     FMOffset(AttendResponseMsgPtr, member_count)},
    {"members", "MemberRec[member_count]", sizeof(MemberRec),
     FMOffset(AttendResponseMsgPtr, members)}
    ,
    {"der_count", "integer", sizeof(int),
     FMOffset(AttendResponseMsgPtr, der_count)},
    {"derivatives", "AttendRec[der_count]", sizeof(AttendRec),
     FMOffset(AttendResponseMsgPtr, derivatives)}
    ,
    {(char *) 0, (char *) 0, 0, 0}
};

FMField old_channel_attend_response_msg_flds[] = {
    {"condition", "integer", sizeof(int),
     FMOffset(OldAttendResponseMsgPtr, condition)},
    {"global_channel_id", "channel_ID", sizeof(channel_ID_struct),
     FMOffset(OldAttendResponseMsgPtr, global_channel_id)},
    {"type_name", "string", sizeof(char *),
     FMOffset(OldAttendResponseMsgPtr, type_name)},
    {"sink_count", "integer", sizeof(int),
     FMOffset(OldAttendResponseMsgPtr, sink_count)},
    {"sinks", "AttendRec[sink_count]", sizeof(AttendRec),
     FMOffset(OldAttendResponseMsgPtr, sinks)},
    {"source_count", "integer", sizeof(int),
     FMOffset(OldAttendResponseMsgPtr, source_count)},
    {"sources", "AttendRec[source_count]", sizeof(AttendRec),
     FMOffset(OldAttendResponseMsgPtr, sources)}
    ,
    {"member_count", "integer", sizeof(int),
     FMOffset(OldAttendResponseMsgPtr, member_count)},
    {"members", "AttendRec[member_count]", sizeof(AttendRec),
     FMOffset(OldAttendResponseMsgPtr, members)}
    ,
    {"der_count", "integer", sizeof(int),
     FMOffset(OldAttendResponseMsgPtr, der_count)},
    {"derivatives", "AttendRec[der_count]", sizeof(AttendRec),
     FMOffset(OldAttendResponseMsgPtr, derivatives)}
    ,
    {(char *) 0, (char *) 0, 0, 0}
};

FMField member_subscribe_msg_flds[] = {
    {"global_channel_id", "channel_ID", sizeof(channel_ID_struct),
     FMOffset(MemberSubscribeMsgPtr, global_channel_id)},
    {"client_channel_id", "channel_ID", sizeof(channel_ID_struct),
     FMOffset(MemberSubscribeMsgPtr, client_channel_id)},
    {"client_contact_str", "string", sizeof(char *),
     FMOffset(MemberSubscribeMsgPtr, client_contact_str)},
    {"client_target_stone_id", "integer", sizeof(int),
     FMOffset(MemberSubscribeMsgPtr, client_target_stone_id)},
    {"event_contact_str", "string", sizeof(char *),
     FMOffset(MemberSubscribeMsgPtr, event_contact_str)},
    {(char *) 0, (char *) 0, 0, 0}
};


FMStructDescRec member_subscribe_msg_formats[] = {
    {"Member Subscribe", member_subscribe_msg_flds, sizeof(MemberSubscribeMsg), NULL},
    {"channel_ID", channel_id_flds, sizeof(channel_ID_struct), NULL},
    {NULL, NULL, 0, NULL}
};
    
FMField member_unsubscribe_msg_flds[] = {
    {"global_channel_id", "channel_ID", sizeof(channel_ID_struct),
     FMOffset(MemberUnsubscribeMsgPtr, global_channel_id)},
    {"client_channel_id", "channel_ID", sizeof(channel_ID_struct),
     FMOffset(MemberUnsubscribeMsgPtr, client_channel_id)},
    {(char *) 0, (char *) 0, 0, 0}
};


FMField event_msg_flds[] = {
    {"global_channel_id", "channel_ID", sizeof(channel_ID_struct),
     FMOffset(EventMsgPtr, global_channel_id)}
    ,
    {"event_len", "integer", sizeof(int),
     FMOffset(EventMsgPtr, event_len)},
    {"event", "char[event_len]", sizeof(char),
     FMOffset(EventMsgPtr, event)},
    {(char *) 0, (char *) 0, 0, 0}
};

FMStructDescRec event_msg_formats[] = {
    {"Event Message", event_msg_flds, sizeof(EventMsg), NULL},
    {"channel_ID", channel_id_flds, sizeof(channel_ID_struct), NULL},
    {NULL, NULL, 0, NULL}
};
    
FMField request_event_msg_flds[] = {
    {"global_channel_id", "channel_ID", sizeof(channel_ID_struct),
     FMOffset(RequestEventMsgPtr, global_channel_id)}
    ,
    {(char *) 0, (char *) 0, 0, 0}
};

FMStructDescRec request_event_msg_formats[] = {
    {"Request Event", request_event_msg_flds, sizeof(RequestEventMsg), NULL},
    {"channel_ID", channel_id_flds, sizeof(channel_ID_struct), NULL},
    {NULL, NULL, 0, NULL}
};
    
FMField event_vec_elem_flds[] = {
    {"len", "integer", sizeof(((FFSEncodeVector) 0)[0].iov_len),
     FMOffset(FFSEncodeVector, iov_len)}
    ,
    {"elem", "char[len]", sizeof(char),
     FMOffset(FFSEncodeVector, iov_base)},
    {(char *) 0, (char *) 0, 0, 0}
};

FMField eventv_msg_flds[] = {
    {"global_channel_id", "channel_ID", sizeof(channel_ID_struct),
     FMOffset(EventVMsgPtr, global_channel_id)},
    {"eventv_len", "integer", sizeof(int),
     FMOffset(EventVMsgPtr, eventv_len)},
    {"eventv", "FFSEncodeElem[eventv_len]", sizeof(struct FFSEncodeVec),
     FMOffset(EventVMsgPtr, eventv)},
    {(char *) 0, (char *) 0, 0, 0}
};

FMStructDescRec eventv_msg_formats[] = {
    {"EventV Message", eventv_msg_flds, sizeof(EventVMsg), NULL},
    {"FFSEncodeElem", event_vec_elem_flds, sizeof(struct FFSEncodeVec), NULL},
    {"channel_ID", channel_id_flds, sizeof(channel_ID_struct), NULL},
    {NULL, NULL, 0, NULL}
};
    
static event_queue dequeue_event ARGS((EControlContext ec));
static void internal_cancel_source_subscribe ARGS((ECSourceHandle handle));

int channel_verbose = -1;
int channel_lock_verbose = 0;
static atom_t ECHO_EVENT_TRANSPORT = -1;
static atom_t ECHO_EVENT_NETWORK = -1;
static atom_t ECHO_USE_EVENT_TRANSPORT = -1;
static atom_t CM_TRANSPORT = -1;
static atom_t CM_NETWORK_POSTFIX = -1;
static atom_t CM_BW_MEASURE_INTERVAL = -1;

/* trace_out - for debuging and tracing */
/* prints the value of function string, formats the output by */
/* indenting first if direction is a 2, and un-indenting if */
/* direction is a 0, if direction is a 1, indenting is increased */
/* for the traces that follow. */
extern void
ECtrace_out(char *format, int direction, ...)
{
    va_list ap;
    static int tabstop = 0;
    int i;

    if (channel_verbose == -1) {
	char *verb = getenv("CHANNEL_VERBOSE");
	channel_verbose = 0;
	if (verb) {
	    if (sscanf(verb, "%d", &channel_verbose) != 1) {
		/* enable at lowest level */
		channel_verbose = 1;
	    }
	}
	verb = getenv("CHANNEL_LOCK_VERBOSE");
	if (verb) {
	    channel_lock_verbose = 1;
	}
    }

    if (channel_verbose) {
#ifdef HAVE_STDARG_H
	va_start(ap, direction);
#else
	va_start(ap);
#endif
	if (direction == 0)
	    tabstop--;
	if (direction == 2)
	    tabstop++;
	for (i = 0; i < tabstop; i++)
	    printf("  ");
	vfprintf(stdout, format, ap);
	va_end(ap);
	switch (direction) {
	case 0:
	    printf(" stop");
	    break;
	case 1:
	    printf(" start");
	    tabstop++;
	    break;
	case 2:
	    tabstop--;
	    break;
	}
	printf("\n");
    }
}

extern void
dump_EChannel(chan)
EChannel chan;
{
    char *ID = ECglobal_id(chan);
    int i;

    printf("Local Channel %016lx\n", (long) chan);
    printf("    Sink count = %d, Source count = %d\n", chan->sink_count,
	   chan->source_count);
    printf("    full contact string = %s\n", ID);
    CMfree(ID);
    printf("Incoming stone ID %d, outgoing stone ID %d\n",
	   chan->incoming_target_stone_id, chan->outgoing_target_stone_id);
    printf("    global ID = ");
    print_channel_id(chan->global_id.global_channel_id);
    printf("\n");
/*    if (chan->type_format) {
	char *format_name = global_name_of_IOformat(chan->type_format);
	printf("	type = %s\n", format_name);
	free(format_name);
	}*/
    printf("    %d remote members\n", chan->rem_member_list_len);
    for (i = 0; i < chan->rem_member_list_len; i++) {
	printf("	%d) - channel ", i);
	print_channel_id(chan->rem_member_list[i].remote_id);
	printf(", %s->stone(%d), %lx %s %s\n",
	       chan->rem_member_list[i].contact_string,
	       chan->rem_member_list[i].remote_target_stone_id,
	       (long) chan->rem_member_list[i].conn,
	       chan->rem_member_list[i].has_sinks ? "Sinks" : "",
	       chan->rem_member_list[i].has_sources ? "Sources" : "");
	if (chan->rem_member_list[i].event_conn) {
	    printf("	Using event conn ->");
	    dump_attr_list(CMConnection_get_attrs
			   (chan->rem_member_list[i].event_conn));
	}
    }
    printf("    %d local sinks\n", chan->local_sink_list_len);
    for (i = 0; i < chan->local_sink_list_len; i++) {
	printf
	    ("	%d) - func %p, data %p, multi_sink %p, ec %p, context %p, condition %d\n",
	     i, chan->local_sink_list[i].func,
	     chan->local_sink_list[i].client_data,
	     chan->local_sink_list[i].multi_sink_handle,
	     chan->local_sink_list[i].ec,
	     (void*)0,
	     (int) chan->local_sink_list[i].cond);
    }
    printf("    %d local sources\n", chan->local_source_list_len);
    for (i = 0; i < chan->local_source_list_len; i++) {
	printf("	%d) -  func %lx, data %lx, ioformat %lx", i,
	       (long) chan->local_source_list[i].func,
	       (long) chan->local_source_list[i].client_data,
	       (long) chan->local_source_list[i].source_handle);
    }
    printf("    %d derived channels\n", chan->der_list_len);
    for (i = 0; i < chan->der_list_len; i++) {
	printf("    derived event channel %d\n", i);
	printf("    ========================\n");
	printf("	filter = %s\n", chan->der_chan_list[i].filter);
	printf("	has_output_param = %d\n",
	       chan->der_chan_list[i].has_output_param);
	dump_EChannel(chan->der_chan_list[i].derived_chan);
    }
}


static void
return_free_block(block)
free_block_rec_p block;
{
    block->next = block->locking_context->free_block_list;
    block->locking_context->free_block_list = block;
}

static free_block_rec_p
alloc_free_block(ec)
EControlContext ec;
{
    if (ec->free_block_list == NULL) {
	return (free_block_rec_p) CMmalloc(sizeof(struct free_block_rec));
    } else {
	free_block_rec_p tmp = ec->free_block_list;
	ec->free_block_list = tmp->next;
	return tmp;
    }
}

static free_block_rec_p
get_free_block_func(locking_context, block, free_func)
EControlContext locking_context;
void *block;
EventFreeFunction free_func;
{
    free_block_rec_p block_rec = alloc_free_block(locking_context);
    block_rec->locking_context = locking_context;
    block_rec->ref_count = 1;
    block_rec->ec = block_rec->free_arg = NULL;
    block_rec->free_func = free_func;
    block_rec->block = block;
    block_rec->attrs = NULL;
    return block_rec;
}

static void
deref_block(block)
free_block_rec_p block;
{
    block->ref_count--;
    if (block->ref_count == 0) {
	if (block->ec) {
	    CMreturn_buffer(block->ec->cm, block->free_arg);
/*	} else if (block->iobuffer != NULL) {
	free_IOBuffer(block->iobuffer);*/
	} else if (block->free_func != (EventFreeFunction) NULL) {
/*	    if (block->ioformat != NULL) {
		IOFormat ioformat = block->ioformat;
		IOfree_var_rec_elements(iofile_of_IOformat(ioformat),
					ioformat, block->block);
					}*/
	    block->free_func(block->block);
	} else if (block->block != NULL) {
	    CMfree(block->block);
	}
	if (block->attrs) {
	    free_attr_list(block->attrs);
	}
	return_free_block(block);
    }
}

static ECSourceHandle
new_source_handle()
{
    ECSourceHandle handle = CMmalloc(sizeof(*handle));
    handle->chan = NULL;
    handle->source_num = 0;
    handle->der_list_len = 0;
    handle->der_source_list = NULL;
    return handle;
}

extern void
IntEChannel_lock(chan, file, line)
EChannel chan;
char *file;
int line;
{
    if (!gen_thr_initialized())
	return;

    if (channel_lock_verbose) {
	printf("Trying to lock channel %lx at %s line %d\n", (long) chan,
	       file, line);
    }
    if (chan->lock) {
	thr_mutex_lock(chan->lock);
    }
    if (channel_lock_verbose) {
	printf("Locked channel %lx at %s line %d\n", (long) chan, file,
	       line);
    }
}

extern void
IntEChannel_unlock(chan, file, line)
EChannel chan;
char *file;
int line;
{
    if (!gen_thr_initialized())
	return;

    if (channel_lock_verbose) {
	printf("Unlocking channel %lx at %s line %d\n", (long) chan, file,
	       line);
    }
    if (chan->lock) {
	thr_mutex_unlock(chan->lock);
    }
}

extern void
IntEContext_lock(cc, file, line)
EControlContext cc;
char *file;
int line;
{
    if (!gen_thr_initialized())
	return;

    if (channel_lock_verbose) {
	printf("Trying to lock context %lx at %s line %d\n", (long) cc,
	       file, line);
    }
    if (cc->lock) {
	thr_mutex_lock(cc->lock);
    }
    if (channel_lock_verbose) {
	printf("Locked context %lx at %s line %d\n", (long) cc, file,
	       line);
    }
}

extern void
IntEContext_unlock(cc, file, line)
EControlContext cc;
char *file;
int line;
{
    if (!gen_thr_initialized())
	return;

    if (channel_lock_verbose) {
	printf("Unlocking context %lx at %s line %d\n", (long) cc, file,
	       line);
    }
    if (cc->lock) {
	thr_mutex_unlock(cc->lock);
    }
}

extern EChannel
string_to_channel_ptr(char *chan_id)
{
    unsigned char chan_char[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };
    unsigned int chan_int[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };
    EChannel chan;

    sscanf(chan_id, "%02x%02x%02x%02x%02x%02x%02x%02x",
	   &chan_int[0], &chan_int[1], &chan_int[2], &chan_int[3],
	   &chan_int[4], &chan_int[5], &chan_int[6], &chan_int[7]);

    chan_char[0] = (unsigned char) chan_int[0];
    chan_char[1] = (unsigned char) chan_int[1];
    chan_char[2] = (unsigned char) chan_int[2];
    chan_char[3] = (unsigned char) chan_int[3];
    chan_char[4] = (unsigned char) chan_int[4];
    chan_char[5] = (unsigned char) chan_int[5];
    chan_char[6] = (unsigned char) chan_int[6];
    chan_char[7] = (unsigned char) chan_int[7];
    memcpy(&chan, chan_char, sizeof(chan));
    return chan;
}

extern char *
channel_ptr_to_string(EChannel chan)
{
    char *chan_str = CMmalloc(17);
    unsigned char chan_char[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };
    memset(chan_char, 0, sizeof(chan_char));
    memcpy(chan_char, &chan, sizeof(chan));
    sprintf(chan_str, "%02x%02x%02x%02x%02x%02x%02x%02x",
	    (unsigned int) chan_char[0],
	    (unsigned int) chan_char[1],
	    (unsigned int) chan_char[2],
	    (unsigned int) chan_char[3],
	    (unsigned int) chan_char[4],
	    (unsigned int) chan_char[5],
	    (unsigned int) chan_char[6], (unsigned int) chan_char[7]);
    return chan_str;
}

extern EChannel
EChannel_typed_create(cc, field_list, format_list)
EControlContext cc;
FMFieldList field_list;
CMFormatList format_list;
{
    return EChannel_typed_create_attr(cc, field_list, format_list, NULL);
}

extern EChannel
EChannel_typed_create_attr(cc, field_list, format_list, attrs)
EControlContext cc;
FMFieldList field_list;
CMFormatList format_list;
attr_list attrs;
{
    EChannel chan = EChannel_create_attr(cc, attrs);
    
/*    chan->type_format =
	CMregister_user_format(cc->cm, chan->subcontext, format_name,
	field_list, format_list);*/
    
    chan->ev_format_list = create_format_list(field_list, format_list, 1);
    return chan;
}

static void
set_attr_info(EChannel chan, CManager cm, attr_list attrs)
{
    attr_list contact_list, old_contact_list, event_contact_list = NULL;
    if (ECHO_EVENT_TRANSPORT == -1) {
	ECHO_EVENT_TRANSPORT = attr_atom_from_string("ECHO_EVENT_TRANSPORT");
	ECHO_USE_EVENT_TRANSPORT = attr_atom_from_string("ECHO_EVENT_TRANSPORT");
	ECHO_EVENT_NETWORK = attr_atom_from_string("ECHO_EVENT_NETWORK");
	CM_TRANSPORT = attr_atom_from_string("CM_TRANSPORT");
	CM_NETWORK_POSTFIX = attr_atom_from_string("CM_NETWORK_POSTFIX");
	CM_BW_MEASURE_INTERVAL = attr_atom_from_string("CM_BW_MEASURE_INTERVAL");
    }
    contact_list = CMget_contact_list(cm);
    if (contact_list == NULL) {
	printf
	    ("Warning, no contact list.  Must do CManager_listen() before using channels.\n");
	CMfree(chan);
	return;
    }
    old_contact_list = contact_list;
    contact_list = attr_copy_list(contact_list);
    free_attr_list(old_contact_list);
    attr_merge_lists(contact_list, attrs);
    if (attrs) {
	char *event_transport = NULL;
	char *event_network = NULL;
	attr_list event_attr_list = NULL;
	if (get_string_attr(attrs, ECHO_EVENT_TRANSPORT, &event_transport)) {
	    if (event_attr_list == NULL)
		event_attr_list = create_attr_list();
	    set_string_attr(event_attr_list, CM_TRANSPORT, 
			    strdup(event_transport));
	}
	if (get_string_attr(attrs, ECHO_EVENT_NETWORK, &event_network)) {
	    if (event_attr_list == NULL)
		event_attr_list = create_attr_list();
	    set_string_attr(event_attr_list, CM_NETWORK_POSTFIX, 
			    strdup(event_network));
	}
	if (channel_verbose) {
	    printf("Getting CMget_specific_contact_list ");
	    dump_attr_list(event_attr_list);
	}
	event_contact_list =
	    CMget_specific_contact_list(cm, event_attr_list);
	if (channel_verbose) {
	    printf("resulting event contact list is ");
	    if (event_contact_list)
		dump_attr_list(event_contact_list);
	    else
		printf("\n");
	}
    }
    if (chan->global_id.contact_string)
	free(chan->global_id.contact_string);
    chan->global_id.contact_string = attr_list_to_string(contact_list);
    if (chan->local_contact_string)
	free(chan->local_contact_string);
    chan->local_contact_string = attr_list_to_string(contact_list);
    if (chan->create_attrs_string)
	free(chan->create_attrs_string);
    chan->create_attrs_string = attr_list_to_string(attrs);
    if (chan->event_contact_string)
	free(chan->event_contact_string);
    if (event_contact_list) {
	chan->event_contact_string =
	    attr_list_to_string(event_contact_list);
    } else {
	chan->event_contact_string = NULL;
    }
    free_attr_list(contact_list);
}

extern EChannel
EChannel_create_attr(ec, attrs)
EControlContext ec;
attr_list attrs;
{
    CManager cm = ec->cm;
    EChannel chan = CMmalloc(sizeof(EChannel_struct));

    ECtrace_out("EChannel_create", 1);

    memset(chan, 0, sizeof(EChannel_struct));
    chan->sink_count = 0;
    chan->source_count = 0;

    chan->rem_member_list_len = 0;
    chan->rem_member_list = CMmalloc(sizeof(struct _rem_member_entry));
    chan->local_sink_list_len = 0;
    chan->local_sink_list = CMmalloc(sizeof(struct _local_sink));
    chan->local_source_list_len = 0;
    chan->local_source_list = CMmalloc(sizeof(struct _local_source));

    chan->der_list_len = 0;
    chan->der_chan_list = CMmalloc(sizeof(struct _derived_chan));
    chan->parent_list_len = 0;
    chan->parent_chan_list = CMmalloc(sizeof(EChannel));

    set_channel_id(&chan->global_id.global_channel_id, chan);
    chan->global_id.contact_string = NULL;
    chan->local_contact_string = NULL;
    chan->create_attrs_string = NULL;
    chan->event_contact_string = NULL;
    set_attr_info(chan, cm, attrs);

    chan->lock = thr_mutex_alloc();
    chan->ready = 1;
    chan->subscribe_func = (ECSubscribeFunction) NULL;

    if (ec->channel_list == NULL) {
	ec->channel_list = CMmalloc(sizeof(EChannel));
    } else {
	ec->channel_list =
	    (EChannel *) CMrealloc(ec->channel_list,
				   sizeof(EChannel) * (ec->channel_count +
						       1));
    }
    ec->channel_list[ec->channel_count++] = chan;

    ECtrace_out("EChannel_create returning channel %lx", 0, chan);
    chan->filter_data = NULL;
    chan->data_field_list = NULL;

    chan->cm = cm;
    /* eventv format included as a frequently used piece of data */
    chan->eventv_format = CMlookup_format(cm, eventv_msg_formats);
    chan->ec = ec;
    chan->periodic_info = NULL;

    chan->incoming_target_stone_id = EValloc_stone(cm);
    chan->incoming_split_action = 
	EVassoc_split_action(cm, chan->incoming_target_stone_id, NULL);
    chan->outgoing_target_stone_id = EValloc_stone(cm);
    chan->outgoing_split_action = 
	EVassoc_split_action(cm, chan->outgoing_target_stone_id, NULL);
    /* outgoing events always to go local targets too */
    EVaction_add_split_target(cm, chan->outgoing_target_stone_id,
			      chan->outgoing_split_action, 
			      chan->incoming_target_stone_id);
    return chan;
}

extern EChannel
EChannel_create(ec)
EControlContext ec;
{
    return EChannel_create_attr(ec, NULL);
}

extern void
EChannel_destroy(chan)
EChannel chan;
{
    int i;
    EControlContext ec = chan->ec;

    ECtrace_out("EChannel_destroy of channel %lx", 1, chan);
    if (chan->periodic_info != NULL) {
	ECperiodic_info_free(chan->cm, chan->periodic_info);
    }
    EChannel_lock(chan);
    for (i = 0; i < chan->rem_member_list_len; i++) {
	free(chan->rem_member_list[i].contact_string);
	free(chan->rem_member_list[i].remote_id.channel);
    }
    CMfree(chan->rem_member_list);
    CMfree(chan->local_sink_list);
    CMfree(chan->local_contact_string);

    /* destroy and free the derived event channels */
    for (i = 0; i < chan->der_list_len; i++) {
	EChannel_destroy(chan->der_chan_list[i].derived_chan);
    }
    for (i = 0; i < chan->parent_list_len; i++) {
	remove_derived_channel(chan->parent_chan_list[i], chan);
    }
    CMfree(chan->parent_chan_list);

    while (chan->local_source_list_len != 0) {
	int last = chan->local_source_list_len - 1;
	internal_cancel_source_subscribe(chan->local_source_list[last].
					 source_handle);
    }
    CMfree(chan->local_source_list);

    CMfree(chan->der_chan_list);
    thr_mutex_free(chan->lock);

    chan->rem_member_list_len = 0;
    chan->ready = 0;

    for (i = 0; i < ec->channel_count; i++) {
	if (ec->channel_list[i] == chan) {
	    ec->channel_count--;
	    for (; i < (ec->channel_count); i++) {
		ec->channel_list[i] = ec->channel_list[i + 1];
	    }
	    break;
	}
    }
    if (chan->global_id.global_channel_id.channel) {
	CMfree(chan->global_id.global_channel_id.channel);
    }
    if (chan->global_id.contact_string) {
	CMfree(chan->global_id.contact_string);
    }
    /* GSE  DESTROY EV ITEMS */
    CMfree(chan);
    ECtrace_out("EChannel_destroy", 0);
}

static void send_request_event ARGS((EChannel chan, int i));

static void sendv_remote_event ARGS((EChannel chan, int i,
				     FFSEncodeVector eventV,
				     attr_list attrs));
static void queue_event_for_dispatch
ARGS((EChannel chan, int sink_index, void *event, int event_len,
      free_block_rec_p block_rec, attr_list event_attrs));
static void queue_eventv_for_dispatch
ARGS((EChannel chan, int sink_index, FFSEncodeVector eventv,
      free_block_rec_p block_rec, attr_list event_attrs));


static void send_sink_subscribe
ARGS((EChannel chan, int i, int subscribe));

static void send_source_subscribe
ARGS((EChannel chan, int i, int subscribe));

static void do_event_dispatch ARGS((EControlContext ec));

static void
    internal_submit_general_event ARGS((ECSourceHandle handle, void *event,
					int event_len,
					free_block_rec_p block_rec,
					attr_list attrs));

static void
    internal_submit_general_eventV ARGS((ECSourceHandle handle,
					 FFSEncodeVector eventV,
					 free_block_rec_p block_rec,
					 attr_list attrs));


static void wake_threaded_server ARGS((EControlContext ec));

extern void
ECrequest_event(handle, req_try)
ECSinkHandle handle;
int req_try;
{
    int i = 0;
    EChannel chan = handle->chan;
    int cond = 0;

    EChannel_lock(chan);
    ECtrace_out("In ecrequest_event, handle %lx", 1, handle);
    for (i = 0; i < chan->rem_member_list_len; i++) {
	if ((chan->rem_member_list[i].conn != NULL) &&
	    (chan->rem_member_list[i].has_sources != 0)) {
	    EChannel_unlock(chan);
	    send_request_event(chan, i);
	    EChannel_lock(chan);
	}
    }

    for (i = 0; i < chan->local_source_list_len; i++) {
	if (chan->local_source_list[i].func) {
	    chan->local_source_list[i].func(chan->local_source_list[i].
					    source_handle,
					    chan->local_source_list[i].
					    client_data);
	}
    }

    /* get condition for req try */
    if (req_try) {
	cond = CMCondition_get(chan->cm, NULL);
	chan->local_sink_list[handle->sink_num].cond = cond;
    }

    EChannel_unlock(chan);

    /* wait on it */
    if (req_try) {
	if (CMCondition_wait(chan->cm, cond)) {
	    ECtrace_out("ECrequest_event sink is ready %lx", 3, chan);
	} else {
	    /* wait failed, conn is dead - is that so ? */
	    EChannel_destroy(chan);
	    ECtrace_out("ECrequest_event, wait failed", 3);
	}
    }

    ECtrace_out("exit ecrequest_event, handle %lx", 0, handle);
}

extern void
ECsubmit_event(handle, event, event_len)
ECSourceHandle handle;
void *event;
int event_len;
{
    internal_submit_general_event(handle, event, event_len, NULL, NULL);
    do_event_dispatch(handle->chan->ec);
}

extern void
ECsubmit_typed_event(handle, event)
ECSourceHandle handle;
void *event;
{
    ECsubmit_typed_event_attr(handle, event, NULL);
}

extern void
ECsubmit_typed_event_attr(ECSourceHandle handle, void *event,
			  attr_list attrs)
{
    internal_submit_general_event(handle, event, 0, NULL, attrs);
    do_event_dispatch(handle->chan->ec);
}

extern void
ECsubmit_general_typed_event(handle, event, free_func, attrs)
ECSourceHandle handle;
void *event;
EventFreeFunction free_func;
attr_list attrs;
{
    EVsubmit_general(handle->chan->local_source_list[handle->source_num].ev_source_handle, 
		     event, (EVFreeFunction) free_func, attrs);
}

extern void
ECsubmit_general_event(handle, event, event_len, free_func, attrs)
ECSourceHandle handle;
void *event;
int event_len;
EventFreeFunction free_func;
attr_list attrs;
{
    EVsubmit_general(handle->chan->local_source_list[handle->source_num].ev_source_handle, 
	     event, (EVFreeFunction) free_func, attrs);
}

extern void
ECsubmit_general_eventV(handle, eventV, free_func)
ECSourceHandle handle;
FFSEncodeVector eventV;
EventFreeFunction free_func;
{
    free_block_rec_p block_rec;

    EContext_lock(handle->chan->ec);
    block_rec =
	get_free_block_func(handle->chan->ec, (void *) eventV, free_func);
    EContext_unlock(handle->chan->ec);

    internal_submit_general_eventV(handle, eventV, block_rec, NULL);

    EContext_lock(handle->chan->ec);
    deref_block(block_rec);
    EContext_unlock(handle->chan->ec);

    do_event_dispatch(handle->chan->ec);
}

extern void
ECsubmit_eventV(handle, eventV)
ECSourceHandle handle;
FFSEncodeVector eventV;
{
    free_block_rec_p block_rec;

    EContext_lock(handle->chan->ec);
    block_rec = get_free_block_func(handle->chan->ec, (void *) NULL, NULL);
    EContext_unlock(handle->chan->ec);

    internal_submit_general_eventV(handle, eventV, block_rec, NULL);
    EContext_lock(handle->chan->ec);
    deref_block(block_rec);
    EContext_unlock(handle->chan->ec);

    do_event_dispatch(handle->chan->ec);
}

static int
sink_valid(local_sink_ptr sink)
{
    return ((sink->func != (ECHandlerFunction) NULL) ||
	    (sink->multi_sink_handle != NULL));
}

static void
internal_submit_general_event(handle, event, event_len, block_rec, attrs)
ECSourceHandle handle;
void *event;
int event_len;
free_block_rec_p block_rec;
attr_list attrs;
{
    ECtrace_out("In ecsubmit_general_event, handle %lx, event %lx", 1,
		handle, event);
    EVsubmit(handle->chan->local_source_list[handle->source_num].ev_source_handle, 
	     event, attrs);
    ECtrace_out("exit ecsubmit_general_event, handle %lx", 0, handle);
    EChannel_unlock(handle->chan);
}

static void
internal_submit_general_eventV(handle, eventV, block_rec, attrs)
ECSourceHandle handle;
FFSEncodeVector eventV;
free_block_rec_p block_rec;
attr_list attrs;
{
    int i = 0;
    EChannel_lock(handle->chan);
    ECtrace_out("In ecsubmit_general_eventV, handle %lx, eventV %lx", 1,
		handle, eventV);
    for (i = 0; i < handle->chan->rem_member_list_len; i++) {
	if (handle->chan->rem_member_list[i].has_sinks == 0)
	    continue;
	if (handle->chan->rem_member_list[i].conn != NULL) {
	    EChannel_unlock(handle->chan);
	    sendv_remote_event(handle->chan, i, eventV, attrs);
	    EChannel_lock(handle->chan);
	}
    }
    EContext_lock(handle->chan->ec);
    for (i = 0; i < handle->chan->local_sink_list_len; i++) {
	if (sink_valid(&handle->chan->local_sink_list[i])) {
	    queue_eventv_for_dispatch(handle->chan, i, eventV, block_rec,
				      attrs);
	}
    }
    EContext_unlock(handle->chan->ec);
    for (i = 0; i < handle->chan->der_list_len; i++) {
	ECSourceHandle der_handle;
	ECtrace_out("working on derivitive", 3);
	der_handle = handle->der_source_list[i].source_handle;
	ECsubmit_eventV(der_handle, eventV);
    }
    ECtrace_out("exit ecsubmit_general_eventV, handle %lx", 0, handle);
    EChannel_unlock(handle->chan);
}

extern int
EChas_sinks(handle)
ECSourceHandle handle;
{
    int i;
    int sinks = 0;
    ECSourceHandle der_handle;
    for (i = 0; i < handle->chan->rem_member_list_len; i++) {
	if (handle->chan->rem_member_list[i].has_sinks != 0)
	    sinks++;
    }
    sinks += handle->chan->local_sink_list_len;

    if (handle->chan->der_list_len == 0) {
	return sinks;
    }
    der_handle = new_source_handle();
    /* check derived event channels */
    for (i = 0; i < handle->chan->der_list_len; i++) {
	der_handle->chan = handle->chan->der_chan_list[i].derived_chan;
	sinks += EChas_sinks(der_handle);
    }
    CMfree(der_handle);
    return sinks;
}

static CMConnection *
add_conn(CMConnection * list, CMConnection conn)
{
    int i = 0;
    if (list == NULL) {
	list = malloc(sizeof(conn) * 2);
	list[0] = conn;
	list[1] = NULL;
	return list;
    }
    while (list[i] != NULL) {
	if (list[i] == conn)
	    return list;
	i++;
    }
    /* conn is new */
    list = realloc(list, sizeof(conn) * (i + 2));
    list[i] = conn;
    list[i + 1] = NULL;
    return list;
}

extern CMConnection *
ECget_outbound_conns(handle)
ECSourceHandle handle;
{
    int i;
    CMConnection *list = NULL;
    for (i = 0; i < handle->chan->rem_member_list_len; i++) {
	if (handle->chan->rem_member_list[i].has_sinks != 0) {
	    CMConnection conn = handle->chan->rem_member_list[i].conn;
	    if (handle->chan->rem_member_list[i].event_conn != NULL) {
		conn = handle->chan->rem_member_list[i].event_conn;
	    }
	    list = add_conn(list, conn);
	}
    }
    if (handle->chan->der_list_len == 0) {
	return list;
    }
    /* check derived event channels */
    for (i = 0; i < handle->chan->der_list_len; i++) {
	EChannel chan = handle->chan->der_chan_list[i].derived_chan;
	for (i = 0; i < chan->rem_member_list_len; i++) {
	    if (chan->rem_member_list[i].has_sinks != 0) {
		CMConnection conn = chan->rem_member_list[i].conn;
		if (chan->rem_member_list[i].event_conn != NULL) {
		    conn = chan->rem_member_list[i].event_conn;
		}
		list = add_conn(list, conn);
	    }
	}
    }
    return list;
}

static int
num_local_sinks(chan)
EChannel chan;
{
    int i;
    int count = 0;
    for (i = 0; i < chan->local_sink_list_len; i++) {
	if (sink_valid(&chan->local_sink_list[i])) {
	    count++;
	}
    }
    if (chan->der_list_len != 0) {
	for (i = 0; i < chan->der_list_len; i++) {
	    count += num_local_sinks(chan->der_chan_list[i].derived_chan);
	}
    }
    return count;
}

extern int
ECQCount(ECEventQueueList queues)
{
    return queues->queue_count;
}

extern int
ECQEventCount(ECEventQueueList queues, int queue)
{
    if (queue > queues->queue_count)
	return -1;
    return queues->event_queues[queue].event_count;
}

extern void
ECEvent_remove(ECEventQueueList queues, int queue, int event_number)
{
    free_block_rec_p block;
    int queue_size;
    if (queue >= queues->queue_count)
	return;
    if (event_number >= queues->event_queues[queue].event_count)
	return;

    block = queues->event_queues[queue].queue[event_number].free_block;
    queue_size = queues->event_queues[queue].queue_size;

    memmove(&(queues->event_queues[queue].queue[event_number]),
	    &(queues->event_queues[queue].queue[event_number + 1]),
	    sizeof(struct _ECEventRec) * (queue_size - event_number - 1));

    deref_block(block);

    queues->event_queues[queue].event_count--;
}

extern void *
ECEvent_get_ptr(ECEventRec event_rec)
{
    return event_rec->event;
}

extern ECEventRec
ECQget_event_rec(ECEventQueueList queues, int queue, int event_number)
{
    if (queue >= queues->queue_count)
	return NULL;
    if (event_number >= queues->event_queues[queue].event_count)
	return NULL;
    return &(queues->event_queues[queue].queue[event_number]);
}

static ECSinkHandle
    internal_sink_subscribe ARGS((EChannel chan, EControlContext ec,
				  multi_sink_info multi_sink_handle,
				  ECHandlerFunction func,
				  void *client_data));

static int
EVTerminal_wrapper_func(CManager cm, void *message, void *client_data,
			attr_list attrs)
{
    ECSinkHandle handle = (ECSinkHandle) client_data;
    EChannel chan = handle->chan;
    ECTypedHandlerFunction func = 
	(ECTypedHandlerFunction)chan->local_sink_list[handle->sink_num].func;
    void *func_client_data =
	chan->local_sink_list[handle->sink_num].client_data;
    if (func) {
	(func)(message, func_client_data, attrs);
    } else {
	printf("ECho Event ignored, func is NULL\n");
    }
    return 1;
}

static ECSinkHandle
internal_typed_sink_subscribe(chan, ec, multi_sink_handle, field_list,
			      format_list, func, client_data)
EChannel chan;
EControlContext ec;
multi_sink_info multi_sink_handle;
FMFieldList field_list;
CMFormatList format_list;
ECTypedHandlerFunction func;
void *client_data;
{
    ECSinkHandle handle, ev_handle;
    FMStructDescList ev_format_list;
    EVstone stone_id;

/*    if (chan->type_format == NULL) {
	fprintf(stderr,
		"Typed subscribe to an untyped channel not allowed\n");
	return NULL;
    }
    if (has_conversion_IOformat(chan->type_format)) {
	type_field_list = field_list_of_IOformat(chan->type_format);

	if (!compare_field_lists(type_field_list, field_list)) {
	    fprintf(stderr,
		    "Incompatible field lists for the same incoming format in different ECtyped_sink_subscribe() calls.\n");
	    return NULL;
	}
    }

*/
    handle = internal_sink_subscribe(chan, ec, NULL,
				     (ECHandlerFunction) func,
				     client_data);
    ev_format_list = create_format_list(field_list, format_list, 0);
    stone_id = chan->local_sink_list[handle->sink_num].stone_id;
    ev_handle = chan->local_sink_list[handle->sink_num].ev_sink_rec;
    chan->local_sink_list[handle->sink_num].term_action = 
	EVassoc_terminal_action(ec->cm, stone_id, ev_format_list, 
				EVTerminal_wrapper_func, ev_handle);

/*    chan->local_sink_list[handle->sink_num].context = context =
	CMget_user_type_context(ec->cm);
    chan->local_sink_list[handle->sink_num].format =
	CMregister_user_format(ec->cm, context, format_name, field_list,
			       format_list);
*/
    chan->local_sink_list[handle->sink_num].multi_sink_handle =
	multi_sink_handle;
    chan->local_sink_list[handle->sink_num].field_list = field_list;
/*    chan->local_sink_list[handle->sink_num].format_list = format_list;*/
    CMfree(ev_format_list);
    return handle;

}

extern int struct_size_field_list(FMFieldList field_list, int pointer_size);

FMStructDescList
create_format_list(FMFieldList field_list, CMFormatList format_list, int copy)
{
    FMStructDescList ev_format_list;
    int format_count = 0, i;

    while ((format_list != NULL) && 
	   (format_list[format_count].format_name != NULL)) format_count++;
    ev_format_list = malloc(sizeof(ev_format_list[0]) * (format_count + 2));
    ev_format_list[0].format_name = "anonymous";
    if (copy) field_list = copy_field_list(field_list);
    if (copy) ev_format_list[0].format_name = strdup("anonymous");
    ev_format_list[0].field_list = field_list;
    ev_format_list[0].struct_size = struct_size_field_list(field_list, sizeof(char*));
    ev_format_list[0].opt_info = NULL;
    for (i=0; i < format_count; i++) {
	if (copy) {
	    ev_format_list[i+1].format_name =
		strdup(format_list[i].format_name);
	    ev_format_list[i+1].field_list =
		copy_field_list(format_list[i].field_list);
	} else {
	    ev_format_list[i+1].format_name = format_list[i].format_name;
	    ev_format_list[i+1].field_list = format_list[i].field_list;
	}
	ev_format_list[i+1].struct_size = struct_size_field_list(format_list[i].field_list, sizeof(char*));
	ev_format_list[i+1].opt_info = NULL;
    }
    ev_format_list[i+1].format_name = NULL;
    ev_format_list[i+1].field_list = NULL;
    ev_format_list[i+1].struct_size = 0;
    ev_format_list[i+1].opt_info = NULL;
    return ev_format_list;
}

static ECSinkHandle
internal_sink_multi_subscribe(sink_specs, ec, func, client_data)
ECMultiSubscribePtr sink_specs;
EControlContext ec;
ECMultiHandlerFunction func;
void *client_data;
{
    multi_sink_info multi_sink;
    ECEventQueueList queue_list;

    int channel_count = 0, i;
    multi_sink = malloc(sizeof(*multi_sink));
    queue_list = malloc(sizeof(*queue_list));
    while (sink_specs[channel_count].channel != NULL)
	channel_count++;

    multi_sink->func = func;
    multi_sink->client_data = client_data;
    multi_sink->queue_list = queue_list;
    queue_list->queue_count = channel_count;
    queue_list->event_queues = malloc(sizeof(queue_list->event_queues[0])
				      * channel_count);
    for (i = 0; i < channel_count; i++) {
	queue_list->event_queues[i].event_count = 0;
	queue_list->event_queues[i].queue_size = 1;
	queue_list->event_queues[i].queue =
	    malloc(sizeof(queue_list->event_queues[i].queue[0]));
	queue_list->event_queues[i].sink_handle =
	    internal_typed_sink_subscribe(sink_specs[i].channel, ec,
					  multi_sink,
					  sink_specs[i].field_list,
					  sink_specs[i].subformat_list,
					  (ECTypedHandlerFunction) NULL,
					  NULL);
    }
    return queue_list->event_queues[0].sink_handle;
}

extern ECSinkHandle
ECsink_typed_subscribe(chan, field_list, format_list, func, client_data)
EChannel chan;
FMFieldList field_list;
CMFormatList format_list;
ECTypedHandlerFunction func;
void *client_data;
{
    return internal_typed_sink_subscribe(chan, chan->ec,
					 (multi_sink_info) NULL,
					 field_list, format_list, func,
					 client_data);
}

extern ECSinkHandle
ECsink_typed_subscribe_context(chan, field_list, format_list, func,
			       client_data, ec)
EChannel chan;
FMFieldList field_list;
CMFormatList format_list;
ECTypedHandlerFunction func;
void *client_data;
EControlContext ec;
{
    return internal_typed_sink_subscribe(chan, ec, (multi_sink_info) NULL,
					 field_list, format_list,
					 func, client_data);
}

extern ECSinkHandle
ECsink_multi_subscribe(sink_specs, ec, func, client_data)
ECMultiSubscribePtr sink_specs;
EControlContext ec;
ECMultiHandlerFunction func;
void *client_data;
{
    return internal_sink_multi_subscribe(sink_specs, ec, func,
					 client_data);
}

extern ECSinkHandle
ECsink_subscribe(chan, func, client_data)
EChannel chan;
ECHandlerFunction func;
void *client_data;
{
    return internal_sink_subscribe(chan, chan->ec, NULL, func,
				   client_data);
}

extern ECSinkHandle
ECsink_subscribe_context(chan, func, client_data, ec)
EChannel chan;
ECHandlerFunction func;
void *client_data;
EControlContext ec;
{
    return internal_sink_subscribe(chan, ec, NULL, func, client_data);
}

extern void
ECcancel_sink_subscribe(handle)
ECSinkHandle handle;
{
    EChannel chan = handle->chan;
    EChannel_lock(handle->chan);

    ECtrace_out("ECcancel_sink_subscribe, chan %lx", 1, chan);

    if (num_local_sinks(chan) == 1) {
	int i;
	ECtrace_out("working on remote members", 3);
	for (i = 0; i < chan->rem_member_list_len; i++) {
	    if (chan->rem_member_list[i].conn != NULL) {
		EChannel_unlock(chan);
		send_sink_subscribe(chan, i, 0);	/* false means
							 * unsubscribe */
		EChannel_lock(chan);
	    }
	}
    }
/*    if (chan->local_sink_list[handle->sink_num].context != NULL) {
	CMfree_user_type_context(chan->cm,
				 chan->local_sink_list[handle->sink_num].
				 context);
	chan->local_sink_list[handle->sink_num].context = NULL;
	}*/
    EVaction_remove_split_target(chan->cm, chan->incoming_target_stone_id,
				 chan->incoming_split_action, 
				 chan->local_sink_list[handle->sink_num].stone_id);
    EVfree_stone(chan->cm, chan->local_sink_list[handle->sink_num].stone_id);
    chan->local_sink_list[handle->sink_num].func =
	(ECHandlerFunction) NULL;
    chan->local_sink_list[handle->sink_num].multi_sink_handle = NULL;
    ECtrace_out("ECcancel_sink_subscribe, chan %lx", 0, chan);
    EChannel_unlock(chan);
    CMfree(handle);
}

static ECSinkHandle
internal_sink_subscribe(chan, ec, multi_sink_handle, func, client_data)
EChannel chan;
EControlContext ec;
multi_sink_info multi_sink_handle;
ECHandlerFunction func;
void *client_data;
{
    int i;
    ECSinkHandle handle = CMmalloc(sizeof(*handle)), ev_handle;
    EVstone stone_id = EValloc_stone(ec->cm);

    ECtrace_out("ECsink_subscribe, chan %lx", 1, chan);
    handle->chan = chan;
    handle->sink_num = chan->local_sink_list_len;
    EChannel_lock(handle->chan);
    ECtrace_out("working on remote members", 3);
    for (i = 0; i < chan->rem_member_list_len; i++) {
	if (chan->rem_member_list[i].conn != NULL) {
	    EChannel_unlock(chan);
	    send_sink_subscribe(chan, i, 1);	/* true means "subscribe" */
	    EChannel_lock(chan);
	}
    }
    chan->local_sink_list =
	CMrealloc(chan->local_sink_list,
		  sizeof(struct _local_sink) * (chan->local_sink_list_len +
						1));
    chan->local_sink_list[chan->local_sink_list_len].func = func;
    chan->local_sink_list[chan->local_sink_list_len].client_data =
	client_data;
    chan->local_sink_list[chan->local_sink_list_len].ec = ec;
    chan->local_sink_list[chan->local_sink_list_len].multi_sink_handle =
	multi_sink_handle;
    chan->local_sink_list[chan->local_sink_list_len].cond = 0;
    chan->local_sink_list[chan->local_sink_list_len].stone_id = stone_id;
    EVaction_add_split_target(ec->cm, chan->incoming_target_stone_id,
			      chan->incoming_split_action, stone_id);
    
    ev_handle = malloc(sizeof(*ev_handle));
    *ev_handle = *handle;
    chan->local_sink_list[chan->local_sink_list_len].ev_sink_rec =
	ev_handle;
    chan->local_sink_list[chan->local_sink_list_len].term_action = 
	EVassoc_terminal_action(ec->cm, stone_id, NULL,
				EVTerminal_wrapper_func, ev_handle);

    (chan->local_sink_list_len)++;
    EChannel_unlock(chan);
    if (chan->subscribe_func) {
	int sink_count = 0, i;
	for (i = 0; i < chan->rem_member_list_len; i++) {
	    if (chan->rem_member_list[i].has_sinks)
		sink_count++;
	}
	chan->subscribe_func(1, sink_count, chan->subscribe_client_data);
    }
    if (channel_verbose > 0) {
	printf("After Subscribe\n");
	dump_EChannel(chan);
    }
    ECtrace_out("ECsink_subscribe chan %lx, return handle %lx", 0, chan,
		handle);
    return handle;
}

/* 
 *   Extended with (func, client_data) to support a pull model. The idea is 
 *   that the handler would be executed when the source get a request and 
 *   it would submit an event as part of its execution - fabianb
 *
 */

extern ECSourceHandle
ECsource_subscribe(chan)
EChannel chan;
{
    return ECPsource_subscribe(chan, (ECRequestHandlerFunction) NULL,
			       NULL);
}

extern ECSourceHandle
ECPsource_subscribe(chan, func, client_data)
EChannel chan;
ECRequestHandlerFunction func;
void *client_data;
{
    int i;

    ECSourceHandle handle = new_source_handle();

    ECtrace_out("ECsource_subscribe, chan %lx", 1, chan);
    handle->chan = chan;
    EChannel_lock(chan);
    chan->local_source_list =
	CMrealloc(chan->local_source_list,
		  sizeof(struct _local_source) *
		  (chan->local_source_list_len + 1));
    chan->local_source_list[chan->local_source_list_len].func = func;
    chan->local_source_list[chan->local_source_list_len].client_data =
	client_data;
    chan->local_source_list[chan->local_source_list_len].source_handle =
	handle;
    chan->local_source_list[chan->local_source_list_len].ev_source_handle =
	EVcreate_submit_handle(chan->cm, chan->outgoing_target_stone_id,
			       NULL);
    handle->source_num = chan->local_source_list_len;

    (chan->local_source_list_len)++;

    ECtrace_out("working on remote members", 3);
    for (i = 0; i < chan->rem_member_list_len; i++) {
	if (chan->rem_member_list[i].conn != NULL) {
	    EChannel_unlock(chan);
	    send_source_subscribe(chan, i, 1);	/* true means "subscribe" */
	    EChannel_lock(chan);
	}
    }

    EChannel_unlock(chan);

    ECtrace_out
	("Member counts: \n Local sources %d Local sinks %d \n Remote Members %d",
	 3, chan->local_source_list_len, chan->local_sink_list_len,
	 chan->rem_member_list_len);

    ECtrace_out("ECsource_subscribe, chan %lx, returning handle %lx", 0,
		chan, handle);
    return handle;
}

extern void
ECcancel_source_subscribe(handle)
ECSourceHandle handle;
{
    EChannel chan = handle->chan;
    EChannel_lock(chan);
    internal_cancel_source_subscribe(handle);
    EChannel_unlock(chan);
}

static void
internal_cancel_source_subscribe(handle)
ECSourceHandle handle;
{
    int i;
    EChannel chan = handle->chan;

    /* remove source from channel's list */
    for (i = 0; i < handle->chan->local_source_list_len; i++) {
	if (handle->chan->local_source_list[i].source_handle == handle) {
	    for (; i < handle->chan->local_source_list_len - 1; i++) {
		handle->chan->local_source_list[i] =
		    handle->chan->local_source_list[i + 1];
	    }
	    handle->chan->local_source_list_len--;
	    break;
	}
    }

    /* 
     *  only if it is the last one in this side it will send a msg 
     *  we need this for remote sources now 
     *
     */

    if (chan->local_source_list_len == 1) {
	int i;
	ECtrace_out("working on remote members", 3);
	for (i = 0; i < chan->rem_member_list_len; i++) {
	    if (chan->rem_member_list[i].conn != NULL) {
		EChannel_unlock(chan);
		send_source_subscribe(chan, i, 0);	/* false means
							 * unsubscribe */
		EChannel_lock(chan);
	    }
	}
    }
    CMfree(handle);
}

/* 
 *   Extended with (func, client_data) to support a pull model. The idea is 
 *   that the handler would be executed when the source get a request and 
 *   it would submit an event as part of its execution - fabianb
 */


extern ECSourceHandle
ECsource_typed_subscribe_attr(chan, field_list, format_list, attrs)
EChannel chan;
FMFieldList field_list;
CMFormatList format_list;
attr_list attrs;
{
    return ECPsource_typed_subscribe_attr(chan, field_list, format_list,
					  (ECRequestHandlerFunction) NULL,
					  NULL, attrs);
}

extern ECSourceHandle
ECsource_typed_subscribe(chan, field_list, format_list)
EChannel chan;
FMFieldList field_list;
CMFormatList format_list;
{
    return ECPsource_typed_subscribe_attr(chan, field_list, format_list,
					  (ECRequestHandlerFunction) NULL,
					  NULL, NULL);
}


extern ECSourceHandle
ECPsource_typed_subscribe(chan, field_list, format_list, func, client_data)
EChannel chan;
FMFieldList field_list;
CMFormatList format_list;
ECRequestHandlerFunction func;
void *client_data;
{
    return ECPsource_typed_subscribe_attr(chan, field_list, format_list,
					  func, client_data, NULL);
}

extern ECSourceHandle
ECPsource_typed_subscribe_attr(chan, field_list, format_list, func,
			       client_data, attrs)
EChannel chan;
FMFieldList field_list;
CMFormatList format_list;
ECRequestHandlerFunction func;
void *client_data;
attr_list attrs;
{
    ECSourceHandle handle = new_source_handle();
    FMStructDescList ev_format_list;
    int i = 0;
    int format_count = 0;

    if (format_list != NULL) 
	while (format_list[format_count].format_name != NULL) format_count++;
    ev_format_list = malloc(sizeof(ev_format_list[0]) * (format_count + 2));
    ev_format_list[0].format_name = "anonymous";
    ev_format_list[0].field_list = field_list;
    ev_format_list[0].struct_size = struct_size_field_list(field_list, sizeof(char*));
    ev_format_list[0].opt_info = NULL;
    for (i=0; i < format_count; i++) {
	ev_format_list[i+1].format_name = format_list[i].format_name;
	ev_format_list[i+1].field_list = format_list[i].field_list;
	ev_format_list[i+1].struct_size = struct_size_field_list(format_list[i].field_list, sizeof(char*));
	ev_format_list[i+1].opt_info = NULL;
    }
    ev_format_list[format_count + 1].format_name = NULL;
    ev_format_list[format_count + 1].field_list = NULL;
    ev_format_list[format_count+1].struct_size = 0;
    ev_format_list[format_count+1].opt_info = NULL;
    ECtrace_out("ECsource_subscribe, chan %lx", 1, chan);
    handle->chan = chan;
    EChannel_lock(chan);

    /* assert that the formats are compatible -GSE- */

    /* add to our channels list of sources */
    chan->local_source_list =
	CMrealloc(chan->local_source_list,
		  sizeof(struct _local_source) *
		  (chan->local_source_list_len + 1));
    chan->local_source_list[chan->local_source_list_len].func = func;
    chan->local_source_list[chan->local_source_list_len].client_data =
	client_data;
    chan->local_source_list[chan->local_source_list_len].source_handle =
	handle;
    chan->local_source_list[chan->local_source_list_len].ev_source_handle =
	EVcreate_submit_handle(chan->cm, chan->outgoing_target_stone_id,
			       ev_format_list);
			       
    handle->source_num = chan->local_source_list_len;
    (chan->local_source_list_len)++;

    ECtrace_out("working on remote members", 3);
    for (i = 0; i < chan->rem_member_list_len; i++) {
	if (chan->rem_member_list[i].conn != NULL) {
	    EChannel_unlock(chan);
	    send_source_subscribe(chan, i, 1);	/* true means "subscribe" */
	    EChannel_lock(chan);
	}
    }


    EChannel_unlock(chan);

    ECtrace_out
	("Member counts: \n Local sources %d Local sinks %d \n Remote Members %d",
	 3, chan->local_source_list_len, chan->local_sink_list_len,
	 chan->rem_member_list_len);

    ECtrace_out("ECsource_subscribe, chan %lx, returning handle %lx", 0,
		chan, handle);

    return handle;
}

extern char *
ECglobal_id(chan)
EChannel chan;
{
    int len = 0;
    char chan_str[17] =
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    unsigned char chan_char[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };
    char *global_id_str;

    memset(chan_char, 0, sizeof(chan_char));
    memcpy(chan_char, &chan, sizeof(chan));
    sprintf(chan_str, "%02x%02x%02x%02x%02x%02x%02x%02x",
	    (unsigned int) chan_char[0],
	    (unsigned int) chan_char[1],
	    (unsigned int) chan_char[2],
	    (unsigned int) chan_char[3],
	    (unsigned int) chan_char[4],
	    (unsigned int) chan_char[5],
	    (unsigned int) chan_char[6], (unsigned int) chan_char[7]);
    len = strlen(chan->global_id.contact_string);
    len += 16 /* chan_str */  + 1 /* separators */ ;
    global_id_str = CMmalloc(len + 1);
    memset(global_id_str, 0, len + 1);
    sprintf(global_id_str, "%s@%s", chan->global_id.contact_string,
	    chan_str);
    return global_id_str;
}

static void
set_conn_characteristics(CMConnection conn)
{
    static char *int_str = NULL;
    static int interval = -1;
    if (interval == -1) {
	interval = 0;
	int_str = getenv("CM_BW_MEASURE_INTERVAL");
	if (int_str == NULL) return;
	if (sscanf(int_str, "%d", &interval) != 1) return;
    }
    if (interval > 0) {
	attr_list attrs = create_attr_list();
	add_attr(attrs, CM_BW_MEASURE_INTERVAL, Attr_Int4, 
		 (attr_value)(long)interval);
	CMConnection_set_character(conn, attrs);
	free_attr_list(attrs);
    }
}

extern void
CMsetup_conn_or_local(ec, chan_id, conn_ptr, local_ptr)
EControlContext ec;
char *chan_id;
CMConnection *conn_ptr;
void **local_ptr;
{
    char *chan_id_copy = CMmalloc(strlen(chan_id) + 1);
    char *contact_str;
    char *chan_str;
    attr_list attrs;
    char *tmp;

    *conn_ptr = NULL;
    *local_ptr = NULL;
    strcpy(chan_id_copy, chan_id);
    contact_str = chan_id_copy;
    tmp = strchr(chan_id_copy, '@');
    if (tmp == NULL) {
	CMfree(chan_id_copy);
	ECtrace_out("ECho, bad id %s", 2, chan_id);
	return;
    }
    *tmp = 0;			/* kill first @ */
    chan_str = ++tmp;

    attrs = attr_list_from_string(contact_str);
    if (CMcontact_self_check(ec->cm, attrs)) {
	free_attr_list(attrs);
	/* this channel is really local */
	*local_ptr = string_to_channel_ptr(chan_str);
	CMfree(chan_id_copy);
	return;
    }
    ECtrace_out("getting a connection to %s, registering close handler", 3,
		chan_id_copy);
    if (attrs == NULL) {
	printf("malformed contactlist\n");
	CMfree(chan_id_copy);
	return;
    }
    CMfree(chan_id_copy);
    *conn_ptr = CMget_conn(ec->cm, attrs);
    free_attr_list(attrs);
    if (*conn_ptr == NULL) {
	return;
    }
    CMconn_register_close_handler(*conn_ptr, EChannel_close_handler, ec);
    set_conn_characteristics(*conn_ptr);
}


extern EChannel
EChannel_open(ec, chan_id)
EControlContext ec;
char *chan_id;
{
    EChannel chan;
    CMConnection conn;
    AttendMsg attend_msg;
    attr_list self_contact_list;
    int cond;

    ECtrace_out("ECchannel_open, ID=%s", 1, chan_id);
    CMsetup_conn_or_local(ec, chan_id, &conn, (void **) (void *) &chan);
    if (conn == NULL) {
	ECtrace_out("EChannel_open, returning %lx", 0, chan);
	return chan;
    }
    chan = EChannel_create(ec);
    cond = CMCondition_get(ec->cm, conn);
    CMCondition_set_client_data(ec->cm, cond, &chan);
    memset(&attend_msg, 0, sizeof(attend_msg));
    attend_msg.chan_str = strchr(chan_id, '@') + 1;
    attend_msg.cond = cond;
    attend_msg.proto_version = 1;	/* don't respond with
					 * Old_Attend_Response */
    set_channel_id(&attend_msg.client_channel_id, chan);
    self_contact_list = CMget_contact_list(ec->cm);
    if (self_contact_list == NULL) {
	printf
	    ("Warning, no contact list.  Must do CManager_listen() before using channels.\n");
	CMfree(chan);
	return NULL;
    }
    attend_msg.contact_str = attr_list_to_string(self_contact_list);
    attend_msg.incoming_target_stone_id = chan->incoming_target_stone_id;
    ECtrace_out("creating attend_msg and writing to connection", 3);
    if (!CMwrite(conn, CMlookup_format(ec->cm, channel_attend_msg_formats),
		 &attend_msg)) {
	EChannel_destroy(chan);
	ECtrace_out("ECchannel_open, write failed, channel stillborn", 0);
	return NULL;
    }
    free(attend_msg.contact_str);
    free(attend_msg.client_channel_id.channel);
    if (channel_verbose > 0) {
	printf("Sending attend msg, channel %s cond %d\n",
	       attend_msg.chan_str, attend_msg.cond);
    }
    /* 
     *  At this point, we wait for a Channel Attend Response message
     *  or a Channel Exists Attend Response message, which will signal 
     *  this condition.  When that happens, we expect everything to be 
     *  done, all necessary network connections setup, etc.  Note that 
     *  the value of the "chan" variable may be changed by the handler.  
     *  In particular, the Channel Exists Attend Response message will 
     *  destroy our partial chan data structure and set "chan" to the 
     *  value of the existing channel.
     */
    if (CMCondition_wait(ec->cm, cond)) {
	ECtrace_out("ECchannel_open channel is ready %lx", 0, chan);
	if (channel_verbose) {
	    printf("\tOpened channel ID is ");
	    print_channel_id(chan->global_id.global_channel_id);
	    printf("\n");
	}
	return chan;
    } else {
	/* wait failed, conn is dead */
	EChannel_destroy(chan);
	ECtrace_out("ECchannel_open, wait failed, channel stillborn", 0);
	return NULL;
    }
}

extern void ECcancel_sink_subscribe ARGS((ECSinkHandle handle));

static void
send_request_event(chan, i)
EChannel chan;
int i;
{
    RequestEventMsg msg;

    ECtrace_out("send_request_event", 1);
    ECtrace_out("create msg & write to rem_source_list port", 3);

    memset(&msg, 0, sizeof(msg));
    msg.global_channel_id = chan->global_id.global_channel_id;

    CMwrite(chan->rem_member_list[i].conn,
	    CMlookup_format(chan->cm, request_event_msg_formats), &msg);
    ECtrace_out("send_request_event", 0);
}

static void
sendv_remote_event(chan, i, eventV, attrs)
EChannel chan;
int i;
FFSEncodeVector eventV;
attr_list attrs;
{
    CMConnection conn = chan->rem_member_list[i].conn;
    int vec_count = 0;
    EventVMsg msg;

    if (chan->rem_member_list[i].event_conn) {
	int use_event_conn = 1;
	if (attrs && query_attr(attrs, ECHO_USE_EVENT_TRANSPORT,	/* type 
									 * pointer 
									 */ NULL,
				/* value pointer */
				(attr_value *) (long) &use_event_conn)) {
	    if (use_event_conn != 0) {
		conn = chan->rem_member_list[i].event_conn;
		ECtrace_out
		    ("sendv_remote_event, event transport enabled, use value is %d",
		     3, use_event_conn);
	    }
	} else {
	    conn = chan->rem_member_list[i].event_conn;
	}
	ECtrace_out
	    ("send_remote_event, event transport enabled, use value is %d",
	     3, use_event_conn);
	ECtrace_out("sendv_remote_event, event_conn %lx, control_conn %lx",
		    (long) chan->rem_member_list[i].event_conn,
		    (long) chan->rem_member_list[i].conn);
    }
    memset(&msg, 0, sizeof(msg));
    while (eventV[vec_count].iov_base != NULL) {
	vec_count++;
    }
    ECtrace_out("sendv_remote_event, chan %lx, eventv %lx, conn %lx", 1,
		chan, eventV, conn);
    ECtrace_out("create event msg and write to remote member list port",
		3);
    msg.eventv = eventV;
    msg.eventv_len = vec_count;

    msg.global_channel_id = chan->global_id.global_channel_id;

    if (!CMwrite_attr(conn, chan->eventv_format, &msg, attrs)) {
	ECtrace_out("send remote event write failed", 0);
    }
    ECtrace_out("sendv_remote_event", 0);
}

static void
send_sink_subscribe(chan, i, subscribe)
EChannel chan;
int i;
int subscribe;
{
    SinkSubscribeMsg msg;

    memset(&msg, 0, sizeof(msg));
    msg.global_channel_id = chan->global_id.global_channel_id;
    set_channel_id(&msg.client_channel_id, chan);
    ECtrace_out("send_sink_subscription", 1);
    if (channel_verbose) {
	printf("\tsending to channel with global id ");
	print_channel_id(msg.global_channel_id);
	printf("\n");
    }
    ECtrace_out("create msg and write to rem_member_list port", 3);
    if (subscribe) {
	msg.client_contact_str = chan->local_contact_string;
	if (!CMwrite(chan->rem_member_list[i].conn,
		     CMlookup_format(chan->cm, sink_subscribe_msg_formats),
		     &msg)) {
	    ECtrace_out("send sink subscribe Write failed", 0);
	}
    } else {
	msg.client_contact_str = NULL;
	if (!CMwrite(chan->rem_member_list[i].conn,
		     CMlookup_format(chan->cm, sink_unsubscribe_msg_formats),
		     &msg)) {
	    ECtrace_out("send sink unsubscribe write failed", 0);
	}
    }
    CMfree(msg.client_channel_id.channel);
    ECtrace_out("send_sink_subscription, subscribe = %d", 0, subscribe);
}

static void
send_source_subscribe(chan, i, subscribe)
EChannel chan;
int i;
int subscribe;
{
    SourceSubscribeMsg msg;

    memset(&msg, 0, sizeof(msg));
    msg.global_channel_id = chan->global_id.global_channel_id;
    set_channel_id(&msg.client_channel_id, chan);
    ECtrace_out("send_source_subscription", 1);
    ECtrace_out("create msg and write to rem_member_list port", 3);
    if (subscribe) {
	msg.client_contact_str = chan->local_contact_string;
	if (!CMwrite(chan->rem_member_list[i].conn,
		     CMlookup_format(chan->cm, source_subscribe_msg_formats),
		     &msg)) {
	    ECtrace_out("send source subscribe write failed", 0);
	}
    } else {
	msg.client_contact_str = NULL;
	if (!CMwrite(chan->rem_member_list[i].conn,
		     CMlookup_format(chan->cm,
				     source_unsubscribe_msg_formats), &msg)) {
	    ECtrace_out("send source unsubscribe write failed", 0);
	}
    }
    CMfree(msg.client_channel_id.channel);
    ECtrace_out("send_source_subscription, subscribe = %d", 0, subscribe);
}

static void
send_member_subscribe(chan, i)
EChannel chan;
int i;
{
    MemberSubscribeMsg msg;

    ECtrace_out("send_member_subscription", 1);
    ECtrace_out("create msg & write to rem_member_list port", 3);
    memset(&msg, 0, sizeof(msg));
    msg.global_channel_id = chan->global_id.global_channel_id;
    msg.client_contact_str = chan->local_contact_string;
    msg.event_contact_str = chan->event_contact_string;
    msg.client_target_stone_id = chan->incoming_target_stone_id;
    set_channel_id(&msg.client_channel_id, chan);
    if (!CMwrite(chan->rem_member_list[i].conn,
		 CMlookup_format(chan->cm, member_subscribe_msg_formats),
		 &msg)) {
	ECtrace_out("send member subscribe write failed\n", 0);
    }
    CMfree(msg.client_channel_id.channel);
    ECtrace_out("send_member_subscription", 0);
}


static event_queue
get_event_item(EControlContext ec)
{
    if (ec->free_event_item_list == NULL) {
	return (event_queue) malloc(sizeof(event_item));
    } else {
	event_queue tmp = ec->free_event_item_list;
	ec->free_event_item_list = tmp->next;
	return tmp;
    }
}

static void
release_event_queue_item(EControlContext ec, event_queue item)
{
    if (item->attrs != NULL) {
	free_attr_list(item->attrs);
	item->attrs = NULL;
    }
    item->next = ec->free_event_item_list;
    ec->free_event_item_list = item;
}

static void
queue_event_for_dispatch(chan, sink_index, event, event_len, block_rec,
			 event_attrs)
EChannel chan;
int sink_index;
void *event;
int event_len;
free_block_rec_p block_rec;
attr_list event_attrs;
{
    event_item *item;
    EControlContext ec = chan->local_sink_list[sink_index].ec;
/*    CMFormatList format_list =
      chan->local_sink_list[sink_index].format_list;*/

#ifdef NOTDEF
    if ((context != NULL) && (event_len != 0)) {
	IOFormat ioformat;
	void *decode_buffer;
	/* decode typed event */
	ioformat = CMget_format_IOcontext(chan->cm, context, event);
	if (ioformat == NULL) {
	    fprintf(stderr, "Unknown IOFormat in encoded block\n");
	}
	ECtrace_out("Queue event got typed event, format \"%s\"",
		    3, name_of_IOformat(ioformat));
	if (!has_conversion_IOformat(ioformat)) {
	    IOFormat *ioformats;
	    IOFormat *format;
	    if ((ioformats = CMget_subformats_IOcontext(chan->cm, context,
							event)) == NULL) {
		fprintf(stderr, "IOFormat list is null\n");
		return;
	    }
	    format = ioformats;
	    while (format_list && (*format != NULL)) {
		int i = 0;
		if (!has_conversion_IOformat(*format)) {
		    while (format_list[i].format_name != NULL) {
			if (strcmp(format_list[i].format_name,
				   name_of_IOformat(*format)) == 0) {
			    break;
			}
			i++;
		    }
		    if (format_list[i].field_list != NULL) {
			native_struct_size =
			    struct_size_IOfield((IOFile) context,
						format_list[i].field_list);
			CMset_conversion_IOcontext(chan->cm, context,
						   *format,
						   format_list[i].
						   field_list,
						   native_struct_size);
		    }
		}
		format++;
	    }
	    free(ioformats);
	    native_struct_size = struct_size_IOfield((IOFile) context,
						     field_list);
	    CMset_conversion_IOcontext(chan->cm, context, ioformat,
				       field_list, native_struct_size);
	}
	if (!decode_in_place_possible(ioformat)) {
	    decode_buffer =
		CMmalloc(this_IOrecord_length(context, event, event_len));
	    decode_to_buffer_IOcontext(context, event, decode_buffer);
	    block_rec = get_free_block_func(chan->ec, decode_buffer,
					    (EventFreeFunction) NULL);
	    block_rec->ref_count = 0;
	    event = decode_buffer;
	    event_len = 0;
	} else {
	    void *decoded_event;
	    decode_in_place_IOcontext(context, event, &decoded_event);
	    event = decoded_event;
	}
    }
#endif

    item = get_event_item(chan->ec);
    if (chan->local_sink_list[sink_index].multi_sink_handle != NULL) {
	multi_sink_info msink_handle =
	    chan->local_sink_list[sink_index].multi_sink_handle;
	struct _ECEventQueue *eq;
	int i = 0;
	while (msink_handle->queue_list->event_queues[i].sink_handle->chan
	       != chan) {
	    i++;
	    if (i >= msink_handle->queue_list->queue_count) {
		printf("Channel not found in  queue \n");
		return;
	    }
	}
	eq = &msink_handle->queue_list->event_queues[i];
	if ((eq->event_count + 1) > eq->queue_size) {
	    eq->queue_size = eq->event_count + 1;
	    eq->queue = realloc(eq->queue,
				sizeof(eq->queue[0]) * eq->queue_size);
	}
	eq->queue[eq->event_count].event = event;
	if (block_rec != NULL) {
	    block_rec->ref_count++;
	} else {
	    static int block_copy_warning_done = 0;
	    if (!block_copy_warning_done) {
		fprintf(stderr,
			"Warning:  An event submitted with ECsubmit_event(), ECsubmit_typed_event() or\n    ECsubmit_eventV() has been passed to a multi-sink handler.\n    This violates ECho event data memory handling requirements.  See\n   http://www.cc.gatech.edu/systems/projects/ECho/event_memory.html\n");
		block_copy_warning_done++;
	    }
	}
	eq->queue[eq->event_count].free_block = block_rec;
	ECtrace_out("Added event %d to queue %d\n", eq->event_count, i);
	eq->event_count++;
	event = NULL;
	event_len = 0;
	block_rec = NULL;
    } else {
	if ((block_rec == NULL) && (chan->ec != ec)) {
	    static int block_copy_warning_done = 0;
	    if (!block_copy_warning_done) {
		fprintf(stderr,
			"Warning:  An event submitted with ECsubmit_event(), ECsubmit_typed_event() or\n   ECsubmit_eventV() has been passed to handler in another EControlContext.\n   This violates ECho event data memory handling requirements.  See \nhttp://www.cc.gatech.edu/systems/projects/ECho/event_memory.html\n");
		block_copy_warning_done++;
	    }
	}
    }

    item->event = event;
    item->event_len = event_len;
    item->eventv = NULL;
    item->sink_index = sink_index;
    add_ref_attr_list(event_attrs);
    item->attrs = event_attrs;
    item->chan = chan;
    if (block_rec != NULL)
	block_rec->ref_count++;
    item->block_rec = block_rec;
    ECtrace_out("Queue event for local dispatch, chan %lx, event %lx",
		3, chan, event);

    item->next = NULL;
    item->prev = ec->event_queue_tail;
    if (item->prev) {
	item->prev->next = item;
    }
    ec->event_queue_tail = item;
    if (ec->event_queue_head == NULL) {
	ec->event_queue_head = item;
    }
    if (chan->ec != ec) {
	/* if we're putting this in some other DE's queue, wake any server 
	 */
	if (ec->cm != NULL) {
	    /* wake_CM_server(ec->cm); */
	    printf("ECho warning - Should be waking server\n");
	} else {
	    wake_threaded_server(ec);
	}
    }
}


static void
queue_eventv_for_dispatch(chan, sink_index, eventv, block_rec, event_attrs)
EChannel chan;
int sink_index;
FFSEncodeVector eventv;
free_block_rec_p block_rec;
attr_list event_attrs;
{
    event_item *item = get_event_item(chan->ec);
    item->event = NULL;
    item->event_len = 0;
    item->eventv = eventv;
    item->sink_index = sink_index;
    item->chan = chan;
    add_ref_attr_list(event_attrs);
    item->attrs = event_attrs;
    block_rec->ref_count++;
    item->block_rec = block_rec;

    item->next = NULL;
    item->prev = chan->ec->event_queue_tail;
    if (item->prev) {
	item->prev->next = item;
    }
    chan->ec->event_queue_tail = item;
    if (chan->ec->event_queue_head == NULL) {
	chan->ec->event_queue_head = item;
    }
    if (channel_verbose > 0)
	printf("queuev for dispatch\n");
}

static event_queue
dequeue_event(ec)
EControlContext ec;
{
    event_queue item;

    if (ec->event_queue_head == NULL) {
	return NULL;
    } else {
	item = ec->event_queue_head;
	ec->event_queue_head = item->next;
    }
    if (item == ec->event_queue_tail) {
	ec->event_queue_tail = NULL;
    }
    item->prev = NULL;
    item->next = NULL;
    return item;
}

static void
dispatch_event(func, event, event_len, client_data, event_attrs)
ECHandlerFunction func;
void *event;
int event_len;
void *client_data;
attr_list event_attrs;
{
    func(event, event_len, client_data, event_attrs);
}

static void
dispatch_typed_event(func, event, client_data, event_attrs)
ECHandlerFunction func;
void *event;
void *client_data;
attr_list event_attrs;
{
    ((ECTypedHandlerFunction) func) (event, client_data, event_attrs);
}

static void
dispatch_multi_sink_event(func, queue_list, client_data)
ECMultiHandlerFunction func;
ECEventQueueList queue_list;
void *client_data;
{
    ((ECMultiHandlerFunction) func) (queue_list, client_data);
}

static void
do_event_dispatch(ec)
EControlContext ec;
{
    event_queue item;
    EContext_lock(ec);
    while ((item = dequeue_event(ec)) != NULL) {
	EChannel chan = item->chan;
	ECtrace_out("dequeued event", 3);
	ec->current_event_item = item;
	EContext_unlock(ec);
	if (chan->local_sink_list[item->sink_index].func !=
	    (ECHandlerFunction) NULL) {
	    int sink = item->sink_index;
	    if (item->eventv != NULL) {
		int i = 0;
		char *tmp;
		char *event;
		int event_len = 0;

		FFSEncodeVector eventV = item->eventv;
		while (eventV[i].iov_base != NULL) {
		    event_len += eventV[i++].iov_len;
		}
		tmp = event = CMmalloc(event_len);
		i = 0;
		while (eventV[i].iov_base != NULL) {
		    memcpy(tmp, eventV[i].iov_base, eventV[i].iov_len);
		    tmp += eventV[i++].iov_len;
		}
		dispatch_event(chan->local_sink_list[sink].func,
			       event, event_len,
			       chan->local_sink_list[sink].client_data,
			       item->attrs);

		EChannel_lock(chan);
		if (chan->local_sink_list[sink].cond) {
		    CMCondition_signal(chan->cm,
				       chan->local_sink_list[sink].cond);
		}
		chan->local_sink_list[sink].cond = 0;
		EChannel_unlock(chan);
		CMfree(event);
	    } else {
/*		if (chan->local_sink_list[item->sink_index].context ==
		    NULL) {
		    * untyped event *
		    dispatch_event(chan->local_sink_list[sink].func,
				   item->event, item->event_len,
				   chan->local_sink_list[sink].client_data,
				   item->attrs);
		} else {*/
		    /* typed event */
		    dispatch_typed_event(chan->local_sink_list[sink].func,
					 item->event,
					 chan->local_sink_list[sink].
					 client_data, item->attrs);
/*		}*/

		EChannel_lock(chan);
		if (chan->local_sink_list[sink].cond) {
		    CMCondition_signal(chan->cm,
				       chan->local_sink_list[sink].cond);
		}
		chan->local_sink_list[sink].cond = 0;
		EChannel_unlock(chan);
	    }
	} else if (chan->local_sink_list[item->sink_index].
		   multi_sink_handle) {
	    multi_sink_info msink_info =
		chan->local_sink_list[item->sink_index].multi_sink_handle;
	    ec->current_event_item = NULL;
	    dispatch_multi_sink_event(msink_info->func,
				      msink_info->queue_list,
				      msink_info->client_data);
	}
	EContext_lock(ec);
	if (item->block_rec) {
	    deref_block(item->block_rec);
	}
	ec->current_event_item = NULL;
	release_event_queue_item(ec, item);
    }
    EContext_unlock(ec);
}

int
ECtake_event_buffer(ec, event)
EControlContext ec;
void *event;
{
    return EVtake_event_buffer(ec->cm, event);
}

void
ECreturn_event_buffer(ec, event)
EControlContext ec;
void *event;
{
    EVreturn_event_buffer(ec->cm, event);
}

static void
do_event_flush(ec)
EControlContext ec;
{
    event_queue item;
    EContext_lock(ec);
    while ((item = dequeue_event(ec)) != NULL) {
	if (channel_verbose > 0)
	    printf("flushing event\n");
	if (item->block_rec) {
	    deref_block(item->block_rec);
	}
	release_event_queue_item(ec, item);
    }
    EContext_unlock(ec);
}

extern void
EChannel_subscribe_handler(chan, func, client_data)
EChannel chan;
ECSubscribeFunction func;
void *client_data;
{
    ECtrace_out("EChannel_subscribe_handler", 1);
    chan->subscribe_func = func;
    chan->subscribe_client_data = client_data;
    ECtrace_out("EChannel_subscribe_handler", 0);
}

extern void
Channel_RequestEvent_handler(cm, conn, data, client_data, attrs)
CManager cm;
CMConnection conn;
void *data;
void *client_data;
attr_list attrs;
{
    int i = 0;
    EChannel chan;
    RequestEventMsg *msg = (RequestEventMsg *) data;
    EControlContext ec = (EControlContext) client_data;

    if (channel_verbose > 0)
	printf("In request event handler\n");
    chan = get_local_channel_from_id(msg->global_channel_id, ec);

    if (chan == NULL)
	return;

    EChannel_lock(chan);

    for (i = 0; i < chan->local_source_list_len; i++) {
	if (chan->local_source_list[i].func !=
	    (ECRequestHandlerFunction) NULL) {
	    chan->local_source_list[i].func(chan->local_source_list[i].
					    source_handle,
					    chan->local_source_list[i].
					    client_data);
	}
    }

    EChannel_unlock(chan);
}

extern void
Channel_Event_handler(cm, conn, data, client_data, attrs)
CManager cm;
CMConnection conn;
void *data;
void *client_data;
attr_list attrs;
{
    int i = 0;
    EChannel chan;
    EControlContext ec = (EControlContext) client_data;
    EventMsg *msg = (EventMsg *) data;
    free_block_rec_p block_rec;

    EContext_lock(ec);
    block_rec = get_free_block_func(ec, NULL, (EventFreeFunction) NULL);
    EContext_unlock(ec);
    block_rec->ec = ec;
    block_rec->free_arg = CMtake_buffer(ec->cm, data);

    if (channel_verbose > 0)
	printf("In event handler\n");
    chan = get_local_channel_from_id(msg->global_channel_id, ec);

    if (chan == NULL)
	return;

    EChannel_lock(chan);
    EContext_lock(ec);
    for (i = 0; i < chan->local_sink_list_len; i++) {
	if (sink_valid(&chan->local_sink_list[i])) {
	    queue_event_for_dispatch(chan, i, msg->event, msg->event_len,
				     block_rec, attrs);
	}
    }
    deref_block(block_rec);
    EContext_unlock(ec);
    EChannel_unlock(chan);
}

extern void
Channel_EventV_handler(cm, conn, data, client_data, attrs)
CManager cm;
CMConnection conn;
void *data;
void *client_data;
attr_list attrs;
{
    EventVMsg *msg = (EventVMsg *) data;
    int i, event_len = 0;
    EChannel chan;
    EControlContext ec = (EControlContext) client_data;
    free_block_rec_p block_rec;

    EContext_lock(ec);
    block_rec = get_free_block_func(ec, NULL, (EventFreeFunction) NULL);
    EContext_unlock(ec);
    block_rec->ec = ec;
    block_rec->free_arg = CMtake_buffer(ec->cm, data);
    if (attrs) {
	block_rec->attrs = attrs;
	add_ref_attr_list(attrs);
    }
    if (channel_verbose > 0)
	printf("In eventv handler\n");
    for (i = 0; i < msg->eventv_len - 1; i++) {
	event_len += msg->eventv[i].iov_len;
    }
    event_len += msg->eventv[msg->eventv_len - 1].iov_len;
    chan = get_local_channel_from_id(msg->global_channel_id, ec);
    if (chan == NULL)
	return;
    EChannel_lock(chan);
    EContext_lock(ec);
    for (i = 0; i < chan->local_sink_list_len; i++) {
	if (sink_valid(&chan->local_sink_list[i])) {
	    queue_event_for_dispatch(chan, i, msg->eventv[0].iov_base,
				     event_len, block_rec, attrs);
	}
    }
    deref_block(block_rec);
    EContext_unlock(ec);
    EChannel_unlock(chan);
}

static void
    send_old_attend_response ARGS((CManager cm, CMConnection conn,
				   EChannel chan, int cond));

/* 
 * Channel_Attend_handler() is the remote portion of EChannel_open().
 * This routine is invoked in response to the Channel Attend message
 * generated by the client trying to open the channel.  It's function is to
 * 1) see if that client already has the channel open and if so, respond
 * with a Channel Exists Attend Response message, else
 * 2) send him contact information for everyone who currently has the
 * channel open (Attendees), has a sink or has a source, *AND* information
 * on the set of channels that has been derived from this channel.
 */
extern void
Channel_Attend_handler(cm, conn, data, client_data, attrs)
CManager cm;
CMConnection conn;
void *data;
void *client_data;
attr_list attrs;
{
    AttendMsgPtr msg = (AttendMsgPtr) data;
    ExistsAttendResponseMsg exists_msg;
    EChannel chan;
    int i;

    ECtrace_out("Channel_Attend_handler, connection %lx", 1, conn);
    CMconn_register_close_handler(conn, EChannel_close_handler,
				  (EControlContext) client_data);
    set_conn_characteristics(conn);

    chan = string_to_channel_ptr(msg->chan_str);

    if (channel_verbose > 0) {
	printf("In attend handler, channel %s (%lx) cond %d, global id",
	       msg->chan_str, (long) chan, msg->cond);
	print_channel_id(chan->global_id.global_channel_id);
	printf("\n");
    }
    /* check to see that channel is valid */

    ECtrace_out("Checking for already existing remote channel chan %lx", 3,
		chan);
    for (i = 0; i < chan->rem_member_list_len; i++) {
	if (chan->rem_member_list[i].conn != NULL) {
	    if (chan->rem_member_list[i].conn == conn) {
		ECtrace_out("Found existing channel, notify attender", 3);
		exists_msg.condition = msg->cond;
		exists_msg.existing_channel_id =
		    chan->rem_member_list[i].remote_id;

		ECtrace_out("writing exists response name", 3);
		if (!CMwrite
		    (conn,
		     CMlookup_format(cm,
				     channel_exists_attend_response_msg_formats),
		     &exists_msg)) {
		    ECtrace_out("send channel exists Write failed", 0);
		}
		ECtrace_out("Channel_Attend_handler", 0);
		return;
	    }
	}
    }

    if (msg->proto_version == 0) {
	send_old_attend_response(cm, conn, chan, msg->cond);
    } else {
	send_attend_response(cm, conn, chan, msg->cond);
    }
    ECtrace_out("adding new member to remote member list", 3);
    if (chan->rem_member_list == NULL) {
	chan->rem_member_list = CMmalloc(sizeof(struct _rem_member_entry));
    }
    chan->rem_member_list =
	CMrealloc(chan->rem_member_list,
		  sizeof(struct _rem_member_entry) *
		  (chan->rem_member_list_len + 1));
    memset(&chan->rem_member_list[chan->rem_member_list_len], 0,
	   sizeof(chan->rem_member_list[0]));
    chan->rem_member_list[chan->rem_member_list_len].conn = conn;
    chan->rem_member_list[chan->rem_member_list_len].remote_id =
	copy_channel_id(msg->client_channel_id);

    chan->rem_member_list[chan->rem_member_list_len].contact_string =
	strdup(msg->contact_str);
    chan->rem_member_list[chan->rem_member_list_len].remote_target_stone_id =
	msg->incoming_target_stone_id;
    chan->rem_member_list[chan->rem_member_list_len].has_sinks = 0;
    chan->rem_member_list[chan->rem_member_list_len].has_sources = 0;
    chan->rem_member_list_len++;
    if (channel_verbose > 0) {
	printf("\n\nChannel_Attend_handler\n");
	dump_EChannel(chan);
    }
    ECtrace_out("Channel_Attend_handler", 0);
}

void
send_attend_response(cm, conn, chan, cond)
CManager cm;
CMConnection conn;
EChannel chan;
int cond;
{
    AttendResponseMsg ret_msg;
    int i;

    ECtrace_out("send him the remote list", 3);

    memset(&ret_msg, 0, sizeof(ret_msg));
    ret_msg.condition = cond;
    ret_msg.type_name = NULL;
    ret_msg.global_channel_id = chan->global_id.global_channel_id;
/*    if (chan->type_format) {
	ret_msg.type_name = global_name_of_IOformat(chan->type_format);
	ECtrace_out("type of format is %s", 3, ret_msg.type_name);
	}*/
    ret_msg.create_attrs_string = chan->create_attrs_string;
    ret_msg.filter_data = chan->encoded_filter_data;
    ret_msg.filter_data_len = chan->encoded_filter_data_len;

    ret_msg.member_count = 1;
    ret_msg.members = CMmalloc(sizeof(MemberRec));
    ret_msg.members[0].contact_string = chan->global_id.contact_string;
    ret_msg.members[0].event_contact_string = chan->event_contact_string;
    ret_msg.members[0].client_target_stone_id = chan->incoming_target_stone_id;
    ret_msg.members[0].has_sinks = (chan->local_sink_list_len != 0);
    ret_msg.members[0].has_sources = (chan->local_source_list_len != 0);
    set_channel_id(&ret_msg.members[0].client_channel_id, chan);
    for (i = 0; i < chan->rem_member_list_len; i++) {
	if (chan->rem_member_list[i].conn != NULL) {
	    ret_msg.members = CMrealloc(ret_msg.members,
					(ret_msg.member_count +
					 1) * sizeof(MemberRec));
	    ret_msg.members[ret_msg.member_count].contact_string =
		chan->rem_member_list[i].contact_string;
	    ret_msg.members[ret_msg.member_count].event_contact_string =
		chan->rem_member_list[i].event_contact_string;
	    ret_msg.members[ret_msg.member_count].client_target_stone_id = 
		chan->rem_member_list[i].remote_target_stone_id;
	    ret_msg.members[ret_msg.member_count].client_channel_id =
		chan->rem_member_list[i].remote_id;
	    ret_msg.members[ret_msg.member_count].has_sinks =
		chan->rem_member_list[i].has_sinks;
	    ret_msg.members[ret_msg.member_count].has_sources =
		chan->rem_member_list[i].has_sources;
	    ret_msg.member_count++;
	}
    }

    ECtrace_out("sending derivitive channel IDs", 3);
    ret_msg.der_count = 0;
    for (i = 0; i < chan->der_list_len; i++) {
	EChannel der_chan = chan->der_chan_list[i].derived_chan;
	if (ret_msg.der_count == 0)
	    ret_msg.derivatives = CMmalloc(sizeof(AttendRec));
	else
	    ret_msg.derivatives = CMrealloc(ret_msg.derivatives,
					    (ret_msg.der_count +
					     1) * sizeof(AttendRec));
	ret_msg.derivatives[ret_msg.der_count].contact_string =
	    ECglobal_id(der_chan);
	ret_msg.derivatives[ret_msg.der_count].client_channel_id =
	    der_chan->global_id.global_channel_id;
	ret_msg.derivatives[ret_msg.der_count].filter =
	    chan->der_chan_list[i].filter;
	ret_msg.derivatives[ret_msg.der_count].filter_has_output_param =
	    chan->der_chan_list[i].has_output_param;
	ret_msg.der_count++;
    }

    if (!CMwrite
	(conn, CMlookup_format(cm, attend_response_format_list),
	 &ret_msg)) {
	ECtrace_out("sent channel attend response write failed", 0);
    }
    for (i = 0; i < ret_msg.der_count; i++) {
	CMfree(ret_msg.derivatives[i].contact_string);
    }
    if (ret_msg.derivatives)
	CMfree(ret_msg.derivatives);
    if (ret_msg.members)
	CMfree(ret_msg.members);
    CMfree(ret_msg.type_name);
}

static void
send_old_attend_response(cm, conn, chan, cond)
CManager cm;
CMConnection conn;
EChannel chan;
int cond;
{
    OldAttendResponseMsg ret_msg;
    int i;

    /* send him the sink list (add local) */
    ECtrace_out("send him sink list, local and remote", 3);
    memset(&ret_msg, 0, sizeof(ret_msg));
    ret_msg.condition = cond;
    ret_msg.type_name = NULL;
    ret_msg.global_channel_id = chan->global_id.global_channel_id;
/*    if (chan->type_format) {
	ret_msg.type_name = global_name_of_IOformat(chan->type_format);
	ECtrace_out("type of format is %s", 3, ret_msg.type_name);
	}*/
    ret_msg.sink_count = 0;
    ret_msg.sinks = CMmalloc(sizeof(AttendRec));
    if (chan->local_sink_list_len != 0) {
	ret_msg.sinks[ret_msg.sink_count].contact_string =
	    chan->global_id.contact_string;
	ret_msg.sinks[ret_msg.sink_count].filter = NULL;
	ret_msg.sinks[ret_msg.sink_count].filter_has_output_param = 0;
	set_channel_id(&ret_msg.sinks[ret_msg.sink_count].
		       client_channel_id, chan);
	ret_msg.sink_count++;
    }



    for (i = 0; i < chan->rem_member_list_len; i++) {
	if (chan->rem_member_list[i].has_sinks == 0)
	    continue;
	if (chan->rem_member_list[i].conn != NULL) {
	    ret_msg.sinks = CMrealloc(ret_msg.sinks,
				      (ret_msg.sink_count +
				       1) * sizeof(AttendRec));

	    ret_msg.sinks[ret_msg.sink_count].contact_string =
		chan->rem_member_list[i].contact_string;
	    ret_msg.sinks[ret_msg.sink_count].filter = NULL;
	    ret_msg.sinks[ret_msg.sink_count].filter_has_output_param = 0;
	    ret_msg.sinks[ret_msg.sink_count].client_channel_id =
		chan->rem_member_list[i].remote_id;
	    ret_msg.sink_count++;
	}
    }

    ECtrace_out("send him source list, local and remote", 3);

    ret_msg.source_count = 0;
    ret_msg.sources = CMmalloc(sizeof(AttendRec));
    if (chan->local_source_list_len != 0) {
	ret_msg.sources[ret_msg.source_count].contact_string =
	    chan->global_id.contact_string;
	ret_msg.sources[ret_msg.source_count].filter = NULL;
	ret_msg.sources[ret_msg.source_count].filter_has_output_param = 0;
	set_channel_id(&ret_msg.sources[ret_msg.source_count].
		       client_channel_id, chan);
	ret_msg.source_count++;
    }
    for (i = 0; i < chan->rem_member_list_len; i++) {
	if (chan->rem_member_list[i].has_sources == 0)
	    continue;
	if (chan->rem_member_list[i].conn != NULL) {
	    ret_msg.sources = CMrealloc(ret_msg.sources,
					(ret_msg.source_count +
					 1) * sizeof(AttendRec));

	    ret_msg.sources[ret_msg.source_count].contact_string =
		chan->rem_member_list[i].contact_string;
	    ret_msg.sources[ret_msg.source_count].filter = NULL;
	    ret_msg.sources[ret_msg.source_count].filter_has_output_param =
		0;
	    ret_msg.sources[ret_msg.source_count].client_channel_id =
		chan->rem_member_list[i].remote_id;
	    ret_msg.source_count++;
	}
    }

    ECtrace_out("send him the remote list", 3);

    ret_msg.member_count = 1;
    ret_msg.members = CMmalloc(sizeof(AttendRec));
    ret_msg.members[0].contact_string = chan->global_id.contact_string;
    ret_msg.members[0].filter = NULL;
    ret_msg.members[0].filter_has_output_param = 0;
    set_channel_id(&ret_msg.members[0].client_channel_id, chan);
    for (i = 0; i < chan->rem_member_list_len; i++) {
	if (chan->rem_member_list[i].conn != NULL) {
	    ret_msg.members = CMrealloc(ret_msg.members,
					(ret_msg.member_count +
					 1) * sizeof(AttendRec));
	    ret_msg.members[ret_msg.member_count].contact_string =
		chan->rem_member_list[i].contact_string;
	    ret_msg.members[ret_msg.member_count].filter = NULL;
	    ret_msg.members[ret_msg.member_count].filter_has_output_param =
		0;
	    ret_msg.members[ret_msg.member_count].client_channel_id =
		chan->rem_member_list[i].remote_id;
	    ret_msg.member_count++;
	}
    }

    ECtrace_out
	("Sending list: \n Remote sink count %d Remote source count %d Remote member count %d",
	 3, ret_msg.sink_count, ret_msg.source_count,
	 ret_msg.member_count);

    ECtrace_out("sending derivitive channel IDs", 3);
    ret_msg.der_count = 0;
    for (i = 0; i < chan->der_list_len; i++) {
	EChannel der_chan = chan->der_chan_list[i].derived_chan;
	if (ret_msg.der_count == 0)
	    ret_msg.derivatives = CMmalloc(sizeof(AttendRec));
	else
	    ret_msg.derivatives = CMrealloc(ret_msg.derivatives,
					    (ret_msg.der_count +
					     1) * sizeof(AttendRec));
	ret_msg.derivatives[ret_msg.der_count].contact_string =
	    ECglobal_id(der_chan);
	ret_msg.derivatives[ret_msg.der_count].client_channel_id =
	    der_chan->global_id.global_channel_id;
	ret_msg.derivatives[ret_msg.der_count].filter =
	    chan->der_chan_list[i].filter;
	ret_msg.derivatives[ret_msg.der_count].filter_has_output_param =
	    chan->der_chan_list[i].has_output_param;
	ret_msg.der_count++;
    }

    if (!CMwrite
	(conn, CMlookup_format(cm, old_attend_response_format_list),
	 &ret_msg)) {
	ECtrace_out("sent channel attend response write failed", 0);
    }
    for (i = 0; i < ret_msg.der_count; i++) {
	CMfree(ret_msg.derivatives[i].contact_string);
    }
    if (ret_msg.derivatives)
	CMfree(ret_msg.derivatives);
    if (ret_msg.sinks)
	CMfree(ret_msg.sinks);
    if (ret_msg.sources)
	CMfree(ret_msg.sources);
    if (ret_msg.members)
	CMfree(ret_msg.members);
    CMfree(ret_msg.type_name);
}

extern void
Old_Channel_Attend_handler(cm, conn, data, client_data, attrs)
CManager cm;
CMConnection conn;
void *data;
void *client_data;
attr_list attrs;
{
    AttendMsg new_msg;
    AttendMsgPtr msg = (AttendMsgPtr) data;
    new_msg.chan_str = msg->chan_str;
    new_msg.contact_str = msg->contact_str;
    new_msg.cond = msg->cond;
    new_msg.client_channel_id = msg->client_channel_id;
    new_msg.proto_version = 1;	/* Send new response and do conversion at
				 * the receiver. -sandip */
    Channel_Attend_handler(cm, conn, (void *) &new_msg, client_data,
			   attrs);
}

extern void
Channel_SinkSubscribe_handler(cm, conn, data, client_data, attrs)
CManager cm;
CMConnection conn;
void *data;
void *client_data;
attr_list attrs;
{

    SinkSubscribeMsgPtr msg = (SinkSubscribeMsgPtr) data;
    EControlContext ec = (EControlContext) client_data;
    EChannel chan;
    attr_list attrs_local;
    EVstone output;

    if (channel_verbose) {
	printf("DEChannel_SinkSubscribe_handler, global chan_id = ");
	print_channel_id(msg->global_channel_id);
	printf("\n");
    }
    chan = get_local_channel_from_id(msg->global_channel_id, ec);

    if (chan != NULL) {
	/* first check to see if we already know about this sink */
	int found_it = 0;
	int i;
	for (i = 0; i < chan->rem_member_list_len; i++) {
	    if (conn != chan->rem_member_list[i].conn) {
		if (channel_verbose)
		    printf("rejecting member[%d].conn %lx, %lx\n", i,
			   (long) conn,
			   (long) chan->rem_member_list[i].conn);
		continue;
	    }
	    if (msg->client_channel_id.len !=
		chan->rem_member_list[i].remote_id.len) {
		if (channel_verbose)
		    printf("rejecting member[%d].len %d, %d\n", i,
			   msg->client_channel_id.len,
			   chan->rem_member_list[i].remote_id.len);
		continue;
	    }
	    if (memcmp(msg->client_channel_id.channel,
		       chan->rem_member_list[i].remote_id.channel,
		       chan->rem_member_list[i].remote_id.len) != 0) {
		if (channel_verbose)
		    printf("rejecting member %d on memcmp\n", i);
		continue;
	    }
	    found_it = 1;
	    if (chan->rem_member_list[i].has_sinks == 0) {
		chan->rem_member_list[i].has_sinks = 1;
		char *tmp = strdup(chan->rem_member_list[i].contact_string);
		attrs_local = attr_list_from_string(tmp);
		free(tmp);
		output = EValloc_stone(cm);
		EVassoc_bridge_action(cm, output, attrs_local,
				      chan->rem_member_list[i].remote_target_stone_id);
		EVaction_add_split_target(cm, chan->outgoing_target_stone_id,
					  chan->outgoing_split_action,
					  output);
		if (channel_verbose > 0)
		    printf("adding sink to chan %lx\n", (long) chan);
		if (chan->subscribe_func) {
		    int sink_count = 0, i;
		    for (i = 0; i < chan->rem_member_list_len; i++) {
			if (chan->rem_member_list[i].has_sinks)
			    sink_count++;
		    }
		    chan->subscribe_func(1, sink_count,
					 chan->subscribe_client_data);
		}
	    } else {
		if (channel_verbose > 0) {
		    printf("already knew about this sink on chan %lx  (Sinks %d)\n",
			   (long) chan, chan->rem_member_list[i].has_sinks);
		}
	    }
	    break;
	}
	if (found_it == 0) {
	    printf("Failed to locate sink member on conn %lx, ID ",
		   (long) conn);
	    print_channel_id(msg->client_channel_id);
	    for (i = 0; i < chan->rem_member_list_len; i++) {
		printf("\n   Member %d conn %lx has ID ", i,
		       (long) chan->rem_member_list[i].conn);
		print_channel_id(chan->rem_member_list[i].remote_id);
	    }
	    printf("\n");
	}
	if (channel_verbose) {
	    printf("\n\nChannel_SinkSubscribe_handler\n");
	    dump_EChannel(chan);
	}
    }
    ECtrace_out("Channel_SinkSubscribe_handler", 0);
}

extern void
Channel_SinkUnsubscribe_handler(cm, conn, data, client_data, attrs)
CManager cm;
CMConnection conn;
void *data;
void *client_data;
attr_list attrs;
{
    SinkSubscribeMsgPtr msg = (SinkSubscribeMsgPtr) data;
    EControlContext ec = (EControlContext) client_data;
    EChannel chan;
    int i;

    chan = get_local_channel_from_id(msg->global_channel_id, ec);
    ECtrace_out("Channel_SinkUnsubscribe_handler, chan %lx", 1, chan);

    if (chan != NULL) {
	ECtrace_out("remove him from the sink list", 3);
	if (channel_verbose > 0)
	    printf("removing sink from chan %lx\n", (long) chan);

	for (i = 0; i < chan->rem_member_list_len; i++) {
	    int local_len = chan->rem_member_list[i].remote_id.len;
	    if ((local_len == msg->client_channel_id.len) &&
		(memcmp(chan->rem_member_list[i].remote_id.channel,
			msg->client_channel_id.channel, local_len) == 0)) {
		chan->rem_member_list[i].has_sinks = 0;
		break;
	    }
	}
	if (channel_verbose) {
	    printf("\n\nChannel_SinkUnsubscribe_handler\n");
	    dump_EChannel(chan);
	}
    }
    ECtrace_out("Channel_SinkUnsubscribe_handler", 0);
}

extern void
Channel_SourceSubscribe_handler(cm, conn, data, client_data, attrs)
CManager cm;
CMConnection conn;
void *data;
void *client_data;
attr_list attrs;
{
    SourceSubscribeMsgPtr msg = (SourceSubscribeMsgPtr) data;
    EControlContext ec = (EControlContext) client_data;
    EChannel chan;

    chan = get_local_channel_from_id(msg->global_channel_id, ec);
    ECtrace_out("DEChannel_SourceSubscribe_handler, chan %lx", 1, chan);

    if (chan != NULL) {

	/* first check to see if we already know about this source */
	int i;
	for (i = 0; i < chan->rem_member_list_len; i++) {
	    if (conn != chan->rem_member_list[i].conn)
		continue;
	    if (msg->client_channel_id.len !=
		chan->rem_member_list[i].remote_id.len)
		continue;
	    if (memcmp(msg->client_channel_id.channel,
		       chan->rem_member_list[i].remote_id.channel,
		       chan->rem_member_list[i].remote_id.len) != 0)
		continue;
	    if (chan->rem_member_list[i].has_sources == 0) {
		chan->rem_member_list[i].has_sources = 1;
		if (channel_verbose > 0)
		    printf("adding source to chan %lx\n", (long) chan);
	    } else {
		if (channel_verbose > 0) {
		    printf("already knew about this source on chan %lx\n",
			   (long) chan);
		}
	    }
	    break;
	}
    }
    ECtrace_out("Channel_SourceSubscribe_handler", 0);
}

extern void
Channel_SourceUnsubscribe_handler(cm, conn, data, client_data, attrs)
CManager cm;
CMConnection conn;
void *data;
void *client_data;
attr_list attrs;
{
    SourceSubscribeMsgPtr msg = (SourceSubscribeMsgPtr) data;
    EControlContext ec = (EControlContext) client_data;
    EChannel chan;
    int i;

    chan = get_local_channel_from_id(msg->global_channel_id, ec);
    ECtrace_out("Channel_SourceUnsubscribe_handler, chan %lx", 1, chan);

    if (chan != NULL) {
	/* check to see that channel is valid */

	ECtrace_out("remove him from the source list", 3);
	if (channel_verbose > 0)
	    printf("removing source from chan %lx\n", (long) chan);

	for (i = 0; i < chan->rem_member_list_len; i++) {
	    int local_len = chan->rem_member_list[i].remote_id.len;
	    if ((local_len == msg->client_channel_id.len) &&
		(memcmp(chan->rem_member_list[i].remote_id.channel,
			msg->client_channel_id.channel, local_len) == 0)) {
		chan->rem_member_list[i].has_sources = 0;
	    }
	}
	if (channel_verbose) {
	    printf("\n\nChannel_SourceUnsubscribe_handler\n");
	    dump_EChannel(chan);
	}
    }
    ECtrace_out("Channel_SourceUnsubscribe_handler", 0);
}

extern void
Channel_MemberSubscribe_handler(cm, conn, data, client_data, attrs)
CManager cm;
CMConnection conn;
void *data;
void *client_data;
attr_list attrs;
{
    MemberSubscribeMsgPtr msg = (MemberSubscribeMsgPtr) data;
    EControlContext ec = (EControlContext) client_data;
    EChannel chan;


    ECtrace_out("Channel_MemberSubscribe_handler", 1);
    CMconn_register_close_handler(conn, EChannel_close_handler, ec);
    set_conn_characteristics(conn);
    /* check to see that channel is valid */
    chan = get_local_channel_from_id(msg->global_channel_id, ec);

    if (chan != NULL) {
	int i, member = chan->rem_member_list_len;
	/* see if he's already a member */
	for (i = 0; i < chan->rem_member_list_len; i++) {
	    if (conn == chan->rem_member_list[i].conn) {
		member = i;
		break;
	    }
	    if (strcmp(msg->client_contact_str,
		       chan->rem_member_list[i].contact_string) == 0) {
		member = i;
		break;
	    }
	}
	ECtrace_out("Channel_MemberSubscribe_handler - member is %d", 3,
		    i);
	if (member == chan->rem_member_list_len) {
	    /* add him to the the member list */
	    ECtrace_out("add him to the the member list", 3);
	    chan->rem_member_list =
		CMrealloc(chan->rem_member_list,
			  sizeof(struct _rem_member_entry) *
			  (chan->rem_member_list_len + 1));
	    memset(&chan->rem_member_list[chan->rem_member_list_len], 0,
		   sizeof(chan->rem_member_list[0]));
	    chan->rem_member_list[chan->rem_member_list_len].conn = conn;
	    chan->rem_member_list[chan->rem_member_list_len].event_conn =
		NULL;
	    chan->rem_member_list[chan->rem_member_list_len].
		contact_string = strdup(msg->client_contact_str);
	    chan->rem_member_list[chan->rem_member_list_len].remote_target_stone_id = msg->client_target_stone_id;
	    chan->rem_member_list[chan->rem_member_list_len].remote_id =
		copy_channel_id(msg->client_channel_id);
	    chan->rem_member_list[chan->rem_member_list_len].has_sinks = 0;
	    chan->rem_member_list[chan->rem_member_list_len].has_sources =
		0;
	    chan->rem_member_list_len++;
	} else {
	    ECtrace_out("Already a member at index %d", 3, member);
	}
	if (msg->event_contact_str) {
	    char *tmp_str = strdup(msg->event_contact_str);
	    attr_list attrs =
		attr_list_from_string(msg->event_contact_str);
	    ECtrace_out
		("connect and register close handler for event contact",
		 3);
	    chan->rem_member_list[member].event_conn =
		CMget_conn(cm, attrs);
	    free_attr_list(attrs);
	    if (chan->rem_member_list[member].event_conn == NULL) {
		printf("Failed to contact %s\n", tmp_str);
		free(tmp_str);
	    }
	    CMconn_register_close_handler(chan->rem_member_list[member].
					  event_conn,
					  EChannel_close_handler, ec);
	    set_conn_characteristics(chan->rem_member_list[member].event_conn);
	}
	if (channel_verbose) {
	    printf("\n\nChannel_MemberSubscribe_handler\n");
	    dump_EChannel(chan);
	}
	/* 
	 * The new opener *might* have missed our subscribe because 
	 * of a race condition.  Just in case, send it to him.
	 */
	if (chan->local_sink_list_len != 0) {
	    /* true means "subscribe" */
	    send_sink_subscribe(chan, chan->rem_member_list_len - 1, 1);
	}
	if (chan->local_source_list_len != 0) {
	    /* true means "subscribe" */
	    send_source_subscribe(chan, chan->rem_member_list_len - 1, 1);
	}
    }
    ECtrace_out("Channel_MemberSubscribe_handler", 0);
}

extern void
EChannel_close_handler(cm, conn, client_data)
CManager cm;
CMConnection conn;
void *client_data;
{
    int i;

    EControlContext ec = (EControlContext) client_data;
    ECtrace_out("Channel_close_handler, ec = %lx, conn = %lx", 1, cm,
		conn);
    for (i = 0; i < ec->channel_count; i++) {
	EChannel chan = ec->channel_list[i];
	int j;
	int deleted = 0;
	ECtrace_out("removing him from remote member list", 3);
	for (j = 0; j < chan->rem_member_list_len; j++) {
	    if (chan->rem_member_list[j].conn == conn) {
		chan->rem_member_list[j].conn = NULL;
		chan->rem_member_list[j].event_conn = NULL;
	    }
	    if (chan->rem_member_list[j].event_conn == conn) {
		chan->rem_member_list[j].conn = NULL;
		chan->rem_member_list[j].event_conn = NULL;
	    }
	}
	for (j = 0; (j + deleted) < chan->rem_member_list_len; j++) {
	    if (chan->rem_member_list[j].conn == NULL) {
		deleted++;
	    }
	    if ((deleted != 0) &&
		(j + deleted != chan->rem_member_list_len)) {
		chan->rem_member_list[j] =
		    chan->rem_member_list[j + deleted];
	    }
	}
	chan->rem_member_list_len -= deleted;

    }
    ECtrace_out("Channel_close_handler", 0);
}


extern void
Channel_Exists_Attend_Response_handler(cm, conn, data, client_data, attrs)
CManager cm;
CMConnection conn;
void *data;
void *client_data;
attr_list attrs;
{
    ExistsAttendResponseMsgPtr msg = (ExistsAttendResponseMsgPtr) data;
    EChannel chan, old_chan, *chan_ptr;
    EControlContext ec = (EControlContext) client_data;

    ECtrace_out("Channel_Exists_Attend_Response_handler", 1);
    if (channel_verbose > 0) {
	printf("In exists attend handler response, cond %d\n",
	       msg->condition);
    }

    CMconn_register_close_handler(conn, EChannel_close_handler, ec);
    set_conn_characteristics(conn);
    chan_ptr = CMCondition_get_client_data(cm, msg->condition);
    old_chan = *chan_ptr;

    ECtrace_out
	("Channel_exists_Attend_Response_handler, destroying old chan %lx",
	 3, old_chan);
    EChannel_destroy(old_chan);

    /* 
     * change the channel variable in the waiting EChannel_open() to return
     * the already-existing channel instead.
     */
    chan = get_local_channel_from_id(msg->existing_channel_id, ec);
    *chan_ptr = chan;
    CMCondition_signal(cm, msg->condition);
    ECtrace_out("Channel_Exists_Attend_Response_handler", 0);
}


/* 
 * Channel_Attend_Response_handler() is the back half of EChannel_open().  
 * This gets invoked in response to the message from the owner of the
 * channel giving us complete channel information (location of sources,
 * sinks and other attendees.  We setup all necessary communications links,
 * fill out the channel data structure and signal the condition to continue
 * the waiting EChannel_open() call.
 */
extern void
Channel_Attend_Response_handler(cm, conn, data, client_data, jattrs)
CManager cm;
CMConnection conn;
void *data;
void *client_data;
attr_list jattrs;
{
    AttendResponseMsgPtr msg = (AttendResponseMsgPtr) data;
    EControlContext ec = (EControlContext) client_data;
    EChannel chan, der_chan, *chan_ptr;
    int i;
    attr_list create_attrs = NULL;

    ECtrace_out("Channel_Attend_Response_handler", 1);
    if (channel_verbose > 0) {
	printf("In attend handler response, cond %d\n", msg->condition);
    }
    /* preserve the message... */
    CMtake_buffer(cm, data);

    CMconn_register_close_handler(conn, EChannel_close_handler, ec);
    set_conn_characteristics(conn);
    chan_ptr = CMCondition_get_client_data(cm, msg->condition);
    chan = *chan_ptr;

    if (msg->create_attrs_string) {
	create_attrs =
	    attr_list_from_string(strdup(msg->create_attrs_string));
	set_attr_info(chan, cm, create_attrs);
    }

    if (chan->global_id.global_channel_id.channel)
	CMfree(chan->global_id.global_channel_id.channel);
    chan->global_id.global_channel_id =
	copy_channel_id(msg->global_channel_id);
    ECtrace_out("Channel_Attend_Response_handler, chan %lx, type %x", 3,
		chan, msg->type_name);
/*    chan->subcontext = CMget_user_type_context(chan->cm);*/

    if (msg->type_name != NULL) {
/*	chan->type_format =
	    CMget_IOformat_by_name(chan->cm, chan->subcontext,
	    msg->type_name);
	if (chan->type_format == NULL) {
	    printf
		("ECho/PBIO was unable to translate the PBIO format ID \"%s\".  Format server problems?\n  Try the FORMAT_SERVER_VERBOSE environment variable.\n",
		 msg->type_name);
		 }*/
    }

    if (msg->filter_data_len != 0) {
	ECsetup_filter_data(chan, msg->filter_data, msg->filter_data_len);
    }

    ECtrace_out
	("Building lists: \n Remote member count %d\n",
	 3, msg->member_count);


    /* build remote member list */

    chan->rem_member_list =
	CMrealloc(chan->rem_member_list,
		  sizeof(struct _rem_member_entry) * msg->member_count);
    memset(chan->rem_member_list, 0,
	   sizeof(chan->rem_member_list[0]) * msg->member_count);
    chan->rem_member_list_len = 0;
    for (i = 0; i < msg->member_count; i++) {
	int j = chan->rem_member_list_len;
	char *tmp_str = strdup(msg->members[i].contact_string);
	attr_list attrs =
	    attr_list_from_string(msg->members[i].contact_string);
	if (CMcontact_self_check(ec->cm, attrs)) {
	    /* this member is really local */
	    free_attr_list(attrs);
	    free(tmp_str);
	    continue;
	}
	ECtrace_out
	    ("connect and register close handler for remote members", 3);
	chan->rem_member_list[j].conn = CMget_conn(cm, attrs);
	free_attr_list(attrs);
	if (chan->rem_member_list[j].conn == NULL) {
	    printf("Failed to contact member %d, %s\n", j, tmp_str);
	    free(tmp_str);
	    continue;
	}
	CMconn_register_close_handler(chan->rem_member_list[j].conn,
				      EChannel_close_handler, ec);
	set_conn_characteristics(chan->rem_member_list[j].conn);
	chan->rem_member_list[j].remote_id =
	    copy_channel_id(msg->members[i].client_channel_id);
	chan->rem_member_list[j].contact_string = tmp_str;
	chan->rem_member_list[j].remote_target_stone_id =
	    msg->members[i].client_target_stone_id;
	chan->rem_member_list[j].has_sinks = msg->members[i].has_sinks;
	chan->rem_member_list[j].has_sources = msg->members[i].has_sources;
	if (chan->rem_member_list[j].has_sinks) {
	    EVstone output = EValloc_stone(cm);
	    char *tmp_str = strdup(msg->members[i].contact_string);
	    attr_list contact_attrs =
		attr_list_from_string(msg->members[i].contact_string);
	    EVassoc_bridge_action(cm, output, contact_attrs,
				  chan->rem_member_list[i].remote_target_stone_id);
	    EVaction_add_split_target(cm, chan->outgoing_target_stone_id,
				      chan->outgoing_split_action,
				      output);
	    free(tmp_str);
	    free_attr_list(contact_attrs);
	}
	if (msg->members[i].event_contact_string) {
	    char *tmp_str = strdup(msg->members[i].event_contact_string);
	    attr_list attrs =
		attr_list_from_string(msg->members[i].
				      event_contact_string);
	    ECtrace_out
		("connect and register close handler for event contact", 3);
	    chan->rem_member_list[j].event_conn = CMget_conn(cm, attrs);
	    free_attr_list(attrs);
	    if (chan->rem_member_list[j].event_conn == NULL) {
		printf("Failed to contact event conn %d, %s\n", j, tmp_str);
		free(tmp_str);
		continue;
	    }
	    CMconn_register_close_handler(chan->rem_member_list[j].event_conn,
					  EChannel_close_handler, ec);
	    set_conn_characteristics(chan->rem_member_list[j].event_conn);
	}

	send_member_subscribe(chan, j);

	chan->rem_member_list_len++;
    }

    for (i = 0; i < msg->der_count; i++) {
	char *filter;
	ECtrace_out("open, subscribe, and add derived channel to der_list",
		    3);
	der_chan = EChannel_open(ec, msg->derivatives[i].contact_string);

	chan->der_chan_list = CMrealloc(chan->der_chan_list,
					(chan->der_list_len +
					 1) *
					sizeof(struct _derived_chan));
	chan->der_chan_list[chan->der_list_len].derived_chan = der_chan;
	filter = CMmalloc(strlen(msg->derivatives[i].filter) + 2);
	strcpy(filter, msg->derivatives[i].filter);
	filter[strlen(filter) + 1] = 0;	/* add extra NULL termination */
	chan->der_chan_list[chan->der_list_len].filter = filter;
	chan->der_chan_list[chan->der_list_len].has_output_param =
	    msg->derivatives[i].filter_has_output_param;
	chan->der_list_len++;
    }

    if (channel_verbose > 0) {
	printf("\n\nChannel_Attend_Response_handler\n");
	dump_EChannel(chan);
    }
    ECtrace_out("Channel_Attend_Response_handler", 0);
    chan->ready++;
    CMCondition_signal(cm, msg->condition);
    CMreturn_buffer(cm, data);
}

extern void
Old_Channel_Attend_Response_handler(cm, conn, data, client_data, attrs)
CManager cm;
CMConnection conn;
void *data;
void *client_data;
attr_list attrs;
{
    OldAttendResponseMsgPtr msg = (OldAttendResponseMsgPtr) data;
    EControlContext ec = (EControlContext) client_data;
    EChannel chan, der_chan, *chan_ptr;
    int i;

    ECtrace_out("Old_Channel_Attend_Response_handler", 1);
    if (channel_verbose > 0) {
	printf("In attend handler response, cond %d\n", msg->condition);
    }
    /* preserve the message... */
    CMtake_buffer(cm, data);

    CMconn_register_close_handler(conn, EChannel_close_handler, ec);
    set_conn_characteristics(conn);
    chan_ptr = CMCondition_get_client_data(cm, msg->condition);
    chan = *chan_ptr;

    if (chan->global_id.global_channel_id.channel)
	CMfree(chan->global_id.global_channel_id.channel);
    chan->global_id.global_channel_id =
	copy_channel_id(msg->global_channel_id);
    ECtrace_out("Old_Channel_Attend_Response_handler, chan %lx, type %x",
		3, chan, msg->type_name);
/*    if (msg->type_name != NULL) {
	chan->type_format =
	    CMget_IOformat_by_name(chan->cm,
				   CMget_user_type_context(chan->cm),
				   msg->type_name);
				   }*/

    ECtrace_out
	("Building lists: \n Remote sink count %d Remote source count %d Remote member count %d\n",
	 3, msg->sink_count, msg->source_count, msg->member_count);


    /* build remote member list */

    chan->rem_member_list =
	CMrealloc(chan->rem_member_list,
		  sizeof(struct _rem_member_entry) * msg->member_count);
    memset(chan->rem_member_list, 0,
	   sizeof(chan->rem_member_list[0]) * msg->member_count);
    chan->rem_member_list_len = 0;
    for (i = 0; i < msg->member_count; i++) {
	int j = chan->rem_member_list_len;
	char *tmp_str = strdup(msg->members[i].contact_string);
	attr_list attrs =
	    attr_list_from_string(msg->members[i].contact_string);
	if (CMcontact_self_check(ec->cm, attrs)) {
	    /* this member is really local */
	    free_attr_list(attrs);
	    free(tmp_str);
	    continue;
	}
	ECtrace_out
	    ("connect and register close handler for remote members", 3);
	chan->rem_member_list[j].conn = CMget_conn(cm, attrs);
	free_attr_list(attrs);
	if (chan->rem_member_list[j].conn == NULL) {
	    printf("Failed to contact old member %d, %s\n", j, tmp_str);
	    free(tmp_str);
	    continue;
	}
	CMconn_register_close_handler(chan->rem_member_list[j].conn,
				      EChannel_close_handler, ec);
	set_conn_characteristics(chan->rem_member_list[j].conn);
	chan->rem_member_list[j].remote_id =
	    copy_channel_id(msg->members[i].client_channel_id);
	chan->rem_member_list[j].contact_string = tmp_str;
	chan->rem_member_list[j].has_sinks = 0;
	chan->rem_member_list[j].has_sources = 0;
	if (i != 0) {
	    /* first entry is channel we got this from */
	    send_member_subscribe(chan, j);
	}
	chan->rem_member_list_len++;
    }

    /* build remote sink list */

    ECtrace_out("connect and register close handler for remote sinks", 3);
    for (i = 0; i < msg->sink_count; i++) {
	int j;
	for (j = 0; j <= chan->rem_member_list_len; j++) {
	    if (j == chan->rem_member_list_len) {
		printf("Bad! sink not found\n");
		break;
	    }
	    if (strcmp(msg->sinks[i].contact_string,
		       chan->rem_member_list[j].contact_string) == 0) {
		chan->rem_member_list[j].has_sinks = 1;
		break;
	    }
	}
    }

    /* build remote source list */

    ECtrace_out("connect and register close handler for remote sources",
		3);
    for (i = 0; i < msg->source_count; i++) {
	int j;
	for (j = 0; j <= chan->rem_member_list_len; j++) {
	    if (j == chan->rem_member_list_len) {
		printf("Bad! source not found\n");
		break;
	    }
	    if (strcmp(msg->sources[i].contact_string,
		       chan->rem_member_list[j].contact_string) == 0) {
		chan->rem_member_list[j].has_sources = 1;
		break;
	    }
	}
    }
    for (i = 0; i < msg->der_count; i++) {
	char *filter;
	ECtrace_out("open, subscribe, and add derived channel to der_list",
		    3);
	der_chan = EChannel_open(ec, msg->derivatives[i].contact_string);

	chan->der_chan_list = CMrealloc(chan->der_chan_list,
					(chan->der_list_len +
					 1) *
					sizeof(struct _derived_chan));
	chan->der_chan_list[chan->der_list_len].derived_chan = der_chan;
	filter = CMmalloc(strlen(msg->derivatives[i].filter) + 2);
	strcpy(filter, msg->derivatives[i].filter);
	filter[strlen(filter) + 1] = 0;	/* add extra NULL termination */
	chan->der_chan_list[chan->der_list_len].filter = filter;
	chan->der_chan_list[chan->der_list_len].has_output_param =
	    msg->derivatives[i].filter_has_output_param;
	chan->der_list_len++;
    }

    if (channel_verbose > 0) {
	printf("\n\nOld_Channel_Attend_Response_handler\n");
	dump_EChannel(chan);
    }
    ECtrace_out("Old_Channel_Attend_Response_handler", 0);
    chan->ready++;
    CMCondition_signal(cm, msg->condition);
    CMreturn_buffer(cm, data);
}

/* Returns 0 if unsuccessful */
FMOptInfo *
set_opt_info(CMFormat orig_cm_format, char *format_name)
{
    FMOptInfo *opt_info = (FMOptInfo *) malloc(sizeof(FMOptInfo) * 2);
#ifdef NOT_DEF
    char *new_channel_response_code = "\n\
    {\n\
	int i;\n\
	int j;\n\
	int sink_count=0;\n\
	int source_count=0;\n\
	old.condition = new.condition;\n\
	old.global_channel_id.len = new.global_channel_id.len;\n\
	for(i=0; i<old.global_channel_id.len; i=i+1)\n\
		old.global_channel_id.id[i] = new.global_channel_id.id[i];\n\
	if(new.type_name)\n\
	    old.type_name = \"Channel Attend Response\";\n\
	old.member_count = new.member_count;\n\
	for(i=0; i<new.member_count; i=i+1){\n\
	    old.members[i].contact_string = new.members[i].contact_string;\n\
	    old.members[i].client_channel_id.len = new.members[i].client_channel_id.len;\n\
	    for(j=0; j<old.members[i].client_channel_id.len; j=j+1)\n\
		old.members[i].client_channel_id.id[j] = new.members[i].client_channel_id.id[j];\n\
	    old.members[i].filter_has_output_param = 0;\n\
	    if(new.members[i].has_sources){\n\
		old.source_count = source_count + 1;\n\
		old.sources[source_count].contact_string = new.members[i].contact_string;\n\
		old.sources[source_count].client_channel_id.len = new.members[i].client_channel_id.len;\n\
		for(j=0; j<old.sources[source_count].client_channel_id.len; j=j+1)\n\
		    old.sources[source_count].client_channel_id.id[j] = new.members[i].client_channel_id.id[j];\n\
		old.sources[source_count].filter_has_output_param = 0;\n\
		source_count = source_count + 1;\n\
	    }\n\
	    if(new.members[i].has_sinks){\n\
		old.sink_count = sink_count + 1;\n\
		old.sinks[sink_count].contact_string = new.members[i].contact_string;\n\
		old.sinks[sink_count].client_channel_id.len = new.members[i].client_channel_id.len;\n\
		for(j=0; j<old.sinks[sink_count].client_channel_id.len; j=j+1)\n\
		    old.sinks[sink_count].client_channel_id.id[j] = new.members[i].client_channel_id.id[j];\n\
		old.sinks[sink_count].filter_has_output_param = 0;\n\
		sink_count = sink_count + 1;\n\
	    }\n\
	}\n\
	old.der_count = new.der_count;\n\
	for(i=0; i<new.der_count; i=i+1){\n\
	    old.derivatives[i].contact_string = new.derivatives[i].contact_string;\n\
	    old.derivatives[i].client_channel_id.len = new.derivatives[i].client_channel_id.len;\n\
	    for(j=0; j<old.derivatives[i].client_channel_id.len; j=j+1)\n\
		old.derivatives[i].client_channel_id.id[j] = new.derivatives[i].client_channel_id.id[j];\n\
	    old.derivatives[i].filter = new.derivatives[i].filter;\n\
	    old.derivatives[i].filter_has_output_param = new.derivatives[i].filter_has_output_param;\n\
	}\n\
    }";
#endif
    memset(opt_info, 0, sizeof(FMOptInfo) * 2);
    opt_info[0].info_type = COMPAT_OPT_INFO;
    opt_info[0].info_block = NULL;
    opt_info[1].info_block = NULL;
    opt_info[1].info_type = 0;
    if (!strcmp(format_name, "New Channel Attend Response")) {
#ifdef NOT_DEF
	opt_info[0].info_block = CMcreate_compat_info(orig_cm_format,
						      new_channel_response_code,
						      &opt_info[0].
						      info_len);
    } else {
#endif
	free(opt_info);
	opt_info = NULL;
    }
    return opt_info;
}

extern void
EControlContext_event_dispatch(ec)
EControlContext ec;
{
    if (ec->event_queue_head != NULL) {
	do_event_dispatch(ec);
    }
}

extern void
EControlContext_event_flush(ec)
EControlContext ec;
{
    if (ec->event_queue_head != NULL) {
	do_event_flush(ec);
    }
}

FMStructDescRec attend_response_format_list[] = {
    {"New Channel Attend Response", channel_attend_response_msg_flds, sizeof(AttendResponseMsg), NULL},
    {"MemberRec", MemberRec_flds, sizeof(MemberRec), NULL},
    {"AttendRec", AttendRec_flds, sizeof(AttendRec), NULL} ,
    {"channel_ID", channel_id_flds, sizeof(channel_ID_struct), NULL},
    {NULL, NULL, 0, NULL}
};

FMStructDescRec old_attend_response_format_list[] = {
    {"Channel Attend Response", old_channel_attend_response_msg_flds, sizeof(OldAttendResponseMsg), NULL},
    {"AttendRec", AttendRec_flds, sizeof(AttendRec), NULL},
    {"channel_ID", channel_id_flds, sizeof(channel_ID_struct), NULL},
    {NULL, NULL, 0, NULL}
};

FMStructDescRec eventv_format_list[] = {
    {"EventV Msg", eventv_msg_flds, sizeof(EventVMsg), NULL},
    {"FFSEncodeElem", event_vec_elem_flds, sizeof(struct FFSEncodeVec), NULL},
    {"channel_ID", channel_id_flds, sizeof(channel_ID_struct), NULL},
    {NULL, NULL, 0, NULL}
};

FMStructDescRec channel_id_format_list[] = {
    {"channel_ID", channel_id_flds, sizeof(channel_ID_struct), NULL},
    {NULL, NULL, 0, NULL}
};

extern FMField field_list_flds[];
extern FMField format_list_flds[];
extern FMStructDescRec channel_derive_msg_formats[];
extern FMStructDescRec channel_remote_derive_msg_formats[];
extern FMStructDescRec src_derive_msg_formats[];
extern FMStructDescRec src_derive_resp_msg_formats[];
extern FMStructDescRec proto_derive_msg_formats[];
extern FMStructDescRec data_update_msg_formats[];
extern FMStructDescRec channel_info_request_msg_formats[];
extern FMStructDescRec channel_info_response_msg_formats[];

static void
ECho_dispatch_poll_func(cm, ecv)
CManager cm;
void *ecv;
{
    EControlContext_event_dispatch((EControlContext) ecv);
}

extern void
EControl_poll(ec)
EControlContext ec;
{
    EControlContext_event_dispatch(ec);
}

extern void
EControl_run(ec)
EControlContext ec;
{
    while (ec->condition != NULL) {
	EControlContext_event_dispatch(ec);
	EContext_lock(ec);
	thr_condition_wait(ec->condition, ec->lock);
	EContext_unlock(ec);
    }
}

static void
wake_threaded_server(ec)
EControlContext ec;
{
    thr_condition_signal(ec->condition);
}

static void
ECho_shutdown(CManager cm, void *ecv)
{
    EControlContext ec = (EControlContext) ecv;
    free_block_rec_p free_block_list = ec->free_block_list;
    event_queue free_event_blocks = ec->free_event_item_list;
    while (free_block_list != NULL) {
	free_block_rec_p tmp = free_block_list;
	free_block_list = tmp->next;
	CMfree(tmp);
    }
    while (free_event_blocks != NULL) {
	event_queue tmp = free_event_blocks;
	free_event_blocks = tmp->next;
	CMfree(tmp);
    }
    if (ec->channel_list)
	CMfree(ec->channel_list);
    ec->channel_list = NULL;
    thr_mutex_free(ec->lock);
    thr_condition_free(ec->condition);
    ec->lock = NULL;
    ec->condition = NULL;
    ec->cm = NULL;
    CMfree(ec);
}

extern EControlContext
EContext_create()
{
    EControlContext ec = malloc(sizeof(struct _EControlContext));
    static int atom_init = 0;
    ec->event_queue_head = NULL;
    ec->event_queue_tail = NULL;
    ec->free_event_item_list = NULL;
    ec->current_event_item = NULL;
    ec->taken_events_list = NULL;

    ec->free_block_list = NULL;
    ec->channel_count = 0;
    ec->channel_list = NULL;
    ec->cm = NULL;
    ec->lock = thr_mutex_alloc();
    ec->condition = thr_condition_alloc();
    if (!atom_init) {
	ECHO_EVENT_TRANSPORT = attr_atom_from_string("ECHO_EVENT_TRANSPORT");
	ECHO_EVENT_NETWORK = attr_atom_from_string("ECHO_EVENT_NETWORK");
	ECHO_USE_EVENT_TRANSPORT = attr_atom_from_string("ECHO_USE_EVENT_TRANSPORT");
    }
    return ec;
}

extern attr_list
EChannel_get_attrs(chan)
EChannel chan;
{
    CMConnection tmp;
    attr_list attr_tmp;

    tmp = CMget_indexed_conn(chan->cm, 0);
    if (tmp != NULL) {
	attr_tmp = CMConnection_get_attrs(tmp);
	return attr_tmp;
    } else {
	return NULL;
    }
}

extern EControlContext
ECho_CM_init(cm)
CManager cm;
{
    EControlContext ec = EContext_create();
    CMFormat tmp_format;
    attr_list contact_list;

    ec->cm = cm;
    if ((contact_list = CMget_contact_list(cm)) == NULL) {
	CMlisten(cm);
    } else {
	free_attr_list(contact_list);
    }
    CMadd_poll(cm, ECho_dispatch_poll_func, (void *) ec);

    tmp_format = CMregister_format(cm, old_channel_attend_msg_formats);
    CMregister_handler(tmp_format, Old_Channel_Attend_handler, ec);

    tmp_format = CMregister_format(cm, channel_attend_msg_formats);
    CMregister_handler(tmp_format, Channel_Attend_handler, ec);

    tmp_format = CMregister_format(cm, sink_subscribe_msg_formats);
    CMregister_handler(tmp_format, Channel_SinkSubscribe_handler, ec);

    tmp_format = CMregister_format(cm, sink_unsubscribe_msg_formats);
    CMregister_handler(tmp_format, Channel_SinkUnsubscribe_handler, ec);

    tmp_format = CMregister_format(cm, source_subscribe_msg_formats);
    CMregister_handler(tmp_format, Channel_SourceSubscribe_handler, ec);

    tmp_format = CMregister_format(cm, source_unsubscribe_msg_formats);
    CMregister_handler(tmp_format, Channel_SourceUnsubscribe_handler, ec);

    tmp_format = CMregister_format(cm, request_event_msg_formats);
    CMregister_handler(tmp_format, Channel_RequestEvent_handler, ec);

    tmp_format = CMregister_format(cm, member_subscribe_msg_formats);
    CMregister_handler(tmp_format, Channel_MemberSubscribe_handler, ec);

    /* Not yet implemented GSE tmp_format = CMregister_format(cm, "Member
     * Unsubscribe", member_unsubscribe_msg_flds, channel_id_format_list);
     * CMregister_handler(tmp_format, Channel_MemberUnsubscribe_handler,
     * ec); */
    tmp_format = CMregister_format(cm, old_attend_response_format_list);
    CMregister_handler(tmp_format, Old_Channel_Attend_Response_handler,
		       ec);
#ifdef GSE
    if ((opt_info =
	 set_opt_info(tmp_format, "New Channel Attend Response"))) {
#endif
	tmp_format =
	    CMregister_format(cm, attend_response_format_list);
	CMregister_handler(tmp_format, Channel_Attend_Response_handler,
			   ec);
#ifdef GSE
    } else
	printf("New cm registration failed :-(\n");
#endif

    tmp_format = CMregister_format(cm, channel_exists_attend_response_msg_formats);
    CMregister_handler(tmp_format, Channel_Exists_Attend_Response_handler,
		       ec);

    tmp_format = CMregister_format(cm, event_msg_formats);
    CMregister_handler(tmp_format, Channel_Event_handler, ec);

    tmp_format = CMregister_format(cm, eventv_msg_formats);
    CMregister_handler(tmp_format, Channel_EventV_handler, ec);

    tmp_format = CMregister_format(cm, channel_derive_msg_formats);
#define NO_DERIVED
#ifndef NO_DERIVED
    CMregister_handler(tmp_format, Channel_Derive_handler, ec);

    tmp_format = CMregister_format(cm, channel_remote_derive_msg_formats);
    CMregister_handler(tmp_format, Channel_Remote_Derive_handler, ec);

    tmp_format = CMregister_format(cm, src_derive_msg_formats);
    CMregister_handler(tmp_format, Channel_Source_Derive_handler, ec);

    tmp_format = CMregister_format(cm, src_derive_resp_msg_formats);
    CMregister_handler(tmp_format, Channel_Source_Derive_Resp_handler, ec);

    tmp_format = CMregister_format(cm, proto_derive_msg_formats);
    CMregister_handler(tmp_format, Proto_Derive_handler, ec);

    tmp_format = CMregister_format(cm, data_update_msg_formats);
    CMregister_handler(tmp_format, Data_Update_handler, ec);

    tmp_format = CMregister_format(cm, channel_info_request_msg_formats);
    CMregister_handler(tmp_format, Channel_Info_Request_handler, ec);

    tmp_format = CMregister_format(cm, channel_info_response_msg_formats);
    CMregister_handler(tmp_format, Channel_Info_Response_handler, ec);
#endif
    CMadd_shutdown_task(cm, ECho_shutdown, ec);
    return ec;
}


extern void
ECdump_typed_event_as_XML(EChannel chan, void *event)
{
/*    dump_unencoded_IOrecord_as_XML(chan->subcontext,
      chan->type_format, event);*/
}

extern void
ECdump_event_as_XML(EChannel chan, void *event)
{
/*    dump_raw_IOrecord_as_XML(chan->subcontext,
      chan->type_format, event);*/
}

extern channel_ID_struct
copy_channel_id(channel_ID_struct chan_id)
{
    channel_ID_struct ret;
    if (chan_id.len == 0) {
	ret.len = 0;
	ret.channel = NULL;
    } else {
	ret.len = chan_id.len;
	ret.channel = malloc(ret.len);
	memcpy(ret.channel, chan_id.channel, ret.len);
    }
    return ret;
}

static int host_IP = 0;
static int pid = -1;

extern void
set_channel_id(channel_ID_struct * chan_id, EChannel chan)
{
    /* 
     * change this if we ever allow more than one control context per process
     */
    chan_id->len =
	4 /* IP addr */  + 4 /* pid */  + 0 /* echo context */  +
	sizeof(chan) /* channel id */ ;
    if (pid == -1) {
	struct hostent *host_addr;
	char my_host_name[256];
	gethostname(my_host_name, sizeof(my_host_name));
	host_addr = gethostbyname(my_host_name);
	if (host_addr != NULL) {
	    if (sizeof(host_IP) != host_addr->h_length) {
		printf("Host addr size mismatch in ECho channel.c\n");
	    }
	    memcpy(&host_IP, host_addr->h_addr, host_addr->h_length);
	}
	pid = (int) getpid();
    }

    chan_id->channel = malloc(chan_id->len);
    *((int *) (chan_id->channel + 0)) = host_IP;
    *((int *) (chan_id->channel + 4)) = pid;
    *((void **) (chan_id->channel + 8)) = (void *) chan;
}

extern EChannel
get_local_channel_from_id(channel_ID_struct chan_id, EControlContext cc)
{
    int id_host_IP;
    int id_pid;
    if (host_IP == 0) {
	host_IP = CMget_self_ip_addr();
	pid = (int) getpid();
    }

    memcpy(&id_host_IP, chan_id.channel + 0, 4);
    memcpy(&id_pid, chan_id.channel + 4, 4);
    if ((id_host_IP == host_IP) && (id_pid == pid)) {
	/* channel ID is local */
	int i;
	EChannel chan;
	memcpy(&chan, chan_id.channel + 8, sizeof(chan));
	for (i = 0; i < cc->channel_count; i++) {
	    if (chan == cc->channel_list[i]) {
		return chan;
	    }
	}
	printf("Channel %lx not found in channel list \n", (long) chan);
	return NULL;
    } else {
	/* channel ID is remote, search for local instance */
	int i;
	for (i = 0; i < cc->channel_count; i++) {
	    channel_ID_struct this_id =
		cc->channel_list[i]->global_id.global_channel_id;
	    if ((chan_id.len == this_id.len) &&
		(memcmp(chan_id.channel, this_id.channel, chan_id.len) ==
		 0)) {
		return cc->channel_list[i];
	    }
	}
	printf("Global channel not found in channel list \n");
    }
    return NULL;
}

extern void
print_channel_id(channel_ID_struct chan_id)
{
    struct in_addr id_host_IP;
    int id_pid;
    int i;

    if (chan_id.len == 0) {
	printf("NULL");
	return;
    }
    if (chan_id.len < 12) {
	printf("Invalid Length");
	return;
    }
    memcpy(&id_host_IP, chan_id.channel + 0, 4);
    memcpy(&id_pid, chan_id.channel + 4, 4);
    printf("host_IP = %s, pid = %d, chan = ", inet_ntoa(id_host_IP),
	   id_pid);
    for (i = 8; i < chan_id.len; i++) {
	printf("%02x", (unsigned char) chan_id.channel[i]);
    }
}
