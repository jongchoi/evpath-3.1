#if defined (__INTEL_COMPILER)
#pragma warning(disable : 310 )
#pragma warning(disable : 2259 )
#pragma warning(disable : 1418 )
#pragma warning(disable : 1419 )
#pragma warning(disable : 188 )
#pragma warning(disable : 193 )
#pragma warning(disable : 589 )
#pragma warning(disable : 61 )
#pragma warning(disable : 556 )
#pragma warning(disable : 187 )
#pragma warning(disable : 175 )
#pragma warning(disable : 869 )
#pragma warning(disable : 180 )
#pragma warning(disable : 1599 )
#endif
typedef int (*FilterFunc) ARGS((void *, void *, void*, void*));

typedef struct {
    int  len;
    char *channel;
} channel_ID_struct;

typedef struct _ECID {
    char *contact_string;
    channel_ID_struct global_channel_id;
} ECID;

typedef struct _rem_attendr {
    CMConnection conn;
    char *contact_string;
    channel_ID_struct remote_id;
} *rem_attendr_list;

typedef struct _rem_member_entry {
    CMConnection conn;
    char *contact_string;
    EVstone remote_target_stone_id;
    CMConnection event_conn;
    char *event_contact_string;
    channel_ID_struct remote_id;
    int has_sinks;
    int has_sources;
} *rem_member_list_p;

typedef struct _multi_sink_rec *multi_sink_info;

typedef struct _local_sink {
    void *client_data;
    ECHandlerFunction func;
    EControlContext ec;
    multi_sink_info multi_sink_handle;
    ECSinkHandle ev_sink_rec;
    FMFieldList field_list;
    int cond;
    EVstone stone_id;
    EVaction term_action;
} *local_sink_ptr;

typedef struct _derived_source {
    FilterFunc filter;
    int has_output_param;
    int target_type_len;
    EChannel derived_chan;
    ECSourceHandle source_handle;
} *derived_source;

typedef struct _local_source {
    void *client_data;
    ECRequestHandlerFunction func;
    ECSourceHandle source_handle;
    EVsource ev_source_handle;
} *local_source_ptr;

typedef struct _derived_chan {
    char *filter;
    int has_output_param;
    EChannel derived_chan;
    EVstone action_stone;
    void *filter_handle;
} *derived_chan;

typedef struct _output_conn_attrs {
    int output_conn_count;
    attr_list *attrs;
} *output_conn_attrs;

typedef struct _EChannel {
    int sink_count;
    int source_count;
    char *local_contact_string;
    char *event_contact_string;
    char *create_attrs_string;
    ECID global_id;

    int rem_member_list_len;
    rem_member_list_p rem_member_list;
    int local_sink_list_len;
    local_sink_ptr local_sink_list;
    int local_source_list_len;
    local_source_ptr local_source_list;

    int der_list_len;
    derived_chan der_chan_list;

    int parent_list_len;
    EChannel *parent_chan_list;

    /* type info */
    void *filter_data;
    void *encoded_filter_data;
    int encoded_filter_data_len;
    FMFieldList data_field_list;

    thr_mutex_t lock;
    ECSubscribeFunction subscribe_func;
    void *subscribe_client_data;
    
    void *periodic_info;
    CManager cm;
    CMFormat eventv_format;
    EControlContext ec;
    int ready;

    /* EV structures */
    EVstone incoming_target_stone_id;    /* supplied to sources elsewhere */
    EVaction incoming_split_action;	/* split to local sinks */
    EVstone outgoing_target_stone_id;  /* used by local sources */
    EVaction outgoing_split_action;  /* split to output actions, derived */
    FMStructDescList ev_format_list;
} EChannel_struct;

typedef struct free_block_rec {
    int ref_count;
    EControlContext ec;
    void *free_arg;
    void *block;
    EventFreeFunction free_func;
    EControlContext locking_context;
    attr_list attrs;
    struct free_block_rec *next;
} *free_block_rec_p;

typedef struct _event_item {
    struct _event_item *next;
    struct _event_item *prev;
    EChannel chan;
    int sink_index;
    void *event;
    int event_len;
    ECEventQueueList event_queue_list;
    FFSEncodeVector eventv;
    free_block_rec_p block_rec;
    attr_list attrs;
} event_item, *event_queue;

struct _EControlContext {
    event_queue event_queue_head;
    event_queue event_queue_tail;
    event_queue free_event_item_list;
    event_queue current_event_item;
    event_queue taken_events_list;

    free_block_rec_p free_block_list;
    int channel_count;
    EChannel *channel_list;
    CManager cm;
    thr_mutex_t lock;
    thr_condition_t condition;
};
    
typedef struct _ECSinkHandle {
    EChannel chan;
    int sink_num;
} ECSinkStruct;

typedef struct _ECSourceHandle {
    EChannel chan;
    int source_num;
    int der_list_len;
    derived_source der_source_list;
} ECSourceStruct;

struct _ECEventRec {
    void *event;
    free_block_rec_p free_block;
};

struct _ECEventQueue {
    int event_count;
    int queue_size;
    struct _ECEventRec *queue;
    ECSinkHandle sink_handle;
};

struct _ECMultiEventQueue {
    int queue_count;
    struct _ECEventQueue *event_queues;
};

struct _multi_sink_rec {
    ECMultiHandlerFunction func;
    void *client_data;
    ECEventQueueList queue_list;
};

typedef struct _AttendMsg {
    char *chan_str;
    char *contact_str;
    int cond;
    channel_ID_struct client_channel_id;
    EVstone incoming_target_stone_id;
    int proto_version;
} AttendMsg, *AttendMsgPtr;

typedef struct _SinkSubscribeMsg {
    channel_ID_struct global_channel_id;
    channel_ID_struct client_channel_id;
    char *client_contact_str;
} SinkSubscribeMsg, *SinkSubscribeMsgPtr;

/* basically the same but still ... */
typedef struct _SourceSubscribeMsg {
    channel_ID_struct global_channel_id;
    channel_ID_struct client_channel_id;
    char *client_contact_str;
} SourceSubscribeMsg, *SourceSubscribeMsgPtr;

typedef struct _ExistsAttendResponseMsg {
    int condition;
    channel_ID_struct existing_channel_id;
} ExistsAttendResponseMsg, *ExistsAttendResponseMsgPtr;

typedef struct _AttendRec {
    char *contact_string;
    channel_ID_struct client_channel_id;
    char *filter;
    int filter_has_output_param;
} AttendRec, *AttendRecPtr;

typedef struct _OldAttendResponseMsg {
    int condition;
    channel_ID_struct global_channel_id;
    char *type_name;
    int sink_count;
    AttendRec *sinks;
    int source_count;
    AttendRec *sources;
    int member_count;
    AttendRec *members;
    int der_count;
    AttendRec *derivatives;
} OldAttendResponseMsg, *OldAttendResponseMsgPtr;

typedef struct _MemberRec {
    char *contact_string;
    char *event_contact_string;
    int client_target_stone_id;
    channel_ID_struct client_channel_id;
    char has_sources;
    char has_sinks;
} MemberRec, *MemberRecPtr;

typedef struct _AttendResponseMsg {
    int condition;
    channel_ID_struct global_channel_id;
    char *type_name;
    char *create_attrs_string;
    int filter_data_len;
    char *filter_data;
    int member_count;
    MemberRec *members;
    int der_count;
    AttendRec *derivatives;
} AttendResponseMsg, *AttendResponseMsgPtr;

typedef struct _MemberSubscribeMsg {
    channel_ID_struct global_channel_id;
    channel_ID_struct client_channel_id;
    char *client_contact_str;
    int client_target_stone_id;
    char *event_contact_str;
} MemberSubscribeMsg, *MemberSubscribeMsgPtr;

typedef struct _MemberUnsubscribeMsg {
    channel_ID_struct global_channel_id;
    channel_ID_struct client_channel_id;
} MemberUnsubscribeMsg, *MemberUnsubscribeMsgPtr;

typedef struct _RequestEventMsg {
    channel_ID_struct global_channel_id;
} RequestEventMsg, *RequestEventMsgPtr;

typedef struct _EventMsg {
    channel_ID_struct global_channel_id;
    int event_len;
    char *event;
} EventMsg, *EventMsgPtr;

typedef struct _EventVMsg {
    channel_ID_struct global_channel_id;
    int eventv_len;
    FFSEncodeVector eventv;
} EventVMsg, *EventVMsgPtr;

typedef struct _FormatListElement {
    char *format_name;
    int field_list_len;
    FMFieldList field_list;
} format_list_element;

extern void ECtrace_out ARGS((char *format, int direction, ...));
extern void dump_EChannel ARGS((EChannel chan));
extern void DEChannel_close_handler ARGS((CManager cm, CMConnection conn));
extern EChannel string_to_channel_ptr ARGS((char *chan_id));
extern char* channel_ptr_to_string ARGS((EChannel chan));
extern int channel_verbose;
extern int do_internal_source_derive ARGS((ECSourceHandle src,
					   EChannel der_chan, 
					   char *filter, 
					   FMFieldList input_field_list,
					   CMFormatList input_format_list,
					   FMFieldList output_field_list,
					   CMFormatList output_format_list,
					   FMFieldList data_field_list,
					   CMFormatList data_format_list));
extern void
remove_derived_channel ARGS((EChannel parent_chan, EChannel der_chan));

extern void ECperiodic_info_free ARGS((CManager cm, void *vperiodic));
extern void EChannel_register_subformats ARGS((CManager cm, 
					       FMContext context,
					       FMFieldList field_list,
					       CMFormatList format_list));
extern void EChannel_lock ARGS((EChannel chan));
extern void EChannel_unlock ARGS((EChannel chan));
extern void EChannel_close_handler ARGS((CManager cm, CMConnection conn, void *ec));
extern void 
Channel_Derive_handler ARGS((CManager cm, CMConnection conn,
			     void *data, void *client_data,
			     attr_list attrs));
extern void 
Channel_Remote_Derive_handler ARGS((CManager cm, CMConnection conn,
				    void *data, void *client_data,
				    attr_list attrs));
extern void 
Data_Update_handler ARGS((CManager cm, CMConnection conn,
			  void *data, void *client_data, attr_list attrs));
extern void 
Proto_Derive_handler ARGS((CManager cm, CMConnection conn,
			     void *data, void *client_data,
			   attr_list attrs));
extern void 
Channel_Source_Derive_Resp_handler ARGS((CManager cm, CMConnection conn,
					 void *data, void *client_data,
					 attr_list attrs));
extern void 
Channel_Source_Derive_handler ARGS((CManager cm, CMConnection conn,
				    void *data, void *client_data,
				    attr_list attrs));
extern void 
Channel_Info_Request_handler ARGS((CManager cm, CMConnection conn,
				   void *data, void *client_data,
				   attr_list attrs));
extern void 
Channel_Info_Response_handler ARGS((CManager cm, CMConnection conn,
				    void *data, void *client_data,
				    attr_list attrs));

extern void
Channel_Attend_handler ARGS((CManager cm, CMConnection conn, void *data,
			     void *client_data, attr_list attrs));


extern void
set_channel_id ARGS((channel_ID_struct * id_struct, EChannel chan));

extern channel_ID_struct
copy_channel_id ARGS((channel_ID_struct id_struct));

extern EChannel
get_local_channel_from_id ARGS((channel_ID_struct id_struct, 
				EControlContext cc));

extern void
print_channel_id ARGS((channel_ID_struct id_struct));

#define EChannel_lock(chan) IntEChannel_lock(chan, __FILE__, __LINE__)
#define EChannel_unlock(chan) IntEChannel_unlock(chan, __FILE__, __LINE__)
#define EContext_lock(context) IntEContext_lock(context, __FILE__, __LINE__)
#define EContext_unlock(context) IntEContext_unlock(context, __FILE__, __LINE__)
extern void IntEChannel_lock ARGS((EChannel chan, char *file, int line));
extern void IntEChannel_unlock ARGS((EChannel chan, char *file, int line));
extern void IntEContext_lock ARGS((EControlContext cc, char *file, int line));
extern void IntEContext_unlock ARGS((EControlContext cc, char *file, int line));
extern void CMsetup_conn_or_local ARGS((EControlContext ec, char *chan_id, 
					CMConnection *conn_ptr,
					void **local_ptr));
extern void ECsetup_filter_data ARGS((EChannel chan, void *init_data,
				      int init_data_len));
extern FMStructDescList create_format_list(FMFieldList field_list, 
				       CMFormatList format_list, int copy);
extern void send_attend_response(CManager cm, CMConnection conn,
				 EChannel chan, int cond);
