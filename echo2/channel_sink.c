#if defined (__INTEL_COMPILER)
#pragma warning(disable : 310 )
#pragma warning(disable : 180 )
#pragma warning(disable : 981 )
#pragma warning(disable : 1418 )
#pragma warning(disable : 869 )
#pragma warning(disable : 167 )
#endif
#ifndef DOC
#include "config.h"
#include "ffs.h"
#include "atl.h"
#include "evpath.h"
#include "gen_thread.h"
#include "echo2.h"

#include <stdio.h>
#include <stdlib.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef HAVE_WINDOWS_H
#include <windows.h>
#endif
#endif
FMField simple_field_list[] = {
    {"entry", "integer", sizeof(long), 0},
    {NULL, NULL, 0, 0}};

void handler(void *event, int length, void *client_data, attr_list attrs)
{
    printf("event data is %ld\n", *(long *) event);
}

int main(int argc, char **argv)
{
    CManager cm;
    EChannel chan;
    EControlContext cc;

    cm = CManager_create();
    cc = ECho_CM_init(cm);

    chan = EChannel_open(cc, argv[1]);
    (void) ECsink_typed_subscribe(chan, simple_field_list, NULL,
				  handler, NULL);
    CMsleep(cm, 120);
    return 0;
}
