#if defined (__INTEL_COMPILER)
#pragma warning(disable : 310 )
#pragma warning(disable : 180 )
#pragma warning(disable : 981 )
#pragma warning(disable : 869 )
#pragma warning(disable : 1418 )
#pragma warning(disable : 1419 )
#endif
#include "ffs.h"
#include "atl.h"
#include "evpath.h"
#include "echo2.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h>

typedef struct {
    int  value;
    char *str;
} record, *record_ptr;

void handler(event, length, client_data)
void *event;
int length;
void *client_data;
{
    record_ptr data = (record_ptr) event;
    printf("event data is %d\n", data->value);
}

extern long read_time;

int
main(argc, argv)
int argc;
char **argv;
{
    CManager cm;
    EControlContext cc;

    cm = CManager_create();
    (void) CMfork_comm_thread(cm);
    cc = ECho_CM_init(cm);

    (void) EChannel_open(cc, argv[1]);
    CMsleep(cm, 120);
    return 0;
}
