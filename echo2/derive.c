#include "config.h"
#include "ffs.h"
#ifdef HAVE_GEN_THREAD_H
#include "gen_thread.h"
#else
#define gen_thr_initialized() 0
#define thr_mutex_lock(m)
#define thr_mutex_unlock(m)
#define thr_mutex_alloc() 0
#define thr_mutex_free(m)
#define thr_condition_wait(c, m)
#define thr_condition_signal(c)
#define thr_condition_alloc() 0
#define thr_condition_free(c)
#define thr_mutex_t void*
#define thr_condition_t void*
#endif
#include "atl.h"
#include "evpath.h"
#ifdef HAVE_MALLOC_H
#include <malloc.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#ifndef PRINTF_DEFINED
extern int printf ARGS((const char *, ...));
#endif
#include "echo2.h"
#include "echo_internal.h"

#include "ltdl.h"

#define FILTER_DLL 0xff

FMField field_list_flds[] = {
    {"field_name", "string", sizeof(char *),
     FMOffset(FMFieldList, field_name)},
    {"field_type", "string", sizeof(char *),
     FMOffset(FMFieldList, field_type)},
    {"field_size", "integer", sizeof(int),
     FMOffset(FMFieldList, field_size)},
    {"field_offset", "integer", sizeof(int),
     FMOffset(FMFieldList, field_offset)},
    {(char *) 0, (char *) 0, 0, 0}
};

FMField format_list_flds[] = {
    {"format_name", "string", sizeof(char *),
     FMOffset(format_list_element *, format_name)},
    {"field_list_len", "integer", sizeof(int),
     FMOffset(format_list_element *, field_list_len)},
    {"field_list", "IOfield_list[field_list_len]", sizeof(FMField),
     FMOffset(format_list_element *, field_list)}
    ,
    {(char *) 0, (char *) 0, 0, 0}
};

/* message to send to arbitrator to derive a new the channel */
typedef struct _DeriveMsg
{
    char *chan_str;
    int cond;
    channel_ID_struct client_channel_id;
    char *client_contact_str;
    int client_stone_id;
    char *filter;
    int field_list_len;
    FMFieldList field_list;
    int format_list_len;
    format_list_element *format_list;

    char *init_data_block;
    int init_data_len;
}
DeriveMsg, *DeriveMsgPtr;

FMField channel_derive_msg_flds[] = {
    {"channel", "string", sizeof(char *),
     FMOffset(DeriveMsgPtr, chan_str)},
    {"condition", "integer", sizeof(int), FMOffset(DeriveMsgPtr, cond)},
    {"client_channel_id", "channel_ID", sizeof(channel_ID_struct),
     FMOffset(DeriveMsgPtr, client_channel_id)},
    {"client_stone_id", "integer", sizeof(int),
     FMOffset(DeriveMsgPtr, client_stone_id)},
    {"client_contact_str", "string", sizeof(char *),
     FMOffset(DeriveMsgPtr, client_contact_str)},
    {"filter", "string", sizeof(char *), FMOffset(DeriveMsgPtr, filter)},
    {"field_list_len", "integer", sizeof(int),
     FMOffset(DeriveMsgPtr, field_list_len)},
    {"field_list", "IOfield_list[field_list_len]", sizeof(FMField),
     FMOffset(DeriveMsgPtr, field_list)}
    ,
    {"format_list_len", "integer", sizeof(int),
     FMOffset(DeriveMsgPtr, format_list_len)},
    {"format_list", "DEFormatList[format_list_len]",
     sizeof(format_list_element), FMOffset(DeriveMsgPtr, format_list)}
    ,
    {"init_data_block", "char[init_data_len]",
     1, FMOffset(DeriveMsgPtr, init_data_block)}
    ,
    {"init_data_len", "integer",
     sizeof(int), FMOffset(DeriveMsgPtr, init_data_len)},
    {(char *) 0, (char *) 0, 0, 0}
};

extern FMField channel_id_flds[];

FMStructDescRec channel_derive_msg_formats[] = {
    {"Channel Derive", channel_derive_msg_flds, sizeof(DeriveMsg), NULL},
    {"IOfield_list", field_list_flds, sizeof(FMField), NULL},
    {"DEFormatList", format_list_flds, sizeof(format_list_element), NULL},
    {"channel_ID", channel_id_flds, sizeof(channel_ID_struct), NULL},
    {NULL, NULL, 0, NULL}
};

FMField channel_remote_derive_msg_flds[] =	/* current same as derive
						 * msg */
{
    {"channel", "string", sizeof(char *),
     FMOffset(DeriveMsgPtr, chan_str)},
    {"condition", "integer", sizeof(int), FMOffset(DeriveMsgPtr, cond)},
    {"client_channel_id", "channel_ID", sizeof(channel_ID_struct),
     FMOffset(DeriveMsgPtr, client_channel_id)},
    {"client_contact_str", "string", sizeof(char *),
     FMOffset(DeriveMsgPtr, client_contact_str)},
    {"client_stone_id", "integer", sizeof(int),
     FMOffset(DeriveMsgPtr, client_stone_id)},
    {"filter", "string", sizeof(char *), FMOffset(DeriveMsgPtr, filter)},
    {"field_list_len", "integer", sizeof(int),
     FMOffset(DeriveMsgPtr, field_list_len)},
    {"field_list", "IOfield_list[field_list_len]", sizeof(FMField),
     FMOffset(DeriveMsgPtr, field_list)},
    {"format_list_len", "integer", sizeof(int),
     FMOffset(DeriveMsgPtr, format_list_len)},
    {"format_list", "DEFormatList[format_list_len]",
     sizeof(format_list_element), FMOffset(DeriveMsgPtr, format_list)},
    {"init_data_block", "char[init_data_len]",
     1, FMOffset(DeriveMsgPtr, init_data_block)},
    {"init_data_len", "integer",
     sizeof(int), FMOffset(DeriveMsgPtr, init_data_len)},
    {(char *) 0, (char *) 0, 0, 0}
};

FMStructDescRec channel_remote_derive_msg_formats[] = {
    {"Channel Remote Derive", channel_remote_derive_msg_flds, sizeof(DeriveMsg), NULL},
    {"IOfield_list", field_list_flds, sizeof(FMField), NULL},
    {"DEFormatList", format_list_flds, sizeof(format_list_element), NULL},
    {"channel_ID", channel_id_flds, sizeof(channel_ID_struct), NULL},
    {NULL, NULL, 0, NULL}
};

/* message to send to members to tell them about a derived channel */
typedef struct _SrcDeriveMsg
{
    char *der_chan_id;
    int cond;
    char *filter;
    int typed_output;
    channel_ID_struct parent_channel_id;
    char *init_data_block;
    int init_data_len;
}
SrcDeriveMsg, *SrcDeriveMsgPtr;

FMField src_derive_msg_flds[] = {
    {"der_channel", "string", sizeof(char *),
     FMOffset(SrcDeriveMsgPtr, der_chan_id)},
    {"condition", "integer", sizeof(int), FMOffset(SrcDeriveMsgPtr, cond)},
    {"filter", "string", sizeof(char *),
     FMOffset(SrcDeriveMsgPtr, filter)},
    {"typed_output", "integer", sizeof(int),
     FMOffset(SrcDeriveMsgPtr, typed_output)},
    {"parent_channel_id", "channel_ID", sizeof(channel_ID_struct),
     FMOffset(SrcDeriveMsgPtr, parent_channel_id)},
    {"init_data_block", "char[init_data_len]",
     1, FMOffset(SrcDeriveMsgPtr, init_data_block)},
    {"init_data_len", "integer",
     sizeof(int), FMOffset(SrcDeriveMsgPtr, init_data_len)},
    {(char *) 0, (char *) 0, 0, 0}
};

FMStructDescRec src_derive_msg_formats[] = {
    {"Channel Source Derive", src_derive_msg_flds, sizeof(SrcDeriveMsg), NULL},
    {"channel_ID", channel_id_flds, sizeof(channel_ID_struct), NULL},
    {NULL, NULL, 0, NULL}
};

/* message to send to members to tell them about a derived channel */
typedef struct _SrcDeriveRespMsg
{
    int cond;
}
SrcDeriveRespMsg, *SrcDeriveRespMsgPtr;

FMField src_derive_resp_msg_flds[] = {
    {"condition", "integer", sizeof(int),
     FMOffset(SrcDeriveRespMsgPtr, cond)},
    {(char *) 0, (char *) 0, 0, 0}
};

FMStructDescRec src_derive_resp_msg_formats[] = {
    {"Channel Source Derive Resp", src_derive_resp_msg_flds, sizeof(SrcDeriveRespMsg), NULL},
    {"channel_ID", channel_id_flds, sizeof(channel_ID_struct), NULL},
    {NULL, NULL, 0, NULL}
};

typedef struct _DataUpdateMsg
{
    channel_ID_struct global_channel_id;
    int data_len;
    char *data;
}
DataUpdateMsg, *DataUpdateMsgPtr;

FMField data_update_msg_flds[] = {
    {"global_channel_id", "channel_ID", sizeof(channel_ID_struct),
     FMOffset(DataUpdateMsgPtr, global_channel_id)}
    ,
    {"data_len", "integer", sizeof(int),
     FMOffset(DataUpdateMsgPtr, data_len)},
    {"data", "char[data_len]", sizeof(char),
     FMOffset(DataUpdateMsgPtr, data)},
    {(char *) 0, (char *) 0, 0, 0}
};

FMStructDescRec data_update_msg_formats[] = {
    {"Data Update Message", data_update_msg_flds, sizeof(DataUpdateMsg), NULL},
    {"IOfield_list", field_list_flds, sizeof(FMField), NULL},
    {"DEFormatList", format_list_flds, sizeof(format_list_element), NULL},
    {"channel_ID", channel_id_flds, sizeof(channel_ID_struct), NULL},
    {NULL, NULL, 0, NULL}
};

extern FMField channel_attend_response_msg_flds[];
extern FMField old_channel_attend_response_msg_flds[];

#ifdef NOT_DEF
static CMFormatList get_format_list ARGS((IOFormat ioformat));
static void free_format_list ARGS((CMFormatList list));
static FMFieldList simplify_field_list ARGS((FMFieldList list));
static ECDataHandle new_data_handle();

struct _ECDataHandle
{
    FFSContext iocontext;
    FMFormat type_format;
    EChannel channel;
    FMFieldList data_field_list;
    CMFormatList data_subformat_list;
    CManager cm;
};

static void
    encode_data_block ARGS((ECdata_spec filter_data, char **data_block,
			    int *data_len, ECDataHandle handle,
			    EControlContext cc));

static EChannel
    do_channel_derive
ARGS((EControlContext cc, EChannel orig_channel, char *filter,
      FMFieldList output_field_list, CMFormatList output_format_list,
      ECdata_spec filter_data,
      char *init_data, int init_data_len));
static void do_chan_derive
ARGS((EChannel parent_chan, EChannel der_chan, char *filter,
      FMFieldList input_field_list, CMFormatList input_format_list,
      FMFieldList output_field_list, CMFormatList output_format_list,
      FMFieldList data_field_list, CMFormatList data_format_list,
      void *filter_data, int has_output_param));

extern EChannel
EChannel_derive(cc, chan_id, event_filter)
EControlContext cc;
char *chan_id;
char *event_filter;
{
    return EChannel_typed_derive_data(cc, chan_id, event_filter, NULL,
				      NULL, NULL);
}

extern EChannel
EChannel_typed_derive(cc, chan_id, event_filter, field_list, format_list)
EControlContext cc;
char *chan_id;
char *event_filter;
FMFieldList field_list;
CMFormatList format_list;
{
    return EChannel_typed_derive_data(cc, chan_id, event_filter,
				      field_list, format_list, NULL);
}

#ifndef NO_DERIVED
static int
    verify_local_filter_function ARGS((EChannel parent_chan, char *filter,
				       FMFieldList output_field_list,
				       CMFormatList output_format_list,
				       ECdata_spec filter_data));
static int
    verify_remote_filter_function ARGS((CManager cm, CMConnection conn,
					char *parent_chan_id, char *filter,
					FMFieldList output_field_list,
					CMFormatList output_format_list,
					ECdata_spec filter_data));
#endif


int check_filter_string ARGS((char *filter));



extern EChannel
EChannel_typed_derive_data(cc, chan_id, event_filter, field_list,
			   format_list, filter_data)
EControlContext cc;
char *chan_id;
char *event_filter;
FMFieldList field_list;
CMFormatList format_list;
ECdata_spec filter_data;
{
#ifndef NO_DERIVED
    CMConnection conn;
    EChannel chan, parent_chan;
    DeriveMsg derive_msg;
    int cond, i;

    ECtrace_out("ECchannel_derive", 1);

    if (field_list == NULL) {
	ECtrace_out
	    ("This is an untyped derivation without an output parameter",
	     3);
    } else {
	ECtrace_out("This is an typed derivation with an output parameter",
		    3);
    }
    CMsetup_conn_or_local(cc, chan_id, &conn, (void *) &parent_chan);
    if (conn == NULL) {
	/* this channel is really local */
	char *filter_data_block = NULL;
	int filter_data_len = 0;

	if (parent_chan == NULL)
	    return NULL;

	if (parent_chan->type_format == NULL) {
	    printf("Attempt to do channel derivation on non-typed event channel\n");
	    return NULL;
	}
	if ((check_filter_string(event_filter) != FILTER_DLL)
	    && !verify_local_filter_function(parent_chan, event_filter,
					     field_list, format_list,
					     filter_data)) {
	    ECtrace_out("EChannel_derive, bad filter", 0);
	    return NULL;
	}
	if (filter_data != NULL) {
	    encode_data_block(filter_data, &filter_data_block,
			      &filter_data_len, NULL, cc);
	}
	chan = do_channel_derive(cc, parent_chan, event_filter, field_list,
				 format_list, filter_data, filter_data_block,
				 filter_data_len);
	if (filter_data_block) {
	    CMfree(filter_data_block);
	}
	ECtrace_out("ECchannel_derive returning local %lx", 0,
		    (long) chan);
	return chan;
    }
    if ((check_filter_string(event_filter) != FILTER_DLL)
	&& !verify_remote_filter_function(cc->cm, conn, chan_id,
					  event_filter, field_list,
					  format_list, filter_data)) {
	ECtrace_out("EChannel_derive, bad filter", 0);
	return NULL;
    }
    if (field_list == NULL) {
	chan = EChannel_create(cc);
    } else {
	chan = EChannel_typed_create(cc, field_list, format_list);
    }

    ECtrace_out("getting condition and setting local chan condition", 3);
    cond = CMCondition_get(cc->cm, conn);
    CMCondition_set_client_data(cc->cm, cond, &chan);

    /* give the filter to the arbitrator for him to propogate */
    /* returns through regular attend response like an open */
    ECtrace_out("creating derive_msg and writing to connection", 3);
    memset(&derive_msg, 0, sizeof(derive_msg));
    derive_msg.chan_str = strchr(chan_id, '@') + 1;
    derive_msg.cond = cond;
    set_channel_id(&derive_msg.client_channel_id, chan);
    derive_msg.client_contact_str = chan->local_contact_string;
    derive_msg.client_stone_id = chan->incoming_target_stone_id;
    derive_msg.filter = event_filter;

    /* send output field and format lists */
    derive_msg.field_list = field_list;
    derive_msg.field_list_len = 0;
    while (field_list &&
	   (field_list[derive_msg.field_list_len].field_name != NULL)) {
	derive_msg.field_list_len++;
    }
    if (field_list)
	derive_msg.field_list_len++;	/* send terminator as well */
    i = 0;
    while (format_list && format_list[i].format_name != NULL)
	i++;
    if (format_list) {
	derive_msg.format_list = CMmalloc(i * sizeof(format_list_element));
	derive_msg.format_list_len = i;	/* +1 because we send terminator */
    } else {
	derive_msg.format_list = NULL;
	derive_msg.format_list_len = 0;
    }
    for (i = 0; i < derive_msg.format_list_len; i++) {
	int j = 0;
	derive_msg.format_list[i].format_name = format_list[i].format_name;
	derive_msg.format_list[i].field_list = format_list[i].field_list;
	while (format_list[i].field_list[j].field_name != NULL) {
	    j++;
	}
	j++;			/* send terminator as well */
	derive_msg.format_list[i].field_list_len = j;
    }
    ECtrace_out("Encoding derive message\n", 3);

    encode_data_block(filter_data, &derive_msg.init_data_block,
		      &derive_msg.init_data_len, NULL, chan->ec);
    if (!CMwrite(conn, CMlookup_format(cc->cm, channel_derive_msg_flds),
		 &derive_msg)) {
	ECtrace_out("channel derive msg write failed", 0);
    }

    CMfree(derive_msg.format_list);
    if (derive_msg.init_data_block != NULL) {
	CMfree(derive_msg.init_data_block);
    }
    ECtrace_out("Sending derive msg, channel %s cond %d filter %d\n", 3,
		derive_msg.chan_str, derive_msg.cond, derive_msg.filter);
    ECtrace_out("Waiting for Channel (Exists) Attend Response", 3);
    if (CMCondition_wait(cc->cm, cond) == 0) {
	/* wait failed, dep is dead */
	EChannel_destroy(chan);
	ECtrace_out("ECchannel_Derive", 0);
	return NULL;
    }
    chan->ready++;
    ECtrace_out("ECchannel_Derive", 0);
    return chan;
#else
    printf("No derived event channels in this version of ECho\n");
    return NULL;
#endif
}

typedef struct _InfoRequestMsg
{
    char *chan_str;
    int cond;
}
InfoRequestMsg, *InfoRequestMsgPtr;

FMField channel_info_request_msg_flds[] = {
    {"channel", "string", sizeof(char *),
     FMOffset(InfoRequestMsgPtr, chan_str)},
    {"condition", "integer", sizeof(int),
     FMOffset(InfoRequestMsgPtr, cond)},
    {(char *) 0, (char *) 0, 0, 0}
};

FMStructDescRec channel_info_request_msg_formats[] = {
    {"Channel Info Request Message", channel_info_request_msg_flds,sizeof(InfoRequestMsg), NULL},
    {NULL, NULL, 0, NULL}};

#ifndef NO_DERIVED
static int
verify_filter_code(char *filter, FMFieldList input_field_list,
		   CMFormatList input_format_list,
		   FMFieldList output_field_list,
		   CMFormatList output_format_list,
		   FMFieldList data_field_list,
		   CMFormatList data_format_list);

static void
add_standard_routines(context)
ecl_parse_context context;
{
    static char extern_string[] = "\
		int printf(string format, ...);\n\
		long lrand48();\n\
		double drand48();\n";

    static ecl_extern_entry externs[] = {
	{"printf", (void *) 0},
	{"lrand48", (void *) 0},
	{"drand48", (void *) 0},
	{(void *) 0, (void *) 0}
    };
    /* 
     * some compilers think it isn't a static initialization to put this
     * in the structure above, so do it explicitly.
     */
    externs[0].extern_value = (void *) (long) printf;
    externs[1].extern_value = (void *) (long) lrand48;
    externs[2].extern_value = (void *) (long) drand48;

    ecl_assoc_externs(context, externs);
    ecl_parse_for_context(extern_string, context);
}

static FMField output_conn_field_list[] = {
    {"output_conn_count", "integer", sizeof(int), 
     FMOffset(output_conn_attrs, output_conn_count)},
    {"attrs", "attr_list[output_conn_count]", sizeof(void*), 
     FMOffset(output_conn_attrs, attrs)},
    { NULL, NULL, 0, 0}};

static ecl_parse_context
build_derivation_context(input_field_list, input_format_list,
			 output_field_list, output_format_list,
			 data_field_list, data_format_list)
FMFieldList input_field_list;
CMFormatList input_format_list;
FMFieldList output_field_list;
CMFormatList output_format_list;
FMFieldList data_field_list;
CMFormatList data_format_list;
{

    sm_ref typ, output_type, input_type, input_param, output_param;
    ecl_parse_context parse_context = new_ecl_parse_context();
    sm_ref conn_info_data_type, conn_info_param;

    add_standard_routines(parse_context);
    while ((input_format_list != NULL) &&
	   (input_format_list->format_name != NULL)) {
	/* step through input formats */
	typ = ecl_build_type_node(input_format_list->format_name,
				  input_format_list->field_list);
	ecl_add_decl_to_parse_context(input_format_list->format_name, typ,
				      parse_context);
	input_format_list++;
    }

    /* handle nested input types???? */
    input_type = ecl_build_type_node("input_type", input_field_list);
    ecl_add_decl_to_parse_context("input_type", input_type, parse_context);

    input_param = ecl_build_param_node("input", input_type, 0);

    ecl_add_decl_to_parse_context("input", input_param, parse_context);

    while ((output_format_list != NULL) &&
	   (output_format_list->format_name != NULL)) {
	/* step through output formats */
	typ = ecl_build_type_node(output_format_list->format_name,
				  output_format_list->field_list);
	ecl_add_decl_to_parse_context(output_format_list->format_name, typ,
				      parse_context);
	output_format_list++;
    }

    if (output_field_list != NULL) {
	output_type =
	    ecl_build_type_node("output_type", output_field_list);
	ecl_add_decl_to_parse_context("output_type", output_type,
				      parse_context);
	output_param = ecl_build_param_node("output", output_type, 1);

	ecl_add_decl_to_parse_context("output", output_param,
				      parse_context);
    } else {
	output_param = ecl_build_param_node("output", input_type, 1);
	ecl_add_decl_to_parse_context("output", output_param,
				      parse_context);
    }

    if (data_field_list != NULL) {
	sm_ref filter_data_type, filter_data_param;
	while ((data_format_list != NULL) &&
	       (data_format_list->format_name != NULL)) {
	    /* step through data formats */
	    typ = ecl_build_type_node(data_format_list->format_name,
				      data_format_list->field_list);
	    ecl_add_decl_to_parse_context(data_format_list->format_name,
					  typ, parse_context);
	    data_format_list++;
	}
	filter_data_type = ecl_build_type_node("filter_data_type",
					       data_field_list);
	ecl_add_decl_to_parse_context("filter_data_type", filter_data_type,
				      parse_context);
	filter_data_param = ecl_build_param_node("filter_data",
						 filter_data_type, 2);

	ecl_add_decl_to_parse_context("filter_data", filter_data_param,
				      parse_context);

    } else {
	ecl_add_param("data", "int", 2, parse_context);
    }
    conn_info_data_type = ecl_build_type_node("output_conn_info_type",
					      output_conn_field_list);
    ecl_add_decl_to_parse_context("output_conn_info_type", 
				  conn_info_data_type, parse_context);
    conn_info_param = ecl_build_param_node("output_conn_info",
					   conn_info_data_type, 3);
    ecl_add_decl_to_parse_context("output_conn_info", conn_info_param,
				  parse_context);
    return parse_context;
}

static int
verify_local_filter_function(parent_chan, filter, output_field_list,
			     output_format_list, filter_data)
EChannel parent_chan;
char *filter;
FMFieldList output_field_list;
CMFormatList output_format_list;
ECdata_spec filter_data;
{
    FMFieldList input_field_list;
    CMFormatList input_format_list;
    FMFieldList data_field_list = NULL;
    CMFormatList data_format_list = NULL;
    int ret;

    input_field_list = EChannel_get_field_list(parent_chan);
    input_format_list = EChannel_get_format_list(parent_chan);

    if (filter_data != NULL) {
	data_field_list = filter_data->data_field_list;
	data_format_list = filter_data->data_subformat_list;
    }
    ret = verify_filter_code(filter, input_field_list, input_format_list,
			     output_field_list, output_format_list,
			     data_field_list, data_format_list);
    if (input_format_list)
	free_format_list(input_format_list);
    if (input_field_list)
	free_field_list(input_field_list);
    return ret;
}

static int
verify_remote_filter_function(cm, conn, parent_chan_id, filter,
			      output_field_list, output_format_list,
			      filter_data)
CManager cm;
CMConnection conn;
char *parent_chan_id;
char *filter;
FMFieldList output_field_list;
CMFormatList output_format_list;
ECdata_spec filter_data;
{
    FMFieldList input_field_list;
    CMFormatList input_format_list;
    FMFieldList data_field_list = NULL;
    CMFormatList data_format_list = NULL;
    InfoRequestMsg request;
    int ret, cond;
    char *channel_type_name = NULL;
    IOFormat chan_format;

    request.chan_str = strchr(parent_chan_id, '@') + 1;
    cond = CMCondition_get(cm, NULL);
    request.cond = cond;

    CMCondition_set_client_data(cm, cond, &channel_type_name);
    if (!CMwrite(conn, CMlookup_format(cm, channel_info_request_msg_flds),
		 &request)) {
	ECtrace_out("verify_remote_filter_function, write failed", 0);
	return 0;
    }
    if (channel_verbose > 0) {
	printf("Sending info request msg, channel %s cond %d\n",
	       request.chan_str, request.cond);
    }

    if (!CMCondition_wait(cm, cond)) {
	/* wait failed, conn is dead */
	ECtrace_out("verify_remote_filter_function, wait failed", 0);
	return 0;
    }

    if (channel_type_name == NULL) {
	ECtrace_out("verify_remote_filter_function, type name is null", 0);
	return 0;
    }
    chan_format = CMget_IOformat_by_name(cm, CMget_user_type_context(cm),
					 channel_type_name);
    if (chan_format == NULL) {
	ECtrace_out("verify_remote_filter_function, type format is null",
		    0);
	return 0;
    }
    input_field_list =
	copy_field_list(field_list_of_IOformat(chan_format));
    input_field_list = simplify_field_list(input_field_list);
    input_format_list = get_format_list(chan_format);

    if (filter_data != NULL) {
	data_field_list = filter_data->data_field_list;
	data_format_list = filter_data->data_subformat_list;
    }
    ret = verify_filter_code(filter, input_field_list, input_format_list,
			     output_field_list, output_format_list,
			     data_field_list, data_format_list);
    free_field_list(input_field_list);
    free_format_list(input_format_list);
    return ret;
}

static int
verify_filter_code(filter, input_field_list, input_format_list,
		   output_field_list, output_format_list,
		   data_field_list, data_format_list)
char *filter;
FMFieldList input_field_list;
CMFormatList input_format_list;
FMFieldList output_field_list;
CMFormatList output_format_list;
FMFieldList data_field_list;
CMFormatList data_format_list;
{
    ecl_parse_context parse_context =
	build_derivation_context(input_field_list, input_format_list,
				 output_field_list, output_format_list,
				 data_field_list, data_format_list);
    int ret;

    ret = ecl_code_verify(filter, parse_context);
    ecl_free_parse_context(parse_context);
    return ret;
}

static cod_code
generate_filter_code(filter, input_field_list, input_format_list,
		     output_field_list, output_format_list,
		     data_field_list, data_format_list)
char *filter;
FMFieldList input_field_list;
CMFormatList input_format_list;
FMFieldList output_field_list;
CMFormatList output_format_list;
FMFieldList data_field_list;
CMFormatList data_format_list;
{
    ecl_parse_context parse_context =
	build_derivation_context(input_field_list, input_format_list,
				 output_field_list, output_format_list,
				 data_field_list, data_format_list);
    ecl_code code;
    code = ecl_code_gen(filter, parse_context);
    ecl_free_parse_context(parse_context);
    return code;
}
#endif

static void
setup_conversions(cm, context, ioformat, field_list, format_list)
CManager cm;
FFSContext context;
FMFormat ioformat;
FMFieldList field_list;
FMStructDescList format_list;
{
/*    FMFormat *ioformats;
    FMFormat *format;
    int native_struct_size;

    if ((ioformats = get_subformats_IOformat(ioformat)) == NULL) {
	fprintf(stderr, "IOFormat list is null\n");
	return;
    }
    format = ioformats;
    while (format_list && (*format != NULL)) {
	int i = 0;
	if (!has_conversion_IOformat(*format)) {
	    while (format_list[i].format_name != NULL) {
		if (strcmp(format_list[i].format_name,
			   name_of_IOformat(*format)) == 0) {
		    break;
		}
		i++;
	    }
	    if (format_list[i].field_list != NULL) {
		native_struct_size =
		    struct_size_IOfield((IOFile) context,
					format_list[i].field_list);
		CMset_conversion_IOcontext(cm, context, *format,
					   format_list[i].field_list,
					   native_struct_size);
	    }
	}
	format++;
    }
    free(ioformats);
    native_struct_size = struct_size_IOfield((IOFile) context, field_list);
    CMset_conversion_IOcontext(cm, context, ioformat,
    field_list, native_struct_size);*/
}

typedef struct _InfoResponseMsg
{
    char *type_str;
    int cond;
}
InfoResponseMsg, *InfoResponseMsgPtr;

FMField channel_info_response_msg_flds[] = {
    {"type_string", "string", sizeof(char *),
     FMOffset(InfoResponseMsgPtr, type_str)},
    {"condition", "integer", sizeof(int),
     FMOffset(InfoResponseMsgPtr, cond)},
    {(char *) 0, (char *) 0, 0, 0}
};

FMStructDescRec channel_info_response_msg_formats[] = {
    {"Channel Info Response Message", channel_info_response_msg_flds,sizeof(InfoResponseMsg), NULL},
    {NULL, NULL, 0, NULL}};

extern void
Channel_Info_Request_handler(cm, conn, data, client_data, attrs)
CManager cm;
CMConnection conn;
void *data;
void *client_data;
attr_list attrs;
{
    InfoRequestMsgPtr request = (InfoRequestMsgPtr) data;
    InfoResponseMsg response;
    EChannel chan;

    chan = string_to_channel_ptr(request->chan_str);
    response.cond = request->cond;
    if (chan == NULL) {
	response.type_str = NULL;
    } else {
	response.type_str = global_name_of_IOformat(chan->type_format);
    }
    if (!CMwrite(conn, CMlookup_format(cm, channel_info_response_msg_flds),
		 &response)) {
	ECtrace_out("sent channel info response write failed", 0);
    }
    if (response.type_str)
	CMfree(response.type_str);
}

extern void
Channel_Info_Response_handler(cm, conn, data, client_data, attrs)
CManager cm;
CMConnection conn;
void *data;
void *client_data;
attr_list attrs;
{
    InfoResponseMsgPtr response = (InfoResponseMsgPtr) data;
    char **type_str_ptr = CMCondition_get_client_data(cm, response->cond);

    ECtrace_out("Channel_Info_Response_handler", 1);
    if (response->type_str == NULL) {
	*type_str_ptr = NULL;
    } else {
	*type_str_ptr = strdup(response->type_str);
    }
    CMCondition_signal(cm, response->cond);
    ECtrace_out("Channel_Info_Response_handler", 0);
}
#endif

extern void
ECsetup_filter_data(chan, init_data, init_data_len)
EChannel chan;
void *init_data;
int init_data_len;
{
    printf("ECsetup_filter_data\n");
#ifdef NOT_DEF
    FMFieldList data_field_list = NULL;
    CMFormatList tmp_list, data_format_list = NULL;
    void *data;
    IOFormat data_format;

    chan->encoded_filter_data_len = init_data_len;
    chan->encoded_filter_data = malloc(init_data_len);
    memcpy(chan->encoded_filter_data, init_data, init_data_len);
    data_format = CMget_format_IOcontext(chan->cm, chan->subcontext,
					 chan->encoded_filter_data);
    data_field_list = field_list_of_IOformat(data_format);
    data_format_list = get_format_list(data_format);
    data = CMmalloc(this_IOrecord_length(chan->subcontext, 
					 chan->encoded_filter_data,
					 init_data_len));
    chan->filter_data = data;
    chan->data_field_list = simplify_field_list(data_field_list);
    tmp_list = data_format_list;
    while ((tmp_list != NULL) && (tmp_list->format_name != NULL)) {
	tmp_list->field_list =
	    simplify_field_list(tmp_list->field_list);
	tmp_list++;
    }
    chan->data_format_list = data_format_list;
    if (!has_conversion_IOformat(data_format)) {
	setup_conversions(chan->cm, chan->subcontext, data_format,
			  chan->data_field_list,
			  chan->data_format_list);
    }
    decode_to_buffer_IOcontext(chan->subcontext, chan->encoded_filter_data, 
			       data);
#endif
}

#ifdef NOT_DEF
/* 
 * do channel_derive does most of the work involved in deriving an event
 * channel.  If output_field_list and output_format_list are NULL, this is
 * this is an untyped derivation and the output types are the same as the
 * input types.  (No "output" parameter to handler.)  If init_data is
 * non-NULL, there is data associated with the derived channel and this
 * gives the (pbio-encoded) initial value.
 */
static EChannel
do_channel_derive(cc, parent_chan, filter, output_field_list,
		  output_format_list, filter_data, 
		  init_data, init_data_len)
EControlContext cc;
EChannel parent_chan;
char *filter;
FMFieldList output_field_list;
CMFormatList output_format_list;
ECdata_spec filter_data;
char *init_data;
int init_data_len;
{
    EChannel chan;
    char *der_chan_str;
    int i;
    FMFieldList input_field_list;
    CMFormatList input_format_list;

    input_field_list = EChannel_get_field_list(parent_chan);
    input_format_list = EChannel_get_format_list(parent_chan);
    if (output_field_list == NULL) {
	/* use field lists for parent channel when doing untyped derive */
	chan = EChannel_typed_create(cc, input_field_list,
				     input_format_list);
    } else {
	chan = EChannel_typed_create(cc, output_field_list,
				     output_format_list);
    }
    der_chan_str = ECglobal_id(chan);
    if (init_data != NULL) {
	ECsetup_filter_data(chan, init_data, init_data_len);
    }
    
    do_chan_derive(parent_chan, chan, filter, input_field_list,
		   input_format_list,
		   output_field_list, output_format_list,
		   filter_data ? filter_data->data_field_list : NULL,
		   filter_data ? filter_data->data_subformat_list : NULL,
		   chan->filter_data,
		   (output_field_list != NULL) /* has_output_param */);

    if (input_format_list)
	free_format_list(input_format_list);
    free_field_list(input_field_list);
    ECtrace_out("send derive request to all *OTHER* members", 3);
    for (i = 0; i < parent_chan->rem_member_list_len; i++) {
	int cond;
	SrcDeriveMsg src_derive_msg;

	cond =
	    CMCondition_get(cc->cm, parent_chan->rem_member_list[i].conn);
	CMCondition_set_client_data(cc->cm, cond, &chan);
	memset(&src_derive_msg, 0, sizeof(src_derive_msg));
	src_derive_msg.der_chan_id = der_chan_str;
	src_derive_msg.filter = filter;
	src_derive_msg.cond = cond;
	src_derive_msg.init_data_block = init_data;
	src_derive_msg.init_data_len = init_data_len;
	src_derive_msg.typed_output = (output_field_list != NULL);
	src_derive_msg.parent_channel_id =
	    parent_chan->rem_member_list[i].remote_id;
	if (!CMwrite(parent_chan->rem_member_list[i].conn,
		     CMlookup_format(cc->cm, src_derive_msg_flds),
		     &src_derive_msg)) {
	    ECtrace_out("source derive msg write failed", 0);
	    continue;
	}
	/* wait here to make sure that each source */
	/* subscribes, need a barrier */
	ECtrace_out("waiting for Channel Source Derive Resp (condition %d)", 3, cond);
	/* this is signaled in Channel_Source_Derive_Resp_handler */
	if (CMCondition_wait(cc->cm, cond))
	    chan->ready++;
	ECtrace_out("return from wait condition %d", 3, cond);
    }
    CMfree(der_chan_str);
    ECtrace_out("done with derive", 3);
    return chan;
}

int
check_filter_string(filter)
char *filter;
{

    if (filter[0] == 'd' && filter[1] == 'l' && filter[2] == 'l' && filter[3] == ':') {
	return FILTER_DLL;
    }
    return 0;
}


char *
extract_dll_path(filter)
char *filter;
{
    char *copy = strdup(filter);
    char *temp;
    char *path;


    temp = strtok(copy, ":");
    if (strcmp(temp, "dll")) {
	return NULL;
    }
    temp = strtok(NULL, ":");

    if (temp == NULL)
	return NULL;

    path = strdup(temp);

    return path;
}


char *
extract_symbol_name(filter)
char *filter;
{

    char *copy = strdup(filter);
    char *temp;
    char *symbol;

    temp = strtok(copy, ":");
    if (strcmp(temp, "dll")) {
	return NULL;
    }
    temp = strtok(NULL, ":");
    temp = strtok(NULL, ":");

    if (temp == NULL)
	return NULL;

    symbol = strdup(temp);

    return symbol;
}


FilterFunc
load_dll_symbol(path, symbol_name)
char *path;
char *symbol_name;
{
    lt_dlhandle handle;
    FilterFunc func;

    if (lt_dlinit() != 0) {
	fprintf(stderr, "error during initialized: %s\n", lt_dlerror());
	return (FilterFunc) NULL;
    }
    handle = lt_dlopen(path);
    if (!handle) {
	fprintf(stderr, "failed on dll open %s\n", lt_dlerror());
	return (FilterFunc) NULL;
    }
    func = (FilterFunc) lt_dlsym(handle, symbol_name);

    return func;
}

typedef struct {
    FilterFunc func;
    void *client_data;
} *ECFilterHandle;

typedef int (*ECTransformHandlerFunction) ARGS((void *event, void *outgoing_event, void *client_data, attr_list event_attrs));

typedef struct {
    ECTransformHandlerFunction func;
    void *client_data;
    int outgoing_base_size;
    EVsource outgoing_source_handle;
    IOFormat outgoing_format;
} *ECTransformHandle;

static int
EVFilter_wrapper_func(CManager cm, void *message, void *client_data,
			attr_list attrs)
{
    ECFilterHandle handle = (ECFilterHandle) client_data;
    FilterFunc func = (FilterFunc) handle->func;
    void *func_client_data = handle->client_data;
    int ret = 0;
    if (func) {
	ret = (func)(message, func_client_data, attrs, NULL);
    } else {
	printf("ECho Event ignored, func is NULL\n");
    }
    return ret;
}

static int
EVTransform_wrapper_func(CManager cm, void *message,
		      void *client_data, attr_list attrs)
{
    ECTransformHandle handle = (ECTransformHandle) client_data;
    ECTransformHandlerFunction func = (ECTransformHandlerFunction) handle->func;
    void *func_client_data = handle->client_data;
    if (func) {
	int ret;
	void *outgoing = malloc(handle->outgoing_base_size);
	memset(outgoing, 0, handle->outgoing_base_size);
	ret = (func)(message, outgoing, func_client_data, attrs);
	if (ret) {
	    ECtrace_out("Transformation produced result, submitting... ", 2);
	    EVsubmit(handle->outgoing_source_handle, outgoing, attrs);
	} else {
	    ECtrace_out("Transformation produced NO result", 2);
	}	    
    } else {
	printf("ECho Event ignored, func is NULL\n");
    }
    return 0;
}

void
pbio_free_func_wrapper(void *data, void *client_data)
{
    ECTransformHandle handle = client_data;
    IOfree_var_rec_elements(iofile_of_IOformat(handle->outgoing_format),
			    handle->outgoing_format,data);
    free(data);
}
static void
do_chan_derive(parent_chan, der_chan, filter, 
	       input_field_list, input_format_list,
	       output_field_list, output_format_list, data_field_list,
	       data_format_list, filter_data, has_output_param)
EChannel parent_chan;
EChannel der_chan;
char *filter;
FMFieldList input_field_list;
CMFormatList input_format_list;
FMFieldList output_field_list;
CMFormatList output_format_list;
FMFieldList data_field_list; 
CMFormatList data_format_list; 
void *filter_data;
int has_output_param;
{
    ecl_code code_struct = NULL;
    void *filter_func = NULL;

    /* add derived chan to list in parent channel */
    parent_chan->der_chan_list = CMrealloc(parent_chan->der_chan_list,
					   (parent_chan->der_list_len +
					    1) *
					   sizeof(struct _derived_chan));
    parent_chan->der_chan_list[parent_chan->der_list_len].derived_chan =
	der_chan;
    parent_chan->der_chan_list[parent_chan->der_list_len].filter =
	strdup(filter);
    parent_chan->der_chan_list[parent_chan->der_list_len].has_output_param = 
	has_output_param;
    parent_chan->der_chan_list[parent_chan->der_list_len].action_stone =
	EValloc_stone(der_chan->cm);
    
    /*GSE*/
    /*  add a split to the original channel (like a sink)
	target of split is a new stone whose action is Action_Filter
	output of Action_Filter targets outgoing stone of der channel
    */

    
    if (check_filter_string(filter) == FILTER_DLL) {
	/* it is a dll */
	char *path = NULL;
	char *symbol_name = NULL;

	path = extract_dll_path(filter);
	symbol_name = extract_symbol_name(filter);

	filter_func = (void*)load_dll_symbol(path, symbol_name);
    } else {
	code_struct = generate_filter_code(filter, input_field_list,
					   input_format_list,
					   output_field_list,
					   output_format_list,
					   data_field_list,
					   data_format_list);
	filter_func = (void*)code_struct->func;
    }
    
    if (output_field_list == NULL) {
	ECFilterHandle handle = malloc(sizeof(*handle));
	parent_chan->der_chan_list[parent_chan->der_list_len].filter_handle = 
	  (void *)handle;
	handle->func = (FilterFunc) (long)filter_func;
	EVassoc_filter_action(der_chan->cm, 
			      parent_chan->der_chan_list[parent_chan->der_list_len].action_stone,
			      parent_chan->ev_format_list,
			      EVFilter_wrapper_func,
			      der_chan->outgoing_target_stone_id,
			      parent_chan->der_chan_list[parent_chan->der_list_len].filter_handle);
    } else {
	/* 
	 * for transformations, use a Terminal action with a submit 
	 * in the wrapper function
	 */
	ECTransformHandle thandle = malloc(sizeof(*thandle));
	thandle->client_data = filter_data;
	parent_chan->der_chan_list[parent_chan->der_list_len].filter_handle = 
	    thandle;
	thandle->func = (ECTransformHandlerFunction) filter_func;
	thandle->outgoing_source_handle = 
	    EVcreate_submit_handle_free(parent_chan->cm, 
					der_chan->outgoing_target_stone_id,
					der_chan->ev_format_list,
					pbio_free_func_wrapper,
					thandle);
	thandle->outgoing_format = 
	    EVget_src_ref_format(thandle->outgoing_source_handle);
	thandle->outgoing_base_size = struct_size_field_list(der_chan->ev_format_list[0].field_list, sizeof(char*));
	EVassoc_terminal_action(der_chan->cm, 
			      parent_chan->der_chan_list[parent_chan->der_list_len].action_stone,
			      parent_chan->ev_format_list,
			      EVTransform_wrapper_func,
			      thandle);
    }
    EVaction_add_split_target(parent_chan->cm, 
			      parent_chan->incoming_target_stone_id,
			      parent_chan->incoming_split_action, 
			      parent_chan->der_chan_list[parent_chan->der_list_len].action_stone);
    parent_chan->der_list_len++;

    /* add parent to list in derived channel */
    der_chan->parent_chan_list = CMrealloc(der_chan->parent_chan_list,
					   (der_chan->parent_list_len +
					    1) * sizeof(EChannel));
    der_chan->parent_chan_list[der_chan->parent_list_len] = parent_chan;
    der_chan->parent_list_len++;

}

#endif
extern void
remove_derived_channel(parent_chan, der_chan)
EChannel parent_chan;
EChannel der_chan;
{
    int i;
    /* walk derived list */
    for (i = 0; i < parent_chan->der_list_len; i++) {
	if (parent_chan->der_chan_list[i].derived_chan == der_chan) {
	    CMfree(parent_chan->der_chan_list[i].filter);

	    /* copy down higher derived channels */
	    for (; i < parent_chan->der_list_len - 1; i++) {
		parent_chan->der_chan_list[i] =
		    parent_chan->der_chan_list[i + 1];
	    }
	    parent_chan->der_list_len--;
	    break;
	}
    }
    /* walk sources */
    for (i = 0; i < parent_chan->local_source_list_len; i++) {
	ECSourceHandle src_entry =
	    parent_chan->local_source_list[i].source_handle;
	int j;
	for (j = 0; j < src_entry->der_list_len; j++) {
	    derived_source src = &src_entry->der_source_list[j];
	    if (src->derived_chan == der_chan) {
		CMfree((void *) (long) src->filter);
		/* src_handle will be freed when der_channel is destroyed */

		/* copy down higher derived sources */
		for (; j < src_entry->der_list_len - 1; j++) {
		    src_entry->der_source_list[j] =
			src_entry->der_source_list[j + 1];
		}
		src_entry->der_list_len--;
		break;		/* break out of inner for loop */
	    }
	}
    }
}
#ifdef NOT_DEF
/* create a derived event channel and reply to sender */
/* send derive request to all sources of the parent channel */
extern void
Channel_Remote_Derive_handler(cm, conn, data, client_data, attrs)
CManager cm;
CMConnection conn;
void *data;
void *client_data;
attr_list attrs;
{

}

/* create a derived event channel and reply to sender */
/* send derive request to all sources of the parent channel */
extern void
Channel_Derive_handler(cm, conn, data, client_data, attrs)
CManager cm;
CMConnection conn;
void *data;
void *client_data;
attr_list attrs;
{
    DeriveMsgPtr derive_msg = (DeriveMsgPtr) data;
    EControlContext cc = (EControlContext) client_data;
    EChannel chan, parent_chan;
    int i, derive_cond;
    CMFormatList output_format_list;
    FMFieldList field_list = NULL;
    char *filter;
    ECdata_spec filter_data = NULL;

    ECtrace_out("Channel_Derive_handler", 1);
    CMconn_register_close_handler(conn, EChannel_close_handler,
				  (EControlContext) client_data);

    parent_chan = string_to_channel_ptr(derive_msg->chan_str);

    /* check to see that parent channel is valid */

    /* check to see if filter already exists */

    ECtrace_out(" condition = %d", 3, derive_msg->cond);
    derive_cond = derive_msg->cond;

    output_format_list = CMmalloc((derive_msg->format_list_len + 1) *
				  sizeof(CMFormatList));
    for (i = 0; i < derive_msg->format_list_len; i++) {
	output_format_list[i].format_name =
	    derive_msg->format_list[i].format_name;
	output_format_list[i].field_list =
	    copy_field_list(derive_msg->format_list[i].field_list);
    }
    if (derive_msg->field_list != NULL) {
	field_list = copy_field_list(derive_msg->field_list);
    }
    filter = malloc(strlen(derive_msg->filter) + 2);
    strcpy(filter, derive_msg->filter);
    filter[strlen(filter) + 1] = 0;	/* add extra NULL termination */

    output_format_list[derive_msg->format_list_len].format_name = NULL;
    output_format_list[derive_msg->format_list_len].field_list = NULL;
    ECtrace_out("Deriving channel with filter %s\n", 3, filter);
    chan = do_channel_derive(cc, parent_chan, filter,
			     field_list, output_format_list,
			     filter_data,
			     derive_msg->init_data_block,
			     derive_msg->init_data_len);
    CMfree(output_format_list);
    
    {
	AttendMsg attend_msg;
	char *der_chan_str = channel_ptr_to_string(chan);
	ECtrace_out("Derive - Dummy up Channel Attend msg to respond to requestor\n", 3);
	attend_msg.chan_str = der_chan_str;
	attend_msg.contact_str = derive_msg->client_contact_str;
	attend_msg.cond = derive_cond;
	attend_msg.client_channel_id = derive_msg->client_channel_id;
	attend_msg.incoming_target_stone_id = derive_msg->client_stone_id;
	attend_msg.proto_version = 1;
	Channel_Attend_handler(cm, conn, &attend_msg, client_data, attrs);
	free(der_chan_str);
    }

    if (channel_verbose) {
	printf("\n\nChannel_Derive_handler\n");
	dump_EChannel(parent_chan);
    }
    ECtrace_out("Channel_Derive_handler", 0);
}

ECDataHandle
EChannel_data_open(channel, data_field_list, data_subformat_list)
EChannel channel;
FMFieldList data_field_list;
CMFormatList data_subformat_list;
{
    ECDataHandle handle = new_data_handle();
    ECdata_struct data_spec;
    data_spec.data_field_list = data_field_list;
    data_spec.data_subformat_list = data_subformat_list;
    data_spec.initial_value = NULL;

    /* this doesn't really encode, just initializes the handle */
    handle->channel = channel;
    handle->data_field_list = data_field_list;
    handle->data_subformat_list = data_subformat_list;
    handle->cm = channel->cm;
    encode_data_block(&data_spec, NULL, NULL, handle, channel->ec);
    return handle;
}

static void
send_remote_data_update(chan, i, encoded_data, data_len)
EChannel chan;
int i;
void *encoded_data;
int data_len;
{
    DataUpdateMsg msg;
    memset(&msg, 0, sizeof(msg));
    msg.data = encoded_data;
    msg.data_len = data_len;
    msg.global_channel_id = chan->global_id.global_channel_id;
    ECtrace_out("send_remote_data_update, chan %lx, event %lx", 1, chan,
		encoded_data);
    if (!CMwrite
	(chan->rem_member_list[i].conn,
	 CMlookup_format(chan->cm, data_update_msg_flds), &msg)) {
	ECtrace_out("send remote data update write failed", 0);
    }
    ECtrace_out("send_remote_data_update", 0);
}

extern void
Data_Update_handler(cm, conn, data, client_data, attrs)
CManager cm;
CMConnection conn;
void *data;
void *client_data;
attr_list attrs;
{
    EChannel chan;
    DataUpdateMsg *msg = (DataUpdateMsg *) data;
    void *local_data;
    IOFormat ioformat;
    EControlContext ec = (EControlContext) client_data;

    chan = get_local_channel_from_id(msg->global_channel_id, ec);
    ECtrace_out("In Data_Update_Handler, chan %lx", 1, (long) chan);

    if (chan != NULL) {
	EChannel_lock(chan);
	/* find out the format of the data in the incoming record */
	ioformat = CMget_format_IOcontext(cm, chan->subcontext, msg->data);

	if (has_conversion_IOformat(ioformat)) {
	    /* if not, set one */
	    setup_conversions(cm, chan->subcontext, ioformat,
			      chan->data_field_list,
			      chan->data_format_list);
	}
	local_data = CMmalloc(this_IOrecord_length(chan->subcontext,
						   msg->data,
						   msg->data_len));
	decode_to_buffer_IOcontext(chan->subcontext, msg->data,
				   local_data);
	CMfree(chan->filter_data);
	chan->filter_data = local_data;
	EChannel_unlock(chan);
    }
    ECtrace_out("Exitting Data_Update_Handler, chan %lx", 0, (long) chan);
}

void
EChannel_data_update(handle, data)
ECDataHandle handle;
void *data;
{
    void *encoded_block, *local_data;
    int block_len, i;
    ECdata_struct data_spec;
    IOFormat data_format;
    data_spec.data_field_list = handle->data_field_list;
    data_spec.data_subformat_list = handle->data_subformat_list;
    data_spec.initial_value = data;
    encode_data_block(&data_spec, (char **) &encoded_block, &block_len,
		      handle, handle->channel->ec);
    EChannel_lock(handle->channel);
    ECtrace_out
	("In echannel_data_update, handle %lx, data %lx, members %d", 1,
	 handle, data, handle->channel->rem_member_list_len);
    for (i = 0; i < handle->channel->rem_member_list_len; i++) {
	if (handle->channel->rem_member_list[i].conn != NULL) {
	    EChannel_unlock(handle->channel);
	    send_remote_data_update(handle->channel, i, encoded_block,
				    block_len);
	    EChannel_lock(handle->channel);
	}
    }
    local_data = CMmalloc(this_IOrecord_length(handle->iocontext,
					       encoded_block, block_len));
    data_format = get_format_IOcontext(handle->iocontext, encoded_block);
    if (!has_conversion_IOformat(data_format)) {
	setup_conversions(handle->cm, handle->iocontext, data_format,
			  handle->data_field_list,
			  handle->data_subformat_list);
    }
    decode_to_buffer_IOcontext(handle->iocontext, encoded_block,
			       local_data);
    CMfree(handle->channel->filter_data);
    handle->channel->filter_data = local_data;
    CMfree(handle->channel->encoded_filter_data);
    handle->channel->encoded_filter_data = encoded_block;
    handle->channel->encoded_filter_data_len = block_len;
    EChannel_unlock(handle->channel);
    ECtrace_out("echannel_data_update", 0);
}

static FMFieldList
simplify_field_list(list)
FMFieldList list;
{
    int i = 0;
    while (list[i].field_name != NULL) {
	char *colon = strchr(list[i].field_type, ':');
	char *bracket = strchr(list[i].field_type, '[');
	if (colon != NULL) {
	    /* make field type be simple, not format server style */
	    *colon = 0;
	    if (bracket != NULL) strcpy(colon, bracket);
	}
	i++;
    }
    return list;
}

extern FMFieldList
EChannel_get_field_list(chan)
EChannel chan;
{

    FMFieldList list =
	copy_field_list(field_list_of_IOformat(chan->type_format));
    return simplify_field_list(list);
}

static CMFormatList
get_format_list(ioformat)
IOFormat ioformat;
{
    IOFormat *formats = get_subformats_IOformat(ioformat);
    CMFormatList format_list = malloc(sizeof(format_list[0]));
    int format_count = 0;
    IOFormat *orig_formats = formats;
    while (formats[1] != NULL) {	/* last format is top-level */
	FMFieldList list =
	    copy_field_list(field_list_of_IOformat(*formats));
	format_list =
	    realloc(format_list,
		    sizeof(format_list[0]) * (format_count + 2));
	format_list[format_count].format_name =
	    strdup(name_of_IOformat(*formats));
	format_list[format_count].field_list = simplify_field_list(list);
	formats++;
	format_count++;
    }
    free(orig_formats);
    format_list[format_count].format_name = NULL;
    format_list[format_count].field_list = NULL;
    if (format_count == 0) {
	free(format_list);
	return NULL;
    }
    return format_list;
}

static void
free_format_list(list)
CMFormatList list;
{
    int i = 0;
    if (list == NULL)
	return;
    while (list[i].format_name != NULL) {
	CMfree(list[i].format_name);
	free_field_list(list[i].field_list);
	i++;
    }
    CMfree(list);
}

extern CMFormatList
EChannel_get_format_list(chan)
EChannel chan;
{
    return get_format_list(chan->type_format);
}

/* open and subscribe to a derived event channel */
extern void
Channel_Source_Derive_handler(cm, conn, data, client_data, attrs)
CManager cm;
CMConnection conn;
void *data;
void *client_data;
attr_list attrs;
{
    SrcDeriveMsgPtr msg = (SrcDeriveMsgPtr) data;
    EChannel parent_chan, der_chan;
    SrcDeriveRespMsg ret_msg;
    FMFieldList output_field_list = NULL, input_field_list;
    CMFormatList output_format_list = NULL, input_format_list;
    EControlContext cc = (EControlContext) client_data;
    int cond;
    int i;

    ECtrace_out("Channel_Source_Derive_handler", 1);

    CMconn_register_close_handler(conn, EChannel_close_handler,
				  (EControlContext) client_data);

    parent_chan = get_local_channel_from_id(msg->parent_channel_id, cc);

    cond = msg->cond;

    /* open the derivative */
    der_chan = EChannel_open(cc, msg->der_chan_id);

    if (msg->init_data_block != NULL) {
	ECsetup_filter_data(der_chan, msg->init_data_block, 
			    msg->init_data_len);
    } else {
	der_chan->filter_data = NULL;
    }
    if (msg->typed_output) {
	output_field_list = EChannel_get_field_list(der_chan);
	output_format_list = EChannel_get_format_list(der_chan);
    }
    input_field_list = EChannel_get_field_list(parent_chan);
    input_format_list = EChannel_get_format_list(parent_chan);

    do_chan_derive(parent_chan, der_chan, msg->filter, 
		   input_field_list, input_format_list,
		   output_field_list, output_format_list,
		   der_chan->data_field_list, der_chan->data_format_list,
		   der_chan->filter_data,
		   msg->typed_output);

    i = 0;
    while (output_format_list &&
	   (output_format_list[i].format_name != NULL)) {
	free_field_list(output_format_list[i].field_list);
	free(output_format_list[i].format_name);
	i++;
    }
    if (output_format_list)
	free(output_format_list);

    ECtrace_out("create return message and send back to arbitrator", 3);
    memset(&ret_msg, 0, sizeof(ret_msg));
    ret_msg.cond = cond;

    if (!CMwrite(conn, CMlookup_format(cm, src_derive_resp_msg_flds),
		 &ret_msg)) {
	ECtrace_out("src derive response write failed", 0);
    }

    if (channel_verbose) {
	printf("\n\nChannel_Source_Derive_handler\n");
	dump_EChannel(parent_chan);
    }
    ECtrace_out("Channel_Source_Derive_handler", 0);
}

/* return message from source to arbitrator signaling that */
/* the derived channel was created on that source */
extern void
Channel_Source_Derive_Resp_handler(cm, conn, data, client_data, attrs)
CManager cm;
CMConnection conn;
void *data;
void *client_data;
attr_list attrs;
{
    SrcDeriveRespMsgPtr msg = (SrcDeriveRespMsgPtr) data;

    ECtrace_out("Channel_Source_Derive_Resp_handler", 1);

    CMconn_register_close_handler(conn, EChannel_close_handler,
				  (EControlContext) client_data);

    CMCondition_signal(cm, msg->cond);

    ECtrace_out("Channel_Source_Derive_Resp_handler", 0);
}

static ECDataHandle
new_data_handle()
{
    ECDataHandle tmp = malloc(sizeof(*tmp));
    tmp->iocontext = NULL;
    tmp->type_format = NULL;
    tmp->channel = NULL;
    tmp->data_field_list = NULL;
    tmp->data_subformat_list = NULL;
    tmp->cm = NULL;
    return tmp;
}

extern void
EChannel_data_close(handle)
ECDataHandle handle;
{
    CMfree_user_type_context(handle->cm, handle->iocontext);
    free(handle);
}

static void
encode_data_block(filter_data, data_block, data_len, handle, cc)
ECdata_spec filter_data;
char **data_block;
int *data_len;
ECDataHandle handle;
EControlContext cc;
{
    ECDataHandle tmp_handle = handle;
    void *tmp_data;
    void *return_data;
    CManager cm = cc->cm;

    if (filter_data == NULL) {
	*data_block = NULL;
	*data_len = 0;
	return;
    }
    if (handle == NULL) {
	tmp_handle = new_data_handle();
	tmp_handle->cm = cm;
    } else {
	cm = handle->channel->cm;
    }
    if (tmp_handle->iocontext == NULL) {
	tmp_handle->iocontext =
	    create_IOsubcontext(CMget_user_type_context(cm));
    }
    if (tmp_handle->type_format == NULL) {
	tmp_handle->type_format =
	    CMregister_user_format(cm, tmp_handle->iocontext,
				   "Tmp channel data",
				   filter_data->data_field_list,
				   filter_data->data_subformat_list);
    }
    if (data_block == NULL) {
	return;
    }
    tmp_data = encode_IOcontext_buffer(tmp_handle->iocontext,
				       tmp_handle->type_format,
				       filter_data->initial_value,
				       data_len);
    /* must preserve data before unlocking */
    return_data = CMmalloc(*data_len);
    memcpy(return_data, tmp_data, *data_len);
    *data_block = return_data;
    if (handle == NULL) {
	EChannel_data_close(tmp_handle);
    }
}

#ifndef NO_DERIVED
typedef struct _remote_derive_handle
{
    ECSinkHandle sink_handle;
    ECSourceHandle src_handle;
    ecl_code code_struct;
    FilterFunc filterfunc;
    int target_type_len;
    int has_output_param;
}
*remote_derive_handle;

static void
ECho_xform_event_handler(void *event, void *client_data, attr_list attrs)
{
    remote_derive_handle handle = (remote_derive_handle) client_data;
    void *filter_data = NULL;
    int type_len = handle->target_type_len;
    FilterFunc func = handle->filterfunc;
    if (handle->has_output_param) {
	int tmp;
	void *event2 = CMmalloc(type_len);
	memset(event2, 0, type_len);
	tmp = (func) (event, event2, filter_data, NULL);
	if (tmp != 0) {
	    ECtrace_out("Xform function returned %d, submitting event",
			3, tmp);
	    /* int_submit_general_typed_event(handle->src_handle, event2,
	     * free); */
	} else {
	    ECtrace_out
		("Derivation function returned 0, no event submitted", 3);
	}
    } else {
	int tmp = (func) (event, NULL, filter_data, NULL);
	if (tmp != 0) {
	    ECtrace_out("Xform function returned %d, submitting event",
			3, tmp);
	    /* internal_submit_general_event(handle->src_handle, event, 0, 
	     * block_rec); */
	} else {
	    ECtrace_out("Xform function returned 0, no event submitted",
			3);
	}
    }

}

static char *
do_local_remote_derive(ec, src_chan_id, event_filter,
		       output_field_list, output_format_list, filter_data)
EControlContext ec;
char *src_chan_id;
char *event_filter;
FMFieldList output_field_list;
CMFormatList output_format_list;
ECdata_spec filter_data;
{
    EChannel chan;
    char *der_chan_str;
    IOFormat data_format = NULL;
    FMFieldList input_field_list;
    CMFormatList input_format_list;
    ecl_code code_struct;
    ECSinkHandle sink_handle;
    ECSourceHandle source_handle;
    remote_derive_handle handle;
    FMFieldList data_field_list = NULL;
    CMFormatList tmp_list, data_format_list = NULL;

    EChannel parent_chan = EChannel_open(ec, src_chan_id);
    if (parent_chan == NULL)
	return NULL;

    input_field_list = EChannel_get_field_list(parent_chan);
    input_format_list = EChannel_get_format_list(parent_chan);
    if (output_field_list == NULL) {
	/* use field lists for parent channel when doing untyped derive */
	chan = EChannel_typed_create(ec, input_field_list,
				     input_format_list);
    } else {
	chan = EChannel_typed_create(ec, output_field_list,
				     output_format_list);
    }
    der_chan_str = ECglobal_id(chan);

    if (filter_data != NULL) {
	void *init_data;
	void *data;
	int init_data_len = 0;
	if (filter_data != NULL) {
	    encode_data_block(filter_data, (char **) &init_data,
			      &init_data_len, NULL, ec);
	}
	data_format = CMget_format_IOcontext(chan->cm, chan->subcontext,
					     filter_data);
	data_field_list = field_list_of_IOformat(data_format);
	data_format_list = get_format_list(data_format);
	data = CMmalloc(this_IOrecord_length(chan->subcontext, init_data,
					     init_data_len));
	chan->filter_data = filter_data;
	chan->data_field_list = simplify_field_list(data_field_list);
	tmp_list = data_format_list;
	while ((tmp_list != NULL) && (tmp_list->format_name != NULL)) {
	    tmp_list->field_list =
		simplify_field_list(tmp_list->field_list);
	    tmp_list++;
	}
	chan->data_format_list = data_format_list;
	if (!has_conversion_IOformat(data_format)) {
	    setup_conversions(ec->cm, chan->subcontext, data_format,
			      chan->data_field_list,
			      chan->data_format_list);
	}
	decode_to_buffer_IOcontext(chan->subcontext, init_data, data);
    }

    if (check_filter_string(event_filter) == FILTER_DLL) {
	char *path = NULL;
	char *symbol_name = NULL;
	FilterFunc filterfunc;


	path = extract_dll_path(event_filter);
	symbol_name = extract_symbol_name(event_filter);

	filterfunc = load_dll_symbol(path, symbol_name);

	handle = malloc(sizeof(*handle));
	sink_handle = ECsink_typed_subscribe(parent_chan, input_field_list,
					     input_format_list,
					     ECho_xform_event_handler,
					     handle);
	source_handle = ECsource_typed_subscribe(chan, output_field_list,
						 output_format_list);
	handle->sink_handle = sink_handle;
	handle->src_handle = source_handle;
	handle->code_struct = NULL;
	handle->filterfunc = (FilterFunc) filterfunc;
	handle->has_output_param = (output_field_list == NULL);
	if (output_field_list != NULL) {
	    handle->target_type_len =
		struct_size_field_list(output_field_list, sizeof(char *));
	} else {
	    handle->target_type_len = 0;
	}
    } else {
	code_struct = generate_filter_code(event_filter, input_field_list,
					   input_format_list,
					   output_field_list,
					   output_format_list,
					   data_field_list,
					   data_format_list);
	if (code_struct == NULL) {
	    EChannel_destroy(chan);
	    free(der_chan_str);
	    return NULL;
	}
	handle = malloc(sizeof(*handle));
	sink_handle = ECsink_typed_subscribe(parent_chan, input_field_list,
					     input_format_list,
					     ECho_xform_event_handler,
					     handle);
	source_handle = ECsource_typed_subscribe(chan, output_field_list,
						 output_format_list);
	handle->sink_handle = sink_handle;
	handle->src_handle = source_handle;
	handle->code_struct = code_struct;
	handle->filterfunc = (FilterFunc) code_struct->func;
	handle->has_output_param = (output_field_list == NULL);
	if (output_field_list != NULL) {
	    handle->target_type_len =
		struct_size_field_list(output_field_list, sizeof(char *));
	} else {
	    handle->target_type_len = 0;
	}
    }

    return der_chan_str;
}
#endif

extern char *
EChannel_remote_typed_data(ec, remote_contact_string, src_chan_id,
			   event_filter,
			   output_field_list, output_format_list,
			   filter_data)
EControlContext ec;
char *remote_contact_string;
char *src_chan_id;
char *event_filter;
FMFieldList output_field_list;
CMFormatList output_format_list;
ECdata_spec filter_data;
{
#ifndef NO_DERIVED
    attr_list attrs;
    CMConnection conn;
    EChannel chan = NULL;
    DeriveMsg derive_msg;
    int cond, i;
    char *chan_id = NULL;

    ECtrace_out("ECchannel_remote_derive", 1);

    if (output_field_list == NULL) {
	ECtrace_out
	    ("This is an untyped remote derivation without an output parameter",
	     3);
    } else {
	ECtrace_out
	    ("This is an typed remote derivation with an output parameter",
	     3);
    }
    attrs = attr_list_from_string(remote_contact_string);
    if (CMcontact_self_check(ec->cm, attrs)) {
	free_attr_list(attrs);
	ECtrace_out("remote derivation to ourselves!", 3,
		    remote_contact_string);
	chan_id = do_local_remote_derive(ec, src_chan_id, event_filter,
					 output_field_list,
					 output_format_list, filter_data);
	return chan_id;
    }
    ECtrace_out("getting a connection to %s, registering close handler", 3,
		remote_contact_string);
    if (attrs == NULL) {
	printf("malformed contactlist\n");
	return NULL;
    }
    conn = CMget_conn(ec->cm, attrs);
    free_attr_list(attrs);
    if (conn == NULL) {
	return NULL;
    }
    CMconn_register_close_handler(conn, EChannel_close_handler, ec);

    ECtrace_out("getting condition and setting local chan_id client data",
		3);
    cond = CMCondition_get(ec->cm, conn);
    CMCondition_set_client_data(ec->cm, cond, &chan_id);

    /* give the filter to the arbitrator for him to propogate */
    /* returns through regular attend response like an open */
    ECtrace_out("creating nd_msg and writing to connection", 3);
    memset(&derive_msg, 0, sizeof(derive_msg));
    derive_msg.chan_str = strchr(chan_id, '@') + 1;
    derive_msg.cond = cond;
    set_channel_id(&derive_msg.client_channel_id, chan);
    derive_msg.client_contact_str = chan->local_contact_string;
    derive_msg.filter = event_filter;

    /* send output field and format lists */
    derive_msg.field_list = output_field_list;
    derive_msg.field_list_len = 0;
    while (output_field_list &&
	   (output_field_list[derive_msg.field_list_len].field_name !=
	    NULL)) {
	derive_msg.field_list_len++;
    }
    if (output_field_list)
	derive_msg.field_list_len++;	/* send terminator as well */
    i = 0;
    while (output_format_list && output_format_list[i].format_name != NULL)
	i++;
    if (output_format_list) {
	derive_msg.format_list = CMmalloc(i * sizeof(format_list_element));
	derive_msg.format_list_len = i;	/* +1 because we send terminator */
    } else {
	derive_msg.format_list = NULL;
	derive_msg.format_list_len = 0;
    }
    for (i = 0; i < derive_msg.format_list_len; i++) {
	int j = 0;
	derive_msg.format_list[i].format_name =
	    output_format_list[i].format_name;
	derive_msg.format_list[i].field_list =
	    output_format_list[i].field_list;
	while (output_format_list[i].field_list[j].field_name != NULL) {
	    j++;
	}
	j++;			/* send terminator as well */
	derive_msg.format_list[i].field_list_len = j;
    }
    ECtrace_out("Encoding remote derive message\n", 3);

    encode_data_block(filter_data, &derive_msg.init_data_block,
		      &derive_msg.init_data_len, NULL, chan->ec);
    if (!CMwrite
	(conn, CMlookup_format(ec->cm, channel_remote_derive_msg_flds),
	 &derive_msg)) {
	ECtrace_out("channel remote derive msg write failed", 0);
    }

    CMfree(derive_msg.format_list);
    if (derive_msg.init_data_block != NULL) {
	CMfree(derive_msg.init_data_block);
    }
    ECtrace_out
	("Sending remote_derive msg, channel %s cond %d filter %d\n", 3,
	 derive_msg.chan_str, derive_msg.cond, derive_msg.filter);
    if (CMCondition_wait(ec->cm, cond)) {
	ECtrace_out("ECchannel_remote_derive", 0);
	return chan_id;
    } else {
	/* wait failed, dep is dead */
	EChannel_destroy(chan);
	ECtrace_out("ECchannel_remote_derive", 0);
	return NULL;
    }
#else
    printf("No derived event channels in this version of DataExchange\n");
    return NULL;
#endif
}

extern char *
EChannel_remote_transform(cc, remote_contact_string, src_chan_id,
			  event_filter, output_field_list,
			  output_format_list)
EControlContext cc;
char *remote_contact_string;
char *src_chan_id;
char *event_filter;
FMFieldList output_field_list;
CMFormatList output_format_list;
{
    return EChannel_remote_typed_data(cc, remote_contact_string,
				      src_chan_id, event_filter,
				      output_field_list,
				      output_format_list,
				      (ECdata_spec) NULL);
}

extern char *
EChannel_remote_filter(cc, remote_contact_list, src_chan_id, event_filter)
EControlContext cc;
char *remote_contact_list;
char *src_chan_id;
char *event_filter;
{
    return EChannel_remote_typed_data(cc, remote_contact_list, src_chan_id,
				      event_filter, (FMFieldList) NULL,
				      (CMFormatList) NULL,
				      (ECdata_spec) NULL);
}
#endif
