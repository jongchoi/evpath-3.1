#ifndef DOC
#include "config.h"
#include "atl.h"
#include "evpath.h"
#include "echo2.h"
#include "gen_thread.h"

#include <stdio.h>
#include <stdlib.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#endif

#ifndef DOC
thr_thread_t pid_array[2];
int count_array[2] = {0,0};
int quiet = 1;
#define printf if (quiet <= 0) printf
#endif

static void handler(void *event, int length, void *client_data, attr_list junk)
{
    printf("In the %s handler, thread (%lx), event is %ld\n", 
	   (char*)client_data, 
	   (long)thr_thread_self(), *(long *) event);
#ifndef DOC
    if (thr_thread_self() == pid_array[0]) {
	count_array[0]++;
    } else if (thr_thread_self() == pid_array[1]) {
	count_array[1]++;
    } else {
	printf("thr_thread_self is %lx, pids are %lx and %lx\n",
	       (long)thr_thread_self(), (long)pid_array[0], (long)pid_array[1]);
	exit(1);
    }
#endif
}

static void thread1_func(EControlContext ec)
{
    EControl_run(ec);
}	

static void thread2_func(EControlContext ec)
{
    while(1) {	/* handle events every 10 sec */
	sleep(10);
	printf("Handling events in thread2\n");
	EControl_poll(ec);
    }
}	

int
main(int argc, char **argv)
{
    CManager cm;
    EControlContext ec, ec1, ec2;
    ECSourceHandle handle;
    EChannel chan;
    long a = time(NULL) % 100, i;

#ifndef DOC
    while (argv[1] && (argv[1][0] == '-')) {
	if (argv[1][1] == 'q') {
	    quiet++;
	} else if (argv[1][1] == 'v') {
	    quiet--;
	}
	argv++;
    }
#endif
    gen_pthread_init();
    cm = CManager_create();
    ec = ECho_CM_init(cm);
    chan = EChannel_create(ec);
    handle = ECsource_subscribe(chan);

    ec1 = EContext_create();

    ECsink_subscribe_context(chan, handler, "first", ec1);
#ifndef DOC
    pid_array[0] = 
#endif
    thr_fork((void_arg_func)thread1_func, (void*)ec1);

    ec2 = EContext_create();
    ECsink_subscribe_context(chan, handler, "second", ec2);
#ifndef DOC
    pid_array[1] = 
#endif
    thr_fork((void_arg_func)thread2_func, (void*)ec2);

    for (i = 0; i < 20; i++) {
	long *event = malloc(sizeof(long));
	*event = a++;
	printf("I'm submitting %ld\n", *event);
	ECsubmit_general_event(handle, event, sizeof(*event), free, NULL);
	CMsleep(cm, 1);
    }
#ifndef DOC
    CMsleep(cm, 20); /* give them all time to handle */
    if ((count_array[0] != count_array[1]) || (count_array[0] != 20)) {
	printf("Counts not as expected, %d and %d\n", count_array[0], count_array[1]);
	exit(1);
    }
    return 0;
#endif
}
