

#include "config.h"
#ifdef HAVE_WINDOWS_H
#include <windows.h>
#else
#include <sys/time.h>
#endif
#include "atl.h"
#include "evpath.h"
#include "echo2.h"
#include "gen_thread.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <sys/types.h>
#include <signal.h>
#ifdef HAVE_WINDOWS_H
#include <windows.h>
#include <process.h>
#define srand48(s) srand(s)
#define drand48() (((double)rand())/((double)RAND_MAX))
#define lrand48() rand()
#define kill(x,y) 
#else
extern double drand48();
extern long lrand48();
void srand48 ARGS((long seedval));
#include <sys/wait.h>
#endif

static int quiet = 0;

static int do_regression_master_test();

typedef struct _simple_rec {
    int integer_field;
    short short_field;
    long long_field;
    char *string;
    double double_field;
    char char_field;
} simple_rec, *simple_rec_ptr;

static FMField simple_field_list[] =
{
    {"integer_field", "integer",
     sizeof(int), FMOffset(simple_rec_ptr, integer_field)},
    {"short_field", "integer",
     sizeof(short), FMOffset(simple_rec_ptr, short_field)},
    {"long_field", "integer",
     sizeof(long), FMOffset(simple_rec_ptr, long_field)},
    {"string_field", "string",
     sizeof(char *), FMOffset(simple_rec_ptr, string)},
    {"double_field", "float",
     sizeof(double), FMOffset(simple_rec_ptr, double_field)},
    {"char_field", "char",
     sizeof(char), FMOffset(simple_rec_ptr, char_field)},
    {NULL, NULL, 0, 0}
};

#define REPEAT_COUNT 10

static
void
multi_handler(event_queues, client_data)
ECEventQueueList event_queues;
void *client_data;
{
    int i, queue_count = ECQCount(event_queues);
    long sum = 0, scan_sum = 0;
    simple_rec_ptr event;

    printf("Event queue count = %d\n", queue_count);
    for (i=0; i<queue_count; i++) {
	printf("    Queue %d contains %d events\n", i,
	       ECQEventCount(event_queues, i));
    }
    event = ECEvent_get_ptr(ECQget_event_rec(event_queues, 0/* first queue */,
					     0 /*first event */));
    sum += event->integer_field % 100;
    sum += event->short_field % 100;
    sum += event->long_field % 100;
    sum += ((int)(event->double_field * 100.0)) % 100;
    sum += event->char_field;
    sum = sum % 100;
    if (event->string) 
	sscanf(event->string, "testing, sum is %ld", &scan_sum);
    if (sum != scan_sum) {
	printf("Received record checksum does not match, expected %ld, got %ld.\n",
	       scan_sum, sum);
    }
    if ((quiet == 0) || (sum != scan_sum)) {
	printf("In %d the handler, event data is :\n", quiet);
	printf("	integer_field = %d\n", event->integer_field);
	printf("	short_field = %d\n", event->short_field);
	printf("	long_field = %ld\n", event->long_field);
	printf("	string = %s\n", event->string ? event->string : "NULL");
	printf("	double_field = %g\n", event->double_field);
	printf("	char_field = %c\n", event->char_field);
    }
    sum = 0;
    if (client_data != NULL) {
	int tmp = *((int*)client_data);
	if (tmp > 0) {
	    *((int*)client_data) = tmp - 1;
	}
    }
}

static char tmp_string[128];

static
void generate_record(event)
simple_rec_ptr event;
{
    long sum = 0;
    event->integer_field = (int) lrand48() % 100;
    sum += event->integer_field;
    event->short_field = (short) lrand48();
    sum += event->short_field % 100;
    event->long_field = (long) lrand48();
    sum += event->long_field % 100;
    event->double_field = drand48();
    sum += ((int)(event->double_field * 100.0)) % 100;
    event->char_field = lrand48() % 128;
    sum += event->char_field;
    sum = sum % 100;
    sprintf(tmp_string, "testing, sum is %ld\n", sum);
    event->string = strdup(tmp_string);
}

static
void free_simple_rec(event)
simple_rec_ptr event;
{
    free(event->string);
    free(event);
}

static int regression = 0;

static void
do_source_side(cm, chans, done_flag, count)
CManager cm;
EChannel *chans;
int *done_flag;
int count;
{
    ECSourceHandle *handles;
    int ret = 0, chan_count = 0, i;

    while (chans[chan_count] != NULL) {
	if (quiet == 0) printf("chan[%d] is %lx\n", chan_count, 
			       (long)chans[chan_count]);
	chan_count++;
    }
    handles = malloc(sizeof(ECSourceHandle));
    for (i=0; i< chan_count; i++) {
	handles[i] = ECsource_typed_subscribe(chans[i], simple_field_list, 
					      NULL);
    }
    while (!ret) {
	simple_rec_ptr a = malloc(sizeof(simple_rec));
	int chan = lrand48() % chan_count;
	generate_record(a);
	if (quiet == 0)
	    printf("I'm submitting %ld to channel %d\n", a->long_field,
		   chan);
	
	ECsubmit_general_typed_event(handles[chan], a, 
				     (EventFreeFunction)free_simple_rec,
				     NULL);
	if (count != -1) {
	    count--;
	}
	CMusleep(cm, 10000);
	if (done_flag && *done_flag) ret++;
	if (count != -1) {
	    if (count == 0) ret++;
	}	    
    }
    for (i=0; i< chan_count; i++) {
	ECcancel_source_subscribe(handles[i]);
    }
}

static pid_t sink_proc = 0;

static void
fail_and_die(signal)
int signal;
{
    fprintf(stderr, "Channel test failed to complete in reasonable time\n");
    if (sink_proc != 0) {
	kill(sink_proc, 9);
    }
    exit(1);
}

int
main(argc, argv)
int argc;
char **argv;
{
    EChannel chan;
    double r = drand48();
    int consumer = 0;
    int both = 0;
    int regression_master = 1;
    CManager cm;
    EControlContext cc;

    srand48(0);
    regression = 1;
    quiet = 1;
    while (argv[1] && (argv[1][0] == '-')) {
	if (argv[1][1] == 'c') {
	    r = .9;
	    regression_master = 0;
	} else if (argv[1][1] == 's') {
	    r = 0;
	    regression_master = 0;
	} else if (argv[1][1] == 'b') {
	    both++;
	    regression_master = 0;
	} else if (argv[1][1] == 'q') {
	    quiet++;
	} else if (argv[1][1] == 'v') {
	    quiet--;
	} else if (argv[1][1] == 'n') {
	    regression = 0;
	    quiet = 0;
	}
	argv++;
    }
    if (regression && regression_master) {
	return do_regression_master_test();
    }
    cm = CManager_create();
    cc = ECho_CM_init(cm);
    consumer = (r > 0.5);
    if (quiet <= 0) printf("r is %g\n", r);
    chan = EChannel_open(cc, argv[1]);
    if (chan == NULL) {
	printf("Failed to contact channel %s\n", argv[1]);
	exit(1);
    }
    if (both) {
	EChannel chan2;
	ECMultiSubscribeList sub_list[2];
	EChannel chans[3];

	chan = EChannel_open(cc, argv[1]);
	chan2 = EChannel_open(cc, argv[1]);
	sub_list[0].channel = chan;
	sub_list[0].field_list = simple_field_list;
	sub_list[0].subformat_list = NULL;
	sub_list[1].channel = NULL;
	sub_list[1].field_list = NULL;
	sub_list[1].subformat_list = NULL;

	(void) ECsink_multi_subscribe(sub_list, cc, multi_handler, NULL);
	if (quiet <= 0) printf("chan is %lx, chan2 is %lx\n", (long)chan, (long)chan2);
	/* do source side forever */
	chans[0] = chan;
	chans[1] = chan2;
	chans[2] = NULL;
	do_source_side(cm, chans, NULL, -1);
    } else if (consumer) {
	int repeat_count = REPEAT_COUNT;
	ECMultiSubscribeList sub_list[2];
	
	if (regression) {
#ifdef HAVE_WINDOWS_H
	  SetTimer(NULL, 5, 1000, (TIMERPROC) fail_and_die);
#else
	  struct sigaction sigact;
	  sigact.sa_flags = 0;
	  sigact.sa_handler = fail_and_die;
	  sigemptyset(&sigact.sa_mask);
	  sigaddset(&sigact.sa_mask, SIGALRM);
	  sigaction(SIGALRM, &sigact, NULL);
	  alarm(300);
#endif
	}

	sub_list[0].channel = chan;
	sub_list[0].field_list = simple_field_list;
	sub_list[0].subformat_list = NULL;
	sub_list[1].channel = NULL;
	sub_list[1].field_list = NULL;
	sub_list[1].subformat_list = NULL;

	if (quiet <= 0) printf("I'm a consumer\n");
	(void) ECsink_multi_subscribe(sub_list, cc, multi_handler,
				      regression ? &repeat_count : NULL);
	while (repeat_count != 0) {
	    CMsleep(cm, 1);
	}
    } else {
	EChannel chans[3];
	if (quiet <= 0) printf("I'm a supplier\n");
	chans[0] = chan;
	chans[1] = NULL;
	do_source_side(cm, chans, NULL, -1);
    }
    return 0;
}


static
pid_t
run_subprocess(args)
char **args;
{
#ifdef HAVE_WINDOWS_H
    int child;
    child = _spawnv(_P_NOWAIT, "./typed_test.exe", args);
    if (child == -1) {
	perror("spawnv");
    }
    return child;
#else
    pid_t child = fork();
    if (child == 0) {
	/* I'm the child */
	execv("./typed_test", args);
    }
    return child;
#endif
}

static int
do_regression_master_test()
{
    CManager cm;
    EControlContext cc;
    EChannel chan;

#ifdef HAVE_WINDOWS_H
    SetTimer(NULL, 5, 1000, (TIMERPROC) fail_and_die);
#else
    struct sigaction sigact;
    sigact.sa_flags = 0;
    sigact.sa_handler = fail_and_die;
    sigemptyset(&sigact.sa_mask);
    sigaddset(&sigact.sa_mask, SIGALRM);
    sigaction(SIGALRM, &sigact, NULL);
    alarm(300);
#endif

#ifdef USE_PTHREADS
    gen_pthread_init();
#endif

    cm = CManager_create();
    (void) CMfork_comm_thread(cm);
    cc = ECho_CM_init(cm);

    srand48(1);

    chan = EChannel_typed_create(cc, simple_field_list, NULL);
    
    if (quiet <= 0) printf("  channel ID is %s\n", ECglobal_id(chan));

    /* local tests */
    {
	int repeat_count = REPEAT_COUNT;
	int repeat_count1 = REPEAT_COUNT / 2;
	int repeat_count2 = REPEAT_COUNT - repeat_count1;
	ECSinkHandle handle;
	ECMultiSubscribeList sub_list[2];
	EChannel chans[3];
	
	sub_list[0].channel = chan;
	sub_list[0].field_list = simple_field_list;
	sub_list[0].subformat_list = NULL;
	sub_list[1].channel = NULL;
	sub_list[1].field_list = NULL;
	sub_list[1].subformat_list = NULL;
	handle = ECsink_multi_subscribe(sub_list, cc, multi_handler,
					&repeat_count);
	chans[0] = chan;
	chans[1] = NULL;
	do_source_side(cm, chans, NULL, repeat_count1);
	do_source_side(cm, chans, NULL, repeat_count2);
	if (repeat_count != 0) {
	    fprintf(stderr, "Not all in-process events delivered, test 1\n");
	} else {
	    if (quiet < 2) 
		fprintf(stderr, "Passed in-process delivery tests\n");
	}	
	ECcancel_sink_subscribe(handle);
    }

    /* multiple process tests */
    {
	int repeat_count1 = REPEAT_COUNT / 2;
	int repeat_count2 = REPEAT_COUNT - repeat_count1;
	char *args[] = {"typed_test", "-c", NULL, NULL};
	int exit_state;
	EChannel chans[3];

	args[sizeof(args)/sizeof(char*) -2] = ECglobal_id(chan);
	sink_proc = run_subprocess(args);

	/* give him time to start */
	CMsleep(cm, 30);

	chans[0] = chan;
	chans[1] = NULL;
	do_source_side(cm, chans, NULL, repeat_count1);
	do_source_side(cm, chans, NULL, repeat_count2);
#ifdef HAVE_WINDOWS_H
	if (_cwait(&exit_state, sink_proc, 0) == -1) {
	    perror("cwait");
	}
	if (exit_state == 0) {
	    if (quiet < 2) 
		printf("Passed single remote sink test\n");
	} else {
	    printf("Single remote sink exit with status %d\n",
		   exit_state);
	}
#else
	if (waitpid(sink_proc, &exit_state, 0) == -1) {
	    perror("waitpid");
	}
	if (WIFEXITED(exit_state)) {
	    if (WEXITSTATUS(exit_state) == 0) {
		if (quiet < 2) 
		    printf("Passed single remote sink test\n");
	    } else {
		printf("Single remote sink exit with status %d\n",
		       WEXITSTATUS(exit_state));
	    }
	} else if (WIFSIGNALED(exit_state)) {
	    printf("Single remote sink died with signal %d\n",
		   WTERMSIG(exit_state));
	}
#endif
    }
    EChannel_destroy(chan);
/*    DExchange_close(de);
      DExchange_free(de);*/
    return 0;
}
