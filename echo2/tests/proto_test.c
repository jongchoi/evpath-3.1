

#include "config.h"
#ifdef HAVE_WINDOWS_H
#include <windows.h>
#else
#include <sys/time.h>
#endif
#include "atl.h"
#ifndef NO_DERIVED
#include "cod.h"
#endif
#include "evpath.h"
#include "echo2.h"
#include "gen_thread.h"

#include <stdio.h>
#include <stdlib.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <sys/types.h>
#include <signal.h>
#ifdef HAVE_WINDOWS_H
#include <windows.h>
#include <process.h>
#define srand48(s) srand(s)
double drand48() { return (((double)rand())/((double)RAND_MAX)); }
long lrand48(){ return rand(); }
#define kill(x,y) 
#else
extern double drand48();
extern long lrand48();
void srand48 ARGS((long seedval));
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <fcntl.h>
#endif

#ifndef NO_DERIVED
static int quiet = 0;
#include "ecl.h"


static int do_regression_master_test();

typedef struct _complex_rec {
    double r;
    double i;
} complex, *complex_ptr;

static FMField complex_field_list[] =
{
    {"r", "double", sizeof(double), FMOffset(complex_ptr, r)},
    {"i", "double", sizeof(double), FMOffset(complex_ptr, i)},
    {NULL, NULL, 0, 0}
};

typedef struct _simple_rec {
    int integer_field;
    short short_field;
    long long_field;
    complex complex_field;
    double double_field;
    char char_field;
    int scan_sum;
} simple_rec, *simple_rec_ptr;

static FMField simple_field_list[] =
{
    {"integer_field", "integer",
     sizeof(int), FMOffset(simple_rec_ptr, integer_field)},
    {"short_field", "integer",
     sizeof(short), FMOffset(simple_rec_ptr, short_field)},
    {"long_field", "integer",
     sizeof(long), FMOffset(simple_rec_ptr, long_field)},
    {"complex_field", "complex",
     sizeof(complex), FMOffset(simple_rec_ptr, complex_field)},
    {"double_field", "float",
     sizeof(double), FMOffset(simple_rec_ptr, double_field)},
    {"char_field", "char",
     sizeof(char), FMOffset(simple_rec_ptr, char_field)},
    {"scan_sum", "integer",
     sizeof(int), FMOffset(simple_rec_ptr, scan_sum)},
    {NULL, NULL, 0, 0}
};

static CMFormatList simple_format_list[] =
{
    {"complex", complex_field_list},
    {NULL, NULL},
};

#define REPEAT_COUNT 10

static int global_cond;
static CManager global_cm = NULL;

static
void
typed_handler(vevent, client_data, attrs)
void *vevent;
void *client_data;
attr_list attrs;
{
    simple_rec_ptr event = vevent;
    long sum = 0, scan_sum = 0;
    sum += event->integer_field % 100;
    sum += event->short_field % 100;
    sum += event->long_field % 100;
    sum += ((int)(event->complex_field.r * 100.0)) % 100;
    sum += ((int)(event->complex_field.i * 100.0)) % 100;
    sum += ((int)(event->double_field * 100.0)) % 100;
    sum += event->char_field;
    sum = sum % 100;
    scan_sum = event->scan_sum;
    if (sum != scan_sum) {
	printf("Received record checksum does not match. expected %d, got %d\n",
	       (int)sum, (int)scan_sum);
    }
    if (!quiet || (sum != scan_sum)) {
	printf("In the handler, event data is :\n");
	printf("	integer_field = %d\n", event->integer_field);
	printf("	short_field = %d\n", event->short_field);
	printf("	long_field = %ld\n", event->long_field);
	printf("	complex_field.r = %g\n", event->complex_field.r);
	printf("	complex_field.i = %g\n", event->complex_field.i);
	printf("	double_field = %g\n", event->double_field);
	printf("	char_field = %c\n", event->char_field);
	printf("	scan_sum = %d\n", event->scan_sum);
    }
    if (client_data != NULL) {
	int tmp = *((int*)client_data);
	if (tmp > 0) {
	    *((int*)client_data) = tmp - 1;
	}
	if (*((int*)client_data) == 0) {
	    CMCondition_signal(global_cm, global_cond);
	}
    }
}

static int regression = 0;

static void
do_source_side(cm, chan, done_flag, count)
CManager cm;
EChannel chan;
int *done_flag;
int count;
{
#ifndef USE_PTHREADS
#endif
}

char *filter = "{\
    long sum = 0;\
    output.integer_field = lrand48() % 100;\n\
    sum = sum + output.integer_field % 100;\n\
    output.short_field = (lrand48());\n\
    sum = sum + output.short_field % 100;\n\
    output.long_field = (lrand48());\n\
    sum = sum + output.long_field % 100;\n\
\
    output.complex_field.r = drand48();\n\
    sum = sum + ((int)(output.complex_field.r * 100.0)) % 100;\n\
    output.complex_field.i = drand48();\n\
    sum = sum + ((int)(output.complex_field.i * 100.0)) % 100;\n\
\
    output.double_field = drand48();\n\
    sum = sum +((int)(output.double_field * 100.0)) % 100;\n\
    output.char_field = lrand48() % 128;\n\
    sum = sum + output.char_field;\n\
    sum = sum % 100;\n\
    output.scan_sum = sum;\n\
    return 1;\n\
}\0\0";

static int local_function();
static ecl_extern_entry externs[] = 
{
    {"printf", (void*)(long)printf},
    {"local_function", (void*)(long)local_function},
    {"lrand48", (void*)(long)lrand48},
    {"drand48", (void*)(long)drand48},
    {NULL, NULL}
};

static char extern_string[] = "int printf(string format, ...);\n\
int local_function();\n\
long lrand48();\n\
double drand48();";

static void fail_and_die ARGS((int signal));

int
main(argc, argv)
int argc;
char **argv;
{
    EChannel chan;
    double r = drand48();
    int consumer = 0;
    int both = 0;
    int regression_master = 1;
    CManager cm;
    EControlContext cc;

    srand48(1);
    regression = 1;
    quiet = 1;
    while (argv[1] && (argv[1][0] == '-')) {
	if (argv[1][1] == 'c') {
	    r = .9;
	    regression_master = 0;
	} else if (argv[1][1] == 's') {
	    r = 0;
	    regression_master = 0;
	} else if (argv[1][1] == 'b') {
	    both++;
	    regression_master = 0;
	} else if (argv[1][1] == 'q') {
	    quiet++;
	} else if (argv[1][1] == 'v') {
	    quiet--;
	} else if (argv[1][1] == 'n') {
	    regression = 0;
	    quiet = 0;
	}
	argv++;
    }
    if (regression && regression_master) {
	return do_regression_master_test();
    }
    global_cm = cm = CManager_create();
    cc = ECho_CM_init(cm);
    consumer = (r > 0.5);
    if (!quiet) printf("r is %g\n", r);
    if (both) {
	EChannel chan2;
	chan = EChannel_open(cc, argv[1]);
	chan2 = EChannel_open(cc, argv[1]);
	(void) ECsink_typed_subscribe(chan, simple_field_list, 
				      simple_format_list,
				      typed_handler, NULL);
	if (!quiet) printf("chan is %lx, chan2 is %lx\n", (long)chan, (long)chan2);
	/* do source side forever */
	do_source_side(cm, chan, NULL, -1);
    } else if (consumer) {
	int repeat_count = REPEAT_COUNT;
	ECSinkHandle handle;
	EChannel new_chan;
	if (regression) {
#ifdef HAVE_WINDOWS_H
	    SetTimer(NULL, 5, 1000, (TIMERPROC) fail_and_die);
#else
	    struct sigaction sigact;
	    sigact.sa_flags = 0;
	    sigact.sa_handler = fail_and_die;
	    sigemptyset(&sigact.sa_mask);
	    sigaddset(&sigact.sa_mask, SIGALRM);
	    sigaction(SIGALRM, &sigact, NULL);
	    alarm(300);
#endif
	}
	if (!quiet) printf("I'm a consumer\n");
	if (!quiet) printf("filter is:\n%s", filter);
	new_chan = ECproto_derive_periodic(cc, argv[1], filter, 
					   simple_field_list, 
					   simple_format_list,
					   1000000);
	global_cond = CMCondition_get(cm, NULL);

	handle = ECsink_typed_subscribe(new_chan, simple_field_list,
					simple_format_list,
					typed_handler, 
					regression ? &repeat_count : NULL);
	CMCondition_wait(cm, global_cond);

	ECcancel_sink_subscribe(handle);
	EChannel_destroy(new_chan);
/*	DExchange_close(de);
	DExchange_free(de);*/
    } else {
	ECproto proto;
	ecl_parse_context proto_context;

	if (!quiet) printf("I'm the master \n");
	proto_context = new_ecl_parse_context();
	ecl_assoc_externs(proto_context, externs);
	ecl_parse_for_context(extern_string, proto_context);

	proto = ECproto_create(cc, proto_context);
    
	if (!quiet) printf("  proto ID is %s\n", ECproto_id(proto));

	CMsleep(cm, 1200);
    }
    return 0;
}

static
pid_t
run_subprocess(args)
char **args;
{
#ifdef HAVE_WINDOWS_H
    int child;
    child = _spawnv(_P_NOWAIT, "./proto_test.exe", args);
    if (child == -1) {
	perror("spawnv");
    }
    return child;
#else
    pid_t child = fork();
    if (child == 0) {
	/* I'm the child */
	if (execv(args[0], args) == -1) {
	    perror("execv");
	}
    }
    return child;
#endif
}

static pid_t sink_proc = 0;

static void
fail_and_die(signal)
int signal;
{
    fprintf(stderr, "Channel test failed to complete in reasonable time\n");
    if (sink_proc != 0) {
	kill(sink_proc, 9);
    }
    exit(1);
}

#ifndef PRINTF_DEFINED
extern int printf();
#endif

static int 
local_function()
{
    printf("In local function\n");
    return 1;
}

int forked = 0;

static int
do_regression_master_test()
{
#ifndef NO_DERIVED
    CManager cm;
    EControlContext cc;
    ECproto proto;
    EChannel new_chan;
    ecl_parse_context proto_context;

#ifdef HAVE_WINDOWS_H
    SetTimer(NULL, 5, 1000, (TIMERPROC) fail_and_die);
#else
    struct sigaction sigact;
    sigact.sa_flags = 0;
    sigact.sa_handler = fail_and_die;
    sigemptyset(&sigact.sa_mask);
    sigaddset(&sigact.sa_mask, SIGALRM);
    sigaction(SIGALRM, &sigact, NULL);
    alarm(300);
#endif

#ifdef USE_PTHREADS
    gen_pthread_init();
#endif

    global_cm = cm = CManager_create();
    forked = CMfork_comm_thread(cm);
    cc = ECho_CM_init(cm);

    if (quiet <= 0) {
	if (forked) {
	    printf("Forked a communication thread\n");
	} else {
	    printf("Doing non-threaded communication handling\n");
	}
    }
    srand48(1);
    proto_context = new_ecl_parse_context();
    ecl_assoc_externs(proto_context, externs);
    ecl_parse_for_context(extern_string, proto_context);

    proto = ECproto_create(cc, proto_context);
    
    if (!quiet) printf("  proto ID is %s\n", ECproto_id(proto));

    {
	int repeat_count = REPEAT_COUNT;
	ECSinkHandle handle;
	/* local tests */

	if (!quiet) printf("filter is:\n%s", filter);
	new_chan = ECproto_derive_periodic(cc, ECproto_id(proto), filter, 
				   simple_field_list, simple_format_list, 1000000);
	global_cond = CMCondition_get(cm, NULL);

	handle = ECsink_typed_subscribe(new_chan, simple_field_list,
					simple_format_list, typed_handler,
					&repeat_count);
	CMCondition_wait(cm, global_cond);

	if (repeat_count != 0) {
	    fprintf(stderr, "Not all in-process events delivered, test 1\n");
	} else {
	    if (quiet <= 0) 
		fprintf(stderr, "Passed in-process protochannel tests\n");
	}	
	ECcancel_sink_subscribe(handle);
	EChannel_destroy(new_chan);
    }

    /* multiple process tests */
    {
	char *args[] = {"./proto_test", "-r", "-c", NULL, NULL, NULL};
	int wait_status;
	int exit_state;

	if (quiet > 0) {
	    args[sizeof(args)/sizeof(char*) -3] = ECproto_id(proto);
	} else {
	    args[sizeof(args)/sizeof(char*) -3] = "-v";
	    args[sizeof(args)/sizeof(char*) -2] = ECproto_id(proto);
	}
	sink_proc = run_subprocess(args);

#ifndef HAVE_WINDOWS_H
	while ((wait_status = waitpid(sink_proc, &exit_state, WNOHANG)) == 0) {
	    CMsleep(cm, 1);
	}
	if (wait_status == -1) {
	    perror("waitpid");
	}
	if (WIFEXITED(exit_state)) {
	    if (WEXITSTATUS(exit_state) == 0) {
		if (quiet < 0) 
		    printf("Passed single remote sink test\n");
	    } else {
		printf("Single remote sink exit with status %d\n",
		       WEXITSTATUS(exit_state));
		return 1;
	    }
	} else if (WIFSIGNALED(exit_state)) {
	    printf("Single remote sink died with signal %d\n",
		   WTERMSIG(exit_state));
	    return 1;
	}

#else /* windows.h */
	exit_state = STILL_ACTIVE;
	while (exit_state == STILL_ACTIVE) {
	    CMsleep(cm, 5);
	    CMsleep(cm, 1);
	    GetExitCodeProcess((HANDLE)sink_proc,&exit_state);
	}
	if (exit_state == 0) {
	    if (quiet < 2) 
		printf("Passed out of process protochannel derivation test\n");
	} else {
	    printf("Single remote sink exit with status %d\n",
		   exit_state);
	}
#endif
    }
    CManager_close(cm);
#endif
    return 0;
}
#else
int
main()
{
	return 0;
}
#endif
