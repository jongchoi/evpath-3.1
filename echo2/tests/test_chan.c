/******************************************************************************
 * test_chan.c: Test driver for channel
 * 
 * Notes: It assumes that is the first one if no arguments are provided 
 *
 */

#include "config.h"
#include "DE.h"

#include <stdio.h>
#include <stdlib.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef HAVE_WINDOWS_H
#include <windows.h>
#define srand48(s) srand(s)
#define drand48() (((double)rand())/((double)RAND_MAX))
#define lrand48() rand()
#endif

enum roles {
    PUSH_SOURCE, PULL_SOURCE, PUSH_SINK, PULL_SINK, PUSH_PUSH
};

static int quiet = 0;

static
void
sourceHandler(handle, client_data)
ECSourceHandle handle;
void *client_data;
{
    long a = lrand48();

    if (!quiet)
	printf("In the source request handler\n");

    printf("I'm submitting inside the handler %ld\n", a);
    ECsubmit_event(handle, &a, sizeof(a));
}


static
void
handler(event, length, client_data)
void *event;
int length;
void *client_data;
{
    if (!quiet)
	printf("In the handler, event is %ld\n", *(long *) event);
}


int
main(argc, argv)
int argc;
char **argv;
{
    DExchange de = DExchange_create();
    EChannel chan, chan2;
    ECSinkHandle sinkHandle;
    ECSourceHandle sourceHandle;
    double r;
    long a;
    enum roles ourRole = PUSH_SOURCE;

    DExchange_listen(de, 0);

    /* First one: just create the channel and publish it */

    if (argc <= 1) {
	chan = EChannel_create(de);
	printf("  channel ID is %s\n", ECglobal_id(chan));

	while (1)
	    DExchange_poll_and_handle_timeout(de, 0, 100000);
    }
    /* Not the first one - if we are not told what to be, pick a role at
     * random */

    /* 
     *
     * [0.00, 0.25] - PUSH_SOURCE
     * (0.25, 0.50] - PULL_SOURCE
     * (0.50, 0.75] - PUSH_SINK
     * (0.75, 1.00] - PULL_SINK 
     *  
     * PUSH_PUSH is only by request
     *
     */

    srand48(getpid());
    r = drand48();

    while (argv[1][0] == '-') {
	if (argv[1][1] == 'S') {	/* PUSH_SOURCE */
	    r = 0;
	} else if (argv[1][1] == 's') {		/* PULL_SOURCE */
	    r = .3;
	} else if (argv[1][1] == 'C') {		/* PUSH_SINK */
	    r = .6;
	} else if (argv[1][1] == 'c') {		/* PULL_SINK */
	    r = .8;
	} else if (argv[1][1] == 'b') {		/* PUSH_SINK and
						 * PUSH_SOURCE */
	    ourRole = PUSH_PUSH;
	} else if (argv[1][1] == 'q') {
	    quiet++;
	}
	argv++;
    }

    if (ourRole != PUSH_PUSH) {
	printf("r is %g\n", r);

	if (r > 0.75)
	    ourRole = PULL_SINK;
	else if (r > 0.5)
	    ourRole = PUSH_SINK;
	else if (r > 0.25)
	    ourRole = PULL_SOURCE;
	else
	    ourRole = PUSH_SOURCE;
    }
    chan = EChannel_open(de, argv[1]);
    if (chan == NULL) {
	printf("Failed to contact channel %s\n", argv[1]);
	exit(1);
    }
    switch (ourRole) {

    case PULL_SINK:
	sinkHandle = ECsink_subscribe(chan, handler, NULL);
	printf("I'm a pull sink \n");
	while (1) {
	    if (drand48() < 0.3) {
		a = lrand48();
		printf("I'm requesting \n");
		ECrequest_event(sinkHandle, 0);
	    }
	    DExchange_poll_and_handle_timeout(de, 0, 1000000);
	}
	/* NOTREACHED */
	break;

    case PUSH_SINK:
	sinkHandle = ECsink_subscribe(chan, handler, NULL);
	printf("I'm a push sink\n");
	while (1)
	    DExchange_poll_and_handle(de, 1);
	/* NOTREACHED */
	break;

    case PULL_SOURCE:
	sourceHandle = ECPsource_subscribe(chan, sourceHandler, NULL);
	printf("I'm a pull source\n");
	while (1)
	    DExchange_poll_and_handle_timeout(de, 0, 1000000);
	/* NOTREACHED */
	break;

    case PUSH_SOURCE:
	sourceHandle = ECsource_subscribe(chan);
	printf("I'm a push source\n");

	while (1) {
	    if (drand48() < 0.1) {
		a = lrand48();
		printf("I'm submitting %ld\n", a);
		ECsubmit_event(sourceHandle, &a, sizeof(a));
	    }
	    DExchange_poll_and_handle_timeout(de, 0, 1000000);
	}
	/* NOTREACHED */
	break;

    case PUSH_PUSH:
	printf("I've an split personality - push-source/push-sink\n");
	chan2 = EChannel_open(de, argv[1]);
	sourceHandle = ECPsource_subscribe(chan2, sourceHandler, NULL);
	printf("I'm a push source\n");

	chan = EChannel_open(de, argv[1]);
	printf("chan is %lx, chan2 is %lx\n", (long) chan, (long) chan2);
	sinkHandle = ECsink_subscribe(chan, handler, NULL);
	printf("I'm a push sink\n");

	while (1) {
	    if (drand48() < 0.1) {
		long a = lrand48();
		long *tmp = malloc(sizeof(a));
		printf("I'm submitting %ld\n", a);
		*tmp = a;
		ECsubmit_general_event(sourceHandle, tmp, sizeof(a), free);
	    }
	    DExchange_poll_and_handle_timeout(de, 0, 1000000);
	}
	/* NOTREACHED */
	break;
    }				/* end switch */

    exit(0);
}				/* end main */
