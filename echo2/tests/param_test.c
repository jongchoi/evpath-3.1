

#include "config.h"
#ifdef HAVE_WINDOWS_H
#include <windows.h>
#else
#include <sys/time.h>
#endif
#include "atl.h"
#include "evpath.h"
#include "echo2.h"
#include "gen_thread.h"

#include <stdio.h>
#include <stdlib.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <sys/types.h>
#include <signal.h>
#ifdef HAVE_WINDOWS_H
#include <windows.h>
#include <process.h>
#define srand48(s) srand(s)
#define drand48() (((double)rand())/((double)RAND_MAX))
#define lrand48() rand()
#define kill(x,y) 
#else
extern double drand48();
extern long lrand48();
void srand48 ARGS((long seedval));
#include <sys/wait.h>
#endif

static int quiet = 0;

static int do_regression_master_test();

typedef struct _complex_rec {
    double r;
    double i;
} complex, *complex_ptr;

static FMField complex_field_list[] =
{
    {"r", "double", sizeof(double), FMOffset(complex_ptr, r)},
    {"i", "double", sizeof(double), FMOffset(complex_ptr, i)},
    {NULL, NULL, 0, 0}
};

typedef struct _simple_rec {
    int integer_field;
    short short_field;
    long long_field;
    complex complex_field;
    double double_field;
    char char_field;
    int scan_sum;
} simple_rec, *simple_rec_ptr;

static FMField simple_field_list[] =
{
    {"integer_field", "integer",
     sizeof(int), FMOffset(simple_rec_ptr, integer_field)},
    {"short_field", "integer",
     sizeof(short), FMOffset(simple_rec_ptr, short_field)},
    {"long_field", "integer",
     sizeof(long), FMOffset(simple_rec_ptr, long_field)},
    {"complex_field", "complex",
     sizeof(complex), FMOffset(simple_rec_ptr, complex_field)},
    {"double_field", "float",
     sizeof(double), FMOffset(simple_rec_ptr, double_field)},
    {"char_field", "char",
     sizeof(char), FMOffset(simple_rec_ptr, char_field)},
    {"scan_sum", "integer",
     sizeof(int), FMOffset(simple_rec_ptr, scan_sum)},
    {NULL, NULL, 0, 0}
};

static CMFormatList simple_format_list[] =
{
    {"complex", complex_field_list},
    {NULL, NULL},
};

typedef struct _data_rec {
    int quiet;
    long multiplier;
    complex complex_field;
} data_rec, *data_rec_ptr;

static FMField data_field_list[] =
{
    {"quiet", "integer",
     sizeof(int), FMOffset(data_rec_ptr, quiet)},
    {"multiplier", "integer",
     sizeof(long), FMOffset(data_rec_ptr, multiplier)},
    {"complex_field", "complex",
     sizeof(complex), FMOffset(data_rec_ptr, complex_field)},
    {NULL, NULL, 0, 0}
};

static CMFormatList data_format_list[] =
{
    {"complex", complex_field_list},
    {NULL, NULL},
};



#define REPEAT_COUNT 10
data_rec_ptr filter_data;

static
void
typed_handler(vevent, client_data, attrs)
void *vevent;
void *client_data;
attr_list attrs;
{
    simple_rec_ptr event = vevent;
    long sum = 0, scan_sum = 0;
    sum += event->integer_field % 100;
    sum += event->short_field % 100;
    sum += event->long_field % 100;
    sum += ((int)(event->complex_field.r * 100.0)) % 100;
    sum += ((int)(event->complex_field.i * 100.0)) % 100;
    sum += ((int)(event->double_field * 100.0)) % 100;
    sum += event->char_field;
    sum = sum % 100;
    if (sum < 0) sum +=100;
    scan_sum = event->scan_sum;
    if (sum != scan_sum) {
	printf("Received record checksum does not match. expected %d, got %d\n",
	       (int)sum, (int)scan_sum);
    }
    if ((quiet <= 0) || (sum != scan_sum)) {
	printf("In the handler, event data is :\n");
	printf("	integer_field = %d\n", event->integer_field);
	printf("	short_field = %d\n", event->short_field);
	printf("	long_field = %ld\n", event->long_field);
	printf("	complex_field.r = %g\n", event->complex_field.r);
	printf("	complex_field.i = %g\n", event->complex_field.i);
	printf("	double_field = %g\n", event->double_field);
	printf("	char_field = %c\n", event->char_field);
	printf("	scan_sum = %d\n", event->scan_sum);
    }
    if (client_data != NULL) {
	int tmp = *((int*)client_data);
	if (tmp > 0) {
	    *((int*)client_data) = tmp - 1;
	}
    }
}

static
void generate_record(event)
simple_rec_ptr event;
{
    long sum = 0;
    event->integer_field = (int) lrand48() % 100;
    sum += 2*event->integer_field % 100;
    event->short_field = ((short) lrand48()) / 2;
    sum += 2*event->short_field % 100;
    event->long_field = ((long) lrand48()) / 2;
    sum += 2*event->long_field % 100;

    event->complex_field.r = drand48();
    sum += ((int)(event->complex_field.r * 100.0)) % 100;
    event->complex_field.i = drand48();
    sum += ((int)(event->complex_field.i * 100.0)) % 100;

    event->double_field = drand48();
    sum += ((int)(event->double_field * 100.0)) % 100;
    event->char_field = lrand48() % 128;
    sum += event->char_field;
    sum = sum % 100;
    event->scan_sum = (int)sum;
}

static int regression = 0;

static void
do_source_side(cm, chan, done_flag, count)
CManager cm;
EChannel chan;
int *done_flag;
int count;
{
    ECSourceHandle handle;
    int ret = 0;

    if (quiet <= 0) printf("chan is %lx\n", (long)chan);
    handle = ECsource_typed_subscribe(chan, simple_field_list, 
				      simple_format_list);
    if (handle == NULL) {
	printf("subscribe failed!\n");
	exit(1);
    }
    while (!ret) {
	if (drand48() < 0.1) {
	    simple_rec a;
	    generate_record(&a);
	    if (quiet <= 0)
		printf("I'm submitting %ld\n", a.long_field);
	    ECsubmit_typed_event(handle, &a);
	    if ((a.long_field%2 == 1) && (count != -1)) {
		count--;
	    }
	}
	CMusleep(cm, 10000);
	if (done_flag && *done_flag) ret++;
	if (count != -1) {
	    if (count == 0) ret++;
	}	    
    }
    ECcancel_source_subscribe(handle);
}

static pid_t sink_proc = 0;

static void
fail_and_die(signal)
int signal;
{
    fprintf(stderr, "Channel test failed to complete in reasonable time\n");
    if (sink_proc != 0) {
	kill(sink_proc, 9);
    }
    exit(1);
}

char *filter = "{\n\
    long sum_delta;\n\
    output.integer_field = input.integer_field * 2;\n\
    output.short_field = input.short_field * 2;\n\
    output.long_field = input.long_field * 2;\n\
    output.complex_field.r = filter_data.complex_field.r;\n\
    output.complex_field.i = filter_data.complex_field.i;\n\
    output.double_field = input.double_field;\n\
    if (filter_data.quiet <= 0) {\n\
	printf(\"filter data is %g, %g\\n\", filter_data.complex_field.r,\n\
		filter_data.complex_field.i);\n\
	printf(\"event data is %g, %g\\n\", input.complex_field.r,\n\
		input.complex_field.i);\n\
    }\
    output.char_field = input.char_field;\n\
    sum_delta = 100 - (((int)(input.complex_field.i * 100.0)) % 100);\n\
    sum_delta = sum_delta + 100 - (((int)(input.complex_field.r * 100.0)) % 100);\n\
    sum_delta = sum_delta + (((int)(output.complex_field.i * 100.0)) % 100);\n\
    sum_delta = sum_delta + (((int)(output.complex_field.r * 100.0)) % 100);\n\
    output.scan_sum = input.scan_sum + sum_delta;\n\
    output.scan_sum = output.scan_sum % 100;\n\
    return input.long_field % 2;\
}\0\0";

int
main(argc, argv)
int argc;
char **argv;
{
    CManager cm;
    EControlContext cc;

    EChannel chan;
    double r = drand48();
    int consumer = 0;
    int both = 0;
    int regression_master = 1;
    srand48(getpid());
    regression = 1;
    quiet = 1;
    while (argv[1] && (argv[1][0] == '-')) {
	if (argv[1][1] == 'c') {
	    r = .9;
	    regression_master = 0;
	} else if (argv[1][1] == 's') {
	    r = 0;
	    regression_master = 0;
	} else if (argv[1][1] == 'b') {
	    both++;
	    regression_master = 0;
	} else if (argv[1][1] == 'q') {
	    quiet++;
	} else if (argv[1][1] == 'n') {
	    regression = 0;
	    quiet = 0;
	} else if (argv[1][1] == 'v') {
	    quiet--;
	}
	argv++;
    }
    if (regression && regression_master) {
	return do_regression_master_test();
    }
    cm = CManager_create();
    (void) CMfork_comm_thread(cm);
    cc = ECho_CM_init(cm);
    consumer = (r > 0.5);
    if (quiet <= 0) printf("r is %g\n", r);
    if (argv[1] == NULL) {
	ECSourceHandle handle;
	/* we must be the first guy */

	chan = EChannel_typed_create(cc, simple_field_list, simple_format_list);
	printf("Channel is \"%s\"\n", ECglobal_id(chan));
	do_source_side(cm, chan, NULL, REPEAT_COUNT);
	printf("Waiting\n");
	CMsleep(cm, 60);

	handle = ECsource_typed_subscribe(chan, simple_field_list, 
					  simple_format_list);
	while(EChas_sinks(handle))
	    do_source_side(cm, chan, NULL, REPEAT_COUNT);

	ECcancel_source_subscribe(handle);
	EChannel_destroy(chan);
	CManager_close(cm);
	exit(0);
    }
    if (both) {
	EChannel chan2;
	chan = EChannel_open(cc, argv[1]);
	chan2 = EChannel_open(cc, argv[1]);
	(void) ECsink_typed_subscribe(chan, simple_field_list, 
				      simple_format_list,
				      typed_handler, NULL);
	if (quiet <= 0) printf("chan is %lx, chan2 is %lx\n", (long)chan, (long)chan2);
	/* do source side forever */
	do_source_side(cm, chan, NULL, -1);
    } else if (consumer) {
	int repeat_count = REPEAT_COUNT;
	int master_repeat = REPEAT_COUNT;
	EChannel new_chan;
	ECdata_struct data_spec;
	ECDataHandle data_handle;
	ECSinkHandle sink_handle;
	data_rec data;

	if (regression) {
#ifdef HAVE_WINDOWS_H
	    SetTimer(NULL, 5, 1000, (TIMERPROC) fail_and_die);
#else
	    struct sigaction sigact;
	    sigact.sa_flags = 0;
	    sigact.sa_handler = fail_and_die;
	    sigemptyset(&sigact.sa_mask);
	    sigaddset(&sigact.sa_mask, SIGALRM);
	    sigaction(SIGALRM, &sigact, NULL);
	    alarm(300);
#endif
	}
	if (quiet <= 0) printf("I'm a consumer\n");
	if (quiet <= 0) printf("filter is:\n%s", filter);
	data_spec.data_field_list = data_field_list;
	data_spec.data_subformat_list = data_format_list;
	data_spec.initial_value = &data;

	data.quiet = quiet;
	data.multiplier = (lrand48() % 5) + 1;
	data.complex_field.r = drand48();
	data.complex_field.i = drand48();
	if (quiet <= 0) {
	    printf("Data values are:  quiet = %d\n", data.quiet);
	    printf("             multiplier = %ld\n", data.multiplier);
	    printf("        complex_field.r = %g\n", data.complex_field.r);
	    printf("        complex_field.i = %g\n", data.complex_field.i);
	}
	new_chan = EChannel_typed_derive_data(cc, argv[1], filter, 
					      simple_field_list, 
					      simple_format_list, 
					      &data_spec);
	sink_handle = ECsink_typed_subscribe(new_chan, simple_field_list,
					     simple_format_list,
					     typed_handler, 
					     &repeat_count);
	while (repeat_count != 0) {
	    CMsleep(cm, 1);
	}
	
	if (!regression) master_repeat = 10000;

	while (master_repeat-- >= 0) {
	    repeat_count = 2;
	    data_handle = EChannel_data_open(new_chan, data_field_list, 
					     data_format_list);

	    data.quiet = quiet;
	    data.multiplier = (lrand48() % 5) + 1;
	    data.complex_field.r = drand48();
	    data.complex_field.i = drand48();
	    
	    if (quiet <= 0) {
		printf("Data values are:  quiet = %d\n", data.quiet);
		printf("             multiplier = %ld\n", data.multiplier);
		printf("        complex_field.r = %g\n", data.complex_field.r);
		printf("        complex_field.i = %g\n", data.complex_field.i);
	    }
	    EChannel_data_update(data_handle, &data);
	    EChannel_data_close(data_handle);
	    while (repeat_count != 0) {
		CMsleep(cm, 1);
	    }
	}
	ECcancel_sink_subscribe(sink_handle);
	EChannel_destroy(new_chan);
    } else {
	if (quiet <= 0) printf("I'm a supplier\n");
	chan = EChannel_open(cc, argv[1]);
	do_source_side(cm, chan, NULL, -1);
    }
    return 0;
}

static
pid_t
run_subprocess(args)
char **args;
{
#ifdef HAVE_WINDOWS_H
    int child;
    child = _spawnv(_P_NOWAIT, "./param_test.exe", args);
    if (child == -1) {
	perror("spawnv");
    }
    return child;
#else
    pid_t child = fork();
    if (child == 0) {
	/* I'm the child */
	if (execv(args[0], args) == -1) {
	    perror("execv");
	}
    }
    return child;
#endif
}

static int
do_regression_master_test()
{
    int ret = 0;
#ifndef NO_DERIVED
    CManager cm;
    EControlContext cc;
    EChannel chan, new_chan;
#ifdef HAVE_WINDOWS_H
    SetTimer(NULL, 5, 1000, (TIMERPROC) fail_and_die);
#else
    struct sigaction sigact;
    sigact.sa_flags = 0;
    sigact.sa_handler = fail_and_die;
    sigemptyset(&sigact.sa_mask);
    sigaddset(&sigact.sa_mask, SIGALRM);
    sigaction(SIGALRM, &sigact, NULL);
    alarm(300);
#endif

#ifdef USE_PTHREADS
    gen_pthread_init();
#endif

    cm = CManager_create();
    (void) CMfork_comm_thread(cm);
    cc = ECho_CM_init(cm);

    chan = EChannel_typed_create(cc, simple_field_list, simple_format_list);
    
    if (quiet <= 0) printf("  channel ID is %s\n", ECglobal_id(chan));

    {
	/* local tests */

	int repeat_count = REPEAT_COUNT;
	ECSinkHandle handle;
	ECdata_struct data_spec;
	ECDataHandle data_handle;
	data_rec data;

	data_spec.data_field_list = data_field_list;
	data_spec.data_subformat_list = data_format_list;
	data_spec.initial_value = &data;

	data.quiet = quiet;
	data.multiplier = (lrand48() % 5) + 1;
	data.complex_field.r = drand48();
	data.complex_field.i = drand48();

	if (quiet <= 0) {
	    printf("filter is:\n%s\n", filter);
	    printf("Data values are:  quiet = %d\n", data.quiet);
	    printf("             multiplier = %ld\n", data.multiplier);
	    printf("        complex_field.r = %g\n", data.complex_field.r);
	    printf("        complex_field.i = %g\n", data.complex_field.i);
	}
	
	new_chan = EChannel_typed_derive_data(cc, ECglobal_id(chan), filter, 
					      simple_field_list, 
					      simple_format_list, &data_spec);

	handle = ECsink_typed_subscribe(new_chan, simple_field_list,
					simple_format_list, typed_handler,
					&repeat_count);
	do_source_side(cm, chan, NULL, repeat_count);
	if (quiet <= 0) {
	    if (repeat_count != 0) {
		fprintf(stderr, "Not all in-process events delivered, test 1\n");
		ret = 1;
	    } else {
		fprintf(stderr, "Passed in-process delivery tests, init data\n");
	    }
	}
	repeat_count = REPEAT_COUNT;
	data_handle = EChannel_data_open(new_chan, data_field_list, 
					 data_format_list);

	data.quiet = quiet;
	data.multiplier = (lrand48() % 5) + 1;
	data.complex_field.r = drand48();
	data.complex_field.i = drand48();

	if (quiet <= 0) {
	    printf("filter is:\n%s\n", filter);
	    printf("Data values are:  quiet = %d\n", data.quiet);
	    printf("             multiplier = %ld\n", data.multiplier);
	    printf("        complex_field.r = %g\n", data.complex_field.r);
	    printf("        complex_field.i = %g\n", data.complex_field.i);
	}
	EChannel_data_update(data_handle, &data);
	do_source_side(cm, chan, NULL, repeat_count);
	if (quiet <= 0) {
	    if (repeat_count != 0) {
		fprintf(stderr, "Not all in-process events delivered, test 1\n");
		ret = 1;
	    } else {
		fprintf(stderr, "Passed in-process delivery tests, new data\n");
	    }
	}
	ECcancel_sink_subscribe(handle);
	EChannel_destroy(new_chan);
    }

    /* multiple process tests */
    {
	char *args[] = {"./param_test", "-r", "-c", NULL, NULL};
	int wait_status;
	int exit_state;

	args[sizeof(args)/sizeof(char*) -2] = ECglobal_id(chan);
	sink_proc = run_subprocess(args);

	/* give him time to start */
	CMsleep(cm, 30);
#ifndef HAVE_WINDOWS_H
	while ((wait_status = waitpid(sink_proc, &exit_state, WNOHANG)) == 0) {
	    do_source_side(cm, chan, NULL, REPEAT_COUNT);
	}
	if (wait_status == -1) {
	    perror("waitpid");
	}
	if (WIFEXITED(exit_state)) {
	    if (WEXITSTATUS(exit_state) == 0) {
		if (quiet <= 0) {
		    printf("Passed single remote sink test\n");
		}
	    } else {
		printf("Single remote sink exit with status %d\n",
		       WEXITSTATUS(exit_state));
		ret = 1;
	    }
	} else if (WIFSIGNALED(exit_state)) {
	    printf("Single remote sink died with signal %d\n",
		   WTERMSIG(exit_state));
	    ret = 1;
	}

#else /* windows.h */
	exit_state = STILL_ACTIVE;
    while (exit_state == STILL_ACTIVE) {
	    do_source_side(cm, chan, NULL, REPEAT_COUNT);
        GetExitCodeProcess((HANDLE)sink_proc,&exit_state);
    }
	if (exit_state == 0) {
	    if (quiet <= 0) {
		printf("Passed single remote sink test\n");
	    }
	} else {
	    printf("Single remote sink exit with status %d\n",
		   exit_state);
	    ret = 1;
	}
#endif
    }

    EChannel_destroy(chan);
    CManager_close(cm);
#endif
    return ret;
}
