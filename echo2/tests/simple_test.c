#if defined (__INTEL_COMPILER)
#pragma warning(disable : 310 )
#pragma warning(disable : 180 )
#pragma warning(disable : 981 )
#pragma warning(disable : 869 )
#pragma warning(disable : 1418 )
#pragma warning(disable : 1419 )
#endif


#include "config.h"
#ifdef HAVE_WINDOWS_H
#include <windows.h>
#else
#include <sys/time.h>
#endif
#include "atl.h"
#include "evpath.h"
#include "echo2.h"
#include "gen_thread.h"

#include <stdio.h>
#include <stdlib.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#ifdef HAVE_WINDOWS_H
#include <windows.h>
#include <process.h>
#define srand48(s) srand(s)
#define drand48() (((double)rand())/((double)RAND_MAX))
#define lrand48() rand()
#define kill(x,y) 
#else
extern double drand48();
extern long lrand48();
void srand48 ARGS((long seedval));
#include <sys/wait.h>
#endif

static int quiet = 0;

static int do_regression_master_test();

#define REPEAT_COUNT 10

static
void
simple_handler(vevent, length, client_data, attrs)
void *vevent;
int length;
void *client_data;
attr_list attrs;
{
    char *data = (char*)vevent;
    if (strcmp(data, "abcdefg") != 0) {
	printf("event doesn't match, %s\n", data);
    }
    if (quiet <= 0) {
	printf("In the handler, received \"%s\"\n", data);
	if (attrs) dump_attr_list(attrs);
	printf("\n");
    }
    if (client_data != NULL) {
	int tmp = *((int*)client_data);
	if (tmp > 0) {
	    *((int*)client_data) = tmp - 1;
	}
    }
}

static int regression = 0;
static int forked = 0;

static void
do_source_side(cm, chan, done_flag, count)
CManager cm;
EChannel chan;
int *done_flag;
int count;
{
    ECSourceHandle handle;
    int ret = 0;
    atom_t ECHO_USE_EVENT_TRANSPORT = attr_atom_from_string("ECHO_USE_EVENT_TRANSPORT");

    if (quiet <= 0) printf("chan is %lx\n", (long)chan);
    handle = ECsource_subscribe(chan);
    while (!ret) {
	char *str = "abcdefg";
	attr_list attrs = create_attr_list();
	add_attr(attrs, ECHO_USE_EVENT_TRANSPORT, Attr_Int4,
		 (attr_value) (long)(count % 2));
	if (quiet <= 0)
	    printf("I'm submitting %s\n", str);
	if (count % 2 == 1) {
	    ECsubmit_event(handle, str, strlen(str) + 1);
	} else {
	    struct FFSEncodeVec vec[3];
	    vec[0].iov_base = str;
	    vec[0].iov_len = 3;
	    vec[1].iov_base = str + 3;
	    vec[1].iov_len = 5;
	    vec[2].iov_base = NULL;
	    vec[2].iov_len = 0;
	    ECsubmit_eventV(handle, &vec[0]);
	}
	if (count != -1) {
	    count--;
	}
	CMusleep(cm, 10000);
	if (done_flag && *done_flag) ret++;
	if (count != -1) {
	    if (count == 0) ret++;
	}	    
    }
    ECcancel_source_subscribe(handle);
}

static pid_t sink_proc = 0;

static void
fail_and_die(signal)
int signal;
{
    fprintf(stderr, "Channel test failed to complete in reasonable time\n");
    if (sink_proc != 0) {
	kill(sink_proc, 9);
    }
    exit(1);
}

int
main(argc, argv)
int argc;
char **argv;
{
    EChannel chan;
    int consumer = 0;
    int both = 0;
    int regression_master = 1;
    CManager cm;
    EControlContext cc;

    srand48(getpid());
    regression = 1;
    quiet = 1;
    while (argv[1] && (argv[1][0] == '-')) {
	if (argv[1][1] == 'c') {
	    consumer = 1;
	    regression_master = 0;
	} else if (argv[1][1] == 's') {
	    regression_master = 0;
	} else if (argv[1][1] == 'b') {
	    both++;
	    regression_master = 0;
	} else if (argv[1][1] == 'q') {
	    quiet++;
	} else if (argv[1][1] == 'v') {
	    quiet--;
	} else if (argv[1][1] == 'n') {
	    regression = 0;
	    quiet = 0;
	}
	argv++;
    }
    if (argv[1] == NULL) {
	consumer = 1;
    }
    if (regression && regression_master) {
	return do_regression_master_test();
    }
    cm = CManager_create();
    cc = ECho_CM_init(cm);
    if (both) {
	EChannel chan2;
	chan = EChannel_open(cc, argv[1]);
	chan2 = EChannel_open(cc, argv[1]);
	(void) ECsink_subscribe(chan, simple_handler, NULL);
	if (quiet <= 0) printf("chan is %lx, chan2 is %lx\n", (long)chan, (long)chan2);
	/* do source side forever */
	do_source_side(cm, chan, NULL, -1);
    } else if (consumer) {
	int repeat_count = REPEAT_COUNT;
	attr_list attrs = NULL;
	char *transport = NULL, *network = NULL;
	if ((transport = getenv("EventTransport")) != NULL) {
	    if (!attrs) attrs = create_attr_list();
	    add_attr(attrs, attr_atom_from_string("ECHO_EVENT_TRANSPORT"), Attr_String,
		     (attr_value) strdup(transport));
	}
	if ((network = getenv("EventNetwork")) != NULL) {
	    if (!attrs) attrs = create_attr_list();
	    add_attr(attrs, attr_atom_from_string("ECHO_EVENT_NETWORK"), Attr_String,
		     (attr_value) strdup(network));
	}
	
	if (regression) {
#ifdef HAVE_WINDOWS_H
	  SetTimer(NULL, 5, 1000, (TIMERPROC) fail_and_die);
#else
	  struct sigaction sigact;
	  sigact.sa_flags = 0;
	  sigact.sa_handler = fail_and_die;
	  sigemptyset(&sigact.sa_mask);
	  sigaddset(&sigact.sa_mask, SIGALRM);
	  sigaction(SIGALRM, &sigact, NULL);
	  alarm(300);
#endif
	  chan = EChannel_open(cc, argv[1]);
	  if (quiet <= 0) printf(" opened %s, doing sink subscribe\n", argv[1]);
	} else {
	    chan = EChannel_create_attr(cc, attrs);
	    if (quiet <= 0) printf("  channel ID is %s\n", ECglobal_id(chan));
	}
	(void) ECsink_subscribe(chan, simple_handler, 
				      regression ? &repeat_count : NULL);
	while (repeat_count != 0) {
	    CMsleep(cm, 10);
	}
    } else {
	if (quiet <= 0) printf("I'm a supplier, opening %s\n", argv[1]);


	chan = EChannel_open(cc, argv[1]);
	if (chan == NULL) {
	    printf("Failed to contact channel %s\n", argv[1]);
	    exit(1);
	}
	if (quiet <= 0) printf("Channel open succeeded, submitting events\n");
	do_source_side(cm, chan, NULL, -1);
    }
    CManager_close(cm);
    return 0;
}


static
pid_t
run_subprocess(args)
char **args;
{
#ifdef HAVE_WINDOWS_H
    int child;
    child = _spawnv(_P_NOWAIT, "./simple_test.exe", args);
    if (child == -1) {
	printf("failed for simple_test\n");
	perror("spawnv");
    }
    return child;
#else
#ifndef DONT_FORK
    pid_t child = fork();
    if (child == 0) {
	/* I'm the child */
	execv("./simple_test", args);
    }
    return child;
#else
    int i = 0;
    printf("Would have run \"");
    while (args[i] != NULL) {
	printf(" %s", args[i++]);
    }
    printf("\n");
#endif
#endif
}

static int
do_regression_master_test()
{
    CManager cm;
    EControlContext cc;
    EChannel chan;
    attr_list attrs = NULL;
    char *transport = NULL, *network = NULL;

#ifdef HAVE_WINDOWS_H
    SetTimer(NULL, 5, 1000, (TIMERPROC) fail_and_die);
#else
    struct sigaction sigact;
    sigact.sa_flags = 0;
    sigact.sa_handler = fail_and_die;
    sigemptyset(&sigact.sa_mask);
    sigaddset(&sigact.sa_mask, SIGALRM);
    sigaction(SIGALRM, &sigact, NULL);
    alarm(300);
#endif
    if ((transport = getenv("EventTransport")) != NULL) {
	attrs = create_attr_list();
	add_attr(attrs, attr_atom_from_string("ECHO_EVENT_TRANSPORT"),
		 Attr_String,
		 (attr_value) strdup(transport));
    }
    if ((network = getenv("EventNetwork")) != NULL) {
	if (!attrs) attrs = create_attr_list();
	add_attr(attrs, attr_atom_from_string("ECHO_EVENT_NETWORK"), Attr_String,
		 (attr_value) strdup(network));
    }

#ifdef USE_PTHREADS
    gen_pthread_init();
#endif
    cm = CManager_create();
    forked = CMfork_comm_thread(cm);
    cc = ECho_CM_init(cm);

    if (quiet <= 0) {
	if (forked) {
	    printf("Forked a communication thread\n");
	} else {
	    printf("Doing non-threaded communication handling\n");
	}
    }
    srand48(1);

    chan = EChannel_create_attr(cc, attrs);
    
    if (quiet <= 0) printf("  channel ID is %s\n", ECglobal_id(chan));

    /* local tests */
    {
	int repeat_count = REPEAT_COUNT;
	int repeat_count1 = REPEAT_COUNT / 2;
	int repeat_count2 = REPEAT_COUNT - repeat_count1;
	ECSinkHandle handle;

	handle = ECsink_subscribe(chan, simple_handler,
					&repeat_count);
	do_source_side(cm, chan, NULL, repeat_count1);
	do_source_side(cm, chan, NULL, repeat_count2);
	if (repeat_count != 0) {
	    fprintf(stderr, "Not all in-process events delivered, test 1\n");
	} else {
	    if (quiet < 1) 
		fprintf(stderr, "Passed in-process delivery tests\n");
	}	
	ECcancel_sink_subscribe(handle);
    }

    /* multiple process tests */
    {
	int repeat_count1 = REPEAT_COUNT / 2;
	int repeat_count2 = REPEAT_COUNT - repeat_count1;
	char *args[] = {"simple_test", "-c", NULL, NULL, NULL};
	int exit_state;

	if (quiet > 0) {
	    args[sizeof(args)/sizeof(char*) -3] = ECglobal_id(chan);
	} else {
	    args[sizeof(args)/sizeof(char*) -3] = "-v";
	    args[sizeof(args)/sizeof(char*) -2] = ECglobal_id(chan);
	}
/*	printf("Would run %s %s %s\n", args[0],args[1],args[2]);*/
	sink_proc = run_subprocess(args);

	/* give him time to start */
	CMsleep(cm, 30);

	do_source_side(cm, chan, NULL, repeat_count1);
	do_source_side(cm, chan, NULL, repeat_count2);
	if (!quiet) {
	    printf("Waiting for remote....\n");
	}
#ifdef HAVE_WINDOWS_H
	if (_cwait(&exit_state, sink_proc, 0) == -1) {
	    perror("cwait");
	}
	if (exit_state == 0) {
	    if (quiet < 1) 
		printf("Passed single remote sink test\n");
	} else {
	    printf("Single remote sink exit with status %d\n",
		   exit_state);
	}
#else
	if (waitpid(sink_proc, &exit_state, 0) == -1) {
	    perror("waitpid");
	}
	if (WIFEXITED(exit_state)) {
	    if (WEXITSTATUS(exit_state) == 0) {
		if (quiet < 1) 
		    printf("Passed single remote sink test\n");
	    } else {
		printf("Single remote sink exit with status %d\n",
		       WEXITSTATUS(exit_state));
	    }
	} else if (WIFSIGNALED(exit_state)) {
	    printf("Single remote sink died with signal %d\n",
		   WTERMSIG(exit_state));
	}
#endif
    }
    EChannel_destroy(chan);
    CManager_close(cm);
/*    DExchange_close(de);
      DExchange_free(de);*/
    return 0;
}
