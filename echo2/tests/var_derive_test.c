

#include "config.h"
#ifdef HAVE_WINDOWS_H
#include <windows.h>
#else
#include <sys/time.h>
#endif
#include "atl.h"
#include "evpath.h"
#include "echo2.h"
#include "gen_thread.h"

#include <stdio.h>
#include <stdlib.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <sys/types.h>
#include <signal.h>
#ifdef HAVE_WINDOWS_H
#include <windows.h>
#include <process.h>
#define srand48(s) srand(s)
#define drand48() (((double)rand())/((double)RAND_MAX))
#define lrand48() rand()
#define kill(x,y) 
#else
extern double drand48();
extern long lrand48();
void srand48 ARGS((long seedval));
#include <sys/wait.h>
#endif

static int quiet = 0;

static int do_regression_master_test();

typedef struct _complex_rec {
    double r;
    double i;
} complex, *complex_ptr;

static FMField complex_field_list[] =
{
    {"r", "double", sizeof(double), FMOffset(complex_ptr, r)},
    {"i", "double", sizeof(double), FMOffset(complex_ptr, i)},
    {NULL, NULL, 0, 0}
};

typedef struct _simple_rec {
    int size;
    complex_ptr array;
} simple_rec, *simple_rec_ptr;

static FMField simple_field_list[] =
{
    {"size", "integer",
     sizeof(int), FMOffset(simple_rec_ptr, size)},
    {"array", "complex[size]",
     sizeof(complex), FMOffset(simple_rec_ptr, array)},
    {NULL, NULL, 0, 0}
};

static FMField output_field_list[] =
{
    {"osize", "integer",
     sizeof(int), FMOffset(simple_rec_ptr, size)},
    {"oarray", "complex[osize]",
     sizeof(complex), FMOffset(simple_rec_ptr, array)},
    {NULL, NULL, 0, 0}
};

static CMFormatList simple_format_list[] =
{
    {"complex", complex_field_list},
    {NULL, NULL},
};

#define REPEAT_COUNT 10

static
void
typed_handler(vevent, client_data, attrs)
void *vevent;
void *client_data;
attr_list attrs;
{
    simple_rec_ptr event = vevent;
    long sum = 0, scan_sum = 0;
    if (sum != scan_sum) {
	printf("Received record checksum does not match. expected %d, got %d\n",
	       (int)sum, (int)scan_sum);
    }
    if ((quiet <= 0) || (sum != scan_sum)) {
	int i;
	printf("In the handler, event data is :\n");
	printf("	size = %d\n	real elements are:", event->size);
	for (i=0; i < event->size; i++) {
	    printf(" %d", (int) event->array[i].r);
	}
	printf("\n	imag elements are:");
	for (i=0; i < event->size; i++) {
	    printf(" %d", (int) event->array[i].i);
	}
	printf("\n");
    }
    if (client_data != NULL) {
	int tmp = *((int*)client_data);
	if (tmp > 0) {
	    *((int*)client_data) = tmp - 1;
	}
    }
}

static
void generate_record(event)
simple_rec_ptr event;
{
    int i;
    event->size = (int) lrand48() % 100;
    event->array = malloc(sizeof(event->array[0]) * event->size);
    for (i=0; i< event->size; i++) {
	event->array[i].r = 2.0*i;
	event->array[i].i = 4.0*i;
    }
}

static int regression = 0;

static void
do_source_side(cm, chan, done_flag, count)
CManager cm;
EChannel chan;
int *done_flag;
int count;
{
    ECSourceHandle handle;
    int ret = 0;

    if (quiet <= 0) printf("chan is %lx\n", (long)chan);
    handle = ECsource_typed_subscribe(chan, simple_field_list, 
				      simple_format_list);
    if (handle == NULL) {
	printf("subscribe failed\n");
	return;
    }
    while (!ret) {
	if (drand48() < 0.1) {
	    simple_rec a;
	    generate_record(&a);
	    if (quiet <= 0)
		printf("I'm submitting %ld\n", (long)a.size);
	    ECsubmit_typed_event(handle, &a);
	    if ((a.size%2 == 1) && (count != -1)) {
		count--;
	    }
	    free(a.array);
	}
	CMusleep(cm, 10000);
	if (done_flag && *done_flag) ret++;
	if (count != -1) {
	    if (count == 0) ret++;
	}	    
    }
    ECcancel_source_subscribe(handle);
}

char *filter = "{\
    int i;\n\
    output.osize = lrand48() % 100;\n\
    for(i = 0; i < output.osize; i = i + 1) {\n\
	output.oarray[i].i = i * 3.0;\n\
    }\n\
    return input.size % 2;\n\
}\n\0\0";

static pid_t sink_proc = 0;

static void
fail_and_die(signal)
int signal;
{
    fprintf(stderr, "Channel test failed to complete in reasonable time\n");
    if (sink_proc != 0) {
	kill(sink_proc, 9);
    }
    exit(1);
}

int
main(argc, argv)
int argc;
char **argv;
{
    EChannel chan;
    double r = drand48();
    int consumer = 0;
    int both = 0;
    int regression_master = 1;
    CManager cm;
    EControlContext ec;

    cm = CManager_create();
    ec = ECho_CM_init(cm);
    srand48(getpid());
    regression = 1;
    quiet = 1;
    while (argv[1] && (argv[1][0] == '-')) {
	if (argv[1][1] == 'c') {
	    r = .9;
	    regression_master = 0;
	} else if (argv[1][1] == 's') {
	    r = 0;
	    regression_master = 0;
	} else if (argv[1][1] == 'b') {
	    both++;
	    regression_master = 0;
	} else if (argv[1][1] == 'q') {
	    quiet++;
	} else if (argv[1][1] == 'v') {
	    quiet--;
	} else if (argv[1][1] == 'n') {
	    regression = 0;
	    quiet = 0;
	}
	argv++;
    }
    if (regression && regression_master) {
	return do_regression_master_test();
    }
    consumer = (r > 0.5);
    if (quiet <= 0) printf("r is %g\n", r);
    if (both) {
	EChannel chan2;
	chan = EChannel_open(ec, argv[1]);
	chan2 = EChannel_open(ec, argv[1]);
	(void) ECsink_typed_subscribe(chan, output_field_list, 
				      simple_format_list,
				      typed_handler, NULL);
	if (quiet <= 0) printf("chan is %lx, chan2 is %lx\n", (long)chan, (long)chan2);
	/* do source side forever */
	do_source_side(cm, chan, NULL, -1);
    } else if (consumer) {
	int repeat_count = REPEAT_COUNT;
	EChannel new_chan;
	if (regression) {
#ifdef HAVE_WINDOWS_H
	    SetTimer(NULL, 5, 1000, (TIMERPROC) fail_and_die);
#else
	    struct sigaction sigact;
	    sigact.sa_flags = 0;
	    sigact.sa_handler = fail_and_die;
	    sigemptyset(&sigact.sa_mask);
	    sigaddset(&sigact.sa_mask, SIGALRM);
	    sigaction(SIGALRM, &sigact, NULL);
	    alarm(300);
#endif
	}
	if (quiet <= 0) printf("I'm a consumer\n");
	if (quiet <= 0) printf("filter is:\n%s\n", filter);
	new_chan = EChannel_typed_derive(ec, argv[1], filter, 
				   output_field_list, simple_format_list);
	if (new_chan == NULL) {
	    printf("Consumer Derivation failed\n");
	    return 1;
	}
	(void) ECsink_typed_subscribe(new_chan, output_field_list,
				      simple_format_list,
				      typed_handler, 
				      regression ? &repeat_count : NULL);
	while (repeat_count != 0) {
	    CMsleep(cm, 1);
	}
    } else {
	if (quiet <= 0) printf("I'm a supplier\n");
	chan = EChannel_open(ec, argv[1]);
	do_source_side(cm, chan, NULL, -1);
    }
    return 0;
}


static
pid_t
run_subprocess(args)
char **args;
{
#ifdef HAVE_WINDOWS_H
    int child;
    child = _spawnv(_P_NOWAIT, "./var_derive_test.exe", args);
    if (child == -1) {
	perror("spawnv");
    }
    return child;
#else
    pid_t child = fork();
    if (child == 0) {
	/* I'm the child */
	if (execv(args[0], args) == -1) {
	    perror("execv");
	}
    }
    return child;
#endif
}

static int
do_regression_master_test()
{
    CManager cm;
    EControlContext ec;
    EChannel chan, new_chan;
    char *chan_id;
    int ret = 0;

#ifdef HAVE_WINDOWS_H
    SetTimer(NULL, 5, 1000, (TIMERPROC) fail_and_die);
#else
    struct sigaction sigact;
    sigact.sa_flags = 0;
    sigact.sa_handler = fail_and_die;
    sigemptyset(&sigact.sa_mask);
    sigaddset(&sigact.sa_mask, SIGALRM);
    sigaction(SIGALRM, &sigact, NULL);
    alarm(300);
#endif

#ifdef USE_PTHREADS
    gen_pthread_init();
#endif

    cm = CManager_create();
    (void) CMfork_comm_thread(cm);
    ec = ECho_CM_init(cm);

    chan = EChannel_typed_create(ec, simple_field_list, simple_format_list);
    
    chan_id = ECglobal_id(chan);
    if (quiet <= 0) printf("  channel ID is %s\n", chan_id);

    {
	int repeat_count = REPEAT_COUNT;
	ECSinkHandle handle;
	/* local tests */

	if (quiet <= 0) printf("filter is:\n%s\n", filter);
	new_chan = EChannel_typed_derive(ec, chan_id, filter, 
					 output_field_list, simple_format_list);

	if (new_chan == NULL) {
	    printf("Derivation failed\n");
	    ret = 1;
	    goto end_test1;
	}
	handle = ECsink_typed_subscribe(new_chan, output_field_list,
					simple_format_list, typed_handler,
					&repeat_count);
	do_source_side(cm, chan, NULL, repeat_count);
	if (repeat_count != 0) {
	    fprintf(stderr, "Not all in-process events delivered, test 1\n");
	    ret = 1;
	} else {
	    if (quiet <= 0) {
		fprintf(stderr, "Passed in-process delivery tests\n");
	    }
	}	
	ECcancel_sink_subscribe(handle);
	EChannel_destroy(new_chan);
    end_test1:
	;
    }

    /* multiple process tests */
    {
	char *args[] = {"./var_derive_test", "-r", "-c", NULL, NULL};
	int exit_state;

	args[sizeof(args)/sizeof(char*) -2] = chan_id;
	sink_proc = run_subprocess(args);

	/* give him time to start */
	CMsleep(cm, 30);
	do_source_side(cm, chan, NULL, REPEAT_COUNT);
#ifdef HAVE_WINDOWS_H
	if (_cwait(&exit_state, sink_proc, 0) == -1) {
	    perror("cwait");
	}
	if (exit_state == 0) {
	    if (quiet <= 0) {
		printf("Passed single remote sink test\n");
	    }
	} else {
	    printf("Single remote sink exit with status %d\n",
		   exit_state);
	    ret = 1;
	}
#else
	if (waitpid(sink_proc, &exit_state, 0) == -1) {
	    perror("waitpid");
	    ret = 1;
	}
	if (WIFEXITED(exit_state)) {
	    if (WEXITSTATUS(exit_state) == 0) {
		if (quiet <= 0) {
		    printf("Passed single remote sink test\n");
		}
	    } else {
		printf("Single remote sink exit with status %d\n",
		       WEXITSTATUS(exit_state));
		ret = 1;
	    }
	} else if (WIFSIGNALED(exit_state)) {
	    printf("Single remote sink died with signal %d\n",
		   WTERMSIG(exit_state));
	    ret = 1;
	}
#endif
    }

    free(chan_id);
    EChannel_destroy(chan);
    CManager_close(cm);
    return ret;
}
