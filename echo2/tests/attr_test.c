#if defined (__INTEL_COMPILER)
#pragma warning(disable : 310 )
#pragma warning(disable : 180 )
#pragma warning(disable : 981 )
#pragma warning(disable : 869 )
#pragma warning(disable : 1418 )
#pragma warning(disable : 1419 )
#endif



#include "config.h"
#ifdef HAVE_WINDOWS_H
#include <windows.h>
#else
#include <sys/time.h>
#endif
#include "atl.h"
#include "evpath.h"
#include "echo2.h"
#include "gen_thread.h"

#include <stdio.h>
#include <stdlib.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#ifdef HAVE_WINDOWS_H
#include <windows.h>
#include <process.h>
#define srand48(s) srand(s)
#define drand48() (((double)rand())/((double)RAND_MAX))
#define lrand48() rand()
#define kill(x,y) 
#else
extern double drand48();
extern long lrand48();
void srand48 ARGS((long seedval));
#include <sys/wait.h>
#endif

static int quiet = 0;

static int do_regression_master_test();

typedef struct _complex_rec {
    double r;
    double i;
} complex, *complex_ptr;

static FMField complex_field_list[] =
{
    {"r", "double", sizeof(double), FMOffset(complex_ptr, r)},
    {"i", "double", sizeof(double), FMOffset(complex_ptr, i)},
    {NULL, NULL, 0, 0}
};

typedef struct _simple_rec {
    short short_field;
    long long_field;
    char *string;
    complex complex_field;
    double double_field;
    char char_field;
} simple_rec, *simple_rec_ptr;

static FMField simple_field_list[] =
{
    {"short_field", "integer",
     sizeof(short), FMOffset(simple_rec_ptr, short_field)},
    {"long_field", "integer",
     sizeof(long), FMOffset(simple_rec_ptr, long_field)},
    {"string_field", "string",
     sizeof(char *), FMOffset(simple_rec_ptr, string)},
    {"complex_field", "complex",
     sizeof(complex), FMOffset(simple_rec_ptr, complex_field)},
    {"double_field", "float",
     sizeof(double), FMOffset(simple_rec_ptr, double_field)},
    {"char_field", "char",
     sizeof(char), FMOffset(simple_rec_ptr, char_field)},
    {NULL, NULL, 0, 0}
};

static CMFormatRec simple_format_list[] =
{
    {"complex", complex_field_list},
    {NULL, NULL},
};

typedef struct _big_complex_rec {
    int junk;
    double r;
    double i;
} big_complex, *big_complex_ptr;

static FMField big_complex_field_list[] =
{
    {"junk", "integer", sizeof(int), FMOffset(big_complex_ptr, junk)},
    {"r", "double", sizeof(double), FMOffset(big_complex_ptr, r)},
    {"i", "double", sizeof(double), FMOffset(big_complex_ptr, i)},
    {NULL, NULL, 0, 0}
};

typedef struct _big_simple_rec {
    int junk_field;
    short short_field;
    long long_field;
    char *string;
    big_complex complex_field;
    double double_field;
    char char_field;
} big_simple_rec, *big_simple_rec_ptr;

static FMField big_simple_field_list[] =
{
    {"junk_field", "integer",
     sizeof(int), FMOffset(big_simple_rec_ptr, junk_field)},
    {"short_field", "integer",
     sizeof(short), FMOffset(big_simple_rec_ptr, short_field)},
    {"long_field", "integer",
     sizeof(long), FMOffset(big_simple_rec_ptr, long_field)},
    {"string_field", "string",
     sizeof(char *), FMOffset(big_simple_rec_ptr, string)},
    {"complex_field", "complex",
     sizeof(big_complex), FMOffset(big_simple_rec_ptr, complex_field)},
    {"double_field", "float",
     sizeof(double), FMOffset(big_simple_rec_ptr, double_field)},
    {"char_field", "char",
     sizeof(char), FMOffset(big_simple_rec_ptr, char_field)},
    {NULL, NULL, 0, 0}
};

static CMFormatRec big_format_list[] =
{
    {"complex", big_complex_field_list},
    {NULL, NULL},
};

#define REPEAT_COUNT 10

static
void
typed_handler(vevent, client_data, attrs)
void *vevent;
void *client_data;
attr_list attrs;
{
    simple_rec_ptr event = vevent;
    long sum = 0, scan_sum = 0;
    int integer_field;
    attr_value_type val_type;
    static int atom_init = 0;
    static atom_t ATTR_TEST_ATOM;

    if (atom_init++ == 0) {
	ATTR_TEST_ATOM = attr_atom_from_string("ECho_attr_test_atom");
	printf("Attr_test_atom is %x\n", ATTR_TEST_ATOM);
    }
    if (query_attr(attrs, ATTR_TEST_ATOM, &val_type, (attr_value*)&integer_field) == 0) {
	printf("ATTR_TEST_ATOM not found\n");
	return;
    }
    
    sum += integer_field % 100;
    sum += event->short_field % 100;
    sum += event->long_field % 100;
    sum += ((int)(event->complex_field.r * 100.0)) % 100;
    sum += ((int)(event->complex_field.i * 100.0)) % 100;
    sum += ((int)(event->double_field * 100.0)) % 100;
    sum += event->char_field;
    sum = sum % 100;
    if (event->string) 
	sscanf(event->string, "testing, sum is %ld", &scan_sum);
    if (sum != scan_sum) {
	printf("Received record checksum does not match, expected %ld, got %ld.\n",
	       scan_sum, sum);
    }
    if ((quiet <= 0) || (sum != scan_sum)) {
	printf("In %d the handler, event data is :\n", quiet);
	printf("	integer_attr = %d\n", integer_field);
	printf("	short_field = %d\n", event->short_field);
	printf("	long_field = %ld\n", event->long_field);
	printf("	string = %s\n", event->string ? event->string : "NULL");
	printf("	complex_field.r = %g\n", event->complex_field.r);
	printf("	complex_field.i = %g\n", event->complex_field.i);
	printf("	double_field = %g\n", event->double_field);
	printf("	char_field = %c\n", event->char_field);
	printf("    Event was received with attrs:");
	if (attrs) dump_attr_list(attrs);
	printf("\n");
    }
    sum = 0;
    if (client_data != NULL) {
	int tmp = *((int*)client_data);
	if (tmp > 0) {
	    *((int*)client_data) = tmp - 1;
	}
    }
}

static char tmp_string[128];

static
void generate_record(event, int_attr_p)
simple_rec_ptr event;
int *int_attr_p;
{
    long sum = 0;
    *int_attr_p = (int) lrand48() % 100;
    sum += *int_attr_p % 100;
    event->short_field = (short) lrand48();
    sum += event->short_field % 100;
    event->long_field = (long) lrand48();
    sum += event->long_field % 100;

    event->complex_field.r = drand48();
    sum += ((int)(event->complex_field.r * 100.0)) % 100;
    event->complex_field.i = drand48();
    sum += ((int)(event->complex_field.i * 100.0)) % 100;

    event->double_field = drand48();
    sum += ((int)(event->double_field * 100.0)) % 100;
    event->char_field = lrand48() % 128;
    sum += event->char_field;
    sum = sum % 100;
    sprintf(tmp_string, "testing, sum is %ld\n", sum);
    event->string = tmp_string;
}

static
void generate_big_record(event, int_attr_p)
big_simple_rec_ptr event;
int *int_attr_p;
{
    long sum = 0;
    event->junk_field = -1;
    *int_attr_p = (int) lrand48() % 100;
    sum += *int_attr_p % 100;
    event->short_field = (short) lrand48();
    sum += event->short_field % 100;
    event->long_field = (long) lrand48();
    sum += event->long_field % 100;

    event->complex_field.r = drand48();
    sum += ((int)(event->complex_field.r * 100.0)) % 100;
    event->complex_field.i = drand48();
    sum += ((int)(event->complex_field.i * 100.0)) % 100;

    event->double_field = drand48();
    sum += ((int)(event->double_field * 100.0)) % 100;
    event->char_field = lrand48() % 128;
    sum += event->char_field;
    sum = sum % 100;
    sprintf(tmp_string, "testing, sum is %ld\n", sum);
    event->string = tmp_string;
}

static int regression = 0;
static int forked = 0;

static void
do_source_side(cm, chan, done_flag, count, use_big)
CManager cm;
EChannel chan;
int *done_flag;
int count;
int use_big;
{
    ECSourceHandle handle;
    int ret = 0;
    static int atom_init = 0;
    attr_list attrs = create_attr_list();
    static atom_t ATTR_TEST_ATOM;
    if (quiet <= 0) printf("chan is %lx\n", (long)chan);
    if (use_big) {
	handle = ECsource_typed_subscribe(chan, big_simple_field_list, 
					  big_format_list);
    } else {
	handle = ECsource_typed_subscribe(chan, simple_field_list, 
					  simple_format_list);
    }
    if (atom_init++ == 0) {
	ATTR_TEST_ATOM = attr_atom_from_string("ECho_attr_test_atom");
	printf("Attr_test_atom is %x\n", ATTR_TEST_ATOM);
    }
    add_attr(attrs, ATTR_TEST_ATOM, Attr_Int4, (attr_value)34567);
    while (!ret) {
	int int_attr;
	if (use_big) {
	    big_simple_rec a;
	    generate_big_record(&a, &int_attr);
	    if (quiet <= 0)
		printf("I'm submitting %ld, int attr %d\n", a.long_field, int_attr);
	    replace_attr(attrs, ATTR_TEST_ATOM, Attr_Int4, 
			 (attr_value)(long)int_attr);
	    ECsubmit_typed_event_attr(handle, &a, attrs);
	} else {
	    simple_rec a;
	    generate_record(&a, &int_attr);
	    if (quiet <= 0)
		printf("I'm submitting %ld, int_attr %d\n", a.long_field, int_attr);
	    replace_attr(attrs, ATTR_TEST_ATOM, Attr_Int4, 
			 (attr_value)(long)int_attr);
	    ECsubmit_typed_event_attr(handle, &a, attrs);
	}
	if (count != -1) {
	    count--;
	}
	CMusleep(cm, 10000);
	if (done_flag && *done_flag) ret++;
	if (count != -1) {
	    if (count == 0) ret++;
	}	    
    }
    ECcancel_source_subscribe(handle);
}

static pid_t sink_proc = 0;

static void
fail_and_die(signal)
int signal;
{
    fprintf(stderr, "Channel test failed to complete in reasonable time\n");
    if (sink_proc != 0) {
	kill(sink_proc, 9);
    }
    exit(1);
}

int
main(argc, argv)
int argc;
char **argv;
{
    EChannel chan;
    int consumer = 0;
    int both = 0;
    int regression_master = 1;
    CManager cm;
    EControlContext cc;

    srand48(getpid());
    regression = 1;
    quiet = 1;
    while (argv[1] && (argv[1][0] == '-')) {
	if (argv[1][1] == 'c') {
	    consumer = 1;
	    regression_master = 0;
	} else if (argv[1][1] == 's') {
	    regression_master = 0;
	} else if (argv[1][1] == 'b') {
	    both++;
	    regression_master = 0;
	} else if (argv[1][1] == 'q') {
	    quiet++;
	} else if (argv[1][1] == 'v') {
	    quiet--;
	} else if (argv[1][1] == 'n') {
	    regression = 0;
	    quiet = 0;
	}
	argv++;
    }
    if (argv[1] == NULL) {
	consumer = 1;
    }
    if (regression && regression_master) {
	return do_regression_master_test();
    }
    cm = CManager_create();
    cc = ECho_CM_init(cm);
    if (both) {
	EChannel chan2;
	chan = EChannel_open(cc, argv[1]);
	chan2 = EChannel_open(cc, argv[1]);
	(void) ECsink_typed_subscribe(chan, simple_field_list, 
				      simple_format_list,
				      typed_handler, NULL);
	if (quiet <= 0) printf("chan is %lx, chan2 is %lx\n", (long)chan, (long)chan2);
	/* do source side forever */
	do_source_side(cm, chan, NULL, -1, 0);
    } else if (consumer) {
	int repeat_count = REPEAT_COUNT;
	attr_list attrs = NULL;
	char *transport = NULL, *network = NULL;
	if ((transport = getenv("EventTransport")) != NULL) {
	    if (!attrs) attrs = create_attr_list();
	    add_attr(attrs, attr_atom_from_string("ECHO_EVENT_TRANSPORT"),
		     Attr_String, (attr_value) strdup(transport));
	}
	if ((network = getenv("EventNetwork")) != NULL) {
	    if (!attrs) attrs = create_attr_list();
	    add_attr(attrs, attr_atom_from_string("ECHO_EVENT_NETWORK"), 
		     Attr_String, (attr_value) strdup(network));
	}
	
	if (regression) {
#ifdef HAVE_WINDOWS_H
	  SetTimer(NULL, 5, 1000, (TIMERPROC) fail_and_die);
#else
	  struct sigaction sigact;
	  sigact.sa_flags = 0;
	  sigact.sa_handler = fail_and_die;
	  sigemptyset(&sigact.sa_mask);
	  sigaddset(&sigact.sa_mask, SIGALRM);
	  sigaction(SIGALRM, &sigact, NULL);
	  alarm(300);
#endif
	  chan = EChannel_open(cc, argv[1]);
	  if (quiet <= 0) printf(" opened %s, doing sink subscribe\n", argv[1]);
	} else {
	    printf("create\n");
	    chan = EChannel_typed_create_attr(cc, simple_field_list, simple_format_list, attrs);
	    if (quiet <= 0) printf("  channel ID is %s\n", ECglobal_id(chan));
	}
	(void) ECsink_typed_subscribe(chan, simple_field_list,
				      simple_format_list,
				      typed_handler, 
				      regression ? &repeat_count : NULL);
	while (repeat_count != 0) {
	    CMsleep(cm, 10);
	}
    } else {
	if (quiet <= 0) printf("I'm a supplier, opening %s\n", argv[1]);


	chan = EChannel_open(cc, argv[1]);
	if (chan == NULL) {
	    printf("Failed to contact channel %s\n", argv[1]);
	    exit(1);
	}
	if (quiet <= 0) printf("Channel open succeeded, submitting events\n");
	printf("Doing source side\n");
	do_source_side(cm, chan, NULL, 6, 0);
    }
    CManager_close(cm);
    return 0;
}

static int stop_poll_thread = 0;
#ifdef USE_PTHREADS
static int
poll_thread(cm)
void *cm;
{
    while (!stop_poll_thread) {
	CMpoll_network(cm);
    }
    return 0;
}
#endif

static
pid_t
run_subprocess(args)
char **args;
{
#ifdef HAVE_WINDOWS_H
    int child;
    child = _spawnv(_P_NOWAIT, "./attr_test.exe", args);
    if (child == -1) {
	printf("failed for typed_test\n");
	perror("spawnv");
    }
    return child;
#else
    pid_t child = fork();
    if (child == 0) {
	/* I'm the child */
	execv("./attr_test", args);
    }
    return child;
#endif
}

static int
do_regression_master_test()
{
    CManager cm;
    EControlContext cc;
    EChannel chan;

#ifdef HAVE_WINDOWS_H
    SetTimer(NULL, 5, 1000, (TIMERPROC) fail_and_die);
#else
    struct sigaction sigact;
    sigact.sa_flags = 0;
    sigact.sa_handler = fail_and_die;
    sigemptyset(&sigact.sa_mask);
    sigaddset(&sigact.sa_mask, SIGALRM);
    sigaction(SIGALRM, &sigact, NULL);
    alarm(300);
#endif

#ifdef USE_PTHREADS
    gen_pthread_init();
#endif
    cm = CManager_create();
    forked = CMfork_comm_thread(cm);
    cc = ECho_CM_init(cm);

    if (quiet <= 0) {
	if (forked) {
	    printf("Forked a communication thread\n");
	} else {
	    printf("Doing non-threaded communication handling\n");
	}
    }
    srand48(1);

    chan = EChannel_typed_create(cc, simple_field_list, simple_format_list);
    
    if (quiet <= 0) printf("  channel ID is %s\n", ECglobal_id(chan));

    /* local tests */
    {
	int repeat_count = REPEAT_COUNT;
	int repeat_count1 = REPEAT_COUNT / 2;
	int repeat_count2 = REPEAT_COUNT - repeat_count1;
	ECSinkHandle handle;

	handle = ECsink_typed_subscribe(chan, simple_field_list,
					simple_format_list, typed_handler,
					&repeat_count);
	do_source_side(cm, chan, NULL, repeat_count1, 0);
	do_source_side(cm, chan, NULL, repeat_count2, 0);
	if (repeat_count != 0) {
	    fprintf(stderr, "Not all in-process events delivered, test 1\n");
	} else {
	    if (quiet < 1) 
		fprintf(stderr, "Passed in-process delivery tests\n");
	}	
	ECcancel_sink_subscribe(handle);
    }

    /* multiple process tests */
    {
	int repeat_count1 = REPEAT_COUNT / 2;
	int repeat_count2 = REPEAT_COUNT - repeat_count1;
	char *args[] = {"attr_test", "-c", NULL, NULL, NULL};
	int exit_state;

	if (quiet > 0) {
	    args[sizeof(args)/sizeof(char*) -3] = ECglobal_id(chan);
	} else {
	    args[sizeof(args)/sizeof(char*) -3] = "-v";
	    args[sizeof(args)/sizeof(char*) -2] = ECglobal_id(chan);
	}
	sink_proc = run_subprocess(args);

	/* give him time to start */
	CMsleep(cm, 30);

	do_source_side(cm, chan, NULL, repeat_count1, 0);
	do_source_side(cm, chan, NULL, repeat_count2, 0);
	if (!quiet) {
	    printf("Waiting for remote....\n");
	}
#ifdef HAVE_WINDOWS_H
	if (_cwait(&exit_state, sink_proc, 0) == -1) {
	    perror("cwait");
	}
	if (exit_state == 0) {
	    if (quiet < 1) 
		printf("Passed single remote sink test\n");
	} else {
	    printf("Single remote sink exit with status %d\n",
		   exit_state);
	}
#else
	if (waitpid(sink_proc, &exit_state, 0) == -1) {
	    perror("waitpid");
	}
	if (WIFEXITED(exit_state)) {
	    if (WEXITSTATUS(exit_state) == 0) {
		if (quiet < 1) 
		    printf("Passed single remote sink test\n");
	    } else {
		printf("Single remote sink exit with status %d\n",
		       WEXITSTATUS(exit_state));
	    }
	} else if (WIFSIGNALED(exit_state)) {
	    printf("Single remote sink died with signal %d\n",
		   WTERMSIG(exit_state));
	}
#endif
    }
    stop_poll_thread++;
    EChannel_destroy(chan);
    CManager_close(cm);
/*    DExchange_close(de);
      DExchange_free(de);*/
    return 0;
}
