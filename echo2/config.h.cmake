/* config.h.in.  Generated from configure.ac by autoheader.  */

/* Define to 1 if you have the <gen_thread.h> header file. */
#cmakedefine HAVE_GEN_THREAD_H

/* Define to 1 if you have the <malloc.h> header file. */
#cmakedefine HAVE_MALLOC_H

/* Define to 1 if you have the <math.h> header file. */
#cmakedefine HAVE_MATH_H

/* Define to 1 if you have the <memory.h> header file. */
#cmakedefine HAVE_MEMORY_H

/* Define to 1 if you have the <netdb.h> header file. */
#cmakedefine HAVE_NETDB_H

/* Define to 1 if you have the <stdint.h> header file. */
#cmakedefine HAVE_STDINT_H

/* Define to 1 if you have the <stdlib.h> header file. */
#cmakedefine HAVE_STDLIB_H

/* Define to 1 if you have the <stdarg.h> header file. */
#cmakedefine HAVE_STDARG_H

/* Define to 1 if you have the <unistd.h> header file. */
#cmakedefine HAVE_UNISTD_H

/* Define to 1 if you have the <windows.h> header file. */
#cmakedefine HAVE_WINDOWS_H

/* Define to 1 if you have the `writev' function. */
#cmakedefine HAVE_WRITEV

/* Define to 1 if you have the ANSI C header files. */
#cmakedefine STDC_HEADERS

/* The target platform is Cray Compute Node Linux (CNL). */
#cmakedefine TARGET_CNL

/* Define this if Pthreads should be used for running tests */
#cmakedefine USE_PTHREADS

/* Version number of package */
#cmakedefine VERSION

/* Define so that glibc/gnulib argp.h does not typedef error_t. */
#cmakedefine __error_t_defined

/* Define to empty if `const' does not conform to ANSI C. */
#cmakedefine const

/* Define to a type to use for `error_t' if it is not otherwise available. */
#cmakedefine error_t

/* Define to `int' if <sys/types.h> does not define. */
#cmakedefine pid_t

/* Define to `int' if <sys/types.h> does not define. */
#cmakedefine pid_t

/* Define to the shared module suffix in use */
#cmakedefine CMAKE_SHARED_MODULE_SUFFIX "@CMAKE_SHARED_MODULE_SUFFIX@"
