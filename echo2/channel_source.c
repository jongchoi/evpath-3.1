#ifndef DOC
#if defined (__INTEL_COMPILER)
#pragma warning(disable : 310 )
#pragma warning(disable : 180 )
#pragma warning(disable : 981 )
#pragma warning(disable : 266 )
#endif
#include "config.h"
#include "ffs.h"
#include "atl.h"
#include "evpath.h"
#include "echo2.h"

#include <stdio.h>
#include <stdlib.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef HAVE_WINDOWS_H
#include <windows.h>
#define drand48() (((double)rand())/((double)RAND_MAX))
#define lrand48() rand()
#endif
#endif
FMField simple_field_list[] = {
    {"entry", "integer", sizeof(long), 0},
    {NULL, NULL, 0, 0}};

int main(int argc, char **argv)
{
    CManager cm;
    EChannel chan;
    EControlContext cc;
    ECSourceHandle handle;
    long a = time(NULL) % 100;

    cm = CManager_create();
    cc = ECho_CM_init(cm);

    chan = EChannel_open(cc, argv[1]);
    handle = ECsource_typed_subscribe(chan, simple_field_list, NULL);
    while (1) {
	printf("I'm submitting %ld\n", ++a);
	ECsubmit_event(handle, &a, sizeof(a));
	CMsleep(cm, 5);
    }
}
