
#include <stdio.h>
#include "config.h"

static char *ECho_version = "ECho Version 3.0.21 rev. 11190  -- 2012-05-15 07:19:24 -0400 (Tue, 15 May 2012)\n";

void
EChoprint_version()
{
    printf("%s", ECho_version);
}
