#include "config.h"
#include "ffs.h"
#ifdef HAVE_GEN_THREAD_H
#include "gen_thread.h"
#else
#define gen_thr_initialized() 0
#define thr_mutex_lock(m)
#define thr_mutex_unlock(m)
#define thr_mutex_alloc() 0
#define thr_mutex_free(m)
#define thr_condition_wait(c, m)
#define thr_condition_signal(c)
#define thr_condition_alloc() 0
#define thr_condition_free(c)
#define thr_mutex_t void*
#define thr_condition_t void*
#endif
#include "atl.h"
#include "evpath.h"
#ifdef HAVE_MALLOC_H
#include <malloc.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifndef PRINTF_DEFINED
extern int printf ARGS((const char *,...));
#endif
#include "echo2.h"
#include "echo_internal.h"

/* message to send to arbitrator to derive a new the channel */
typedef struct _ProtoDeriveMsg {
    char *proto_str;
    char *contact_str;
    int cond;
    channel_ID_struct client_channel;
    char *filter;
    int  field_list_len;
    FMFieldList field_list;
    int  format_list_len;
    format_list_element *format_list;
    long usec_period;
} ProtoDeriveMsg, *ProtoDeriveMsgPtr;

extern FMField channel_attend_response_msg_flds[];
extern FMField old_channel_attend_response_msg_flds[];

FMField proto_derive_msg_flds[] =
{
    {"proto_channel", "string", sizeof(char *), 
     FMOffset(ProtoDeriveMsgPtr, proto_str)},
    {"contact_str", "string", sizeof(char*),
     FMOffset(ProtoDeriveMsgPtr, contact_str)},
    {"condition", "integer", sizeof(int), 
     FMOffset(ProtoDeriveMsgPtr, cond)},
    {"client channel", "channel_ID", sizeof(channel_ID_struct), 
     FMOffset(ProtoDeriveMsgPtr, client_channel)},
    {"filter", "string", sizeof(char*), 
     FMOffset(ProtoDeriveMsgPtr, filter)},
    {"field_list_len", "integer", sizeof(int),
     FMOffset(ProtoDeriveMsgPtr, field_list_len)},
    {"field_list", "IOfield_list[field_list_len]", sizeof(FMField),
     FMOffset(ProtoDeriveMsgPtr, field_list)},
    {"format_list_len", "integer", sizeof(int),
     FMOffset(ProtoDeriveMsgPtr, format_list_len)},
    {"format_list", "DEFormatList[format_list_len]", 
     sizeof(format_list_element), FMOffset(ProtoDeriveMsgPtr, format_list)},
    {"usec_period", "integer", sizeof(long),
     FMOffset(ProtoDeriveMsgPtr, usec_period)},
    {(char *) 0, (char *) 0, 0, 0}
};

extern FMField field_list_flds[];
extern FMField format_list_flds[];
extern FMField channel_id_flds[];

FMStructDescRec proto_derive_msg_formats[] = {
    {"Proto Derive", proto_derive_msg_flds, sizeof(ProtoDeriveMsg), NULL},
    {"IOfield_list", field_list_flds, sizeof(FMField), NULL},
    {"DEFormatList", format_list_flds, sizeof(format_list_element), NULL},
    {"channel_ID", channel_id_flds, sizeof(channel_ID_struct), NULL},
    {NULL, NULL, 0, NULL}
};
#define NO_DERIVED

#ifndef NO_DERIVED
static EChannel
do_proto_derive ARGS((EControlContext cc, ECproto proto_channel, char *filter, 
		      FMFieldList output_field_list, 
		      CMFormatList output_format_list, int period));

typedef struct _ECproto {
    EControlContext cc;
    ecl_parse_context base_context;
    char *contact_string;
} ECproto_struct;

extern ECproto ECproto_create(cc, context)
EControlContext cc;
ecl_parse_context context;
{
    ECproto proto_chan = malloc(sizeof(struct _ECproto));
    attr_list contact_list;

    proto_chan->cc = cc;
    proto_chan->base_context = context;

    contact_list = CMget_contact_list(cc->cm);
    if (contact_list == NULL) {
	printf("Warning, no contact list.  Must do CManager_listen() before using channels.\n");
	CMfree(proto_chan);
	return NULL;
    }
    proto_chan->contact_string = attr_list_to_string(contact_list);
    return proto_chan;
}

extern char *ECproto_id(proto_chan)
ECproto proto_chan;
{
    int len = 0;
    char proto_str[17] =
    {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    unsigned char proto_char[8] =
    {0, 0, 0, 0, 0, 0, 0, 0};
    char *global_id_str;

    memset(proto_char, 0, sizeof(proto_char));
    memcpy(proto_char, &proto_chan, sizeof(proto_chan));
    sprintf(proto_str, "%02x%02x%02x%02x%02x%02x%02x%02x",
	    (unsigned int) proto_char[0],
	    (unsigned int) proto_char[1],
	    (unsigned int) proto_char[2],
	    (unsigned int) proto_char[3],
	    (unsigned int) proto_char[4],
	    (unsigned int) proto_char[5],
	    (unsigned int) proto_char[6],
	    (unsigned int) proto_char[7]);
    len = strlen(proto_chan->contact_string);
    len += 16 /* proto_str */  + 1 /* separators */ ;
    global_id_str = CMmalloc(len + 1);
    memset(global_id_str, 0, len + 1);
    sprintf(global_id_str, "%s@%s", proto_chan->contact_string,
	    proto_str);
    return global_id_str;
}

extern EChannel
ECproto_derive_periodic(cc, proto_id, event_filter, field_list, format_list,
			period)
EControlContext cc;
char *proto_id;
char *event_filter;
FMFieldList field_list;
CMFormatList format_list;
int period;
{
#ifndef NO_DERIVED
    char *proto_id_copy = CMmalloc(strlen(proto_id) + 1);
    attr_list attrs;
    char *contact_str;
    char *proto_str, *tmp;
    CMConnection conn;
    EChannel chan;
    ProtoDeriveMsg derive_msg;
    attr_list self_contact_list;
    int cond, i;

    ECtrace_out("ECproto_derive", 2);

    strcpy(proto_id_copy, proto_id);
    contact_str = proto_id_copy;
    tmp = strchr(proto_id_copy, '@');
    if (tmp == NULL) {
	ECtrace_out("ECchannel_derive, bad id", 0);
	return NULL;
    }
    *tmp = 0;			/* kill first @ */
    proto_str = ++tmp;

    attrs = attr_list_from_string(contact_str);
    if (CMcontact_self_check(cc->cm, attrs)) {
	/* this proto-channel is really local */
	ECproto proto_chan;
	free_attr_list(attrs);
	proto_chan = (ECproto) string_to_channel_ptr(proto_str);
	chan = do_proto_derive(cc, proto_chan, event_filter, field_list,
			       format_list, period);
	return chan;
    }

    chan = EChannel_typed_create(cc, field_list, format_list);

    ECtrace_out("getting a connection, registering close handler", 3);
    conn = CMget_conn(cc->cm, attrs);
    free_attr_list(attrs);
    if (conn == NULL) {
	printf("Failed to contact host of channel \"%s\"\n", proto_id);
	EChannel_destroy(chan);
	return NULL;
    }
    CMconn_register_close_handler(conn, EChannel_close_handler, cc);


    ECtrace_out("getting condition and setting local chan condition", 3);
    cond = CMCondition_get(cc->cm, conn);
    CMCondition_set_client_data(cc->cm, cond, &chan);

    self_contact_list = CMget_contact_list(cc->cm);
    memset(&derive_msg, 0, sizeof(derive_msg));
    derive_msg.proto_str = proto_str;
    derive_msg.cond = cond;
    set_channel_id(&derive_msg.client_channel, chan);
    derive_msg.filter = event_filter;
    derive_msg.contact_str = attr_list_to_string(self_contact_list);
    derive_msg.usec_period = period;
    derive_msg.field_list = field_list;
    derive_msg.field_list_len = 0;
    while(field_list[derive_msg.field_list_len].field_name != NULL) {
	derive_msg.field_list_len++;
    }
    derive_msg.field_list_len++;  /* send terminator as well */
    i = 0;
    while(format_list[i].format_name != NULL) i++;
    derive_msg.format_list = CMmalloc(i * sizeof(format_list_element));  
    derive_msg.format_list_len = i;  /* +1 because we send terminator */
    for (i=0; i<derive_msg.format_list_len; i++) {
	int j = 0;
	derive_msg.format_list[i].format_name = format_list[i].format_name;
	derive_msg.format_list[i].field_list = format_list[i].field_list;
	while (format_list[i].field_list[j].field_name != NULL) {
	    j++;
	}
	j++; /* send terminator as well */
	derive_msg.format_list[i].field_list_len = j;
    }
    if (!CMwrite(conn, CMlookup_format(cc->cm, proto_derive_msg_flds), 
		 &derive_msg)) {
	ECtrace_out("proto derive write failed", 0);
    }
    CMfree(derive_msg.format_list);
    ECtrace_out("Sending derive msg, proto-channel %s cond %d filter %d\n", 3,
		derive_msg.proto_str, derive_msg.cond, derive_msg.filter);
    if (CMCondition_wait(cc->cm, cond)) {
	chan->ready++;
	ECtrace_out("ECproto_derive", 0);
	return chan;
    } else {
	/* wait failed, dep is dead */
	EChannel_destroy(chan);
	ECtrace_out("ECproto_derive", 0);
	return NULL;
    }
#else
    printf("No derived event channels in this version of DataExchange\n");
    return NULL;
#endif
}

static ecl_code
generate_periodic_code(context, filter, output_field_list,
		     output_format_list)
ecl_parse_context context;
char *filter;
FMFieldList output_field_list;
CMFormatList output_format_list;
{
#ifndef NO_DERIVED
    sm_ref typ, output_type, output_param;
    ecl_parse_context parse_context = ecl_copy_context(context);

    while ((output_format_list != NULL) && 
	   (output_format_list->format_name != NULL)) {
	/* set through output formats */
	typ = ecl_build_type_node(output_format_list->format_name,
			      output_format_list->field_list);
	ecl_add_decl_to_parse_context(output_format_list->format_name, typ, 
				  parse_context);
	output_format_list++;
    }
    output_type = ecl_build_type_node("output_type", output_field_list);
    ecl_add_decl_to_parse_context("output_type", output_type, parse_context);

    output_param = ecl_build_param_node("output", output_type, 0);

    ecl_add_decl_to_parse_context("output", output_param, parse_context);

    return ecl_code_gen(filter, parse_context);
#else
    return NULL;
#endif
}

typedef struct _proto_generator {
    EChannel chan;
    ecl_code code;
    int event_size;
    EVsource outgoing_source_handle;
    ECSourceHandle src_handle;
    EVstone action_stone;
    IOFormat outgoing_format;
} *proto_generator_t;

static int
run_periodic_proto_task(cm, junk_message, periodic_p, junk_attrs)
CManager cm;
void *junk_message;
void *periodic_p;
attr_list junk_attrs;
{
    proto_generator_t periodic = (proto_generator_t) periodic_p;

    ECtrace_out("ECproto trying generator function for periodic %lx", 2, 
		(long)periodic);
    if (EChas_sinks(periodic->src_handle)) {
	int (*generator_func)(void *) = (int(*)(void*)) periodic->code->func;
	void *event = malloc(periodic->event_size);
	int return_value;
	ECtrace_out("ECproto running generator function for periodic %lx", 2, 
		    (long)periodic);
	return_value = generator_func(event);
	if (return_value != 0) {
	    ECtrace_out("ECproto submitting event to channel %lx", 0, 
			(long)periodic->chan);
	    EVsubmit(periodic->outgoing_source_handle, event, NULL);
	} else {
	    ECtrace_out("ECproto generator suppressed, return was %d", 
			0, return_value, (long)periodic->chan);
	}
    }
    return 0;
}

static void
pbio_free_func_wrapper(void *data, void *client_data)
{
    proto_generator_t periodic = client_data;
    IOfree_var_rec_elements(iofile_of_IOformat(periodic->outgoing_format),
			    periodic->outgoing_format,data);
    free(data);
}

static EChannel
do_proto_derive(cc, proto_chan, filter, output_field_list,
		  output_format_list, period)
EControlContext cc;
ECproto proto_chan;
char *filter;
FMFieldList output_field_list;
CMFormatList output_format_list;
int period;
{
    EChannel chan;
    EVstone action_stone;
    char *der_chan_str;
    proto_generator_t periodic = malloc(sizeof(*periodic));
    ecl_code code;
    int period_secs = period / (1000 * 1000);
    int period_usecs = period - (period_secs * 1000 * 1000); 
    chan = EChannel_typed_create(cc, output_field_list, output_format_list);

    der_chan_str = ECglobal_id(chan);
    (void) der_chan_str;

    code = generate_periodic_code(proto_chan->base_context, filter, 
				  output_field_list,
				  output_format_list);
    if (code == NULL) return NULL;
    periodic->chan = chan;
    periodic->code = code;
    periodic->event_size = struct_size_field_list(output_field_list, 
						  sizeof(char*));
    periodic->outgoing_source_handle = 
	EVcreate_submit_handle_free(cc->cm, chan->outgoing_target_stone_id,
				    chan->ev_format_list,
				    pbio_free_func_wrapper,
				    periodic);
    periodic->outgoing_format = 
	    EVget_src_ref_format(periodic->outgoing_source_handle);
    periodic->src_handle = ECsource_typed_subscribe(chan, output_field_list,
						    output_format_list);
    action_stone = EValloc_stone(cc->cm);
    EVassoc_terminal_action(cc->cm, action_stone, NULL, 
			     run_periodic_proto_task,
			     (void*)periodic);
    
    EVenable_auto_stone(cc->cm, action_stone, period_secs, period_usecs);
    periodic->action_stone = action_stone;

    chan->periodic_info = periodic;
    return chan;
}

extern void
ECperiodic_info_free(cm, vperiodic)
CManager cm;
void *vperiodic;
{
    proto_generator_t periodic = (proto_generator_t) vperiodic;
    EVfree_stone(cm, periodic->action_stone);
    ecl_code_free(periodic->code);
    CMfree(periodic);
}
    
/* send derive request to all sources of the parent channel */
extern void
Proto_Derive_handler(cm, conn, data, client_data, attrs)
CManager cm;
CMConnection conn;
void *data;
void *client_data;
attr_list attrs;
{
    ProtoDeriveMsgPtr derive_msg = (ProtoDeriveMsgPtr) data;
    EControlContext cc = (EControlContext)client_data;
    EChannel chan;
    ECproto proto_chan;
    int i, derive_cond, modifier_index;
    CMFormatList output_format_list;
    FMFieldList field_list;
    char *filter;

    ECtrace_out("DEProto_Derive_handler", 1);
    CMconn_register_close_handler(conn, EChannel_close_handler, 
				  (EControlContext) client_data);

    proto_chan = (ECproto) string_to_channel_ptr(derive_msg->proto_str);


    derive_cond = derive_msg->cond;

    output_format_list = CMmalloc((derive_msg->format_list_len+1) * 
				  sizeof(CMFormatList));
    for (i=0; i< derive_msg->format_list_len; i++) {
	output_format_list[i].format_name = 
	    derive_msg->format_list[i].format_name;
	output_format_list[i].field_list =
	    copy_field_list(derive_msg->format_list[i].field_list);
    }
    field_list = copy_field_list(derive_msg->field_list);
    filter = malloc(strlen(derive_msg->filter) + 2);
    strcpy(filter, derive_msg->filter);
    filter[strlen(filter) + 1] = 0; /* add extra NULL termination */

    output_format_list[derive_msg->format_list_len].format_name = NULL;
    output_format_list[derive_msg->format_list_len].field_list = NULL;
    ECtrace_out("Deriving proto with filter %s\n", 3, filter);
    chan = do_proto_derive(cc, proto_chan, filter, 
			   field_list, output_format_list, 
			   derive_msg->usec_period);

    CMfree(output_format_list);
    ECtrace_out("adding modifier to  remote member list", 3);
    if (chan->rem_member_list == NULL) {
	chan->rem_member_list = CMmalloc(sizeof(struct _rem_member_entry));
    }
    chan->rem_member_list = CMrealloc(chan->rem_member_list,
	  sizeof(struct _rem_member_entry) * (chan->rem_member_list_len + 1));
    memset(&chan->rem_member_list[chan->rem_member_list_len], 0,
	   sizeof(chan->rem_member_list[0]));
    chan->rem_member_list[chan->rem_member_list_len].conn = conn;
    chan->rem_member_list[chan->rem_member_list_len].remote_id =
	copy_channel_id(derive_msg->client_channel);
    chan->rem_member_list[chan->rem_member_list_len].has_sinks = 0;
    chan->rem_member_list[chan->rem_member_list_len].has_sources = 0;
    chan->rem_member_list[chan->rem_member_list_len].contact_string =
	strdup(derive_msg->contact_str);
    modifier_index = chan->rem_member_list_len;
    chan->rem_member_list_len++;

    ECtrace_out("send sink list, local and remote to modifier", 3);
    send_attend_response(cm, conn, chan, derive_cond);

    ECtrace_out("DEChannel_Derive_handler", 0);
}

#else
extern void
ECperiodic_info_free(cm, vperiodic)
CManager cm;
void *vperiodic;
{
}
extern void
Proto_Derive_handler(cm, conn, data, client_data, attrs)
CManager cm;
CMConnection conn;
void *data;
void *client_data;
attr_list attrs;
{
}
#endif
